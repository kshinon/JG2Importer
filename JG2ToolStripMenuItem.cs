using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

namespace JG2Importer
{
	public class JG2ToolStripMenuItem : ToolStripMenuItem
	{
		public int SelectedIndex
		{
			get
			{
				int num = -1;
				if (!this.DesignMode)
				{
					int num1 = -1;
					int count = checked(this.DropDownItems.Count - 1);
					for (int i = 0; i <= count; i = checked(i + 1))
					{
						object item = this.DropDownItems[i];
						if (item is JG2ToolStripMenuItem)
						{
							num1 = checked(num1 + 1);
							if (Conversions.ToBoolean(NewLateBinding.LateGet(item, null, "Checked", new object[0], null, null, null)))
							{
								num = num1;
								break;
							}
						}
					}
				}
				return num;
			}
		}

		public JG2ToolStripMenuItem()
		{
		}

		public JG2ToolStripMenuItem(string argText, string argName)
		{
			this.Text = argText;
			this.Name = argName;
			this.CheckOnClick = false;
		}

		public JG2ToolStripMenuItem(string argText)
		{
			this.Text = argText;
			this.CheckOnClick = true;
		}

		protected override void OnCheckedChanged(EventArgs e)
		{
			IEnumerator enumerator = null;
			base.OnCheckedChanged(e);
			if (!this.DesignMode)
			{
				if (!this.Checked)
				{
					return;
				}
				this.CheckState = System.Windows.Forms.CheckState.Indeterminate;
				try
				{
					enumerator = this.Owner.Items.GetEnumerator();
					while (enumerator.MoveNext())
					{
						object objectValue = RuntimeHelpers.GetObjectValue(enumerator.Current);
						if (!(objectValue is JG2ToolStripMenuItem))
						{
							continue;
						}
						if (objectValue != this)
						{
							if (!Conversions.ToBoolean(NewLateBinding.LateGet(objectValue, null, "Checked", new object[0], null, null, null)))
							{
								continue;
							}
							object[] objArray = new object[] { false };
							NewLateBinding.LateSet(objectValue, null, "Checked", objArray, null, null);
							return;
						}
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
			}
		}

		protected override void OnClick(EventArgs e)
		{
			if (!this.DesignMode)
			{
				if (this.Checked)
				{
					return;
				}
			}
			base.OnClick(e);
		}

		protected override void OnEnabledChanged(EventArgs e)
		{
			IEnumerator enumerator = null;
			if (!this.DesignMode)
			{
				try
				{
					enumerator = this.DropDownItems.GetEnumerator();
					while (enumerator.MoveNext())
					{
						object objectValue = RuntimeHelpers.GetObjectValue(enumerator.Current);
						if (!(objectValue is ToolStripMenuItem))
						{
							continue;
						}
						object[] enabled = new object[] { this.Enabled };
						NewLateBinding.LateSet(objectValue, null, "Enabled", enabled, null, null);
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
			}
			base.OnEnabledChanged(e);
		}
	}
}