JG2Importer

This is a decompiled version of the Japanese JG2Importer 1.5.7, using Telerek's JustDecompile in the interests of having a newer version translated to english. Because of this decompiliation, a lot of the code is a mess, and form editors won't work currently.

Currently the following modifications have been done:
- Fixed the problem that was causing people to have to switch currency and clock formats, no longer needs to be done.
- All of the hardcoded strings have been moved to Resources (Resources.resx)
- Some of the dropdown menus have been converted to numeric spinners to handle the additional slots afforded by AA2Unlimited
- Some file saving overloaded methods have been modified so that they actually work
- The resource strings have been translated (poorly) with google translate, a few were translated by me using existing translations in the AA2Make editor (Resource Strings.xlsx is more of a reference dictionary I started containing the original japanese text and equivalent resource variable name and english string)

Things that still need to be done:
- Strings need to be translated properly
- External Eye Iris and Highlight files are not handled. Currently they APPEAR to still be saved properly, but I triggered a condition where they were overwritten somehow and have not been able to replicate it
- Several of the form boxes are too small to handle english text, they need to be resized or whatever
- Additional personality types added by mods do not show up in the class view window
- Some more thorough testing to make sure the file handling is actually saving properly with mods
- Code could use some tidying up