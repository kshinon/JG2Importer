using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace JG2Importer
{
	public class JG2NumericUpDown : NumericUpDown
	{
		private Button m_BatchButton;

		private string m_Caption;

		private bool bolOperation;

		private object InitValue;

		[DefaultValue(typeof(Button), null)]
		public Button BatchButton
		{
			get
			{
				return this.m_BatchButton;
			}
			set
			{
				this.m_BatchButton = value;
			}
		}

		[DefaultValue(null)]
		public string Caption
		{
			get
			{
				return this.m_Caption;
			}
			set
			{
				this.m_Caption = value;
			}
		}

		public bool Modified
		{
			get
			{
				return Operators.ConditionalCompareObjectNotEqual(this.InitValue, this.Value, false);
			}
		}

		public JG2NumericUpDown()
		{
			this.bolOperation = false;
			this.InitValue = null;
		}

		public void Initialize(decimal argValue)
		{
			this.InitValue = argValue;
			this.SetValue(argValue);
		}

		protected override void OnClick(EventArgs e)
		{
			if (!this.DesignMode)
			{
				this.SetBatchButton();
			}
			base.OnClick(e);
		}

		protected override void OnEnter(EventArgs e)
		{
			if (!this.DesignMode)
			{
				this.SetBatchButton();
			}
			base.OnEnter(e);
		}

		protected override void OnValueChanged(EventArgs e)
		{
			if (!this.DesignMode)
			{
				if (this.bolOperation)
				{
					return;
				}
			}
			base.OnValueChanged(e);
		}

		public void SetBatchButton()
		{
			if (!Information.IsNothing(this.m_BatchButton))
			{
				this.m_BatchButton.Tag = this;
				this.m_BatchButton.Text = Strings.Replace("[%Caption%]" + Resources.MenuTxtBatch, "%Caption%", this.m_Caption, 1, -1, CompareMethod.Binary);
			}
		}

		public void SetValue(decimal argValue)
		{
			this.bolOperation = true;
			this.Value = argValue;
			this.bolOperation = false;
		}
	}
}