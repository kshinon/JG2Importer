using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace JG2Importer
{
	public class JG2ComboBox : ComboBox
	{
		private Button m_BatchButton;

		private string m_Caption;

		private bool bolOperation;

		private object InitSelectedIndex;

		[DefaultValue(typeof(Button), null)]
		public Button BatchButton
		{
			get
			{
				return this.m_BatchButton;
			}
			set
			{
				this.m_BatchButton = value;
			}
		}

		[DefaultValue(null)]
		public string Caption
		{
			get
			{
				return this.m_Caption;
			}
			set
			{
				this.m_Caption = value;
			}
		}

		public bool Modified
		{
			get
			{
				return Operators.ConditionalCompareObjectNotEqual(this.InitSelectedIndex, this.SelectedIndex, false);
			}
		}

		public JG2ComboBox()
		{
			this.bolOperation = false;
			this.InitSelectedIndex = null;
		}

		public void Initialize(int argSelectedIndex)
		{
			this.InitSelectedIndex = argSelectedIndex;
			this.SetValue(argSelectedIndex);
		}

		protected override void OnEnter(EventArgs e)
		{
			if (!this.DesignMode)
			{
				this.SetBatchButton();
			}
			base.OnEnter(e);
		}

		protected override void OnMouseHover(EventArgs e)
		{
			if (!this.DesignMode)
			{
				this.SetBatchButton();
			}
			base.OnMouseHover(e);
		}

		protected override void OnSelectedIndexChanged(EventArgs e)
		{
			if (!this.DesignMode)
			{
				if (this.bolOperation)
				{
					return;
				}
			}
			base.OnSelectedIndexChanged(e);
		}

		public void SetBatchButton()
		{
			if (!Information.IsNothing(this.m_BatchButton))
			{
				this.m_BatchButton.Tag = this;
				this.m_BatchButton.Text = Strings.Replace("[%Caption%]" + Resources.MenuTxtBatch, "%Caption%", this.m_Caption, 1, -1, CompareMethod.Binary);
			}
		}

		public void SetValue(int argSelectedIndex)
		{
			this.bolOperation = true;
			this.SelectedIndex = argSelectedIndex;
			this.bolOperation = false;
		}
	}
}