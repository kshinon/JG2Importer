using Microsoft.VisualBasic;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace JG2Importer
{
	public class JG2Button : Button
	{
		private Button m_BatchButton;

		private string m_Caption;

		private bool bolOperation;

		private object InitBackColor;

		[DefaultValue(typeof(Button), null)]
		public Button BatchButton
		{
			get
			{
				return this.m_BatchButton;
			}
			set
			{
				this.m_BatchButton = value;
			}
		}

		[DefaultValue(null)]
		public string Caption
		{
			get
			{
				return this.m_Caption;
			}
			set
			{
				this.m_Caption = value;
			}
		}

		public bool Modified
		{
			get
			{
				Color color = new Color();
				Color color1;
				object initBackColor = this.InitBackColor;
				if (initBackColor != null)
				{
					color1 = (Color)initBackColor;
				}
				else
				{
					color1 = color;
				}
				return color1 != this.BackColor;
			}
		}

		public JG2Button()
		{
			this.bolOperation = false;
			this.InitBackColor = null;
		}

		public void Initialize(Color argBackColor)
		{
			this.InitBackColor = argBackColor;
			this.SetValue(argBackColor);
		}

		protected override void OnBackColorChanged(EventArgs e)
		{
			if (!this.DesignMode)
			{
				if (this.bolOperation)
				{
					return;
				}
			}
			base.OnBackColorChanged(e);
		}

		protected override void OnEnter(EventArgs e)
		{
			if (!this.DesignMode)
			{
				this.SetBatchButton();
			}
			base.OnEnter(e);
		}

		protected override void OnMouseHover(EventArgs e)
		{
			if (!this.DesignMode)
			{
				this.SetBatchButton();
			}
			base.OnMouseHover(e);
		}

		public void SetBatchButton()
		{
			if (!Information.IsNothing(this.m_BatchButton))
			{
				this.m_BatchButton.Tag = this;
				this.m_BatchButton.Text = Strings.Replace("[%Caption%]" + Resources.MenuTxtBatch, "%Caption%", this.m_Caption, 1, -1, CompareMethod.Binary);
			}
		}

		public void SetValue(Color argBackColor)
		{
			this.bolOperation = true;
			this.BackColor = argBackColor;
			this.bolOperation = false;
		}
	}
}