using JG2Importer.JG2;
using JG2Importer.My;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

namespace JG2Importer
{
	[DesignerGenerated]
	public class frmDetail : Form
	{
		private IContainer components;

		[AccessedThroughProperty("tbpTools")]
		private TabPage _tbpTools;

		[AccessedThroughProperty("tbpJinkaku")]
		private TabPage _tbpJinkaku;

		[AccessedThroughProperty("lblTiryoku1")]
		private Label _lblTiryoku1;

		[AccessedThroughProperty("lblTairyoku1")]
		private Label _lblTairyoku1;

		[AccessedThroughProperty("lblBukatu1")]
		private Label _lblBukatu1;

		[AccessedThroughProperty("lblKenka")]
		private Label _lblKenka;

		[AccessedThroughProperty("lblSeiai")]
		private Label _lblSeiai;

		[AccessedThroughProperty("lblTeisou")]
		private Label _lblTeisou;

		[AccessedThroughProperty("lblVoice")]
		private Label _lblVoice;

		[AccessedThroughProperty("lblSeikaku")]
		private Label _lblSeikaku;

		[AccessedThroughProperty("tbpKosei")]
		private TabPage _tbpKosei;

		[AccessedThroughProperty("Label511")]
		private Label _Label511;

		[AccessedThroughProperty("tbpNinshin")]
		private TabPage _tbpNinshin;

		[AccessedThroughProperty("lblMonday1")]
		private Label _lblMonday1;

		[AccessedThroughProperty("lblSunday1")]
		private Label _lblSunday1;

		[AccessedThroughProperty("lblSunday2")]
		private Label _lblSunday2;

		[AccessedThroughProperty("lblSaturday1")]
		private Label _lblSaturday1;

		[AccessedThroughProperty("lblFriday1")]
		private Label _lblFriday1;

		[AccessedThroughProperty("lblThursday1")]
		private Label _lblThursday1;

		[AccessedThroughProperty("lblWednesday1")]
		private Label _lblWednesday1;

		[AccessedThroughProperty("lblTuesday1")]
		private Label _lblTuesday1;

		[AccessedThroughProperty("lblSaturday2")]
		private Label _lblSaturday2;

		[AccessedThroughProperty("lblFriday2")]
		private Label _lblFriday2;

		[AccessedThroughProperty("lblThursday2")]
		private Label _lblThursday2;

		[AccessedThroughProperty("lblWednesday2")]
		private Label _lblWednesday2;

		[AccessedThroughProperty("lblTuesday2")]
		private Label _lblTuesday2;

		[AccessedThroughProperty("lblMonday2")]
		private Label _lblMonday2;

		[AccessedThroughProperty("btnExport")]
		private Button _btnExport;

		[AccessedThroughProperty("btnImport")]
		private Button _btnImport;

		[AccessedThroughProperty("lblSyakousei")]
		private Label _lblSyakousei;

		[AccessedThroughProperty("pnlFooter")]
		private Panel _pnlFooter;

		[AccessedThroughProperty("btnSelectAll")]
		private Button _btnSelectAll;

		[AccessedThroughProperty("btnClearAll")]
		private Button _btnClearAll;

		[AccessedThroughProperty("btnBatch")]
		private Button _btnBatch;

		[AccessedThroughProperty("btnSubmit")]
		private Button _btnSubmit;

		[AccessedThroughProperty("tbpHair")]
		private TabPage _tbpHair;

		[AccessedThroughProperty("lblColorHair")]
		private Label _lblColorHair;

		[AccessedThroughProperty("lblHeight")]
		private Label _lblHeight;

		[AccessedThroughProperty("lblStyle")]
		private Label _lblStyle;

		[AccessedThroughProperty("lblAtama1")]
		private Label _lblAtama1;

		[AccessedThroughProperty("lblAtama2")]
		private Label _lblAtama2;

		[AccessedThroughProperty("lblWaist")]
		private Label _lblWaist;

		[AccessedThroughProperty("Label501")]
		private Label _Label501;

		[AccessedThroughProperty("lblLip2")]
		private Label _lblLip2;

		[AccessedThroughProperty("lblLip1")]
		private Label _lblLip1;

		[AccessedThroughProperty("lblHokuro")]
		private Label _lblHokuro;

		[AccessedThroughProperty("lblColorGlasses")]
		private Label _lblColorGlasses;

		[AccessedThroughProperty("lblGlasses2")]
		private Label _lblGlasses2;

		[AccessedThroughProperty("lblMatuge2")]
		private Label _lblMatuge2;

		[AccessedThroughProperty("lblMatuge1")]
		private Label _lblMatuge1;

		[AccessedThroughProperty("lblMabuta")]
		private Label _lblMabuta;

		[AccessedThroughProperty("tbpFace")]
		private TabPage _tbpFace;

		[AccessedThroughProperty("lblColorEye1")]
		private Label _lblColorEye1;

		[AccessedThroughProperty("lblColorEye2")]
		private Label _lblColorEye2;

		[AccessedThroughProperty("btnColorEye2Sub")]
		private Button _btnColorEye2Sub;

		[AccessedThroughProperty("btnColorEye1Sub")]
		private Button _btnColorEye1Sub;

		[AccessedThroughProperty("lblMayu2")]
		private Label _lblMayu2;

		[AccessedThroughProperty("lblMayu1")]
		private Label _lblMayu1;

		[AccessedThroughProperty("lblColorMayu")]
		private Label _lblColorMayu;

		[AccessedThroughProperty("btnColorHairSub2")]
		private Button _btnColorHairSub2;

		[AccessedThroughProperty("btnColorHairSub1")]
		private Button _btnColorHairSub1;

		[AccessedThroughProperty("Label301")]
		private Label _Label301;

		[AccessedThroughProperty("lblHair1")]
		private Label _lblHair1;

		[AccessedThroughProperty("lblHair4")]
		private Label _lblHair4;

		[AccessedThroughProperty("lblHair3")]
		private Label _lblHair3;

		[AccessedThroughProperty("lblHair2")]
		private Label _lblHair2;

		[AccessedThroughProperty("lblNyurin")]
		private Label _lblNyurin;

		[AccessedThroughProperty("lblMune8")]
		private Label _lblMune8;

		[AccessedThroughProperty("lblMune7")]
		private Label _lblMune7;

		[AccessedThroughProperty("lblMune6")]
		private Label _lblMune6;

		[AccessedThroughProperty("lblMune5")]
		private Label _lblMune5;

		[AccessedThroughProperty("lblMune4")]
		private Label _lblMune4;

		[AccessedThroughProperty("lblMune3")]
		private Label _lblMune3;

		[AccessedThroughProperty("lblMune2")]
		private Label _lblMune2;

		[AccessedThroughProperty("lblMune1")]
		private Label _lblMune1;

		[AccessedThroughProperty("lblColorUnder")]
		private Label _lblColorUnder;

		[AccessedThroughProperty("lblUnder2")]
		private Label _lblUnder2;

		[AccessedThroughProperty("lblUnder1")]
		private Label _lblUnder1;

		[AccessedThroughProperty("lblMosaic")]
		private Label _lblMosaic;

		[AccessedThroughProperty("lblHiyake2")]
		private Label _lblHiyake2;

		[AccessedThroughProperty("lblHiyake1")]
		private Label _lblHiyake1;

		[AccessedThroughProperty("lblTikubi3")]
		private Label _lblTikubi3;

		[AccessedThroughProperty("lblTikubi2")]
		private Label _lblTikubi2;

		[AccessedThroughProperty("lblTikubi1")]
		private Label _lblTikubi1;

		[AccessedThroughProperty("lblColorSkin2")]
		private Label _lblColorSkin2;

		[AccessedThroughProperty("lblKao")]
		private Label _lblKao;

		[AccessedThroughProperty("lblEye1")]
		private Label _lblEye1;

		[AccessedThroughProperty("lblEye5")]
		private Label _lblEye5;

		[AccessedThroughProperty("lblEye4")]
		private Label _lblEye4;

		[AccessedThroughProperty("lblEye3")]
		private Label _lblEye3;

		[AccessedThroughProperty("lblEye2")]
		private Label _lblEye2;

		[AccessedThroughProperty("lblPupil2")]
		private Label _lblPupil2;

		[AccessedThroughProperty("lblPupil4")]
		private Label _lblPupil4;

		[AccessedThroughProperty("lblPupil3")]
		private Label _lblPupil3;

		[AccessedThroughProperty("lblPupil1")]
		private Label _lblPupil1;

		[AccessedThroughProperty("lblPupil5")]
		private Label _lblPupil5;

		[AccessedThroughProperty("lblHighlight1")]
		private Label _lblHighlight1;

		[AccessedThroughProperty("MenuStrip")]
		private System.Windows.Forms.MenuStrip _MenuStrip;

		[AccessedThroughProperty("mnuFile")]
		private ToolStripMenuItem _mnuFile;

		[AccessedThroughProperty("mnuSubmit")]
		private ToolStripMenuItem _mnuSubmit;

		[AccessedThroughProperty("toolStripSeparator1")]
		private ToolStripSeparator _toolStripSeparator1;

		[AccessedThroughProperty("mnuClose")]
		private ToolStripMenuItem _mnuClose;

		[AccessedThroughProperty("mnuEdit")]
		private JG2ToolStripMenuItem _mnuEdit;

		[AccessedThroughProperty("mnuSelectAll")]
		private ToolStripMenuItem _mnuSelectAll;

		[AccessedThroughProperty("mnuClearAll")]
		private ToolStripMenuItem _mnuClearAll;

		[AccessedThroughProperty("mnuTab")]
		private ToolStripMenuItem _mnuTab;

		[AccessedThroughProperty("mnuTabHair")]
		private ToolStripMenuItem _mnuTabHair;

		[AccessedThroughProperty("mnuTabBody")]
		private ToolStripMenuItem _mnuTabBody;

		[AccessedThroughProperty("mnuTabFace")]
		private ToolStripMenuItem _mnuTabFace;

		[AccessedThroughProperty("mnuTabJinkaku")]
		private ToolStripMenuItem _mnuTabJinkaku;

		[AccessedThroughProperty("mnuTabKosei")]
		private ToolStripMenuItem _mnuTabKosei;

		[AccessedThroughProperty("mnuTabNinshin")]
		private ToolStripMenuItem _mnuTabNinshin;

		[AccessedThroughProperty("mnuTabTools")]
		private ToolStripMenuItem _mnuTabTools;

		[AccessedThroughProperty("mnuBatchSeparator")]
		private ToolStripSeparator _mnuBatchSeparator;

		[AccessedThroughProperty("mnuBatch")]
		private ToolStripMenuItem _mnuBatch;

		[AccessedThroughProperty("lblHair1Name")]
		private Label _lblHair1Name;

		[AccessedThroughProperty("lblHair2Name")]
		private Label _lblHair2Name;

		[AccessedThroughProperty("lblHair4Name")]
		private Label _lblHair4Name;

		[AccessedThroughProperty("lblHair3Name")]
		private Label _lblHair3Name;

		[AccessedThroughProperty("lblSeikakuName")]
		private Label _lblSeikakuName;

		[AccessedThroughProperty("lblTairyoku1Name")]
		private Label _lblTairyoku1Name;

		[AccessedThroughProperty("lblTiryoku1Name")]
		private Label _lblTiryoku1Name;

		[AccessedThroughProperty("pnlPair")]
		private Panel _pnlPair;

		[AccessedThroughProperty("lblPair")]
		private Label _lblPair;

		[AccessedThroughProperty("pnlProfile")]
		private Panel _pnlProfile;

		[AccessedThroughProperty("lblPersonal")]
		private Label _lblPersonal;

		[AccessedThroughProperty("Label85")]
		private Label _Label85;

		[AccessedThroughProperty("Label84")]
		private Label _Label84;

		[AccessedThroughProperty("lblAite1")]
		private Label _lblAite1;

		[AccessedThroughProperty("lblCntKousai")]
		private Label _lblCntKousai;

		[AccessedThroughProperty("Label87")]
		private Label _Label87;

		[AccessedThroughProperty("lblCntSabori")]
		private Label _lblCntSabori;

		[AccessedThroughProperty("lblCntSoudatu")]
		private Label _lblCntSoudatu;

		[AccessedThroughProperty("Label105")]
		private Label _Label105;

		[AccessedThroughProperty("lblTest3")]
		private Label _lblTest3;

		[AccessedThroughProperty("lblCntKenka")]
		private Label _lblCntKenka;

		[AccessedThroughProperty("Label102")]
		private Label _Label102;

		[AccessedThroughProperty("Label103")]
		private Label _Label103;

		[AccessedThroughProperty("lblTest2")]
		private Label _lblTest2;

		[AccessedThroughProperty("lblCntFurareta")]
		private Label _lblCntFurareta;

		[AccessedThroughProperty("Label98")]
		private Label _Label98;

		[AccessedThroughProperty("Label99")]
		private Label _Label99;

		[AccessedThroughProperty("lblTest1")]
		private Label _lblTest1;

		[AccessedThroughProperty("lblCntSeikouAite")]
		private Label _lblCntSeikouAite;

		[AccessedThroughProperty("Label94")]
		private Label _Label94;

		[AccessedThroughProperty("Label95")]
		private Label _Label95;

		[AccessedThroughProperty("lblAite3")]
		private Label _lblAite3;

		[AccessedThroughProperty("Label109")]
		private Label _Label109;

		[AccessedThroughProperty("lblAite2")]
		private Label _lblAite2;

		[AccessedThroughProperty("Label107")]
		private Label _Label107;

		[AccessedThroughProperty("lblCntCondom")]
		private Label _lblCntCondom;

		[AccessedThroughProperty("Label111")]
		private Label _Label111;

		[AccessedThroughProperty("lblSyasei2")]
		private Label _lblSyasei2;

		[AccessedThroughProperty("lblCntSeikou3")]
		private Label _lblCntSeikou3;

		[AccessedThroughProperty("Label114")]
		private Label _Label114;

		[AccessedThroughProperty("Label115")]
		private Label _Label115;

		[AccessedThroughProperty("lblSyasei1")]
		private Label _lblSyasei1;

		[AccessedThroughProperty("lblCntSeikou2")]
		private Label _lblCntSeikou2;

		[AccessedThroughProperty("Label118")]
		private Label _Label118;

		[AccessedThroughProperty("Label119")]
		private Label _Label119;

		[AccessedThroughProperty("lblCntSynchronize")]
		private Label _lblCntSynchronize;

		[AccessedThroughProperty("lblCntSeikou1")]
		private Label _lblCntSeikou1;

		[AccessedThroughProperty("Label122")]
		private Label _Label122;

		[AccessedThroughProperty("Label123")]
		private Label _Label123;

		[AccessedThroughProperty("lblCntOrgasm")]
		private Label _lblCntOrgasm;

		[AccessedThroughProperty("Label126")]
		private Label _Label126;

		[AccessedThroughProperty("Label127")]
		private Label _Label127;

		[AccessedThroughProperty("lblSyasei4")]
		private Label _lblSyasei4;

		[AccessedThroughProperty("Label129")]
		private Label _Label129;

		[AccessedThroughProperty("lblSyasei3")]
		private Label _lblSyasei3;

		[AccessedThroughProperty("Label131")]
		private Label _Label131;

		[AccessedThroughProperty("lblAction")]
		private Label _lblAction;

		[AccessedThroughProperty("Label83")]
		private Label _Label83;

		[AccessedThroughProperty("mnuTabToolsImport")]
		private ToolStripMenuItem _mnuTabToolsImport;

		[AccessedThroughProperty("mnuTabToolsExport")]
		private ToolStripMenuItem _mnuTabToolsExport;

		[AccessedThroughProperty("ToolStripSeparator6")]
		private ToolStripSeparator _ToolStripSeparator6;

		[AccessedThroughProperty("mnuTabToolsPlayer")]
		private ToolStripMenuItem _mnuTabToolsPlayer;

		[AccessedThroughProperty("mnuTabToolsSeparator")]
		private ToolStripSeparator _mnuTabToolsSeparator;

		[AccessedThroughProperty("mnuTabToolsHMax")]
		private ToolStripMenuItem _mnuTabToolsHMax;

		[AccessedThroughProperty("mnuTabToolsGohoubi")]
		private ToolStripMenuItem _mnuTabToolsGohoubi;

		[AccessedThroughProperty("mnuTabToolsAfterPill")]
		private ToolStripMenuItem _mnuTabToolsAfterPill;

		[AccessedThroughProperty("mnuTabToolsAction")]
		private ToolStripMenuItem _mnuTabToolsAction;

		[AccessedThroughProperty("mnuTabToolsAction1")]
		private ToolStripMenuItem _mnuTabToolsAction1;

		[AccessedThroughProperty("mnuTabToolsAction2")]
		private ToolStripMenuItem _mnuTabToolsAction2;

		[AccessedThroughProperty("mnuTabToolsAction3")]
		private ToolStripMenuItem _mnuTabToolsAction3;

		[AccessedThroughProperty("mnuTabToolsAction4")]
		private ToolStripMenuItem _mnuTabToolsAction4;

		[AccessedThroughProperty("ToolStripSeparator9")]
		private ToolStripSeparator _ToolStripSeparator9;

		[AccessedThroughProperty("ToolStripSeparator3")]
		private ToolStripSeparator _ToolStripSeparator3;

		[AccessedThroughProperty("mnuSelectCondition")]
		private ToolStripMenuItem _mnuSelectCondition;

		[AccessedThroughProperty("lblCntNakaKiken")]
		private Label _lblCntNakaKiken;

		[AccessedThroughProperty("lblHAisyou")]
		private Label _lblHAisyou;

		[AccessedThroughProperty("btnPlayer")]
		private Button _btnPlayer;

		[AccessedThroughProperty("lblThumbnail")]
		private Label _lblThumbnail;

		[AccessedThroughProperty("lblThumbnailPlayer")]
		private Label _lblThumbnailPlayer;

		[AccessedThroughProperty("Label26")]
		private Label _Label26;

		[AccessedThroughProperty("Label90")]
		private Label _Label90;

		[AccessedThroughProperty("Label91")]
		private Label _Label91;

		[AccessedThroughProperty("lblGoods1")]
		private Label _lblGoods1;

		[AccessedThroughProperty("lblGoods3")]
		private Label _lblGoods3;

		[AccessedThroughProperty("lblGoods2")]
		private Label _lblGoods2;

		[AccessedThroughProperty("ToolTip")]
		private System.Windows.Forms.ToolTip _ToolTip;

		[AccessedThroughProperty("Label701")]
		private Label _Label701;

		[AccessedThroughProperty("tbpCostume")]
		private TabPage _tbpCostume;

		[AccessedThroughProperty("mnuTabCostume")]
		private ToolStripMenuItem _mnuTabCostume;

		[AccessedThroughProperty("pnlYouto")]
		private Panel _pnlYouto;

		[AccessedThroughProperty("mnuOpen")]
		private ToolStripMenuItem _mnuOpen;

		[AccessedThroughProperty("ToolStripSeparator10")]
		private ToolStripSeparator _ToolStripSeparator10;

		[AccessedThroughProperty("mnuTabToolsDelAction1")]
		private ToolStripMenuItem _mnuTabToolsDelAction1;

		[AccessedThroughProperty("mnuTabToolsDelAction2")]
		private ToolStripMenuItem _mnuTabToolsDelAction2;

		[AccessedThroughProperty("mnuTabToolsDelAction3")]
		private ToolStripMenuItem _mnuTabToolsDelAction3;

		[AccessedThroughProperty("mnuTabToolsDelAction4")]
		private ToolStripMenuItem _mnuTabToolsDelAction4;

		[AccessedThroughProperty("ToolStripSeparator11")]
		private ToolStripSeparator _ToolStripSeparator11;

		[AccessedThroughProperty("lblCntTodaySeikou")]
		private Label _lblCntTodaySeikou;

		[AccessedThroughProperty("Label89")]
		private Label _Label89;

		[AccessedThroughProperty("lblRenzoku")]
		private Label _lblRenzoku;

		[AccessedThroughProperty("Label92")]
		private Label _Label92;

		[AccessedThroughProperty("pnlAction")]
		private Panel _pnlAction;

		[AccessedThroughProperty("pnlTools")]
		private Panel _pnlTools;

		[AccessedThroughProperty("pnlActionRev")]
		private Panel _pnlActionRev;

		[AccessedThroughProperty("lblActionRev")]
		private Label _lblActionRev;

		[AccessedThroughProperty("mnuTabToolsClearAction")]
		private ToolStripMenuItem _mnuTabToolsClearAction;

		[AccessedThroughProperty("mnuTabToolsActionRev")]
		private ToolStripMenuItem _mnuTabToolsActionRev;

		[AccessedThroughProperty("mnuTabToolsActionRev1")]
		private ToolStripMenuItem _mnuTabToolsActionRev1;

		[AccessedThroughProperty("mnuTabToolsActionRev2")]
		private ToolStripMenuItem _mnuTabToolsActionRev2;

		[AccessedThroughProperty("mnuTabToolsActionRev3")]
		private ToolStripMenuItem _mnuTabToolsActionRev3;

		[AccessedThroughProperty("mnuTabToolsActionRev4")]
		private ToolStripMenuItem _mnuTabToolsActionRev4;

		[AccessedThroughProperty("ToolStripSeparator4")]
		private ToolStripSeparator _ToolStripSeparator4;

		[AccessedThroughProperty("mnuTabToolsDelActionRev1")]
		private ToolStripMenuItem _mnuTabToolsDelActionRev1;

		[AccessedThroughProperty("mnuTabToolsDelActionRev2")]
		private ToolStripMenuItem _mnuTabToolsDelActionRev2;

		[AccessedThroughProperty("mnuTabToolsDelActionRev3")]
		private ToolStripMenuItem _mnuTabToolsDelActionRev3;

		[AccessedThroughProperty("mnuTabToolsDelActionRev4")]
		private ToolStripMenuItem _mnuTabToolsDelActionRev4;

		[AccessedThroughProperty("tabBase")]
		private TabControl _tabBase;

		[AccessedThroughProperty("tbpBody")]
		private TabPage _tbpBody;

		[AccessedThroughProperty("pnlHokuro")]
		private Panel _pnlHokuro;

		[AccessedThroughProperty("pnlKosei")]
		private Panel _pnlKosei;

		[AccessedThroughProperty("pnlH")]
		private Panel _pnlH;

		[AccessedThroughProperty("lblMune1Name")]
		private Label _lblMune1Name;

		[AccessedThroughProperty("numMune1")]
		private JG2NumericUpDown _numMune1;

		[AccessedThroughProperty("numAtama2")]
		private JG2NumericUpDown _numAtama2;

		[AccessedThroughProperty("numWaist")]
		private JG2NumericUpDown _numWaist;

		[AccessedThroughProperty("numNyurin")]
		private JG2NumericUpDown _numNyurin;

		[AccessedThroughProperty("numMune7")]
		private JG2NumericUpDown _numMune7;

		[AccessedThroughProperty("numMune6")]
		private JG2NumericUpDown _numMune6;

		[AccessedThroughProperty("numMune5")]
		private JG2NumericUpDown _numMune5;

		[AccessedThroughProperty("numMune4")]
		private JG2NumericUpDown _numMune4;

		[AccessedThroughProperty("numMune3")]
		private JG2NumericUpDown _numMune3;

		[AccessedThroughProperty("numTikubi3")]
		private JG2NumericUpDown _numTikubi3;

		[AccessedThroughProperty("numHiyake2")]
		private JG2NumericUpDown _numHiyake2;

		[AccessedThroughProperty("numUnder2")]
		private JG2NumericUpDown _numUnder2;

		[AccessedThroughProperty("numLip2")]
		private JG2NumericUpDown _numLip2;

		[AccessedThroughProperty("numMayu2")]
		private JG2NumericUpDown _numMayu2;

		[AccessedThroughProperty("numEye1")]
		private JG2NumericUpDown _numEye1;

		[AccessedThroughProperty("numEye5")]
		private JG2NumericUpDown _numEye5;

		[AccessedThroughProperty("numEye4")]
		private JG2NumericUpDown _numEye4;

		[AccessedThroughProperty("numEye3")]
		private JG2NumericUpDown _numEye3;

		[AccessedThroughProperty("numEye2")]
		private JG2NumericUpDown _numEye2;

		[AccessedThroughProperty("numPupil2")]
		private JG2NumericUpDown _numPupil2;

		[AccessedThroughProperty("numPupil4")]
		private JG2NumericUpDown _numPupil4;

		[AccessedThroughProperty("numPupil3")]
		private JG2NumericUpDown _numPupil3;

		[AccessedThroughProperty("numHairAdj1")]
		private JG2NumericUpDown _numHairAdj1;

		[AccessedThroughProperty("numHairAdj4")]
		private JG2NumericUpDown _numHairAdj4;

		[AccessedThroughProperty("numHairAdj3")]
		private JG2NumericUpDown _numHairAdj3;

		[AccessedThroughProperty("numHairAdj2")]
		private JG2NumericUpDown _numHairAdj2;

		[AccessedThroughProperty("numHair1")]
		private JG2NumericUpDown _numHair1;

		[AccessedThroughProperty("numHair2")]
		private JG2NumericUpDown _numHair2;

		[AccessedThroughProperty("numHair4")]
		private JG2NumericUpDown _numHair4;

		[AccessedThroughProperty("numHair3")]
		private JG2NumericUpDown _numHair3;

		[AccessedThroughProperty("numBukatu3")]
		private JG2NumericUpDown _numBukatu3;

		[AccessedThroughProperty("numTairyoku3")]
		private JG2NumericUpDown _numTairyoku3;

		[AccessedThroughProperty("numTiryoku3")]
		private JG2NumericUpDown _numTiryoku3;

		[AccessedThroughProperty("numSeikaku")]
		private JG2NumericUpDown _numSeikaku;

		[AccessedThroughProperty("numAtama1")]
		private JG2NumericUpDown _numAtama1;

		[AccessedThroughProperty("btnHMax")]
		private JG2Button _btnHMax;

		[AccessedThroughProperty("btnGohoubi")]
		private JG2Button _btnGohoubi;

		[AccessedThroughProperty("btnAfterPill")]
		private JG2Button _btnAfterPill;

		[AccessedThroughProperty("btnClearAction")]
		private JG2Button _btnClearAction;

		[AccessedThroughProperty("btnAction1")]
		private JG2Button _btnAction1;

		[AccessedThroughProperty("btnAction2")]
		private JG2Button _btnAction2;

		[AccessedThroughProperty("btnAction4")]
		private JG2Button _btnAction4;

		[AccessedThroughProperty("btnAction3")]
		private JG2Button _btnAction3;

		[AccessedThroughProperty("btnDelAction4")]
		private JG2Button _btnDelAction4;

		[AccessedThroughProperty("btnDelAction1")]
		private JG2Button _btnDelAction1;

		[AccessedThroughProperty("btnDelAction2")]
		private JG2Button _btnDelAction2;

		[AccessedThroughProperty("btnDelAction3")]
		private JG2Button _btnDelAction3;

		[AccessedThroughProperty("btnActionRev3")]
		private JG2Button _btnActionRev3;

		[AccessedThroughProperty("btnDelActionRev3")]
		private JG2Button _btnDelActionRev3;

		[AccessedThroughProperty("btnDelActionRev2")]
		private JG2Button _btnDelActionRev2;

		[AccessedThroughProperty("btnActionRev4")]
		private JG2Button _btnActionRev4;

		[AccessedThroughProperty("btnDelActionRev4")]
		private JG2Button _btnDelActionRev4;

		[AccessedThroughProperty("btnActionRev2")]
		private JG2Button _btnActionRev2;

		[AccessedThroughProperty("btnDelActionRev1")]
		private JG2Button _btnDelActionRev1;

		[AccessedThroughProperty("btnActionRev1")]
		private JG2Button _btnActionRev1;

		[AccessedThroughProperty("btnColorUnder")]
		private JG2Button _btnColorUnder;

		[AccessedThroughProperty("btnColorSkin2")]
		private JG2Button _btnColorSkin2;

		[AccessedThroughProperty("btnColorGlasses")]
		private JG2Button _btnColorGlasses;

		[AccessedThroughProperty("btnColorEye1")]
		private JG2Button _btnColorEye1;

		[AccessedThroughProperty("btnColorEye2")]
		private JG2Button _btnColorEye2;

		[AccessedThroughProperty("btnColorMayu")]
		private JG2Button _btnColorMayu;

		[AccessedThroughProperty("btnColorHair")]
		private JG2Button _btnColorHair;

		[AccessedThroughProperty("chkKousai")]
		private JG2CheckBox _chkKousai;

		[AccessedThroughProperty("radYouto3")]
		private JG2RadioButton _radYouto3;

		[AccessedThroughProperty("radYouto2")]
		private JG2RadioButton _radYouto2;

		[AccessedThroughProperty("radYouto1")]
		private JG2RadioButton _radYouto1;

		[AccessedThroughProperty("radYouto0")]
		private JG2RadioButton _radYouto0;

		[AccessedThroughProperty("cmbHeight")]
		private JG2ComboBox _cmbHeight;

		[AccessedThroughProperty("cmbStyle")]
		private JG2ComboBox _cmbStyle;

		[AccessedThroughProperty("cmbMune8")]
		private JG2ComboBox _cmbMune8;

		[AccessedThroughProperty("cmbMune2")]
		private JG2ComboBox _cmbMune2;

		[AccessedThroughProperty("cmbTikubi1")]
		private JG2NumericUpDown _cmbTikubi1;

		[AccessedThroughProperty("cmbTikubi2")]
		private JG2NumericUpDown _cmbTikubi2;

		[AccessedThroughProperty("cmbHiyake1")]
		private JG2NumericUpDown _cmbHiyake1;

		[AccessedThroughProperty("cmbMosaic")]
		private JG2NumericUpDown _cmbMosaic;

		[AccessedThroughProperty("cmbUnder1")]
		private JG2NumericUpDown _cmbUnder1;

		[AccessedThroughProperty("cmbMatuge1")]
		private JG2ComboBox _cmbMatuge1;

		[AccessedThroughProperty("cmbGlasses2")]
		private JG2NumericUpDown _cmbGlasses2;

		[AccessedThroughProperty("cmbMatuge2")]
		private JG2ComboBox _cmbMatuge2;

		[AccessedThroughProperty("cmbMabuta")]
		private JG2ComboBox _cmbMabuta;

		[AccessedThroughProperty("cmbLip1")]
		private JG2NumericUpDown _cmbLip1;

		[AccessedThroughProperty("chkHokuro4")]
		private JG2CheckBox _chkHokuro4;

		[AccessedThroughProperty("chkHokuro2")]
		private JG2CheckBox _chkHokuro2;

		[AccessedThroughProperty("chkHokuro3")]
		private JG2CheckBox _chkHokuro3;

		[AccessedThroughProperty("chkHokuro1")]
		private JG2CheckBox _chkHokuro1;

		[AccessedThroughProperty("cmbMayu1")]
		private JG2ComboBox _cmbMayu1;

		[AccessedThroughProperty("cmbKao")]
		private JG2NumericUpDown _cmbKao;

		[AccessedThroughProperty("cmbPupil1")]
		private JG2ComboBox _cmbPupil1;

		[AccessedThroughProperty("cmbPupil5")]
		private JG2ComboBox _cmbPupil5;

		[AccessedThroughProperty("cmbHighlight1")]
		private JG2ComboBox _cmbHighlight1;

		[AccessedThroughProperty("chkHairRev1")]
		private JG2CheckBox _chkHairRev1;

		[AccessedThroughProperty("chkHairRev4")]
		private JG2CheckBox _chkHairRev4;

		[AccessedThroughProperty("chkHairRev3")]
		private JG2CheckBox _chkHairRev3;

		[AccessedThroughProperty("chkHairRev2")]
		private JG2CheckBox _chkHairRev2;

		[AccessedThroughProperty("cmbSyakousei")]
		private JG2ComboBox _cmbSyakousei;

		[AccessedThroughProperty("cmbKenka")]
		private JG2ComboBox _cmbKenka;

		[AccessedThroughProperty("cmbSeiai")]
		private JG2ComboBox _cmbSeiai;

		[AccessedThroughProperty("cmbTeisou")]
		private JG2ComboBox _cmbTeisou;

		[AccessedThroughProperty("chkSeikou1")]
		private JG2CheckBox _chkSeikou1;

		[AccessedThroughProperty("chkSeikou2")]
		private JG2CheckBox _chkSeikou2;

		[AccessedThroughProperty("cmbVoice")]
		private JG2ComboBox _cmbVoice;

		[AccessedThroughProperty("cmbBukatu1")]
		private JG2ComboBox _cmbBukatu1;

		[AccessedThroughProperty("chkKake")]
		private JG2CheckBox _chkKake;

		[AccessedThroughProperty("chkNaka")]
		private JG2CheckBox _chkNaka;

		[AccessedThroughProperty("chkNama")]
		private JG2CheckBox _chkNama;

		[AccessedThroughProperty("chkSeiin")]
		private JG2CheckBox _chkSeiin;

		[AccessedThroughProperty("chkAnal")]
		private JG2CheckBox _chkAnal;

		[AccessedThroughProperty("chkOnna")]
		private JG2CheckBox _chkOnna;

		[AccessedThroughProperty("chkFellatio")]
		private JG2CheckBox _chkFellatio;

		[AccessedThroughProperty("chkBack")]
		private JG2CheckBox _chkBack;

		[AccessedThroughProperty("chkCunnilingus")]
		private JG2CheckBox _chkCunnilingus;

		[AccessedThroughProperty("chkSeiki2")]
		private JG2CheckBox _chkSeiki2;

		[AccessedThroughProperty("chkBust")]
		private JG2CheckBox _chkBust;

		[AccessedThroughProperty("chkSeiki1")]
		private JG2CheckBox _chkSeiki1;

		[AccessedThroughProperty("chkKiss")]
		private JG2CheckBox _chkKiss;

		[AccessedThroughProperty("chkLuck")]
		private JG2CheckBox _chkLuck;

		[AccessedThroughProperty("chkRare")]
		private JG2CheckBox _chkRare;

		[AccessedThroughProperty("chkMiseityo")]
		private JG2CheckBox _chkMiseityo;

		[AccessedThroughProperty("chkYowami")]
		private JG2CheckBox _chkYowami;

		[AccessedThroughProperty("chkYami")]
		private JG2CheckBox _chkYami;

		[AccessedThroughProperty("chkNantyo")]
		private JG2CheckBox _chkNantyo;

		[AccessedThroughProperty("chkAsekaki")]
		private JG2CheckBox _chkAsekaki;

		[AccessedThroughProperty("chkM")]
		private JG2CheckBox _chkM;

		[AccessedThroughProperty("chkKinben")]
		private JG2CheckBox _chkKinben;

		[AccessedThroughProperty("chkHonpou")]
		private JG2CheckBox _chkHonpou;

		[AccessedThroughProperty("chkHaraguro")]
		private JG2CheckBox _chkHaraguro;

		[AccessedThroughProperty("chkMakenki")]
		private JG2CheckBox _chkMakenki;

		[AccessedThroughProperty("chkItizu")]
		private JG2CheckBox _chkItizu;

		[AccessedThroughProperty("chkYuujyufudan")]
		private JG2CheckBox _chkYuujyufudan;

		[AccessedThroughProperty("chkRenai")]
		private JG2CheckBox _chkRenai;

		[AccessedThroughProperty("chkHarapeko")]
		private JG2CheckBox _chkHarapeko;

		[AccessedThroughProperty("chkIintyou")]
		private JG2CheckBox _chkIintyou;

		[AccessedThroughProperty("chkOsyaberi")]
		private JG2CheckBox _chkOsyaberi;

		[AccessedThroughProperty("chkSewayaki")]
		private JG2CheckBox _chkSewayaki;

		[AccessedThroughProperty("chkSousyoku")]
		private JG2CheckBox _chkSousyoku;

		[AccessedThroughProperty("chkPoyayan")]
		private JG2CheckBox _chkPoyayan;

		[AccessedThroughProperty("chkBouryoku")]
		private JG2CheckBox _chkBouryoku;

		[AccessedThroughProperty("chkTyokujyo")]
		private JG2CheckBox _chkTyokujyo;

		[AccessedThroughProperty("chkCool")]
		private JG2CheckBox _chkCool;

		[AccessedThroughProperty("chkSukebe")]
		private JG2CheckBox _chkSukebe;

		[AccessedThroughProperty("chkMajime")]
		private JG2CheckBox _chkMajime;

		[AccessedThroughProperty("chkToufu")]
		private JG2CheckBox _chkToufu;

		[AccessedThroughProperty("chkYakimochi")]
		private JG2CheckBox _chkYakimochi;

		[AccessedThroughProperty("chkMaemuki")]
		private JG2CheckBox _chkMaemuki;

		[AccessedThroughProperty("chkTereya")]
		private JG2CheckBox _chkTereya;

		[AccessedThroughProperty("chkSunao")]
		private JG2CheckBox _chkSunao;

		[AccessedThroughProperty("chkMiha")]
		private JG2CheckBox _chkMiha;

		[AccessedThroughProperty("chkTundere")]
		private JG2CheckBox _chkTundere;

		[AccessedThroughProperty("chkKyouki")]
		private JG2CheckBox _chkKyouki;

		[AccessedThroughProperty("chkCharm")]
		private JG2CheckBox _chkCharm;

		[AccessedThroughProperty("chkNigate2")]
		private JG2CheckBox _chkNigate2;

		[AccessedThroughProperty("chkNekketu")]
		private JG2CheckBox _chkNekketu;

		[AccessedThroughProperty("chkNigate1")]
		private JG2CheckBox _chkNigate1;

		[AccessedThroughProperty("chkCyoroi")]
		private JG2CheckBox _chkCyoroi;

		[AccessedThroughProperty("cmbMonday1")]
		private JG2ComboBox _cmbMonday1;

		[AccessedThroughProperty("cmbSunday1")]
		private JG2ComboBox _cmbSunday1;

		[AccessedThroughProperty("cmbSunday2")]
		private JG2ComboBox _cmbSunday2;

		[AccessedThroughProperty("cmbSaturday1")]
		private JG2ComboBox _cmbSaturday1;

		[AccessedThroughProperty("cmbFriday1")]
		private JG2ComboBox _cmbFriday1;

		[AccessedThroughProperty("cmbThursday1")]
		private JG2ComboBox _cmbThursday1;

		[AccessedThroughProperty("cmbWednesday1")]
		private JG2ComboBox _cmbWednesday1;

		[AccessedThroughProperty("cmbTuesday1")]
		private JG2ComboBox _cmbTuesday1;

		[AccessedThroughProperty("cmbSaturday2")]
		private JG2ComboBox _cmbSaturday2;

		[AccessedThroughProperty("cmbFriday2")]
		private JG2ComboBox _cmbFriday2;

		[AccessedThroughProperty("cmbThursday2")]
		private JG2ComboBox _cmbThursday2;

		[AccessedThroughProperty("cmbWednesday2")]
		private JG2ComboBox _cmbWednesday2;

		[AccessedThroughProperty("cmbTuesday2")]
		private JG2ComboBox _cmbTuesday2;

		[AccessedThroughProperty("cmbMonday2")]
		private JG2ComboBox _cmbMonday2;

		private bool CloseCheck;

		private int PrevSeatNo;

		private int SeatNo;

		private byte Sex;

		public virtual JG2Button btnAction1
		{
			get
			{
				return this._btnAction1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._btnAction1 != null)
				{
					this._btnAction1.Click -= eventHandler;
				}
				this._btnAction1 = value;
				if (this._btnAction1 != null)
				{
					this._btnAction1.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnAction2
		{
			get
			{
				return this._btnAction2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._btnAction2 != null)
				{
					this._btnAction2.Click -= eventHandler;
				}
				this._btnAction2 = value;
				if (this._btnAction2 != null)
				{
					this._btnAction2.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnAction3
		{
			get
			{
				return this._btnAction3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._btnAction3 != null)
				{
					this._btnAction3.Click -= eventHandler;
				}
				this._btnAction3 = value;
				if (this._btnAction3 != null)
				{
					this._btnAction3.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnAction4
		{
			get
			{
				return this._btnAction4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._btnAction4 != null)
				{
					this._btnAction4.Click -= eventHandler;
				}
				this._btnAction4 = value;
				if (this._btnAction4 != null)
				{
					this._btnAction4.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnActionRev1
		{
			get
			{
				return this._btnActionRev1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._btnActionRev1 != null)
				{
					this._btnActionRev1.Click -= eventHandler;
				}
				this._btnActionRev1 = value;
				if (this._btnActionRev1 != null)
				{
					this._btnActionRev1.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnActionRev2
		{
			get
			{
				return this._btnActionRev2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._btnActionRev2 != null)
				{
					this._btnActionRev2.Click -= eventHandler;
				}
				this._btnActionRev2 = value;
				if (this._btnActionRev2 != null)
				{
					this._btnActionRev2.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnActionRev3
		{
			get
			{
				return this._btnActionRev3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._btnActionRev3 != null)
				{
					this._btnActionRev3.Click -= eventHandler;
				}
				this._btnActionRev3 = value;
				if (this._btnActionRev3 != null)
				{
					this._btnActionRev3.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnActionRev4
		{
			get
			{
				return this._btnActionRev4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._btnActionRev4 != null)
				{
					this._btnActionRev4.Click -= eventHandler;
				}
				this._btnActionRev4 = value;
				if (this._btnActionRev4 != null)
				{
					this._btnActionRev4.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnAfterPill
		{
			get
			{
				return this._btnAfterPill;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAfterPill_Click);
				if (this._btnAfterPill != null)
				{
					this._btnAfterPill.Click -= eventHandler;
				}
				this._btnAfterPill = value;
				if (this._btnAfterPill != null)
				{
					this._btnAfterPill.Click += eventHandler;
				}
			}
		}

		public virtual Button btnBatch
		{
			get
			{
				return this._btnBatch;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.btnBatch_TextChanged);
				frmDetail _frmDetail1 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail1.mnuBatch_Click);
				if (this._btnBatch != null)
				{
					this._btnBatch.TextChanged -= eventHandler;
					this._btnBatch.Click -= eventHandler1;
				}
				this._btnBatch = value;
				if (this._btnBatch != null)
				{
					this._btnBatch.TextChanged += eventHandler;
					this._btnBatch.Click += eventHandler1;
				}
			}
		}

		public virtual JG2Button btnClearAction
		{
			get
			{
				return this._btnClearAction;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsClearAction_Click);
				if (this._btnClearAction != null)
				{
					this._btnClearAction.Click -= eventHandler;
				}
				this._btnClearAction = value;
				if (this._btnClearAction != null)
				{
					this._btnClearAction.Click += eventHandler;
				}
			}
		}

		public virtual Button btnClearAll
		{
			get
			{
				return this._btnClearAll;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuClearAll_Click);
				if (this._btnClearAll != null)
				{
					this._btnClearAll.Click -= eventHandler;
				}
				this._btnClearAll = value;
				if (this._btnClearAll != null)
				{
					this._btnClearAll.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnColorEye1
		{
			get
			{
				return this._btnColorEye1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				MouseEventHandler mouseEventHandler = new MouseEventHandler(_frmDetail.btnColor_MouseDown);
				frmDetail _frmDetail1 = this;
				DragEventHandler dragEventHandler = new DragEventHandler(_frmDetail1.btnColor_DragEnter);
				frmDetail _frmDetail2 = this;
				DragEventHandler dragEventHandler1 = new DragEventHandler(_frmDetail2.btnColor_DragDrop);
				frmDetail _frmDetail3 = this;
				EventHandler eventHandler = new EventHandler(_frmDetail3.btnColor_Click);
				frmDetail _frmDetail4 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail4.Item_Changed);
				if (this._btnColorEye1 != null)
				{
					this._btnColorEye1.MouseDown -= mouseEventHandler;
					this._btnColorEye1.DragEnter -= dragEventHandler;
					this._btnColorEye1.DragDrop -= dragEventHandler1;
					this._btnColorEye1.Click -= eventHandler;
					this._btnColorEye1.BackColorChanged -= eventHandler1;
				}
				this._btnColorEye1 = value;
				if (this._btnColorEye1 != null)
				{
					this._btnColorEye1.MouseDown += mouseEventHandler;
					this._btnColorEye1.DragEnter += dragEventHandler;
					this._btnColorEye1.DragDrop += dragEventHandler1;
					this._btnColorEye1.Click += eventHandler;
					this._btnColorEye1.BackColorChanged += eventHandler1;
				}
			}
		}

		public virtual Button btnColorEye1Sub
		{
			get
			{
				return this._btnColorEye1Sub;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.btnColorSub_Click);
				if (this._btnColorEye1Sub != null)
				{
					this._btnColorEye1Sub.Click -= eventHandler;
				}
				this._btnColorEye1Sub = value;
				if (this._btnColorEye1Sub != null)
				{
					this._btnColorEye1Sub.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnColorEye2
		{
			get
			{
				return this._btnColorEye2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				MouseEventHandler mouseEventHandler = new MouseEventHandler(_frmDetail.btnColor_MouseDown);
				frmDetail _frmDetail1 = this;
				DragEventHandler dragEventHandler = new DragEventHandler(_frmDetail1.btnColor_DragEnter);
				frmDetail _frmDetail2 = this;
				DragEventHandler dragEventHandler1 = new DragEventHandler(_frmDetail2.btnColor_DragDrop);
				frmDetail _frmDetail3 = this;
				EventHandler eventHandler = new EventHandler(_frmDetail3.btnColor_Click);
				frmDetail _frmDetail4 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail4.Item_Changed);
				if (this._btnColorEye2 != null)
				{
					this._btnColorEye2.MouseDown -= mouseEventHandler;
					this._btnColorEye2.DragEnter -= dragEventHandler;
					this._btnColorEye2.DragDrop -= dragEventHandler1;
					this._btnColorEye2.Click -= eventHandler;
					this._btnColorEye2.BackColorChanged -= eventHandler1;
				}
				this._btnColorEye2 = value;
				if (this._btnColorEye2 != null)
				{
					this._btnColorEye2.MouseDown += mouseEventHandler;
					this._btnColorEye2.DragEnter += dragEventHandler;
					this._btnColorEye2.DragDrop += dragEventHandler1;
					this._btnColorEye2.Click += eventHandler;
					this._btnColorEye2.BackColorChanged += eventHandler1;
				}
			}
		}

		public virtual Button btnColorEye2Sub
		{
			get
			{
				return this._btnColorEye2Sub;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.btnColorSub_Click);
				if (this._btnColorEye2Sub != null)
				{
					this._btnColorEye2Sub.Click -= eventHandler;
				}
				this._btnColorEye2Sub = value;
				if (this._btnColorEye2Sub != null)
				{
					this._btnColorEye2Sub.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnColorGlasses
		{
			get
			{
				return this._btnColorGlasses;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				MouseEventHandler mouseEventHandler = new MouseEventHandler(_frmDetail.btnColor_MouseDown);
				frmDetail _frmDetail1 = this;
				DragEventHandler dragEventHandler = new DragEventHandler(_frmDetail1.btnColor_DragEnter);
				frmDetail _frmDetail2 = this;
				DragEventHandler dragEventHandler1 = new DragEventHandler(_frmDetail2.btnColor_DragDrop);
				frmDetail _frmDetail3 = this;
				EventHandler eventHandler = new EventHandler(_frmDetail3.btnColor_Click);
				frmDetail _frmDetail4 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail4.Item_Changed);
				if (this._btnColorGlasses != null)
				{
					this._btnColorGlasses.MouseDown -= mouseEventHandler;
					this._btnColorGlasses.DragEnter -= dragEventHandler;
					this._btnColorGlasses.DragDrop -= dragEventHandler1;
					this._btnColorGlasses.Click -= eventHandler;
					this._btnColorGlasses.BackColorChanged -= eventHandler1;
				}
				this._btnColorGlasses = value;
				if (this._btnColorGlasses != null)
				{
					this._btnColorGlasses.MouseDown += mouseEventHandler;
					this._btnColorGlasses.DragEnter += dragEventHandler;
					this._btnColorGlasses.DragDrop += dragEventHandler1;
					this._btnColorGlasses.Click += eventHandler;
					this._btnColorGlasses.BackColorChanged += eventHandler1;
				}
			}
		}

		public virtual JG2Button btnColorHair
		{
			get
			{
				return this._btnColorHair;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.btnColor_Click);
				frmDetail _frmDetail1 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail1.Item_Changed);
				if (this._btnColorHair != null)
				{
					this._btnColorHair.Click -= eventHandler;
					this._btnColorHair.BackColorChanged -= eventHandler1;
				}
				this._btnColorHair = value;
				if (this._btnColorHair != null)
				{
					this._btnColorHair.Click += eventHandler;
					this._btnColorHair.BackColorChanged += eventHandler1;
				}
			}
		}

		public virtual Button btnColorHairSub1
		{
			get
			{
				return this._btnColorHairSub1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.btnColorSub_Click);
				if (this._btnColorHairSub1 != null)
				{
					this._btnColorHairSub1.Click -= eventHandler;
				}
				this._btnColorHairSub1 = value;
				if (this._btnColorHairSub1 != null)
				{
					this._btnColorHairSub1.Click += eventHandler;
				}
			}
		}

		public virtual Button btnColorHairSub2
		{
			get
			{
				return this._btnColorHairSub2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.btnColorSub_Click);
				if (this._btnColorHairSub2 != null)
				{
					this._btnColorHairSub2.Click -= eventHandler;
				}
				this._btnColorHairSub2 = value;
				if (this._btnColorHairSub2 != null)
				{
					this._btnColorHairSub2.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnColorMayu
		{
			get
			{
				return this._btnColorMayu;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				MouseEventHandler mouseEventHandler = new MouseEventHandler(_frmDetail.btnColor_MouseDown);
				frmDetail _frmDetail1 = this;
				DragEventHandler dragEventHandler = new DragEventHandler(_frmDetail1.btnColor_DragEnter);
				frmDetail _frmDetail2 = this;
				DragEventHandler dragEventHandler1 = new DragEventHandler(_frmDetail2.btnColor_DragDrop);
				frmDetail _frmDetail3 = this;
				EventHandler eventHandler = new EventHandler(_frmDetail3.btnColor_Click);
				frmDetail _frmDetail4 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail4.Item_Changed);
				if (this._btnColorMayu != null)
				{
					this._btnColorMayu.MouseDown -= mouseEventHandler;
					this._btnColorMayu.DragEnter -= dragEventHandler;
					this._btnColorMayu.DragDrop -= dragEventHandler1;
					this._btnColorMayu.Click -= eventHandler;
					this._btnColorMayu.BackColorChanged -= eventHandler1;
				}
				this._btnColorMayu = value;
				if (this._btnColorMayu != null)
				{
					this._btnColorMayu.MouseDown += mouseEventHandler;
					this._btnColorMayu.DragEnter += dragEventHandler;
					this._btnColorMayu.DragDrop += dragEventHandler1;
					this._btnColorMayu.Click += eventHandler;
					this._btnColorMayu.BackColorChanged += eventHandler1;
				}
			}
		}

		public virtual JG2Button btnColorSkin2
		{
			get
			{
				return this._btnColorSkin2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				MouseEventHandler mouseEventHandler = new MouseEventHandler(_frmDetail.btnColor_MouseDown);
				frmDetail _frmDetail1 = this;
				DragEventHandler dragEventHandler = new DragEventHandler(_frmDetail1.btnColor_DragEnter);
				frmDetail _frmDetail2 = this;
				DragEventHandler dragEventHandler1 = new DragEventHandler(_frmDetail2.btnColor_DragDrop);
				frmDetail _frmDetail3 = this;
				EventHandler eventHandler = new EventHandler(_frmDetail3.btnColor_Click);
				frmDetail _frmDetail4 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail4.Item_Changed);
				if (this._btnColorSkin2 != null)
				{
					this._btnColorSkin2.MouseDown -= mouseEventHandler;
					this._btnColorSkin2.DragEnter -= dragEventHandler;
					this._btnColorSkin2.DragDrop -= dragEventHandler1;
					this._btnColorSkin2.Click -= eventHandler;
					this._btnColorSkin2.BackColorChanged -= eventHandler1;
				}
				this._btnColorSkin2 = value;
				if (this._btnColorSkin2 != null)
				{
					this._btnColorSkin2.MouseDown += mouseEventHandler;
					this._btnColorSkin2.DragEnter += dragEventHandler;
					this._btnColorSkin2.DragDrop += dragEventHandler1;
					this._btnColorSkin2.Click += eventHandler;
					this._btnColorSkin2.BackColorChanged += eventHandler1;
				}
			}
		}

		public virtual JG2Button btnColorUnder
		{
			get
			{
				return this._btnColorUnder;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				MouseEventHandler mouseEventHandler = new MouseEventHandler(_frmDetail.btnColor_MouseDown);
				frmDetail _frmDetail1 = this;
				DragEventHandler dragEventHandler = new DragEventHandler(_frmDetail1.btnColor_DragEnter);
				frmDetail _frmDetail2 = this;
				DragEventHandler dragEventHandler1 = new DragEventHandler(_frmDetail2.btnColor_DragDrop);
				frmDetail _frmDetail3 = this;
				EventHandler eventHandler = new EventHandler(_frmDetail3.btnColor_Click);
				frmDetail _frmDetail4 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail4.Item_Changed);
				if (this._btnColorUnder != null)
				{
					this._btnColorUnder.MouseDown -= mouseEventHandler;
					this._btnColorUnder.DragEnter -= dragEventHandler;
					this._btnColorUnder.DragDrop -= dragEventHandler1;
					this._btnColorUnder.Click -= eventHandler;
					this._btnColorUnder.BackColorChanged -= eventHandler1;
				}
				this._btnColorUnder = value;
				if (this._btnColorUnder != null)
				{
					this._btnColorUnder.MouseDown += mouseEventHandler;
					this._btnColorUnder.DragEnter += dragEventHandler;
					this._btnColorUnder.DragDrop += dragEventHandler1;
					this._btnColorUnder.Click += eventHandler;
					this._btnColorUnder.BackColorChanged += eventHandler1;
				}
			}
		}

		public virtual JG2Button btnDelAction1
		{
			get
			{
				return this._btnDelAction1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._btnDelAction1 != null)
				{
					this._btnDelAction1.Click -= eventHandler;
				}
				this._btnDelAction1 = value;
				if (this._btnDelAction1 != null)
				{
					this._btnDelAction1.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnDelAction2
		{
			get
			{
				return this._btnDelAction2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._btnDelAction2 != null)
				{
					this._btnDelAction2.Click -= eventHandler;
				}
				this._btnDelAction2 = value;
				if (this._btnDelAction2 != null)
				{
					this._btnDelAction2.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnDelAction3
		{
			get
			{
				return this._btnDelAction3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._btnDelAction3 != null)
				{
					this._btnDelAction3.Click -= eventHandler;
				}
				this._btnDelAction3 = value;
				if (this._btnDelAction3 != null)
				{
					this._btnDelAction3.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnDelAction4
		{
			get
			{
				return this._btnDelAction4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._btnDelAction4 != null)
				{
					this._btnDelAction4.Click -= eventHandler;
				}
				this._btnDelAction4 = value;
				if (this._btnDelAction4 != null)
				{
					this._btnDelAction4.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnDelActionRev1
		{
			get
			{
				return this._btnDelActionRev1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._btnDelActionRev1 != null)
				{
					this._btnDelActionRev1.Click -= eventHandler;
				}
				this._btnDelActionRev1 = value;
				if (this._btnDelActionRev1 != null)
				{
					this._btnDelActionRev1.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnDelActionRev2
		{
			get
			{
				return this._btnDelActionRev2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._btnDelActionRev2 != null)
				{
					this._btnDelActionRev2.Click -= eventHandler;
				}
				this._btnDelActionRev2 = value;
				if (this._btnDelActionRev2 != null)
				{
					this._btnDelActionRev2.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnDelActionRev3
		{
			get
			{
				return this._btnDelActionRev3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._btnDelActionRev3 != null)
				{
					this._btnDelActionRev3.Click -= eventHandler;
				}
				this._btnDelActionRev3 = value;
				if (this._btnDelActionRev3 != null)
				{
					this._btnDelActionRev3.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnDelActionRev4
		{
			get
			{
				return this._btnDelActionRev4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._btnDelActionRev4 != null)
				{
					this._btnDelActionRev4.Click -= eventHandler;
				}
				this._btnDelActionRev4 = value;
				if (this._btnDelActionRev4 != null)
				{
					this._btnDelActionRev4.Click += eventHandler;
				}
			}
		}

		public virtual Button btnExport
		{
			get
			{
				return this._btnExport;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsExport_Click);
				if (this._btnExport != null)
				{
					this._btnExport.Click -= eventHandler;
				}
				this._btnExport = value;
				if (this._btnExport != null)
				{
					this._btnExport.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnGohoubi
		{
			get
			{
				return this._btnGohoubi;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsGohoubi_Click);
				if (this._btnGohoubi != null)
				{
					this._btnGohoubi.Click -= eventHandler;
				}
				this._btnGohoubi = value;
				if (this._btnGohoubi != null)
				{
					this._btnGohoubi.Click += eventHandler;
				}
			}
		}

		public virtual JG2Button btnHMax
		{
			get
			{
				return this._btnHMax;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsHMax_Click);
				if (this._btnHMax != null)
				{
					this._btnHMax.Click -= eventHandler;
				}
				this._btnHMax = value;
				if (this._btnHMax != null)
				{
					this._btnHMax.Click += eventHandler;
				}
			}
		}

		public virtual Button btnImport
		{
			get
			{
				return this._btnImport;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsImport_Click);
				if (this._btnImport != null)
				{
					this._btnImport.Click -= eventHandler;
				}
				this._btnImport = value;
				if (this._btnImport != null)
				{
					this._btnImport.Click += eventHandler;
				}
			}
		}

		public virtual Button btnPlayer
		{
			get
			{
				return this._btnPlayer;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsPlayer_Click);
				if (this._btnPlayer != null)
				{
					this._btnPlayer.Click -= eventHandler;
				}
				this._btnPlayer = value;
				if (this._btnPlayer != null)
				{
					this._btnPlayer.Click += eventHandler;
				}
			}
		}

		public virtual Button btnSelectAll
		{
			get
			{
				return this._btnSelectAll;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuSelectAll_Click);
				if (this._btnSelectAll != null)
				{
					this._btnSelectAll.Click -= eventHandler;
				}
				this._btnSelectAll = value;
				if (this._btnSelectAll != null)
				{
					this._btnSelectAll.Click += eventHandler;
				}
			}
		}

		public virtual Button btnSubmit
		{
			get
			{
				return this._btnSubmit;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuSubmit_Click);
				frmDetail _frmDetail1 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail1.btnSubmit_EnabledChanged);
				if (this._btnSubmit != null)
				{
					this._btnSubmit.Click -= eventHandler;
					this._btnSubmit.EnabledChanged -= eventHandler1;
				}
				this._btnSubmit = value;
				if (this._btnSubmit != null)
				{
					this._btnSubmit.Click += eventHandler;
					this._btnSubmit.EnabledChanged += eventHandler1;
				}
			}
		}

		public virtual JG2CheckBox chkAnal
		{
			get
			{
				return this._chkAnal;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkAnal != null)
				{
					this._chkAnal.CheckedChanged -= eventHandler;
				}
				this._chkAnal = value;
				if (this._chkAnal != null)
				{
					this._chkAnal.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkAsekaki
		{
			get
			{
				return this._chkAsekaki;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkAsekaki != null)
				{
					this._chkAsekaki.CheckedChanged -= eventHandler;
				}
				this._chkAsekaki = value;
				if (this._chkAsekaki != null)
				{
					this._chkAsekaki.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkBack
		{
			get
			{
				return this._chkBack;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkBack != null)
				{
					this._chkBack.CheckedChanged -= eventHandler;
				}
				this._chkBack = value;
				if (this._chkBack != null)
				{
					this._chkBack.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkBouryoku
		{
			get
			{
				return this._chkBouryoku;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkBouryoku != null)
				{
					this._chkBouryoku.CheckedChanged -= eventHandler;
				}
				this._chkBouryoku = value;
				if (this._chkBouryoku != null)
				{
					this._chkBouryoku.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkBust
		{
			get
			{
				return this._chkBust;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkBust != null)
				{
					this._chkBust.CheckedChanged -= eventHandler;
				}
				this._chkBust = value;
				if (this._chkBust != null)
				{
					this._chkBust.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkCharm
		{
			get
			{
				return this._chkCharm;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkCharm != null)
				{
					this._chkCharm.CheckedChanged -= eventHandler;
				}
				this._chkCharm = value;
				if (this._chkCharm != null)
				{
					this._chkCharm.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkCool
		{
			get
			{
				return this._chkCool;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkCool != null)
				{
					this._chkCool.CheckedChanged -= eventHandler;
				}
				this._chkCool = value;
				if (this._chkCool != null)
				{
					this._chkCool.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkCunnilingus
		{
			get
			{
				return this._chkCunnilingus;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkCunnilingus != null)
				{
					this._chkCunnilingus.CheckedChanged -= eventHandler;
				}
				this._chkCunnilingus = value;
				if (this._chkCunnilingus != null)
				{
					this._chkCunnilingus.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkCyoroi
		{
			get
			{
				return this._chkCyoroi;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkCyoroi != null)
				{
					this._chkCyoroi.CheckedChanged -= eventHandler;
				}
				this._chkCyoroi = value;
				if (this._chkCyoroi != null)
				{
					this._chkCyoroi.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkFellatio
		{
			get
			{
				return this._chkFellatio;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkFellatio != null)
				{
					this._chkFellatio.CheckedChanged -= eventHandler;
				}
				this._chkFellatio = value;
				if (this._chkFellatio != null)
				{
					this._chkFellatio.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkHairRev1
		{
			get
			{
				return this._chkHairRev1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkHairRev1 != null)
				{
					this._chkHairRev1.CheckedChanged -= eventHandler;
				}
				this._chkHairRev1 = value;
				if (this._chkHairRev1 != null)
				{
					this._chkHairRev1.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkHairRev2
		{
			get
			{
				return this._chkHairRev2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkHairRev2 != null)
				{
					this._chkHairRev2.CheckedChanged -= eventHandler;
				}
				this._chkHairRev2 = value;
				if (this._chkHairRev2 != null)
				{
					this._chkHairRev2.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkHairRev3
		{
			get
			{
				return this._chkHairRev3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkHairRev3 != null)
				{
					this._chkHairRev3.CheckedChanged -= eventHandler;
				}
				this._chkHairRev3 = value;
				if (this._chkHairRev3 != null)
				{
					this._chkHairRev3.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkHairRev4
		{
			get
			{
				return this._chkHairRev4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkHairRev4 != null)
				{
					this._chkHairRev4.CheckedChanged -= eventHandler;
				}
				this._chkHairRev4 = value;
				if (this._chkHairRev4 != null)
				{
					this._chkHairRev4.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkHaraguro
		{
			get
			{
				return this._chkHaraguro;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkHaraguro != null)
				{
					this._chkHaraguro.CheckedChanged -= eventHandler;
				}
				this._chkHaraguro = value;
				if (this._chkHaraguro != null)
				{
					this._chkHaraguro.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkHarapeko
		{
			get
			{
				return this._chkHarapeko;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkHarapeko != null)
				{
					this._chkHarapeko.CheckedChanged -= eventHandler;
				}
				this._chkHarapeko = value;
				if (this._chkHarapeko != null)
				{
					this._chkHarapeko.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkHokuro1
		{
			get
			{
				return this._chkHokuro1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkHokuro1 != null)
				{
					this._chkHokuro1.CheckedChanged -= eventHandler;
				}
				this._chkHokuro1 = value;
				if (this._chkHokuro1 != null)
				{
					this._chkHokuro1.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkHokuro2
		{
			get
			{
				return this._chkHokuro2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkHokuro2 != null)
				{
					this._chkHokuro2.CheckedChanged -= eventHandler;
				}
				this._chkHokuro2 = value;
				if (this._chkHokuro2 != null)
				{
					this._chkHokuro2.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkHokuro3
		{
			get
			{
				return this._chkHokuro3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkHokuro3 != null)
				{
					this._chkHokuro3.CheckedChanged -= eventHandler;
				}
				this._chkHokuro3 = value;
				if (this._chkHokuro3 != null)
				{
					this._chkHokuro3.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkHokuro4
		{
			get
			{
				return this._chkHokuro4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkHokuro4 != null)
				{
					this._chkHokuro4.CheckedChanged -= eventHandler;
				}
				this._chkHokuro4 = value;
				if (this._chkHokuro4 != null)
				{
					this._chkHokuro4.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkHonpou
		{
			get
			{
				return this._chkHonpou;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkHonpou != null)
				{
					this._chkHonpou.CheckedChanged -= eventHandler;
				}
				this._chkHonpou = value;
				if (this._chkHonpou != null)
				{
					this._chkHonpou.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkIintyou
		{
			get
			{
				return this._chkIintyou;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkIintyou != null)
				{
					this._chkIintyou.CheckedChanged -= eventHandler;
				}
				this._chkIintyou = value;
				if (this._chkIintyou != null)
				{
					this._chkIintyou.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkItizu
		{
			get
			{
				return this._chkItizu;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkItizu != null)
				{
					this._chkItizu.CheckedChanged -= eventHandler;
				}
				this._chkItizu = value;
				if (this._chkItizu != null)
				{
					this._chkItizu.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkKake
		{
			get
			{
				return this._chkKake;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkKake != null)
				{
					this._chkKake.CheckedChanged -= eventHandler;
				}
				this._chkKake = value;
				if (this._chkKake != null)
				{
					this._chkKake.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkKinben
		{
			get
			{
				return this._chkKinben;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkKinben != null)
				{
					this._chkKinben.CheckedChanged -= eventHandler;
				}
				this._chkKinben = value;
				if (this._chkKinben != null)
				{
					this._chkKinben.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkKiss
		{
			get
			{
				return this._chkKiss;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkKiss != null)
				{
					this._chkKiss.CheckedChanged -= eventHandler;
				}
				this._chkKiss = value;
				if (this._chkKiss != null)
				{
					this._chkKiss.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkKousai
		{
			get
			{
				return this._chkKousai;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkKousai != null)
				{
					this._chkKousai.CheckedChanged -= eventHandler;
				}
				this._chkKousai = value;
				if (this._chkKousai != null)
				{
					this._chkKousai.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkKyouki
		{
			get
			{
				return this._chkKyouki;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkKyouki != null)
				{
					this._chkKyouki.CheckedChanged -= eventHandler;
				}
				this._chkKyouki = value;
				if (this._chkKyouki != null)
				{
					this._chkKyouki.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkLuck
		{
			get
			{
				return this._chkLuck;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkLuck != null)
				{
					this._chkLuck.CheckedChanged -= eventHandler;
				}
				this._chkLuck = value;
				if (this._chkLuck != null)
				{
					this._chkLuck.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkM
		{
			get
			{
				return this._chkM;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkM != null)
				{
					this._chkM.CheckedChanged -= eventHandler;
				}
				this._chkM = value;
				if (this._chkM != null)
				{
					this._chkM.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkMaemuki
		{
			get
			{
				return this._chkMaemuki;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkMaemuki != null)
				{
					this._chkMaemuki.CheckedChanged -= eventHandler;
				}
				this._chkMaemuki = value;
				if (this._chkMaemuki != null)
				{
					this._chkMaemuki.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkMajime
		{
			get
			{
				return this._chkMajime;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkMajime != null)
				{
					this._chkMajime.CheckedChanged -= eventHandler;
				}
				this._chkMajime = value;
				if (this._chkMajime != null)
				{
					this._chkMajime.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkMakenki
		{
			get
			{
				return this._chkMakenki;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkMakenki != null)
				{
					this._chkMakenki.CheckedChanged -= eventHandler;
				}
				this._chkMakenki = value;
				if (this._chkMakenki != null)
				{
					this._chkMakenki.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkMiha
		{
			get
			{
				return this._chkMiha;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkMiha != null)
				{
					this._chkMiha.CheckedChanged -= eventHandler;
				}
				this._chkMiha = value;
				if (this._chkMiha != null)
				{
					this._chkMiha.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkMiseityo
		{
			get
			{
				return this._chkMiseityo;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkMiseityo != null)
				{
					this._chkMiseityo.CheckedChanged -= eventHandler;
				}
				this._chkMiseityo = value;
				if (this._chkMiseityo != null)
				{
					this._chkMiseityo.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkNaka
		{
			get
			{
				return this._chkNaka;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkNaka != null)
				{
					this._chkNaka.CheckedChanged -= eventHandler;
				}
				this._chkNaka = value;
				if (this._chkNaka != null)
				{
					this._chkNaka.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkNama
		{
			get
			{
				return this._chkNama;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkNama != null)
				{
					this._chkNama.CheckedChanged -= eventHandler;
				}
				this._chkNama = value;
				if (this._chkNama != null)
				{
					this._chkNama.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkNantyo
		{
			get
			{
				return this._chkNantyo;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkNantyo != null)
				{
					this._chkNantyo.CheckedChanged -= eventHandler;
				}
				this._chkNantyo = value;
				if (this._chkNantyo != null)
				{
					this._chkNantyo.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkNekketu
		{
			get
			{
				return this._chkNekketu;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkNekketu != null)
				{
					this._chkNekketu.CheckedChanged -= eventHandler;
				}
				this._chkNekketu = value;
				if (this._chkNekketu != null)
				{
					this._chkNekketu.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkNigate1
		{
			get
			{
				return this._chkNigate1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkNigate1 != null)
				{
					this._chkNigate1.CheckedChanged -= eventHandler;
				}
				this._chkNigate1 = value;
				if (this._chkNigate1 != null)
				{
					this._chkNigate1.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkNigate2
		{
			get
			{
				return this._chkNigate2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkNigate2 != null)
				{
					this._chkNigate2.CheckedChanged -= eventHandler;
				}
				this._chkNigate2 = value;
				if (this._chkNigate2 != null)
				{
					this._chkNigate2.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkOnna
		{
			get
			{
				return this._chkOnna;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkOnna != null)
				{
					this._chkOnna.CheckedChanged -= eventHandler;
				}
				this._chkOnna = value;
				if (this._chkOnna != null)
				{
					this._chkOnna.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkOsyaberi
		{
			get
			{
				return this._chkOsyaberi;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkOsyaberi != null)
				{
					this._chkOsyaberi.CheckedChanged -= eventHandler;
				}
				this._chkOsyaberi = value;
				if (this._chkOsyaberi != null)
				{
					this._chkOsyaberi.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkPoyayan
		{
			get
			{
				return this._chkPoyayan;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkPoyayan != null)
				{
					this._chkPoyayan.CheckedChanged -= eventHandler;
				}
				this._chkPoyayan = value;
				if (this._chkPoyayan != null)
				{
					this._chkPoyayan.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkRare
		{
			get
			{
				return this._chkRare;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkRare != null)
				{
					this._chkRare.CheckedChanged -= eventHandler;
				}
				this._chkRare = value;
				if (this._chkRare != null)
				{
					this._chkRare.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkRenai
		{
			get
			{
				return this._chkRenai;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkRenai != null)
				{
					this._chkRenai.CheckedChanged -= eventHandler;
				}
				this._chkRenai = value;
				if (this._chkRenai != null)
				{
					this._chkRenai.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkSeiin
		{
			get
			{
				return this._chkSeiin;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkSeiin != null)
				{
					this._chkSeiin.CheckedChanged -= eventHandler;
				}
				this._chkSeiin = value;
				if (this._chkSeiin != null)
				{
					this._chkSeiin.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkSeiki1
		{
			get
			{
				return this._chkSeiki1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkSeiki1 != null)
				{
					this._chkSeiki1.CheckedChanged -= eventHandler;
				}
				this._chkSeiki1 = value;
				if (this._chkSeiki1 != null)
				{
					this._chkSeiki1.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkSeiki2
		{
			get
			{
				return this._chkSeiki2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkSeiki2 != null)
				{
					this._chkSeiki2.CheckedChanged -= eventHandler;
				}
				this._chkSeiki2 = value;
				if (this._chkSeiki2 != null)
				{
					this._chkSeiki2.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkSeikou1
		{
			get
			{
				return this._chkSeikou1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkSeikou1 != null)
				{
					this._chkSeikou1.CheckedChanged -= eventHandler;
				}
				this._chkSeikou1 = value;
				if (this._chkSeikou1 != null)
				{
					this._chkSeikou1.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkSeikou2
		{
			get
			{
				return this._chkSeikou2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkSeikou2 != null)
				{
					this._chkSeikou2.CheckedChanged -= eventHandler;
				}
				this._chkSeikou2 = value;
				if (this._chkSeikou2 != null)
				{
					this._chkSeikou2.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkSewayaki
		{
			get
			{
				return this._chkSewayaki;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkSewayaki != null)
				{
					this._chkSewayaki.CheckedChanged -= eventHandler;
				}
				this._chkSewayaki = value;
				if (this._chkSewayaki != null)
				{
					this._chkSewayaki.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkSousyoku
		{
			get
			{
				return this._chkSousyoku;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkSousyoku != null)
				{
					this._chkSousyoku.CheckedChanged -= eventHandler;
				}
				this._chkSousyoku = value;
				if (this._chkSousyoku != null)
				{
					this._chkSousyoku.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkSukebe
		{
			get
			{
				return this._chkSukebe;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkSukebe != null)
				{
					this._chkSukebe.CheckedChanged -= eventHandler;
				}
				this._chkSukebe = value;
				if (this._chkSukebe != null)
				{
					this._chkSukebe.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkSunao
		{
			get
			{
				return this._chkSunao;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkSunao != null)
				{
					this._chkSunao.CheckedChanged -= eventHandler;
				}
				this._chkSunao = value;
				if (this._chkSunao != null)
				{
					this._chkSunao.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkTereya
		{
			get
			{
				return this._chkTereya;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkTereya != null)
				{
					this._chkTereya.CheckedChanged -= eventHandler;
				}
				this._chkTereya = value;
				if (this._chkTereya != null)
				{
					this._chkTereya.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkToufu
		{
			get
			{
				return this._chkToufu;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkToufu != null)
				{
					this._chkToufu.CheckedChanged -= eventHandler;
				}
				this._chkToufu = value;
				if (this._chkToufu != null)
				{
					this._chkToufu.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkTundere
		{
			get
			{
				return this._chkTundere;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkTundere != null)
				{
					this._chkTundere.CheckedChanged -= eventHandler;
				}
				this._chkTundere = value;
				if (this._chkTundere != null)
				{
					this._chkTundere.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkTyokujyo
		{
			get
			{
				return this._chkTyokujyo;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkTyokujyo != null)
				{
					this._chkTyokujyo.CheckedChanged -= eventHandler;
				}
				this._chkTyokujyo = value;
				if (this._chkTyokujyo != null)
				{
					this._chkTyokujyo.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkYakimochi
		{
			get
			{
				return this._chkYakimochi;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkYakimochi != null)
				{
					this._chkYakimochi.CheckedChanged -= eventHandler;
				}
				this._chkYakimochi = value;
				if (this._chkYakimochi != null)
				{
					this._chkYakimochi.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkYami
		{
			get
			{
				return this._chkYami;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkYami != null)
				{
					this._chkYami.CheckedChanged -= eventHandler;
				}
				this._chkYami = value;
				if (this._chkYami != null)
				{
					this._chkYami.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkYowami
		{
			get
			{
				return this._chkYowami;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkYowami != null)
				{
					this._chkYowami.CheckedChanged -= eventHandler;
				}
				this._chkYowami = value;
				if (this._chkYowami != null)
				{
					this._chkYowami.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2CheckBox chkYuujyufudan
		{
			get
			{
				return this._chkYuujyufudan;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._chkYuujyufudan != null)
				{
					this._chkYuujyufudan.CheckedChanged -= eventHandler;
				}
				this._chkYuujyufudan = value;
				if (this._chkYuujyufudan != null)
				{
					this._chkYuujyufudan.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbBukatu1
		{
			get
			{
				return this._cmbBukatu1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbBukatu1 != null)
				{
					this._cmbBukatu1.SelectedIndexChanged -= eventHandler;
				}
				this._cmbBukatu1 = value;
				if (this._cmbBukatu1 != null)
				{
					this._cmbBukatu1.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbFriday1
		{
			get
			{
				return this._cmbFriday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbFriday1 != null)
				{
					this._cmbFriday1.SelectedIndexChanged -= eventHandler;
				}
				this._cmbFriday1 = value;
				if (this._cmbFriday1 != null)
				{
					this._cmbFriday1.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbFriday2
		{
			get
			{
				return this._cmbFriday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbFriday2 != null)
				{
					this._cmbFriday2.SelectedIndexChanged -= eventHandler;
				}
				this._cmbFriday2 = value;
				if (this._cmbFriday2 != null)
				{
					this._cmbFriday2.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown cmbGlasses2
		{
			get
			{
				return this._cmbGlasses2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbGlasses2 != null)
				{
					this._cmbGlasses2.ValueChanged -= eventHandler;
				}
				this._cmbGlasses2 = value;
				if (this._cmbGlasses2 != null)
				{
					this._cmbGlasses2.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbHeight
		{
			get
			{
				return this._cmbHeight;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbHeight != null)
				{
					this._cmbHeight.SelectedIndexChanged -= eventHandler;
				}
				this._cmbHeight = value;
				if (this._cmbHeight != null)
				{
					this._cmbHeight.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbHighlight1
		{
			get
			{
				return this._cmbHighlight1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbHighlight1 != null)
				{
					this._cmbHighlight1.SelectedIndexChanged -= eventHandler;
				}
				this._cmbHighlight1 = value;
				if (this._cmbHighlight1 != null)
				{
					this._cmbHighlight1.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown cmbHiyake1
		{
			get
			{
				return this._cmbHiyake1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbHiyake1 != null)
				{
					this._cmbHiyake1.ValueChanged -= eventHandler;
				}
				this._cmbHiyake1 = value;
				if (this._cmbHiyake1 != null)
				{
					this._cmbHiyake1.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown cmbKao
		{
			get
			{
				return this._cmbKao;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbKao != null)
				{
					this._cmbKao.ValueChanged -= eventHandler;
				}
				this._cmbKao = value;
				if (this._cmbKao != null)
				{
					this._cmbKao.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbKenka
		{
			get
			{
				return this._cmbKenka;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbKenka != null)
				{
					this._cmbKenka.SelectedIndexChanged -= eventHandler;
				}
				this._cmbKenka = value;
				if (this._cmbKenka != null)
				{
					this._cmbKenka.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown cmbLip1
		{
			get
			{
				return this._cmbLip1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbLip1 != null)
				{
					this._cmbLip1.ValueChanged -= eventHandler;
				}
				this._cmbLip1 = value;
				if (this._cmbLip1 != null)
				{
					this._cmbLip1.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbMabuta
		{
			get
			{
				return this._cmbMabuta;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbMabuta != null)
				{
					this._cmbMabuta.SelectedIndexChanged -= eventHandler;
				}
				this._cmbMabuta = value;
				if (this._cmbMabuta != null)
				{
					this._cmbMabuta.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbMatuge1
		{
			get
			{
				return this._cmbMatuge1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbMatuge1 != null)
				{
					this._cmbMatuge1.SelectedIndexChanged -= eventHandler;
				}
				this._cmbMatuge1 = value;
				if (this._cmbMatuge1 != null)
				{
					this._cmbMatuge1.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbMatuge2
		{
			get
			{
				return this._cmbMatuge2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbMatuge2 != null)
				{
					this._cmbMatuge2.SelectedIndexChanged -= eventHandler;
				}
				this._cmbMatuge2 = value;
				if (this._cmbMatuge2 != null)
				{
					this._cmbMatuge2.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbMayu1
		{
			get
			{
				return this._cmbMayu1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbMayu1 != null)
				{
					this._cmbMayu1.SelectedIndexChanged -= eventHandler;
				}
				this._cmbMayu1 = value;
				if (this._cmbMayu1 != null)
				{
					this._cmbMayu1.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbMonday1
		{
			get
			{
				return this._cmbMonday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbMonday1 != null)
				{
					this._cmbMonday1.SelectedIndexChanged -= eventHandler;
				}
				this._cmbMonday1 = value;
				if (this._cmbMonday1 != null)
				{
					this._cmbMonday1.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbMonday2
		{
			get
			{
				return this._cmbMonday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbMonday2 != null)
				{
					this._cmbMonday2.SelectedIndexChanged -= eventHandler;
				}
				this._cmbMonday2 = value;
				if (this._cmbMonday2 != null)
				{
					this._cmbMonday2.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown cmbMosaic
		{
			get
			{
				return this._cmbMosaic;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbMosaic != null)
				{
					this._cmbMosaic.ValueChanged -= eventHandler;
				}
				this._cmbMosaic = value;
				if (this._cmbMosaic != null)
				{
					this._cmbMosaic.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbMune2
		{
			get
			{
				return this._cmbMune2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbMune2 != null)
				{
					this._cmbMune2.SelectedIndexChanged -= eventHandler;
				}
				this._cmbMune2 = value;
				if (this._cmbMune2 != null)
				{
					this._cmbMune2.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbMune8
		{
			get
			{
				return this._cmbMune8;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbMune8 != null)
				{
					this._cmbMune8.SelectedIndexChanged -= eventHandler;
				}
				this._cmbMune8 = value;
				if (this._cmbMune8 != null)
				{
					this._cmbMune8.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbPupil1
		{
			get
			{
				return this._cmbPupil1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbPupil1 != null)
				{
					this._cmbPupil1.SelectedIndexChanged -= eventHandler;
				}
				this._cmbPupil1 = value;
				if (this._cmbPupil1 != null)
				{
					this._cmbPupil1.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbPupil5
		{
			get
			{
				return this._cmbPupil5;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbPupil5 != null)
				{
					this._cmbPupil5.SelectedIndexChanged -= eventHandler;
				}
				this._cmbPupil5 = value;
				if (this._cmbPupil5 != null)
				{
					this._cmbPupil5.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbSaturday1
		{
			get
			{
				return this._cmbSaturday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbSaturday1 != null)
				{
					this._cmbSaturday1.SelectedIndexChanged -= eventHandler;
				}
				this._cmbSaturday1 = value;
				if (this._cmbSaturday1 != null)
				{
					this._cmbSaturday1.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbSaturday2
		{
			get
			{
				return this._cmbSaturday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbSaturday2 != null)
				{
					this._cmbSaturday2.SelectedIndexChanged -= eventHandler;
				}
				this._cmbSaturday2 = value;
				if (this._cmbSaturday2 != null)
				{
					this._cmbSaturday2.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbSeiai
		{
			get
			{
				return this._cmbSeiai;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbSeiai != null)
				{
					this._cmbSeiai.SelectedIndexChanged -= eventHandler;
				}
				this._cmbSeiai = value;
				if (this._cmbSeiai != null)
				{
					this._cmbSeiai.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbStyle
		{
			get
			{
				return this._cmbStyle;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbStyle != null)
				{
					this._cmbStyle.SelectedIndexChanged -= eventHandler;
				}
				this._cmbStyle = value;
				if (this._cmbStyle != null)
				{
					this._cmbStyle.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbSunday1
		{
			get
			{
				return this._cmbSunday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbSunday1 != null)
				{
					this._cmbSunday1.SelectedIndexChanged -= eventHandler;
				}
				this._cmbSunday1 = value;
				if (this._cmbSunday1 != null)
				{
					this._cmbSunday1.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbSunday2
		{
			get
			{
				return this._cmbSunday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbSunday2 != null)
				{
					this._cmbSunday2.SelectedIndexChanged -= eventHandler;
				}
				this._cmbSunday2 = value;
				if (this._cmbSunday2 != null)
				{
					this._cmbSunday2.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbSyakousei
		{
			get
			{
				return this._cmbSyakousei;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbSyakousei != null)
				{
					this._cmbSyakousei.SelectedIndexChanged -= eventHandler;
				}
				this._cmbSyakousei = value;
				if (this._cmbSyakousei != null)
				{
					this._cmbSyakousei.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbTeisou
		{
			get
			{
				return this._cmbTeisou;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbTeisou != null)
				{
					this._cmbTeisou.SelectedIndexChanged -= eventHandler;
				}
				this._cmbTeisou = value;
				if (this._cmbTeisou != null)
				{
					this._cmbTeisou.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbThursday1
		{
			get
			{
				return this._cmbThursday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbThursday1 != null)
				{
					this._cmbThursday1.SelectedIndexChanged -= eventHandler;
				}
				this._cmbThursday1 = value;
				if (this._cmbThursday1 != null)
				{
					this._cmbThursday1.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbThursday2
		{
			get
			{
				return this._cmbThursday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbThursday2 != null)
				{
					this._cmbThursday2.SelectedIndexChanged -= eventHandler;
				}
				this._cmbThursday2 = value;
				if (this._cmbThursday2 != null)
				{
					this._cmbThursday2.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown cmbTikubi1
		{
			get
			{
				return this._cmbTikubi1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbTikubi1 != null)
				{
					this._cmbTikubi1.ValueChanged -= eventHandler;
				}
				this._cmbTikubi1 = value;
				if (this._cmbTikubi1 != null)
				{
					this._cmbTikubi1.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown cmbTikubi2
		{
			get
			{
				return this._cmbTikubi2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbTikubi2 != null)
				{
					this._cmbTikubi2.ValueChanged -= eventHandler;
				}
				this._cmbTikubi2 = value;
				if (this._cmbTikubi2 != null)
				{
					this._cmbTikubi2.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbTuesday1
		{
			get
			{
				return this._cmbTuesday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbTuesday1 != null)
				{
					this._cmbTuesday1.SelectedIndexChanged -= eventHandler;
				}
				this._cmbTuesday1 = value;
				if (this._cmbTuesday1 != null)
				{
					this._cmbTuesday1.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbTuesday2
		{
			get
			{
				return this._cmbTuesday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbTuesday2 != null)
				{
					this._cmbTuesday2.SelectedIndexChanged -= eventHandler;
				}
				this._cmbTuesday2 = value;
				if (this._cmbTuesday2 != null)
				{
					this._cmbTuesday2.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown cmbUnder1
		{
			get
			{
				return this._cmbUnder1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbUnder1 != null)
				{
					this._cmbUnder1.ValueChanged -= eventHandler;
				}
				this._cmbUnder1 = value;
				if (this._cmbUnder1 != null)
				{
					this._cmbUnder1.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbVoice
		{
			get
			{
				return this._cmbVoice;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbVoice != null)
				{
					this._cmbVoice.SelectedIndexChanged -= eventHandler;
				}
				this._cmbVoice = value;
				if (this._cmbVoice != null)
				{
					this._cmbVoice.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbWednesday1
		{
			get
			{
				return this._cmbWednesday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbWednesday1 != null)
				{
					this._cmbWednesday1.SelectedIndexChanged -= eventHandler;
				}
				this._cmbWednesday1 = value;
				if (this._cmbWednesday1 != null)
				{
					this._cmbWednesday1.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbWednesday2
		{
			get
			{
				return this._cmbWednesday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._cmbWednesday2 != null)
				{
					this._cmbWednesday2.SelectedIndexChanged -= eventHandler;
				}
				this._cmbWednesday2 = value;
				if (this._cmbWednesday2 != null)
				{
					this._cmbWednesday2.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual Label Label102
		{
			get
			{
				return this._Label102;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label102 = value;
			}
		}

		public virtual Label Label103
		{
			get
			{
				return this._Label103;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label103 = value;
			}
		}

		public virtual Label Label105
		{
			get
			{
				return this._Label105;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label105 = value;
			}
		}

		public virtual Label Label107
		{
			get
			{
				return this._Label107;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label107 = value;
			}
		}

		public virtual Label Label109
		{
			get
			{
				return this._Label109;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label109 = value;
			}
		}

		public virtual Label Label111
		{
			get
			{
				return this._Label111;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label111 = value;
			}
		}

		public virtual Label Label114
		{
			get
			{
				return this._Label114;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label114 = value;
			}
		}

		public virtual Label Label115
		{
			get
			{
				return this._Label115;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label115 = value;
			}
		}

		public virtual Label Label118
		{
			get
			{
				return this._Label118;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label118 = value;
			}
		}

		public virtual Label Label119
		{
			get
			{
				return this._Label119;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label119 = value;
			}
		}

		public virtual Label Label122
		{
			get
			{
				return this._Label122;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label122 = value;
			}
		}

		public virtual Label Label123
		{
			get
			{
				return this._Label123;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label123 = value;
			}
		}

		public virtual Label Label126
		{
			get
			{
				return this._Label126;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label126 = value;
			}
		}

		public virtual Label Label127
		{
			get
			{
				return this._Label127;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label127 = value;
			}
		}

		public virtual Label Label129
		{
			get
			{
				return this._Label129;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label129 = value;
			}
		}

		public virtual Label Label131
		{
			get
			{
				return this._Label131;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label131 = value;
			}
		}

		public virtual Label Label26
		{
			get
			{
				return this._Label26;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label26 = value;
			}
		}

		public virtual Label Label301
		{
			get
			{
				return this._Label301;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label301 = value;
			}
		}

		public virtual Label Label501
		{
			get
			{
				return this._Label501;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label501 = value;
			}
		}

		public virtual Label Label511
		{
			get
			{
				return this._Label511;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label511 = value;
			}
		}

		public virtual Label Label701
		{
			get
			{
				return this._Label701;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label701 = value;
			}
		}

		public virtual Label Label83
		{
			get
			{
				return this._Label83;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label83 = value;
			}
		}

		public virtual Label Label84
		{
			get
			{
				return this._Label84;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label84 = value;
			}
		}

		public virtual Label Label85
		{
			get
			{
				return this._Label85;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label85 = value;
			}
		}

		public virtual Label Label87
		{
			get
			{
				return this._Label87;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label87 = value;
			}
		}

		public virtual Label Label89
		{
			get
			{
				return this._Label89;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label89 = value;
			}
		}

		public virtual Label Label90
		{
			get
			{
				return this._Label90;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label90 = value;
			}
		}

		public virtual Label Label91
		{
			get
			{
				return this._Label91;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label91 = value;
			}
		}

		public virtual Label Label92
		{
			get
			{
				return this._Label92;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label92 = value;
			}
		}

		public virtual Label Label94
		{
			get
			{
				return this._Label94;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label94 = value;
			}
		}

		public virtual Label Label95
		{
			get
			{
				return this._Label95;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label95 = value;
			}
		}

		public virtual Label Label98
		{
			get
			{
				return this._Label98;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label98 = value;
			}
		}

		public virtual Label Label99
		{
			get
			{
				return this._Label99;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label99 = value;
			}
		}

		public virtual Label lblAction
		{
			get
			{
				return this._lblAction;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblAction = value;
			}
		}

		public virtual Label lblActionRev
		{
			get
			{
				return this._lblActionRev;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblActionRev = value;
			}
		}

		public virtual Label lblAite1
		{
			get
			{
				return this._lblAite1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblAite1 = value;
			}
		}

		public virtual Label lblAite2
		{
			get
			{
				return this._lblAite2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblAite2 = value;
			}
		}

		public virtual Label lblAite3
		{
			get
			{
				return this._lblAite3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblAite3 = value;
			}
		}

		public virtual Label lblAtama1
		{
			get
			{
				return this._lblAtama1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblAtama1 = value;
			}
		}

		public virtual Label lblAtama2
		{
			get
			{
				return this._lblAtama2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblAtama2 = value;
			}
		}

		public virtual Label lblBukatu1
		{
			get
			{
				return this._lblBukatu1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblBukatu1 = value;
			}
		}

		public virtual Label lblCntCondom
		{
			get
			{
				return this._lblCntCondom;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntCondom = value;
			}
		}

		public virtual Label lblCntFurareta
		{
			get
			{
				return this._lblCntFurareta;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntFurareta = value;
			}
		}

		public virtual Label lblCntKenka
		{
			get
			{
				return this._lblCntKenka;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntKenka = value;
			}
		}

		public virtual Label lblCntKousai
		{
			get
			{
				return this._lblCntKousai;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntKousai = value;
			}
		}

		public virtual Label lblCntNakaKiken
		{
			get
			{
				return this._lblCntNakaKiken;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntNakaKiken = value;
			}
		}

		public virtual Label lblCntOrgasm
		{
			get
			{
				return this._lblCntOrgasm;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntOrgasm = value;
			}
		}

		public virtual Label lblCntSabori
		{
			get
			{
				return this._lblCntSabori;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntSabori = value;
			}
		}

		public virtual Label lblCntSeikou1
		{
			get
			{
				return this._lblCntSeikou1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntSeikou1 = value;
			}
		}

		public virtual Label lblCntSeikou2
		{
			get
			{
				return this._lblCntSeikou2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntSeikou2 = value;
			}
		}

		public virtual Label lblCntSeikou3
		{
			get
			{
				return this._lblCntSeikou3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntSeikou3 = value;
			}
		}

		public virtual Label lblCntSeikouAite
		{
			get
			{
				return this._lblCntSeikouAite;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntSeikouAite = value;
			}
		}

		public virtual Label lblCntSoudatu
		{
			get
			{
				return this._lblCntSoudatu;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntSoudatu = value;
			}
		}

		public virtual Label lblCntSynchronize
		{
			get
			{
				return this._lblCntSynchronize;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntSynchronize = value;
			}
		}

		public virtual Label lblCntTodaySeikou
		{
			get
			{
				return this._lblCntTodaySeikou;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblCntTodaySeikou = value;
			}
		}

		public virtual Label lblColorEye1
		{
			get
			{
				return this._lblColorEye1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblColorEye1 = value;
			}
		}

		public virtual Label lblColorEye2
		{
			get
			{
				return this._lblColorEye2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblColorEye2 = value;
			}
		}

		public virtual Label lblColorGlasses
		{
			get
			{
				return this._lblColorGlasses;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblColorGlasses = value;
			}
		}

		public virtual Label lblColorHair
		{
			get
			{
				return this._lblColorHair;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblColorHair = value;
			}
		}

		public virtual Label lblColorMayu
		{
			get
			{
				return this._lblColorMayu;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblColorMayu = value;
			}
		}

		public virtual Label lblColorSkin2
		{
			get
			{
				return this._lblColorSkin2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblColorSkin2 = value;
			}
		}

		public virtual Label lblColorUnder
		{
			get
			{
				return this._lblColorUnder;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblColorUnder = value;
			}
		}

		public virtual Label lblEye1
		{
			get
			{
				return this._lblEye1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblEye1 = value;
			}
		}

		public virtual Label lblEye2
		{
			get
			{
				return this._lblEye2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblEye2 = value;
			}
		}

		public virtual Label lblEye3
		{
			get
			{
				return this._lblEye3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblEye3 = value;
			}
		}

		public virtual Label lblEye4
		{
			get
			{
				return this._lblEye4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblEye4 = value;
			}
		}

		public virtual Label lblEye5
		{
			get
			{
				return this._lblEye5;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblEye5 = value;
			}
		}

		public virtual Label lblFriday1
		{
			get
			{
				return this._lblFriday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblFriday1 = value;
			}
		}

		public virtual Label lblFriday2
		{
			get
			{
				return this._lblFriday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblFriday2 = value;
			}
		}

		public virtual Label lblGlasses2
		{
			get
			{
				return this._lblGlasses2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblGlasses2 = value;
			}
		}

		public virtual Label lblGoods1
		{
			get
			{
				return this._lblGoods1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblGoods1 = value;
			}
		}

		public virtual Label lblGoods2
		{
			get
			{
				return this._lblGoods2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblGoods2 = value;
			}
		}

		public virtual Label lblGoods3
		{
			get
			{
				return this._lblGoods3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblGoods3 = value;
			}
		}

		public virtual Label lblHair1
		{
			get
			{
				return this._lblHair1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHair1 = value;
			}
		}

		public virtual Label lblHair1Name
		{
			get
			{
				return this._lblHair1Name;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHair1Name = value;
			}
		}

		public virtual Label lblHair2
		{
			get
			{
				return this._lblHair2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHair2 = value;
			}
		}

		public virtual Label lblHair2Name
		{
			get
			{
				return this._lblHair2Name;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHair2Name = value;
			}
		}

		public virtual Label lblHair3
		{
			get
			{
				return this._lblHair3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHair3 = value;
			}
		}

		public virtual Label lblHair3Name
		{
			get
			{
				return this._lblHair3Name;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHair3Name = value;
			}
		}

		public virtual Label lblHair4
		{
			get
			{
				return this._lblHair4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHair4 = value;
			}
		}

		public virtual Label lblHair4Name
		{
			get
			{
				return this._lblHair4Name;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHair4Name = value;
			}
		}

		public virtual Label lblHAisyou
		{
			get
			{
				return this._lblHAisyou;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHAisyou = value;
			}
		}

		public virtual Label lblHeight
		{
			get
			{
				return this._lblHeight;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHeight = value;
			}
		}

		public virtual Label lblHighlight1
		{
			get
			{
				return this._lblHighlight1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHighlight1 = value;
			}
		}

		public virtual Label lblHiyake1
		{
			get
			{
				return this._lblHiyake1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHiyake1 = value;
			}
		}

		public virtual Label lblHiyake2
		{
			get
			{
				return this._lblHiyake2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHiyake2 = value;
			}
		}

		public virtual Label lblHokuro
		{
			get
			{
				return this._lblHokuro;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblHokuro = value;
			}
		}

		public virtual Label lblKao
		{
			get
			{
				return this._lblKao;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblKao = value;
			}
		}

		public virtual Label lblKenka
		{
			get
			{
				return this._lblKenka;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblKenka = value;
			}
		}

		public virtual Label lblLip1
		{
			get
			{
				return this._lblLip1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblLip1 = value;
			}
		}

		public virtual Label lblLip2
		{
			get
			{
				return this._lblLip2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblLip2 = value;
			}
		}

		public virtual Label lblMabuta
		{
			get
			{
				return this._lblMabuta;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMabuta = value;
			}
		}

		public virtual Label lblMatuge1
		{
			get
			{
				return this._lblMatuge1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMatuge1 = value;
			}
		}

		public virtual Label lblMatuge2
		{
			get
			{
				return this._lblMatuge2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMatuge2 = value;
			}
		}

		public virtual Label lblMayu1
		{
			get
			{
				return this._lblMayu1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMayu1 = value;
			}
		}

		public virtual Label lblMayu2
		{
			get
			{
				return this._lblMayu2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMayu2 = value;
			}
		}

		public virtual Label lblMonday1
		{
			get
			{
				return this._lblMonday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMonday1 = value;
			}
		}

		public virtual Label lblMonday2
		{
			get
			{
				return this._lblMonday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMonday2 = value;
			}
		}

		public virtual Label lblMosaic
		{
			get
			{
				return this._lblMosaic;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMosaic = value;
			}
		}

		public virtual Label lblMune1
		{
			get
			{
				return this._lblMune1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMune1 = value;
			}
		}

		public virtual Label lblMune1Name
		{
			get
			{
				return this._lblMune1Name;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMune1Name = value;
			}
		}

		public virtual Label lblMune2
		{
			get
			{
				return this._lblMune2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMune2 = value;
			}
		}

		public virtual Label lblMune3
		{
			get
			{
				return this._lblMune3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMune3 = value;
			}
		}

		public virtual Label lblMune4
		{
			get
			{
				return this._lblMune4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMune4 = value;
			}
		}

		public virtual Label lblMune5
		{
			get
			{
				return this._lblMune5;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMune5 = value;
			}
		}

		public virtual Label lblMune6
		{
			get
			{
				return this._lblMune6;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMune6 = value;
			}
		}

		public virtual Label lblMune7
		{
			get
			{
				return this._lblMune7;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMune7 = value;
			}
		}

		public virtual Label lblMune8
		{
			get
			{
				return this._lblMune8;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblMune8 = value;
			}
		}

		public virtual Label lblNyurin
		{
			get
			{
				return this._lblNyurin;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblNyurin = value;
			}
		}

		public virtual Label lblPair
		{
			get
			{
				return this._lblPair;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblPair = value;
			}
		}

		public virtual Label lblPersonal
		{
			get
			{
				return this._lblPersonal;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblPersonal = value;
			}
		}

		public virtual Label lblPupil1
		{
			get
			{
				return this._lblPupil1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblPupil1 = value;
			}
		}

		public virtual Label lblPupil2
		{
			get
			{
				return this._lblPupil2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblPupil2 = value;
			}
		}

		public virtual Label lblPupil3
		{
			get
			{
				return this._lblPupil3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblPupil3 = value;
			}
		}

		public virtual Label lblPupil4
		{
			get
			{
				return this._lblPupil4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblPupil4 = value;
			}
		}

		public virtual Label lblPupil5
		{
			get
			{
				return this._lblPupil5;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblPupil5 = value;
			}
		}

		public virtual Label lblRenzoku
		{
			get
			{
				return this._lblRenzoku;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblRenzoku = value;
			}
		}

		public virtual Label lblSaturday1
		{
			get
			{
				return this._lblSaturday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSaturday1 = value;
			}
		}

		public virtual Label lblSaturday2
		{
			get
			{
				return this._lblSaturday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSaturday2 = value;
			}
		}

		public virtual Label lblSeiai
		{
			get
			{
				return this._lblSeiai;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSeiai = value;
			}
		}

		public virtual Label lblSeikaku
		{
			get
			{
				return this._lblSeikaku;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSeikaku = value;
			}
		}

		public virtual Label lblSeikakuName
		{
			get
			{
				return this._lblSeikakuName;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSeikakuName = value;
			}
		}

		public virtual Label lblStyle
		{
			get
			{
				return this._lblStyle;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblStyle = value;
			}
		}

		public virtual Label lblSunday1
		{
			get
			{
				return this._lblSunday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSunday1 = value;
			}
		}

		public virtual Label lblSunday2
		{
			get
			{
				return this._lblSunday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSunday2 = value;
			}
		}

		public virtual Label lblSyakousei
		{
			get
			{
				return this._lblSyakousei;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSyakousei = value;
			}
		}

		public virtual Label lblSyasei1
		{
			get
			{
				return this._lblSyasei1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSyasei1 = value;
			}
		}

		public virtual Label lblSyasei2
		{
			get
			{
				return this._lblSyasei2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSyasei2 = value;
			}
		}

		public virtual Label lblSyasei3
		{
			get
			{
				return this._lblSyasei3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSyasei3 = value;
			}
		}

		public virtual Label lblSyasei4
		{
			get
			{
				return this._lblSyasei4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSyasei4 = value;
			}
		}

		public virtual Label lblTairyoku1
		{
			get
			{
				return this._lblTairyoku1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTairyoku1 = value;
			}
		}

		public virtual Label lblTairyoku1Name
		{
			get
			{
				return this._lblTairyoku1Name;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTairyoku1Name = value;
			}
		}

		public virtual Label lblTeisou
		{
			get
			{
				return this._lblTeisou;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTeisou = value;
			}
		}

		public virtual Label lblTest1
		{
			get
			{
				return this._lblTest1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTest1 = value;
			}
		}

		public virtual Label lblTest2
		{
			get
			{
				return this._lblTest2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTest2 = value;
			}
		}

		public virtual Label lblTest3
		{
			get
			{
				return this._lblTest3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTest3 = value;
			}
		}

		public virtual Label lblThumbnail
		{
			get
			{
				return this._lblThumbnail;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblThumbnail = value;
			}
		}

		public virtual Label lblThumbnailPlayer
		{
			get
			{
				return this._lblThumbnailPlayer;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblThumbnailPlayer = value;
			}
		}

		public virtual Label lblThursday1
		{
			get
			{
				return this._lblThursday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblThursday1 = value;
			}
		}

		public virtual Label lblThursday2
		{
			get
			{
				return this._lblThursday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblThursday2 = value;
			}
		}

		public virtual Label lblTikubi1
		{
			get
			{
				return this._lblTikubi1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTikubi1 = value;
			}
		}

		public virtual Label lblTikubi2
		{
			get
			{
				return this._lblTikubi2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTikubi2 = value;
			}
		}

		public virtual Label lblTikubi3
		{
			get
			{
				return this._lblTikubi3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTikubi3 = value;
			}
		}

		public virtual Label lblTiryoku1
		{
			get
			{
				return this._lblTiryoku1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTiryoku1 = value;
			}
		}

		public virtual Label lblTiryoku1Name
		{
			get
			{
				return this._lblTiryoku1Name;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTiryoku1Name = value;
			}
		}

		public virtual Label lblTuesday1
		{
			get
			{
				return this._lblTuesday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTuesday1 = value;
			}
		}

		public virtual Label lblTuesday2
		{
			get
			{
				return this._lblTuesday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblTuesday2 = value;
			}
		}

		public virtual Label lblUnder1
		{
			get
			{
				return this._lblUnder1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblUnder1 = value;
			}
		}

		public virtual Label lblUnder2
		{
			get
			{
				return this._lblUnder2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblUnder2 = value;
			}
		}

		public virtual Label lblVoice
		{
			get
			{
				return this._lblVoice;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblVoice = value;
			}
		}

		public virtual Label lblWaist
		{
			get
			{
				return this._lblWaist;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblWaist = value;
			}
		}

		public virtual Label lblWednesday1
		{
			get
			{
				return this._lblWednesday1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblWednesday1 = value;
			}
		}

		public virtual Label lblWednesday2
		{
			get
			{
				return this._lblWednesday2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblWednesday2 = value;
			}
		}

		public virtual System.Windows.Forms.MenuStrip MenuStrip
		{
			get
			{
				return this._MenuStrip;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._MenuStrip = value;
			}
		}

		public virtual ToolStripMenuItem mnuBatch
		{
			get
			{
				return this._mnuBatch;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuBatch_Click);
				if (this._mnuBatch != null)
				{
					this._mnuBatch.Click -= eventHandler;
				}
				this._mnuBatch = value;
				if (this._mnuBatch != null)
				{
					this._mnuBatch.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripSeparator mnuBatchSeparator
		{
			get
			{
				return this._mnuBatchSeparator;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuBatchSeparator = value;
			}
		}

		public virtual ToolStripMenuItem mnuClearAll
		{
			get
			{
				return this._mnuClearAll;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuClearAll_Click);
				if (this._mnuClearAll != null)
				{
					this._mnuClearAll.Click -= eventHandler;
				}
				this._mnuClearAll = value;
				if (this._mnuClearAll != null)
				{
					this._mnuClearAll.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuClose
		{
			get
			{
				return this._mnuClose;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuClose_Click);
				if (this._mnuClose != null)
				{
					this._mnuClose.Click -= eventHandler;
				}
				this._mnuClose = value;
				if (this._mnuClose != null)
				{
					this._mnuClose.Click += eventHandler;
				}
			}
		}

		public virtual JG2ToolStripMenuItem mnuEdit
		{
			get
			{
				return this._mnuEdit;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuEdit = value;
			}
		}

		public virtual ToolStripMenuItem mnuFile
		{
			get
			{
				return this._mnuFile;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuFile = value;
			}
		}

		public virtual ToolStripMenuItem mnuOpen
		{
			get
			{
				return this._mnuOpen;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuOpen_Click);
				if (this._mnuOpen != null)
				{
					this._mnuOpen.Click -= eventHandler;
				}
				this._mnuOpen = value;
				if (this._mnuOpen != null)
				{
					this._mnuOpen.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuSelectAll
		{
			get
			{
				return this._mnuSelectAll;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuSelectAll_Click);
				frmDetail _frmDetail1 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail1.mnuSelectAll_Click);
				if (this._mnuSelectAll != null)
				{
					this._mnuSelectAll.Click -= eventHandler;
					this._mnuSelectAll.Click -= eventHandler1;
				}
				this._mnuSelectAll = value;
				if (this._mnuSelectAll != null)
				{
					this._mnuSelectAll.Click += eventHandler;
					this._mnuSelectAll.Click += eventHandler1;
				}
			}
		}

		public virtual ToolStripMenuItem mnuSelectCondition
		{
			get
			{
				return this._mnuSelectCondition;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuSelectCondition = value;
			}
		}

		public virtual ToolStripMenuItem mnuSubmit
		{
			get
			{
				return this._mnuSubmit;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuSubmit_Click);
				if (this._mnuSubmit != null)
				{
					this._mnuSubmit.Click -= eventHandler;
				}
				this._mnuSubmit = value;
				if (this._mnuSubmit != null)
				{
					this._mnuSubmit.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTab
		{
			get
			{
				return this._mnuTab;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuTab = value;
			}
		}

		public virtual ToolStripMenuItem mnuTabBody
		{
			get
			{
				return this._mnuTabBody;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTab_Click);
				if (this._mnuTabBody != null)
				{
					this._mnuTabBody.Click -= eventHandler;
				}
				this._mnuTabBody = value;
				if (this._mnuTabBody != null)
				{
					this._mnuTabBody.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabCostume
		{
			get
			{
				return this._mnuTabCostume;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTab_Click);
				if (this._mnuTabCostume != null)
				{
					this._mnuTabCostume.Click -= eventHandler;
				}
				this._mnuTabCostume = value;
				if (this._mnuTabCostume != null)
				{
					this._mnuTabCostume.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabFace
		{
			get
			{
				return this._mnuTabFace;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTab_Click);
				if (this._mnuTabFace != null)
				{
					this._mnuTabFace.Click -= eventHandler;
				}
				this._mnuTabFace = value;
				if (this._mnuTabFace != null)
				{
					this._mnuTabFace.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabHair
		{
			get
			{
				return this._mnuTabHair;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTab_Click);
				if (this._mnuTabHair != null)
				{
					this._mnuTabHair.Click -= eventHandler;
				}
				this._mnuTabHair = value;
				if (this._mnuTabHair != null)
				{
					this._mnuTabHair.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabJinkaku
		{
			get
			{
				return this._mnuTabJinkaku;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTab_Click);
				if (this._mnuTabJinkaku != null)
				{
					this._mnuTabJinkaku.Click -= eventHandler;
				}
				this._mnuTabJinkaku = value;
				if (this._mnuTabJinkaku != null)
				{
					this._mnuTabJinkaku.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabKosei
		{
			get
			{
				return this._mnuTabKosei;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTab_Click);
				if (this._mnuTabKosei != null)
				{
					this._mnuTabKosei.Click -= eventHandler;
				}
				this._mnuTabKosei = value;
				if (this._mnuTabKosei != null)
				{
					this._mnuTabKosei.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabNinshin
		{
			get
			{
				return this._mnuTabNinshin;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTab_Click);
				if (this._mnuTabNinshin != null)
				{
					this._mnuTabNinshin.Click -= eventHandler;
				}
				this._mnuTabNinshin = value;
				if (this._mnuTabNinshin != null)
				{
					this._mnuTabNinshin.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabTools
		{
			get
			{
				return this._mnuTabTools;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTab_Click);
				if (this._mnuTabTools != null)
				{
					this._mnuTabTools.Click -= eventHandler;
				}
				this._mnuTabTools = value;
				if (this._mnuTabTools != null)
				{
					this._mnuTabTools.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsAction
		{
			get
			{
				return this._mnuTabToolsAction;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuTabToolsAction = value;
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsAction1
		{
			get
			{
				return this._mnuTabToolsAction1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._mnuTabToolsAction1 != null)
				{
					this._mnuTabToolsAction1.Click -= eventHandler;
				}
				this._mnuTabToolsAction1 = value;
				if (this._mnuTabToolsAction1 != null)
				{
					this._mnuTabToolsAction1.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsAction2
		{
			get
			{
				return this._mnuTabToolsAction2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._mnuTabToolsAction2 != null)
				{
					this._mnuTabToolsAction2.Click -= eventHandler;
				}
				this._mnuTabToolsAction2 = value;
				if (this._mnuTabToolsAction2 != null)
				{
					this._mnuTabToolsAction2.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsAction3
		{
			get
			{
				return this._mnuTabToolsAction3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._mnuTabToolsAction3 != null)
				{
					this._mnuTabToolsAction3.Click -= eventHandler;
				}
				this._mnuTabToolsAction3 = value;
				if (this._mnuTabToolsAction3 != null)
				{
					this._mnuTabToolsAction3.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsAction4
		{
			get
			{
				return this._mnuTabToolsAction4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._mnuTabToolsAction4 != null)
				{
					this._mnuTabToolsAction4.Click -= eventHandler;
				}
				this._mnuTabToolsAction4 = value;
				if (this._mnuTabToolsAction4 != null)
				{
					this._mnuTabToolsAction4.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsActionRev
		{
			get
			{
				return this._mnuTabToolsActionRev;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuTabToolsActionRev = value;
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsActionRev1
		{
			get
			{
				return this._mnuTabToolsActionRev1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._mnuTabToolsActionRev1 != null)
				{
					this._mnuTabToolsActionRev1.Click -= eventHandler;
				}
				this._mnuTabToolsActionRev1 = value;
				if (this._mnuTabToolsActionRev1 != null)
				{
					this._mnuTabToolsActionRev1.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsActionRev2
		{
			get
			{
				return this._mnuTabToolsActionRev2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._mnuTabToolsActionRev2 != null)
				{
					this._mnuTabToolsActionRev2.Click -= eventHandler;
				}
				this._mnuTabToolsActionRev2 = value;
				if (this._mnuTabToolsActionRev2 != null)
				{
					this._mnuTabToolsActionRev2.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsActionRev3
		{
			get
			{
				return this._mnuTabToolsActionRev3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._mnuTabToolsActionRev3 != null)
				{
					this._mnuTabToolsActionRev3.Click -= eventHandler;
				}
				this._mnuTabToolsActionRev3 = value;
				if (this._mnuTabToolsActionRev3 != null)
				{
					this._mnuTabToolsActionRev3.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsActionRev4
		{
			get
			{
				return this._mnuTabToolsActionRev4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAction_Click);
				if (this._mnuTabToolsActionRev4 != null)
				{
					this._mnuTabToolsActionRev4.Click -= eventHandler;
				}
				this._mnuTabToolsActionRev4 = value;
				if (this._mnuTabToolsActionRev4 != null)
				{
					this._mnuTabToolsActionRev4.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsAfterPill
		{
			get
			{
				return this._mnuTabToolsAfterPill;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsAfterPill_Click);
				if (this._mnuTabToolsAfterPill != null)
				{
					this._mnuTabToolsAfterPill.Click -= eventHandler;
				}
				this._mnuTabToolsAfterPill = value;
				if (this._mnuTabToolsAfterPill != null)
				{
					this._mnuTabToolsAfterPill.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsClearAction
		{
			get
			{
				return this._mnuTabToolsClearAction;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsClearAction_Click);
				if (this._mnuTabToolsClearAction != null)
				{
					this._mnuTabToolsClearAction.Click -= eventHandler;
				}
				this._mnuTabToolsClearAction = value;
				if (this._mnuTabToolsClearAction != null)
				{
					this._mnuTabToolsClearAction.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsDelAction1
		{
			get
			{
				return this._mnuTabToolsDelAction1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._mnuTabToolsDelAction1 != null)
				{
					this._mnuTabToolsDelAction1.Click -= eventHandler;
				}
				this._mnuTabToolsDelAction1 = value;
				if (this._mnuTabToolsDelAction1 != null)
				{
					this._mnuTabToolsDelAction1.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsDelAction2
		{
			get
			{
				return this._mnuTabToolsDelAction2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._mnuTabToolsDelAction2 != null)
				{
					this._mnuTabToolsDelAction2.Click -= eventHandler;
				}
				this._mnuTabToolsDelAction2 = value;
				if (this._mnuTabToolsDelAction2 != null)
				{
					this._mnuTabToolsDelAction2.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsDelAction3
		{
			get
			{
				return this._mnuTabToolsDelAction3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._mnuTabToolsDelAction3 != null)
				{
					this._mnuTabToolsDelAction3.Click -= eventHandler;
				}
				this._mnuTabToolsDelAction3 = value;
				if (this._mnuTabToolsDelAction3 != null)
				{
					this._mnuTabToolsDelAction3.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsDelAction4
		{
			get
			{
				return this._mnuTabToolsDelAction4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._mnuTabToolsDelAction4 != null)
				{
					this._mnuTabToolsDelAction4.Click -= eventHandler;
				}
				this._mnuTabToolsDelAction4 = value;
				if (this._mnuTabToolsDelAction4 != null)
				{
					this._mnuTabToolsDelAction4.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsDelActionRev1
		{
			get
			{
				return this._mnuTabToolsDelActionRev1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._mnuTabToolsDelActionRev1 != null)
				{
					this._mnuTabToolsDelActionRev1.Click -= eventHandler;
				}
				this._mnuTabToolsDelActionRev1 = value;
				if (this._mnuTabToolsDelActionRev1 != null)
				{
					this._mnuTabToolsDelActionRev1.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsDelActionRev2
		{
			get
			{
				return this._mnuTabToolsDelActionRev2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._mnuTabToolsDelActionRev2 != null)
				{
					this._mnuTabToolsDelActionRev2.Click -= eventHandler;
				}
				this._mnuTabToolsDelActionRev2 = value;
				if (this._mnuTabToolsDelActionRev2 != null)
				{
					this._mnuTabToolsDelActionRev2.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsDelActionRev3
		{
			get
			{
				return this._mnuTabToolsDelActionRev3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._mnuTabToolsDelActionRev3 != null)
				{
					this._mnuTabToolsDelActionRev3.Click -= eventHandler;
				}
				this._mnuTabToolsDelActionRev3 = value;
				if (this._mnuTabToolsDelActionRev3 != null)
				{
					this._mnuTabToolsDelActionRev3.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsDelActionRev4
		{
			get
			{
				return this._mnuTabToolsDelActionRev4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsDelAction_Click);
				if (this._mnuTabToolsDelActionRev4 != null)
				{
					this._mnuTabToolsDelActionRev4.Click -= eventHandler;
				}
				this._mnuTabToolsDelActionRev4 = value;
				if (this._mnuTabToolsDelActionRev4 != null)
				{
					this._mnuTabToolsDelActionRev4.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsExport
		{
			get
			{
				return this._mnuTabToolsExport;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsExport_Click);
				if (this._mnuTabToolsExport != null)
				{
					this._mnuTabToolsExport.Click -= eventHandler;
				}
				this._mnuTabToolsExport = value;
				if (this._mnuTabToolsExport != null)
				{
					this._mnuTabToolsExport.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsGohoubi
		{
			get
			{
				return this._mnuTabToolsGohoubi;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsGohoubi_Click);
				if (this._mnuTabToolsGohoubi != null)
				{
					this._mnuTabToolsGohoubi.Click -= eventHandler;
				}
				this._mnuTabToolsGohoubi = value;
				if (this._mnuTabToolsGohoubi != null)
				{
					this._mnuTabToolsGohoubi.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsHMax
		{
			get
			{
				return this._mnuTabToolsHMax;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsHMax_Click);
				if (this._mnuTabToolsHMax != null)
				{
					this._mnuTabToolsHMax.Click -= eventHandler;
				}
				this._mnuTabToolsHMax = value;
				if (this._mnuTabToolsHMax != null)
				{
					this._mnuTabToolsHMax.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsImport
		{
			get
			{
				return this._mnuTabToolsImport;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsImport_Click);
				if (this._mnuTabToolsImport != null)
				{
					this._mnuTabToolsImport.Click -= eventHandler;
				}
				this._mnuTabToolsImport = value;
				if (this._mnuTabToolsImport != null)
				{
					this._mnuTabToolsImport.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuTabToolsPlayer
		{
			get
			{
				return this._mnuTabToolsPlayer;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.mnuTabToolsPlayer_Click);
				if (this._mnuTabToolsPlayer != null)
				{
					this._mnuTabToolsPlayer.Click -= eventHandler;
				}
				this._mnuTabToolsPlayer = value;
				if (this._mnuTabToolsPlayer != null)
				{
					this._mnuTabToolsPlayer.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripSeparator mnuTabToolsSeparator
		{
			get
			{
				return this._mnuTabToolsSeparator;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuTabToolsSeparator = value;
			}
		}

		public virtual JG2NumericUpDown numAtama1
		{
			get
			{
				return this._numAtama1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numAtama1 != null)
				{
					this._numAtama1.ValueChanged -= eventHandler;
				}
				this._numAtama1 = value;
				if (this._numAtama1 != null)
				{
					this._numAtama1.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numAtama2
		{
			get
			{
				return this._numAtama2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numAtama2 != null)
				{
					this._numAtama2.ValueChanged -= eventHandler;
				}
				this._numAtama2 = value;
				if (this._numAtama2 != null)
				{
					this._numAtama2.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numBukatu3
		{
			get
			{
				return this._numBukatu3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numBukatu3 != null)
				{
					this._numBukatu3.ValueChanged -= eventHandler;
				}
				this._numBukatu3 = value;
				if (this._numBukatu3 != null)
				{
					this._numBukatu3.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numEye1
		{
			get
			{
				return this._numEye1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numEye1 != null)
				{
					this._numEye1.ValueChanged -= eventHandler;
				}
				this._numEye1 = value;
				if (this._numEye1 != null)
				{
					this._numEye1.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numEye2
		{
			get
			{
				return this._numEye2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numEye2 != null)
				{
					this._numEye2.ValueChanged -= eventHandler;
				}
				this._numEye2 = value;
				if (this._numEye2 != null)
				{
					this._numEye2.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numEye3
		{
			get
			{
				return this._numEye3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numEye3 != null)
				{
					this._numEye3.ValueChanged -= eventHandler;
				}
				this._numEye3 = value;
				if (this._numEye3 != null)
				{
					this._numEye3.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numEye4
		{
			get
			{
				return this._numEye4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numEye4 != null)
				{
					this._numEye4.ValueChanged -= eventHandler;
				}
				this._numEye4 = value;
				if (this._numEye4 != null)
				{
					this._numEye4.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numEye5
		{
			get
			{
				return this._numEye5;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numEye5 != null)
				{
					this._numEye5.ValueChanged -= eventHandler;
				}
				this._numEye5 = value;
				if (this._numEye5 != null)
				{
					this._numEye5.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numHair1
		{
			get
			{
				return this._numHair1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				frmDetail _frmDetail1 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail1.num_ValueChanged);
				if (this._numHair1 != null)
				{
					this._numHair1.ValueChanged -= eventHandler;
					this._numHair1.ValueChanged -= eventHandler1;
				}
				this._numHair1 = value;
				if (this._numHair1 != null)
				{
					this._numHair1.ValueChanged += eventHandler;
					this._numHair1.ValueChanged += eventHandler1;
				}
			}
		}

		public virtual JG2NumericUpDown numHair2
		{
			get
			{
				return this._numHair2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				frmDetail _frmDetail1 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail1.num_ValueChanged);
				if (this._numHair2 != null)
				{
					this._numHair2.ValueChanged -= eventHandler;
					this._numHair2.ValueChanged -= eventHandler1;
				}
				this._numHair2 = value;
				if (this._numHair2 != null)
				{
					this._numHair2.ValueChanged += eventHandler;
					this._numHair2.ValueChanged += eventHandler1;
				}
			}
		}

		public virtual JG2NumericUpDown numHair3
		{
			get
			{
				return this._numHair3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				frmDetail _frmDetail1 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail1.num_ValueChanged);
				if (this._numHair3 != null)
				{
					this._numHair3.ValueChanged -= eventHandler;
					this._numHair3.ValueChanged -= eventHandler1;
				}
				this._numHair3 = value;
				if (this._numHair3 != null)
				{
					this._numHair3.ValueChanged += eventHandler;
					this._numHair3.ValueChanged += eventHandler1;
				}
			}
		}

		public virtual JG2NumericUpDown numHair4
		{
			get
			{
				return this._numHair4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				frmDetail _frmDetail1 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail1.num_ValueChanged);
				if (this._numHair4 != null)
				{
					this._numHair4.ValueChanged -= eventHandler;
					this._numHair4.ValueChanged -= eventHandler1;
				}
				this._numHair4 = value;
				if (this._numHair4 != null)
				{
					this._numHair4.ValueChanged += eventHandler;
					this._numHair4.ValueChanged += eventHandler1;
				}
			}
		}

		public virtual JG2NumericUpDown numHairAdj1
		{
			get
			{
				return this._numHairAdj1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numHairAdj1 != null)
				{
					this._numHairAdj1.ValueChanged -= eventHandler;
				}
				this._numHairAdj1 = value;
				if (this._numHairAdj1 != null)
				{
					this._numHairAdj1.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numHairAdj2
		{
			get
			{
				return this._numHairAdj2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numHairAdj2 != null)
				{
					this._numHairAdj2.ValueChanged -= eventHandler;
				}
				this._numHairAdj2 = value;
				if (this._numHairAdj2 != null)
				{
					this._numHairAdj2.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numHairAdj3
		{
			get
			{
				return this._numHairAdj3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numHairAdj3 != null)
				{
					this._numHairAdj3.ValueChanged -= eventHandler;
				}
				this._numHairAdj3 = value;
				if (this._numHairAdj3 != null)
				{
					this._numHairAdj3.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numHairAdj4
		{
			get
			{
				return this._numHairAdj4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numHairAdj4 != null)
				{
					this._numHairAdj4.ValueChanged -= eventHandler;
				}
				this._numHairAdj4 = value;
				if (this._numHairAdj4 != null)
				{
					this._numHairAdj4.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numHiyake2
		{
			get
			{
				return this._numHiyake2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numHiyake2 != null)
				{
					this._numHiyake2.ValueChanged -= eventHandler;
				}
				this._numHiyake2 = value;
				if (this._numHiyake2 != null)
				{
					this._numHiyake2.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numLip2
		{
			get
			{
				return this._numLip2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numLip2 != null)
				{
					this._numLip2.ValueChanged -= eventHandler;
				}
				this._numLip2 = value;
				if (this._numLip2 != null)
				{
					this._numLip2.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numMayu2
		{
			get
			{
				return this._numMayu2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numMayu2 != null)
				{
					this._numMayu2.ValueChanged -= eventHandler;
				}
				this._numMayu2 = value;
				if (this._numMayu2 != null)
				{
					this._numMayu2.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numMune1
		{
			get
			{
				return this._numMune1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				frmDetail _frmDetail1 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail1.num_ValueChanged);
				if (this._numMune1 != null)
				{
					this._numMune1.ValueChanged -= eventHandler;
					this._numMune1.ValueChanged -= eventHandler1;
				}
				this._numMune1 = value;
				if (this._numMune1 != null)
				{
					this._numMune1.ValueChanged += eventHandler;
					this._numMune1.ValueChanged += eventHandler1;
				}
			}
		}

		public virtual JG2NumericUpDown numMune3
		{
			get
			{
				return this._numMune3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numMune3 != null)
				{
					this._numMune3.ValueChanged -= eventHandler;
				}
				this._numMune3 = value;
				if (this._numMune3 != null)
				{
					this._numMune3.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numMune4
		{
			get
			{
				return this._numMune4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numMune4 != null)
				{
					this._numMune4.ValueChanged -= eventHandler;
				}
				this._numMune4 = value;
				if (this._numMune4 != null)
				{
					this._numMune4.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numMune5
		{
			get
			{
				return this._numMune5;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numMune5 != null)
				{
					this._numMune5.ValueChanged -= eventHandler;
				}
				this._numMune5 = value;
				if (this._numMune5 != null)
				{
					this._numMune5.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numMune6
		{
			get
			{
				return this._numMune6;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numMune6 != null)
				{
					this._numMune6.ValueChanged -= eventHandler;
				}
				this._numMune6 = value;
				if (this._numMune6 != null)
				{
					this._numMune6.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numMune7
		{
			get
			{
				return this._numMune7;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numMune7 != null)
				{
					this._numMune7.ValueChanged -= eventHandler;
				}
				this._numMune7 = value;
				if (this._numMune7 != null)
				{
					this._numMune7.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numNyurin
		{
			get
			{
				return this._numNyurin;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numNyurin != null)
				{
					this._numNyurin.ValueChanged -= eventHandler;
				}
				this._numNyurin = value;
				if (this._numNyurin != null)
				{
					this._numNyurin.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numPupil2
		{
			get
			{
				return this._numPupil2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numPupil2 != null)
				{
					this._numPupil2.ValueChanged -= eventHandler;
				}
				this._numPupil2 = value;
				if (this._numPupil2 != null)
				{
					this._numPupil2.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numPupil3
		{
			get
			{
				return this._numPupil3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numPupil3 != null)
				{
					this._numPupil3.ValueChanged -= eventHandler;
				}
				this._numPupil3 = value;
				if (this._numPupil3 != null)
				{
					this._numPupil3.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numPupil4
		{
			get
			{
				return this._numPupil4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numPupil4 != null)
				{
					this._numPupil4.ValueChanged -= eventHandler;
				}
				this._numPupil4 = value;
				if (this._numPupil4 != null)
				{
					this._numPupil4.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numSeikaku
		{
			get
			{
				return this._numSeikaku;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				frmDetail _frmDetail1 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail1.num_ValueChanged);
				if (this._numSeikaku != null)
				{
					this._numSeikaku.ValueChanged -= eventHandler;
					this._numSeikaku.ValueChanged -= eventHandler1;
				}
				this._numSeikaku = value;
				if (this._numSeikaku != null)
				{
					this._numSeikaku.ValueChanged += eventHandler;
					this._numSeikaku.ValueChanged += eventHandler1;
				}
			}
		}

		public virtual JG2NumericUpDown numTairyoku3
		{
			get
			{
				return this._numTairyoku3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				frmDetail _frmDetail1 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail1.num_ValueChanged);
				if (this._numTairyoku3 != null)
				{
					this._numTairyoku3.ValueChanged -= eventHandler;
					this._numTairyoku3.ValueChanged -= eventHandler1;
				}
				this._numTairyoku3 = value;
				if (this._numTairyoku3 != null)
				{
					this._numTairyoku3.ValueChanged += eventHandler;
					this._numTairyoku3.ValueChanged += eventHandler1;
				}
			}
		}

		public virtual JG2NumericUpDown numTikubi3
		{
			get
			{
				return this._numTikubi3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numTikubi3 != null)
				{
					this._numTikubi3.ValueChanged -= eventHandler;
				}
				this._numTikubi3 = value;
				if (this._numTikubi3 != null)
				{
					this._numTikubi3.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numTiryoku3
		{
			get
			{
				return this._numTiryoku3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				frmDetail _frmDetail1 = this;
				EventHandler eventHandler1 = new EventHandler(_frmDetail1.num_ValueChanged);
				if (this._numTiryoku3 != null)
				{
					this._numTiryoku3.ValueChanged -= eventHandler;
					this._numTiryoku3.ValueChanged -= eventHandler1;
				}
				this._numTiryoku3 = value;
				if (this._numTiryoku3 != null)
				{
					this._numTiryoku3.ValueChanged += eventHandler;
					this._numTiryoku3.ValueChanged += eventHandler1;
				}
			}
		}

		public virtual JG2NumericUpDown numUnder2
		{
			get
			{
				return this._numUnder2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numUnder2 != null)
				{
					this._numUnder2.ValueChanged -= eventHandler;
				}
				this._numUnder2 = value;
				if (this._numUnder2 != null)
				{
					this._numUnder2.ValueChanged += eventHandler;
				}
			}
		}

		public virtual JG2NumericUpDown numWaist
		{
			get
			{
				return this._numWaist;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.Item_Changed);
				if (this._numWaist != null)
				{
					this._numWaist.ValueChanged -= eventHandler;
				}
				this._numWaist = value;
				if (this._numWaist != null)
				{
					this._numWaist.ValueChanged += eventHandler;
				}
			}
		}

		public virtual Panel pnlAction
		{
			get
			{
				return this._pnlAction;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlAction = value;
			}
		}

		public virtual Panel pnlActionRev
		{
			get
			{
				return this._pnlActionRev;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlActionRev = value;
			}
		}

		public virtual Panel pnlFooter
		{
			get
			{
				return this._pnlFooter;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlFooter = value;
			}
		}

		public virtual Panel pnlH
		{
			get
			{
				return this._pnlH;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlH = value;
			}
		}

		public virtual Panel pnlHokuro
		{
			get
			{
				return this._pnlHokuro;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlHokuro = value;
			}
		}

		public virtual Panel pnlKosei
		{
			get
			{
				return this._pnlKosei;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlKosei = value;
			}
		}

		public virtual Panel pnlPair
		{
			get
			{
				return this._pnlPair;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlPair = value;
			}
		}

		public virtual Panel pnlProfile
		{
			get
			{
				return this._pnlProfile;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlProfile = value;
			}
		}

		public virtual Panel pnlTools
		{
			get
			{
				return this._pnlTools;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlTools = value;
			}
		}

		public virtual Panel pnlYouto
		{
			get
			{
				return this._pnlYouto;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlYouto = value;
			}
		}

		public virtual JG2RadioButton radYouto0
		{
			get
			{
				return this._radYouto0;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				MouseEventHandler mouseEventHandler = new MouseEventHandler(_frmDetail.radYouto_MouseDown);
				frmDetail _frmDetail1 = this;
				DragEventHandler dragEventHandler = new DragEventHandler(_frmDetail1.radYouto_DragEnter);
				frmDetail _frmDetail2 = this;
				DragEventHandler dragEventHandler1 = new DragEventHandler(_frmDetail2.radYouto_DragDrop);
				frmDetail _frmDetail3 = this;
				EventHandler eventHandler = new EventHandler(_frmDetail3.radYouto_CheckedChanged);
				if (this._radYouto0 != null)
				{
					this._radYouto0.MouseDown -= mouseEventHandler;
					this._radYouto0.DragEnter -= dragEventHandler;
					this._radYouto0.DragDrop -= dragEventHandler1;
					this._radYouto0.CheckedChanged -= eventHandler;
				}
				this._radYouto0 = value;
				if (this._radYouto0 != null)
				{
					this._radYouto0.MouseDown += mouseEventHandler;
					this._radYouto0.DragEnter += dragEventHandler;
					this._radYouto0.DragDrop += dragEventHandler1;
					this._radYouto0.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2RadioButton radYouto1
		{
			get
			{
				return this._radYouto1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				MouseEventHandler mouseEventHandler = new MouseEventHandler(_frmDetail.radYouto_MouseDown);
				frmDetail _frmDetail1 = this;
				DragEventHandler dragEventHandler = new DragEventHandler(_frmDetail1.radYouto_DragEnter);
				frmDetail _frmDetail2 = this;
				DragEventHandler dragEventHandler1 = new DragEventHandler(_frmDetail2.radYouto_DragDrop);
				frmDetail _frmDetail3 = this;
				EventHandler eventHandler = new EventHandler(_frmDetail3.radYouto_CheckedChanged);
				if (this._radYouto1 != null)
				{
					this._radYouto1.MouseDown -= mouseEventHandler;
					this._radYouto1.DragEnter -= dragEventHandler;
					this._radYouto1.DragDrop -= dragEventHandler1;
					this._radYouto1.CheckedChanged -= eventHandler;
				}
				this._radYouto1 = value;
				if (this._radYouto1 != null)
				{
					this._radYouto1.MouseDown += mouseEventHandler;
					this._radYouto1.DragEnter += dragEventHandler;
					this._radYouto1.DragDrop += dragEventHandler1;
					this._radYouto1.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2RadioButton radYouto2
		{
			get
			{
				return this._radYouto2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				MouseEventHandler mouseEventHandler = new MouseEventHandler(_frmDetail.radYouto_MouseDown);
				frmDetail _frmDetail1 = this;
				DragEventHandler dragEventHandler = new DragEventHandler(_frmDetail1.radYouto_DragEnter);
				frmDetail _frmDetail2 = this;
				DragEventHandler dragEventHandler1 = new DragEventHandler(_frmDetail2.radYouto_DragDrop);
				frmDetail _frmDetail3 = this;
				EventHandler eventHandler = new EventHandler(_frmDetail3.radYouto_CheckedChanged);
				if (this._radYouto2 != null)
				{
					this._radYouto2.MouseDown -= mouseEventHandler;
					this._radYouto2.DragEnter -= dragEventHandler;
					this._radYouto2.DragDrop -= dragEventHandler1;
					this._radYouto2.CheckedChanged -= eventHandler;
				}
				this._radYouto2 = value;
				if (this._radYouto2 != null)
				{
					this._radYouto2.MouseDown += mouseEventHandler;
					this._radYouto2.DragEnter += dragEventHandler;
					this._radYouto2.DragDrop += dragEventHandler1;
					this._radYouto2.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual JG2RadioButton radYouto3
		{
			get
			{
				return this._radYouto3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				MouseEventHandler mouseEventHandler = new MouseEventHandler(_frmDetail.radYouto_MouseDown);
				frmDetail _frmDetail1 = this;
				DragEventHandler dragEventHandler = new DragEventHandler(_frmDetail1.radYouto_DragEnter);
				frmDetail _frmDetail2 = this;
				DragEventHandler dragEventHandler1 = new DragEventHandler(_frmDetail2.radYouto_DragDrop);
				frmDetail _frmDetail3 = this;
				EventHandler eventHandler = new EventHandler(_frmDetail3.radYouto_CheckedChanged);
				if (this._radYouto3 != null)
				{
					this._radYouto3.MouseDown -= mouseEventHandler;
					this._radYouto3.DragEnter -= dragEventHandler;
					this._radYouto3.DragDrop -= dragEventHandler1;
					this._radYouto3.CheckedChanged -= eventHandler;
				}
				this._radYouto3 = value;
				if (this._radYouto3 != null)
				{
					this._radYouto3.MouseDown += mouseEventHandler;
					this._radYouto3.DragEnter += dragEventHandler;
					this._radYouto3.DragDrop += dragEventHandler1;
					this._radYouto3.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual TabControl tabBase
		{
			get
			{
				return this._tabBase;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmDetail _frmDetail = this;
				EventHandler eventHandler = new EventHandler(_frmDetail.tabBase_SelectedIndexChanged);
				if (this._tabBase != null)
				{
					this._tabBase.SelectedIndexChanged -= eventHandler;
				}
				this._tabBase = value;
				if (this._tabBase != null)
				{
					this._tabBase.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual TabPage tbpBody
		{
			get
			{
				return this._tbpBody;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._tbpBody = value;
			}
		}

		public virtual TabPage tbpCostume
		{
			get
			{
				return this._tbpCostume;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._tbpCostume = value;
			}
		}

		public virtual TabPage tbpFace
		{
			get
			{
				return this._tbpFace;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._tbpFace = value;
			}
		}

		public virtual TabPage tbpHair
		{
			get
			{
				return this._tbpHair;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._tbpHair = value;
			}
		}

		public virtual TabPage tbpJinkaku
		{
			get
			{
				return this._tbpJinkaku;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._tbpJinkaku = value;
			}
		}

		public virtual TabPage tbpKosei
		{
			get
			{
				return this._tbpKosei;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._tbpKosei = value;
			}
		}

		public virtual TabPage tbpNinshin
		{
			get
			{
				return this._tbpNinshin;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._tbpNinshin = value;
			}
		}

		public virtual TabPage tbpTools
		{
			get
			{
				return this._tbpTools;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._tbpTools = value;
			}
		}

		public virtual ToolStripSeparator toolStripSeparator1
		{
			get
			{
				return this._toolStripSeparator1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._toolStripSeparator1 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator10
		{
			get
			{
				return this._ToolStripSeparator10;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator10 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator11
		{
			get
			{
				return this._ToolStripSeparator11;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator11 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator3
		{
			get
			{
				return this._ToolStripSeparator3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator3 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator4
		{
			get
			{
				return this._ToolStripSeparator4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator4 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator6
		{
			get
			{
				return this._ToolStripSeparator6;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator6 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator9
		{
			get
			{
				return this._ToolStripSeparator9;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator9 = value;
			}
		}

		public virtual System.Windows.Forms.ToolTip ToolTip
		{
			get
			{
				return this._ToolTip;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolTip = value;
			}
		}

		public frmDetail()
		{
			int i;
			Point location;
			frmDetail _frmDetail = this;
			base.Load += new EventHandler(_frmDetail.frmDetail_Load);
			frmDetail _frmDetail1 = this;
			base.FormClosing += new FormClosingEventHandler(_frmDetail1.frmDetail_FormClosing);
			frmDetail _frmDetail2 = this;
			base.FormClosed += new FormClosedEventHandler(_frmDetail2.frmDetail_FormClosed);
			this.InitializeComponent();
			if (JG2Importer.Common.MyDetailLocation == new Point(0, 0))
			{
				if (checked(checked(checked(MyProject.Forms.frmClass.Location.X + MyProject.Forms.frmClass.Width) + this.Width) + 10) <= Screen.PrimaryScreen.WorkingArea.Width)
				{
					Point point = MyProject.Forms.frmClass.Location;
					int x = checked(checked(point.X + MyProject.Forms.frmClass.Width) + 10);
					location = MyProject.Forms.frmClass.Location;
					JG2Importer.Common.MyDetailLocation = new Point(x, location.Y);
				}
				else
				{
					Point location1 = MyProject.Forms.frmClass.Location;
					int num = checked(checked(location1.X - this.Width) - 10);
					location = MyProject.Forms.frmClass.Location;
					JG2Importer.Common.MyDetailLocation = new Point(num, location.Y);
				}
			}
			this.Location = JG2Importer.Common.MyDetailLocation;
			this.CloseCheck = true;
			this.SeatNo = -1;
			this.PrevSeatNo = -1;
			this.mnuTabToolsAction.Text = string.Concat(Resources.CaptionCodeAction, Resources.mnuTabToolsActionText, Resources.CaptionCodeAction1);
			this.mnuTabToolsActionRev.Text = string.Concat(Resources.CaptionCodeAction, Resources.mnuTabToolsActionText, Resources.CaptionCodeAction2);
			int length = checked(checked((int)JG2Importer.JG2.Common.cnsCodeAction.Length) - 1);
			for (i = 1; i <= length; i = checked(i + 1))
			{
				this.mnuTabToolsAction.DropDownItems[checked(checked(i - 1) + 0)].Text = string.Concat("'", JG2Importer.JG2.Common.cnsCodeAction[i], Resources.mnuTabToolsActionDropDownItems1);
			}
			int length1 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeAction.Length) - 1);
			for (i = 1; i <= length1; i = checked(i + 1))
			{
				this.mnuTabToolsAction.DropDownItems[checked(checked(i - 1) + 5)].Text = string.Concat("'", JG2Importer.JG2.Common.cnsCodeAction[i], Resources.mnuTabToolsActionDropDownItems2);
			}
			int num1 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeAction.Length) - 1);
			for (i = 1; i <= num1; i = checked(i + 1))
			{
				this.mnuTabToolsActionRev.DropDownItems[checked(checked(i - 1) + 0)].Text = string.Concat("'", JG2Importer.JG2.Common.cnsCodeAction[i], Resources.mnuTabToolsActionDropDownItems1);
			}
			int length2 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeAction.Length) - 1);
			for (i = 1; i <= length2; i = checked(i + 1))
			{
				this.mnuTabToolsActionRev.DropDownItems[checked(checked(i - 1) + 5)].Text = string.Concat("'", JG2Importer.JG2.Common.cnsCodeAction[i], Resources.mnuTabToolsActionDropDownItems2);
			}
			frmClass forms = MyProject.Forms.frmClass;
			ToolStripMenuItem toolStripMenuItem = this.mnuSelectCondition;
			forms.CreateSelectCondition(ref toolStripMenuItem);
			this.mnuSelectCondition = toolStripMenuItem;
			frmClass _frmClass = MyProject.Forms.frmClass;
			toolStripMenuItem = this.mnuSelectCondition;
			_frmClass.CreateSelectConditionBukatu1(ref toolStripMenuItem);
			this.mnuSelectCondition = toolStripMenuItem;
			toolStripMenuItem = this.mnuSelectCondition;
			this.CreateSelectConditionEvent(ref toolStripMenuItem);
			this.mnuSelectCondition = toolStripMenuItem;
			this.lblHeight.Text = Resources.CaptionHeight;
			this.lblStyle.Text = Resources.CaptionTaikei;
			this.lblAtama1.Text = Resources.CaptionAtama1;
			this.lblAtama2.Text = Resources.CaptionAtama2;
			this.lblWaist.Text = Resources.CaptionWaist;
			this.lblMune1.Text = Resources.CaptionMune1;
			this.lblMune2.Text = Resources.CaptionMune2;
			this.lblMune3.Text = Resources.CaptionMune3;
			this.lblMune4.Text = Resources.CaptionMune4;
			this.lblMune5.Text = Resources.CaptionMune5;
			this.lblMune6.Text = Resources.CaptionMune6;
			this.lblMune7.Text = Resources.CaptionMune7;
			this.lblMune8.Text = Resources.CaptionMune8;
			this.lblNyurin.Text = Resources.CaptionNyurin;
			this.lblColorSkin2.Text = Resources.CaptionColorSkin;
			this.lblTikubi1.Text = Resources.CaptionTikubi1;
			this.lblTikubi2.Text = Resources.CaptionTikubi2;
			this.lblTikubi3.Text = Resources.CaptionTikubi3;
			this.lblHiyake1.Text = Resources.CaptionHiyake1;
			this.lblHiyake2.Text = Resources.CaptionHiyake2;
			this.lblMosaic.Text = Resources.CaptionMosaic;
			this.lblUnder1.Text = Resources.CaptionUnder1;
			this.lblUnder2.Text = Resources.CaptionUnder2;
			this.lblColorUnder.Text = Resources.CaptionColorUnder;
			this.lblKao.Text = Resources.CaptionKao;
			this.lblEye1.Text = Resources.CaptionEye1;
			this.lblEye2.Text = Resources.CaptionEye2;
			this.lblEye3.Text = Resources.CaptionEye3;
			this.lblEye4.Text = Resources.CaptionEye4;
			this.lblEye5.Text = Resources.CaptionEye5;
			this.lblPupil1.Text = Resources.CaptionPupil1;
			this.lblPupil2.Text = Resources.CaptionPupil2;
			this.lblPupil3.Text = Resources.CaptionPupil3;
			this.lblPupil4.Text = Resources.CaptionPupil4;
			this.lblPupil5.Text = Resources.CaptionPupil5;
			this.lblHighlight1.Text = Resources.CaptionHighlight1;
			this.lblColorEye1.Text = Resources.CaptionColorEye1;
			this.lblColorEye2.Text = Resources.CaptionColorEye2;
			this.lblMayu1.Text = Resources.CaptionMayu1;
			this.lblMayu2.Text = Resources.CaptionMayu2;
			this.lblColorMayu.Text = Resources.CaptionColorMayu;
			this.lblMabuta.Text = Resources.CaptionMabuta;
			this.lblMatuge1.Text = Resources.CaptionMatuge1;
			this.lblMatuge2.Text = Resources.CaptionMatuge2;
			this.lblGlasses2.Text = Resources.CaptionGlasses;
			this.lblColorGlasses.Text = Resources.CaptionColorGlasses;
			this.lblHokuro.Text = Resources.CaptionHokuro;
			this.chkHokuro1.Text = Resources.CaptionHokuro1;
			this.chkHokuro2.Text = Resources.CaptionHokuro2;
			this.chkHokuro3.Text = Resources.CaptionHokuro3;
			this.chkHokuro4.Text = Resources.CaptionHokuro4;
			this.lblLip1.Text = Resources.CaptionLip1;
			this.lblLip2.Text = Resources.CaptionLip2;
			this.lblHair1.Text = Resources.CaptionHair1;
			this.chkHairRev1.Text = Strings.Mid(Resources.CaptionHairRev1, 4);
			this.lblHair2.Text = Resources.CaptionHair2;
			this.chkHairRev2.Text = Strings.Mid(Resources.CaptionHairRev2, 4);
			this.lblHair3.Text = Resources.CaptionHair3;
			this.chkHairRev3.Text = Strings.Mid(Resources.CaptionHairRev3, 4);
			this.lblHair4.Text = Resources.CaptionHair4;
			this.chkHairRev4.Text = Strings.Mid(Resources.CaptionHairRev4, 4);
			this.lblColorHair.Text = Resources.CaptionColorHair;
			this.lblTiryoku1.Text = Resources.CaptionTiryoku1;
			this.lblTairyoku1.Text = Resources.CaptionTairyoku1;
			this.lblBukatu1.Text = Resources.CaptionBukatu1;
			this.lblSyakousei.Text = Resources.CaptionSyakousei;
			this.lblKenka.Text = Resources.CaptionKenka;
			this.lblTeisou.Text = Resources.CaptionTeisou;
			this.lblSeiai.Text = Resources.CaptionSeiai;
			this.chkSeikou1.Text = Resources.CaptionSeikou1;
			this.chkSeikou2.Text = Resources.CaptionSeikou2;
			this.lblVoice.Text = Resources.CaptionVoice;
			this.lblSeikaku.Text = Resources.CaptionSeikaku;
			this.chkCyoroi.Text = Resources.CaptionCyoroi;
			this.chkNekketu.Text = Resources.CaptionNekketu;
			this.chkNigate1.Text = Resources.CaptionNigate1;
			this.chkNigate2.Text = Resources.CaptionNigate2;
			this.chkCharm.Text = Resources.CaptionCharm;
			this.chkTundere.Text = Resources.CaptionTundere;
			this.chkKyouki.Text = Resources.CaptionKyouki;
			this.chkMiha.Text = Resources.CaptionMiha;
			this.chkSunao.Text = Resources.CaptionSunao;
			this.chkMaemuki.Text = Resources.CaptionMaemuki;
			this.chkTereya.Text = Resources.CaptionTereya;
			this.chkYakimochi.Text = Resources.CaptionYakimochi;
			this.chkToufu.Text = Resources.CaptionToufu;
			this.chkSukebe.Text = Resources.CaptionSukebe;
			this.chkMajime.Text = Resources.CaptionMajime;
			this.chkCool.Text = Resources.CaptionCool;
			this.chkTyokujyo.Text = Resources.CaptionTyokujyo;
			this.chkPoyayan.Text = Resources.CaptionPoyayan;
			this.chkBouryoku.Text = Resources.CaptionBouryoku;
			this.chkSousyoku.Text = Resources.CaptionSousyoku;
			this.chkSewayaki.Text = Resources.CaptionSewayaki;
			this.chkIintyou.Text = Resources.CaptionIintyou;
			this.chkOsyaberi.Text = Resources.CaptionOsyaberi;
			this.chkHarapeko.Text = Resources.CaptionHarapeko;
			this.chkRenai.Text = Resources.CaptionRenai;
			this.chkItizu.Text = Resources.CaptionItizu;
			this.chkYuujyufudan.Text = Resources.CaptionYuujyufudan;
			this.chkMakenki.Text = Resources.CaptionMakenki;
			this.chkHaraguro.Text = Resources.CaptionHaraguro;
			this.chkKinben.Text = Resources.CaptionKinben;
			this.chkHonpou.Text = Resources.CaptionHonpou;
			this.chkM.Text = Resources.CaptionM;
			this.chkAsekaki.Text = Resources.CaptionAsekaki;
			this.chkYami.Text = Resources.CaptionYami;
			this.chkNantyo.Text = Resources.CaptionNantyo;
			this.chkYowami.Text = Resources.CaptionYowami;
			this.chkMiseityo.Text = Resources.CaptionMiseityo;
			this.chkLuck.Text = Resources.CaptionLuck;
			this.chkRare.Text = Resources.CaptionRare;
			this.chkKiss.Text = Resources.CaptionKiss;
			this.chkBust.Text = Resources.CaptionBust;
			this.chkSeiki1.Text = Resources.CaptionSeiki1;
			this.chkSeiki2.Text = Resources.CaptionSeiki2;
			this.chkCunnilingus.Text = Resources.CaptionCunnilingus;
			this.chkFellatio.Text = Resources.CaptionFellatio;
			this.chkBack.Text = Resources.CaptionBack;
			this.chkOnna.Text = Resources.CaptionOnna;
			this.chkAnal.Text = Resources.CaptionAnal;
			this.chkNama.Text = Resources.CaptionNama;
			this.chkSeiin.Text = Resources.CaptionSeiin;
			this.chkNaka.Text = Resources.CaptionNaka;
			this.chkKake.Text = Resources.CaptionKake;
			this.lblSunday1.Text = Resources.CaptionSunday1;
			this.lblMonday1.Text = Resources.CaptionMonday1;
			this.lblTuesday1.Text = Resources.CaptionTuesday1;
			this.lblWednesday1.Text = Resources.CaptionWednesday1;
			this.lblThursday1.Text = Resources.CaptionThursday1;
			this.lblFriday1.Text = Resources.CaptionFriday1;
			this.lblSaturday1.Text = Resources.CaptionSaturday1;
			this.lblSunday2.Text = Resources.CaptionSunday2;
			this.lblMonday2.Text = Resources.CaptionMonday2;
			this.lblTuesday2.Text = Resources.CaptionTuesday2;
			this.lblWednesday2.Text = Resources.CaptionWednesday2;
			this.lblThursday2.Text = Resources.CaptionThursday2;
			this.lblFriday2.Text = Resources.CaptionFriday2;
			this.lblSaturday2.Text = Resources.CaptionSaturday2;
			this.radYouto0.Text = JG2Importer.JG2.Common.cnsYouto[0];
			this.radYouto1.Text = JG2Importer.JG2.Common.cnsYouto[1];
			this.radYouto2.Text = JG2Importer.JG2.Common.cnsYouto[2];
			this.radYouto3.Text = JG2Importer.JG2.Common.cnsYouto[3];
			this.chkKousai.Text = JG2Importer.JG2.Common.cnsCodeKousai[1];
			this.cmbHeight.Items.AddRange(JG2Importer.JG2.Common.cnsHeight);
			this.cmbStyle.Items.AddRange(JG2Importer.JG2.Common.cnsStyle);
			i = 1;
			do
			{
				this.cmbMune2.Items.Add(string.Concat(Resources.FilterTxtBustShape, Strings.Format(i, "")));
				i = checked(i + 1);
			}
			while (i <= 4);
			this.cmbMune8.Items.AddRange(JG2Importer.JG2.Common.cnsMune8);
			i = 1;
			do
			{
				this.cmbPupil1.Items.Add(string.Concat(Resources.CaptionPupil1, Strings.Format(i, "")));
				i = checked(i + 1);
			}
			while (i <= 4);
			i = 1;
			do
			{
				this.cmbPupil5.Items.Add(string.Concat(Resources.CaptionPupil5, Strings.Format(i, "")));
				i = checked(i + 1);
			}
			while (i <= 7);
			i = 1;
			do
			{
				this.cmbHighlight1.Items.Add(string.Concat(Resources.CaptionHighlight1, Strings.Format(i, "")));
				i = checked(i + 1);
			}
			while (i <= 7);
			i = 1;
			do
			{
				this.cmbMayu1.Items.Add(string.Concat(Resources.CaptionMayu1, Strings.Format(i, "")));
				i = checked(i + 1);
			}
			while (i <= 20);
			i = 1;
			do
			{
				this.cmbMabuta.Items.Add(string.Concat(Resources.CaptionMabuta, Strings.Format(i, "")));
				i = checked(i + 1);
			}
			while (i <= 8);
			i = 1;
			do
			{
				this.cmbMatuge1.Items.Add(string.Concat(Resources.CaptionMatuge1, Strings.Format(i, "")));
				i = checked(i + 1);
			}
			while (i <= 8);
			i = 1;
			do
			{
				this.cmbMatuge2.Items.Add(string.Concat(Resources.CaptionMatuge2, Strings.Format(i, "")));
				i = checked(i + 1);
			}
			while (i <= 8);
			int num2 = checked(checked((int)JG2Importer.Common.MySaveData.Header.Bukatu.Length) - 1);
			for (i = 0; i <= num2; i = checked(i + 1))
			{
				this.cmbBukatu1.Items.Add(JG2Importer.Common.MySaveData.Header.Bukatu[i].Name);
			}
			this.cmbSyakousei.Items.AddRange(JG2Importer.JG2.Common.cnsSyakousei);
			this.cmbKenka.Items.AddRange(JG2Importer.JG2.Common.cnsKenka);
			this.cmbTeisou.Items.AddRange(JG2Importer.JG2.Common.cnsTeisou);
			this.cmbSeiai.Items.AddRange(JG2Importer.JG2.Common.cnsSeiai);
			this.cmbVoice.Items.AddRange(JG2Importer.JG2.Common.cnsVoice);
			this.cmbSunday1.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbMonday1.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbTuesday1.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbWednesday1.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbThursday1.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbFriday1.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbSaturday1.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbSunday2.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbMonday2.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbTuesday2.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbWednesday2.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbThursday2.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbFriday2.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbSaturday2.Items.AddRange(JG2Importer.JG2.Common.cnsNinshin);
			this.cmbHeight.Caption = Resources.CaptionHeight;
			this.cmbStyle.Caption = Resources.CaptionTaikei;
			this.numAtama1.Caption = Resources.CaptionAtama1;
			this.numAtama2.Caption = Resources.CaptionAtama2;
			this.numWaist.Caption = Resources.CaptionWaist;
			this.numMune1.Caption = Resources.CaptionMune1;
			this.cmbMune2.Caption = Resources.CaptionMune2;
			this.numMune3.Caption = Resources.CaptionMune3;
			this.numMune4.Caption = Resources.CaptionMune4;
			this.numMune5.Caption = Resources.CaptionMune5;
			this.numMune6.Caption = Resources.CaptionMune6;
			this.numMune7.Caption = Resources.CaptionMune7;
			this.cmbMune8.Caption = Resources.CaptionMune8;
			this.numNyurin.Caption = Resources.CaptionNyurin;
			this.btnColorSkin2.Caption = Resources.CaptionColorSkin;
			this.cmbTikubi1.Caption = Resources.CaptionTikubi1;
			this.cmbTikubi2.Caption = Resources.CaptionTikubi2;
			this.numTikubi3.Caption = Resources.CaptionTikubi3;
			this.cmbHiyake1.Caption = Resources.CaptionHiyake1;
			this.numHiyake2.Caption = Resources.CaptionHiyake2;
			this.cmbMosaic.Caption = Resources.CaptionMosaic;
			this.cmbUnder1.Caption = Resources.CaptionUnder1;
			this.numUnder2.Caption = Resources.CaptionUnder2;
			this.btnColorUnder.Caption = Resources.CaptionColorUnder;
			this.cmbKao.Caption = Resources.CaptionKao;
			this.numEye1.Caption = Resources.CaptionEye1;
			this.numEye2.Caption = Resources.CaptionEye2;
			this.numEye3.Caption = Resources.CaptionEye3;
			this.numEye4.Caption = Resources.CaptionEye4;
			this.numEye5.Caption = Resources.CaptionEye5;
			this.cmbPupil1.Caption = Resources.CaptionPupil1;
			this.numPupil2.Caption = Resources.CaptionPupil2;
			this.numPupil3.Caption = Resources.CaptionPupil3;
			this.numPupil4.Caption = Resources.CaptionPupil4;
			this.cmbPupil5.Caption = Resources.CaptionPupil5;
			this.cmbHighlight1.Caption = Resources.CaptionHighlight1;
			this.btnColorEye1.Caption = Resources.CaptionColorEye1;
			this.btnColorEye2.Caption = Resources.CaptionColorEye2;
			this.cmbMayu1.Caption = Resources.CaptionMayu1;
			this.numMayu2.Caption = Resources.CaptionMayu2;
			this.btnColorMayu.Caption = Resources.CaptionColorMayu;
			this.cmbMabuta.Caption = Resources.CaptionMabuta;
			this.cmbMatuge1.Caption = Resources.CaptionMatuge1;
			this.cmbMatuge2.Caption = Resources.CaptionMatuge2;
			this.cmbGlasses2.Caption = Resources.CaptionGlasses;
			this.btnColorGlasses.Caption = Resources.CaptionColorGlasses;
			this.chkHokuro1.Caption = string.Concat(Resources.CaptionHokuro, "/", Resources.CaptionHokuro1);
			this.chkHokuro2.Caption = string.Concat(Resources.CaptionHokuro, "/", Resources.CaptionHokuro2);
			this.chkHokuro3.Caption = string.Concat(Resources.CaptionHokuro, "/", Resources.CaptionHokuro3);
			this.chkHokuro4.Caption = string.Concat(Resources.CaptionHokuro, "/", Resources.CaptionHokuro4);
			this.cmbLip1.Caption = Resources.CaptionLip1;
			this.numLip2.Caption = Resources.CaptionLip2;
			this.numHair1.Caption = Resources.CaptionHair1;
			this.numHairAdj1.Caption = Resources.CaptionHairAdj1;
			this.chkHairRev1.Caption = Resources.CaptionHairRev1;
			this.numHair2.Caption = Resources.CaptionHair2;
			this.numHairAdj2.Caption = Resources.CaptionHairAdj2;
			this.chkHairRev2.Caption = Resources.CaptionHairRev2;
			this.numHair3.Caption = Resources.CaptionHair3;
			this.numHairAdj3.Caption = Resources.CaptionHairAdj3;
			this.chkHairRev3.Caption = Resources.CaptionHairRev3;
			this.numHair4.Caption = Resources.CaptionHair4;
			this.numHairAdj4.Caption = Resources.CaptionHairAdj4;
			this.chkHairRev4.Caption = Resources.CaptionHairRev4;
			this.btnColorHair.Caption = Resources.CaptionColorHair;
			this.numTiryoku3.Caption = Resources.CaptionTiryoku3;
			this.numTairyoku3.Caption = Resources.CaptionTairyoku3;
			this.numBukatu3.Caption = Resources.CaptionBukatu3;
			this.cmbBukatu1.Caption = Resources.CaptionBukatu1;
			this.cmbSyakousei.Caption = Resources.CaptionSyakousei;
			this.cmbKenka.Caption = Resources.CaptionKenka;
			this.cmbTeisou.Caption = Resources.CaptionTeisou;
			this.cmbSeiai.Caption = Resources.CaptionSeiai;
			this.chkSeikou1.Caption = Resources.CaptionSeikou1;
			this.chkSeikou2.Caption = Resources.CaptionSeikou2;
			this.cmbVoice.Caption = Resources.CaptionVoice;
			this.numSeikaku.Caption = Resources.CaptionSeikaku;
			this.chkCyoroi.Caption = Resources.CaptionCyoroi;
			this.chkNekketu.Caption = Resources.CaptionNekketu;
			this.chkNigate1.Caption = Resources.CaptionNigate1;
			this.chkNigate2.Caption = Resources.CaptionNigate2;
			this.chkCharm.Caption = Resources.CaptionCharm;
			this.chkTundere.Caption = Resources.CaptionTundere;
			this.chkKyouki.Caption = Resources.CaptionKyouki;
			this.chkMiha.Caption = Resources.CaptionMiha;
			this.chkSunao.Caption = Resources.CaptionSunao;
			this.chkMaemuki.Caption = Resources.CaptionMaemuki;
			this.chkTereya.Caption = Resources.CaptionTereya;
			this.chkYakimochi.Caption = Resources.CaptionYakimochi;
			this.chkToufu.Caption = Resources.CaptionToufu;
			this.chkSukebe.Caption = Resources.CaptionSukebe;
			this.chkMajime.Caption = Resources.CaptionMajime;
			this.chkCool.Caption = Resources.CaptionCool;
			this.chkTyokujyo.Caption = Resources.CaptionTyokujyo;
			this.chkPoyayan.Caption = Resources.CaptionPoyayan;
			this.chkBouryoku.Caption = Resources.CaptionBouryoku;
			this.chkSousyoku.Caption = Resources.CaptionSousyoku;
			this.chkSewayaki.Caption = Resources.CaptionSewayaki;
			this.chkIintyou.Caption = Resources.CaptionIintyou;
			this.chkOsyaberi.Caption = Resources.CaptionOsyaberi;
			this.chkHarapeko.Caption = Resources.CaptionHarapeko;
			this.chkRenai.Caption = Resources.CaptionRenai;
			this.chkItizu.Caption = Resources.CaptionItizu;
			this.chkYuujyufudan.Caption = Resources.CaptionYuujyufudan;
			this.chkMakenki.Caption = Resources.CaptionMakenki;
			this.chkHaraguro.Caption = Resources.CaptionHaraguro;
			this.chkKinben.Caption = Resources.CaptionKinben;
			this.chkHonpou.Caption = Resources.CaptionHonpou;
			this.chkM.Caption = Resources.CaptionM;
			this.chkAsekaki.Caption = Resources.CaptionAsekaki;
			this.chkYami.Caption = Resources.CaptionYami;
			this.chkNantyo.Caption = Resources.CaptionNantyo;
			this.chkYowami.Caption = Resources.CaptionYowami;
			this.chkMiseityo.Caption = Resources.CaptionMiseityo;
			this.chkLuck.Caption = Resources.CaptionLuck;
			this.chkRare.Caption = Resources.CaptionRare;
			this.chkKiss.Caption = Resources.CaptionKiss;
			this.chkBust.Caption = Resources.CaptionBust;
			this.chkSeiki1.Caption = Resources.CaptionSeiki1;
			this.chkSeiki2.Caption = Resources.CaptionSeiki2;
			this.chkCunnilingus.Caption = Resources.CaptionCunnilingus;
			this.chkFellatio.Caption = Resources.CaptionFellatio;
			this.chkBack.Caption = Resources.CaptionBack;
			this.chkOnna.Caption = Resources.CaptionOnna;
			this.chkAnal.Caption = Resources.CaptionAnal;
			this.chkNama.Caption = Resources.CaptionNama;
			this.chkSeiin.Caption = Resources.CaptionSeiin;
			this.chkNaka.Caption = Resources.CaptionNaka;
			this.chkKake.Caption = Resources.CaptionKake;
			this.cmbSunday1.Caption = Resources.CaptionSunday1;
			this.cmbMonday1.Caption = Resources.CaptionMonday1;
			this.cmbTuesday1.Caption = Resources.CaptionTuesday1;
			this.cmbWednesday1.Caption = Resources.CaptionWednesday1;
			this.cmbThursday1.Caption = Resources.CaptionThursday1;
			this.cmbFriday1.Caption = Resources.CaptionFriday1;
			this.cmbSaturday1.Caption = Resources.CaptionSaturday1;
			this.cmbSunday2.Caption = Resources.CaptionSunday2;
			this.cmbMonday2.Caption = Resources.CaptionMonday2;
			this.cmbTuesday2.Caption = Resources.CaptionTuesday2;
			this.cmbWednesday2.Caption = Resources.CaptionWednesday2;
			this.cmbThursday2.Caption = Resources.CaptionThursday2;
			this.cmbFriday2.Caption = Resources.CaptionFriday2;
			this.cmbSaturday2.Caption = Resources.CaptionSaturday2;
			this.radYouto0.Caption = Strings.Replace(Resources.DescriptionYouto, "%Destination%", JG2Importer.JG2.Common.cnsYouto[0], 1, -1, CompareMethod.Binary);
			this.radYouto1.Caption = Strings.Replace(Resources.DescriptionYouto, "%Destination%", JG2Importer.JG2.Common.cnsYouto[1], 1, -1, CompareMethod.Binary);
			this.radYouto2.Caption = Strings.Replace(Resources.DescriptionYouto, "%Destination%", JG2Importer.JG2.Common.cnsYouto[2], 1, -1, CompareMethod.Binary);
			this.radYouto3.Caption = Strings.Replace(Resources.DescriptionYouto, "%Destination%", JG2Importer.JG2.Common.cnsYouto[3], 1, -1, CompareMethod.Binary);
			this.btnAction1.Caption = JG2Importer.JG2.Common.cnsCodeAction[1];
			this.btnAction2.Caption = JG2Importer.JG2.Common.cnsCodeAction[2];
			this.btnAction3.Caption = JG2Importer.JG2.Common.cnsCodeAction[3];
			this.btnAction4.Caption = JG2Importer.JG2.Common.cnsCodeAction[4];
			this.btnDelAction1.Caption = string.Concat(JG2Importer.JG2.Common.cnsCodeAction[1], "削除");
			this.btnDelAction2.Caption = string.Concat(JG2Importer.JG2.Common.cnsCodeAction[2], "削除");
			this.btnDelAction3.Caption = string.Concat(JG2Importer.JG2.Common.cnsCodeAction[3], "削除");
			this.btnDelAction4.Caption = string.Concat(JG2Importer.JG2.Common.cnsCodeAction[4], "削除");
			this.chkKousai.Caption = Resources.CaptionKousai;
			this.btnActionRev1.Caption = string.Concat(JG2Importer.JG2.Common.cnsCodeAction[1], "/PC");
			this.btnActionRev2.Caption = string.Concat(JG2Importer.JG2.Common.cnsCodeAction[2], "/PC");
			this.btnActionRev3.Caption = string.Concat(JG2Importer.JG2.Common.cnsCodeAction[3], "/PC");
			this.btnActionRev4.Caption = string.Concat(JG2Importer.JG2.Common.cnsCodeAction[4], "/PC");
			this.btnDelActionRev1.Caption = string.Concat(JG2Importer.JG2.Common.cnsCodeAction[1], "削除/PC");
			this.btnDelActionRev2.Caption = string.Concat(JG2Importer.JG2.Common.cnsCodeAction[2], "削除/PC");
			this.btnDelActionRev3.Caption = string.Concat(JG2Importer.JG2.Common.cnsCodeAction[3], "削除/PC");
			this.btnDelActionRev4.Caption = string.Concat(JG2Importer.JG2.Common.cnsCodeAction[4], "削除/PC");
			this.btnAction1.ForeColor = JG2Importer.JG2.Common.ColorLove;
			this.btnAction2.ForeColor = JG2Importer.JG2.Common.ColorLike;
			this.btnAction3.ForeColor = JG2Importer.JG2.Common.ColorDislike;
			this.btnAction4.ForeColor = JG2Importer.JG2.Common.ColorHate;
			this.btnDelAction1.ForeColor = JG2Importer.JG2.Common.ColorLove;
			this.btnDelAction2.ForeColor = JG2Importer.JG2.Common.ColorLike;
			this.btnDelAction3.ForeColor = JG2Importer.JG2.Common.ColorDislike;
			this.btnDelAction4.ForeColor = JG2Importer.JG2.Common.ColorHate;
			this.btnActionRev1.ForeColor = JG2Importer.JG2.Common.ColorLove;
			this.btnActionRev2.ForeColor = JG2Importer.JG2.Common.ColorLike;
			this.btnActionRev3.ForeColor = JG2Importer.JG2.Common.ColorDislike;
			this.btnActionRev4.ForeColor = JG2Importer.JG2.Common.ColorHate;
			this.btnDelActionRev1.ForeColor = JG2Importer.JG2.Common.ColorLove;
			this.btnDelActionRev2.ForeColor = JG2Importer.JG2.Common.ColorLike;
			this.btnDelActionRev3.ForeColor = JG2Importer.JG2.Common.ColorDislike;
			this.btnDelActionRev4.ForeColor = JG2Importer.JG2.Common.ColorHate;
			this.ToolTip.SetToolTip(this.btnColorSkin2, Resources.HintColorButton);
			this.ToolTip.SetToolTip(this.btnColorUnder, Resources.HintColorButton);
			this.ToolTip.SetToolTip(this.btnColorEye1, Resources.HintColorButton);
			this.ToolTip.SetToolTip(this.btnColorEye2, Resources.HintColorButton);
			this.ToolTip.SetToolTip(this.btnColorMayu, Resources.HintColorButton);
			this.ToolTip.SetToolTip(this.btnColorGlasses, Resources.HintColorButton);
			this.ToolTip.SetToolTip(this.radYouto0, Strings.Replace(Resources.HintYouto, "%Destination%", JG2Importer.JG2.Common.cnsYouto[0], 1, -1, CompareMethod.Binary));
			this.ToolTip.SetToolTip(this.radYouto1, Strings.Replace(Resources.HintYouto, "%Destination%", JG2Importer.JG2.Common.cnsYouto[1], 1, -1, CompareMethod.Binary));
			this.ToolTip.SetToolTip(this.radYouto2, Strings.Replace(Resources.HintYouto, "%Destination%", JG2Importer.JG2.Common.cnsYouto[2], 1, -1, CompareMethod.Binary));
			this.ToolTip.SetToolTip(this.radYouto3, Strings.Replace(Resources.HintYouto, "%Destination%", JG2Importer.JG2.Common.cnsYouto[3], 1, -1, CompareMethod.Binary));
			this.CreateCostumePanel();
		}

		private void btnBatch_TextChanged(object sender, EventArgs e)
		{
			this.mnuBatch.Text = this.btnBatch.Text;
		}

		private void btnColor_Click(object sender, EventArgs e)
		{
			Color color = new Color();
			Color color1;
			ColorDialog myColorDialog = JG2Importer.Common.MyColorDialog;
			object obj = NewLateBinding.LateGet(sender, null, "BackColor", new object[0], null, null, null);
			if (obj != null)
			{
				color1 = (Color)obj;
			}
			else
			{
				color1 = color;
			}
			myColorDialog.Color = color1;
			JG2Importer.Common.MyColorDialog.ShowDialog(this);
			object[] objArray = new object[] { JG2Importer.Common.MyColorDialog.Color };
			NewLateBinding.LateSet(sender, null, "BackColor", objArray, null, null);
			if (Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null), "btnColorSocks", false))
			{
				object obj1 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray = new object[] { "lblColorPansuto" };
				object obj2 = NewLateBinding.LateGet(obj1, null, "Controls", objArray, null, null, null);
				object[] objArray1 = new object[1];
				object obj3 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				object[] objArray2 = new object[] { "numPansuto" };
				int integer = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(obj3, null, "Controls", objArray2, null, null, null), null, "Value", new object[0], null, null, null));
				byte r = JG2Importer.Common.MyColorDialog.Color.R;
				byte g = JG2Importer.Common.MyColorDialog.Color.G;
				Color color2 = JG2Importer.Common.MyColorDialog.Color;
				objArray1[0] = Color.FromArgb(integer, (int)r, (int)g, (int)color2.B);
				NewLateBinding.LateSetComplex(obj2, null, "BackColor", objArray1, null, null, false, true);
			}
		}

		private void btnColor_DragDrop(object sender, DragEventArgs e)
		{
			Color color = new Color();
			Color color1;
			object obj = NewLateBinding.LateGet(e.Data.GetData(typeof(JG2Button)), null, "BackColor", new object[0], null, null, null);
			if (obj != null)
			{
				color1 = (Color)obj;
			}
			else
			{
				color1 = color;
			}
			Color color2 = color1;
			object[] objArray = new object[] { color2 };
			NewLateBinding.LateSet(sender, null, "BackColor", objArray, null, null);
			if (Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null), "btnColorSocks", false))
			{
				object obj1 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray = new object[] { "lblColorPansuto" };
				object obj2 = NewLateBinding.LateGet(obj1, null, "Controls", objArray, null, null, null);
				object[] objArray1 = new object[1];
				object obj3 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				object[] objArray2 = new object[] { "numPansuto" };
				objArray1[0] = Color.FromArgb(Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(obj3, null, "Controls", objArray2, null, null, null), null, "Value", new object[0], null, null, null)), (int)color2.R, (int)color2.G, (int)color2.B);
				NewLateBinding.LateSetComplex(obj2, null, "BackColor", objArray1, null, null, false, true);
			}
		}

		private void btnColor_DragEnter(object sender, DragEventArgs e)
		{
			e.Effect = (e.Data.GetDataPresent(typeof(JG2Button)) ? DragDropEffects.Copy : DragDropEffects.None);
		}

		private void btnColor_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Right)
			{
				object[] objectValue = new object[] { RuntimeHelpers.GetObjectValue(sender), DragDropEffects.Copy };
				object[] objArray = objectValue;
				bool[] flagArray = new bool[] { true, false };
				NewLateBinding.LateCall(sender, null, "DoDragDrop", objArray, null, null, flagArray, true);
				if (flagArray[0])
				{
					sender = RuntimeHelpers.GetObjectValue(objArray[0]);
				}
			}
		}

		private void btnColorSub_Click(object sender, EventArgs e)
		{
			object obj = NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null);
			if (Operators.ConditionalCompareObjectEqual(obj, "btnColorEye1Sub", false))
			{
				this.btnColorEye2.BackColor = this.btnColorEye1.BackColor;
			}
			else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorEye2Sub", false))
			{
				this.btnColorEye1.BackColor = this.btnColorEye2.BackColor;
			}
			else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorHairSub1", false))
			{
				this.btnColorMayu.BackColor = this.btnColorHair.BackColor;
			}
			else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorHairSub2", false))
			{
				this.btnColorUnder.BackColor = this.btnColorHair.BackColor;
			}
		}

		private void btnDefault_Click(object sender, EventArgs e)
		{
			object[] objArray;
			object[] objectValue;
			object obj = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
			object[] objArray1 = new object[] { "numType" };
			object obj1 = NewLateBinding.LateGet(NewLateBinding.LateGet(obj, null, "Controls", objArray1, null, null, null), null, "Value", new object[0], null, null, null);
			if (Operators.ConditionalCompareObjectEqual(obj1, 0, false))
			{
				object obj2 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray = new object[] { "numPansuto" };
				object obj3 = NewLateBinding.LateGet(obj2, null, "Controls", objArray, null, null, null);
				objectValue = new object[] { 255 };
				NewLateBinding.LateCall(obj3, null, "SetValue", objectValue, null, null, null, true);
				object obj4 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorBottom2" };
				object obj5 = NewLateBinding.LateGet(obj4, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj5, null, "SetValue", objArray, null, null, null, true);
				object obj6 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSocks" };
				object obj7 = NewLateBinding.LateGet(obj6, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 1 };
				NewLateBinding.LateCall(obj7, null, "SetValue", objArray, null, null, null, true);
				object obj8 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSkirt" };
				object obj9 = NewLateBinding.LateGet(obj8, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj9, null, "SetValue", objArray, null, null, null, true);
				object obj10 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numUtibaki" };
				object obj11 = NewLateBinding.LateGet(obj10, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 1 };
				NewLateBinding.LateCall(obj11, null, "SetValue", objArray, null, null, null, true);
				object obj12 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSotobaki" };
				object obj13 = NewLateBinding.LateGet(obj12, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 2 };
				NewLateBinding.LateCall(obj13, null, "SetValue", objArray, null, null, null, true);
				object obj14 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagJyoge" };
				object obj15 = NewLateBinding.LateGet(obj14, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj15, null, "SetValue", objArray, null, null, null, true);
				object obj16 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSitagi" };
				object obj17 = NewLateBinding.LateGet(obj16, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { true };
				NewLateBinding.LateCall(obj17, null, "SetValue", objArray, null, null, null, true);
				object obj18 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSkirt" };
				object obj19 = NewLateBinding.LateGet(obj18, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { true };
				NewLateBinding.LateCall(obj19, null, "SetValue", objArray, null, null, null, true);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj1, 1, false))
			{
				object obj20 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numPansuto" };
				object obj21 = NewLateBinding.LateGet(obj20, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 255 };
				NewLateBinding.LateCall(obj21, null, "SetValue", objArray, null, null, null, true);
				object obj22 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSkirt" };
				object obj23 = NewLateBinding.LateGet(obj22, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj23, null, "SetValue", objArray, null, null, null, true);
				object obj24 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop2" };
				object obj25 = NewLateBinding.LateGet(obj24, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj25, null, "SetValue", objArray, null, null, null, true);
				object obj26 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop4" };
				object obj27 = NewLateBinding.LateGet(obj26, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj27, null, "SetValue", objArray, null, null, null, true);
				object obj28 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSocks" };
				object obj29 = NewLateBinding.LateGet(obj28, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 2 };
				NewLateBinding.LateCall(obj29, null, "SetValue", objArray, null, null, null, true);
				object obj30 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numUtibaki" };
				object obj31 = NewLateBinding.LateGet(obj30, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 3 };
				NewLateBinding.LateCall(obj31, null, "SetValue", objArray, null, null, null, true);
				object obj32 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSotobaki" };
				object obj33 = NewLateBinding.LateGet(obj32, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 3 };
				NewLateBinding.LateCall(obj33, null, "SetValue", objArray, null, null, null, true);
				object obj34 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagJyoge" };
				object obj35 = NewLateBinding.LateGet(obj34, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj35, null, "SetValue", objArray, null, null, null, true);
				object obj36 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSitagi" };
				object obj37 = NewLateBinding.LateGet(obj36, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { true };
				NewLateBinding.LateCall(obj37, null, "SetValue", objArray, null, null, null, true);
				object obj38 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSkirt" };
				object obj39 = NewLateBinding.LateGet(obj38, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj39, null, "SetValue", objArray, null, null, null, true);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj1, 2, false))
			{
				object obj40 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numPansuto" };
				object obj41 = NewLateBinding.LateGet(obj40, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 255 };
				NewLateBinding.LateCall(obj41, null, "SetValue", objArray, null, null, null, true);
				object obj42 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSkirt" };
				object obj43 = NewLateBinding.LateGet(obj42, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj43, null, "SetValue", objArray, null, null, null, true);
				object obj44 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop2" };
				object obj45 = NewLateBinding.LateGet(obj44, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj45, null, "SetValue", objArray, null, null, null, true);
				object obj46 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop3" };
				object obj47 = NewLateBinding.LateGet(obj46, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj47, null, "SetValue", objArray, null, null, null, true);
				object obj48 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop4" };
				object obj49 = NewLateBinding.LateGet(obj48, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj49, null, "SetValue", objArray, null, null, null, true);
				object obj50 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorBottom1" };
				object obj51 = NewLateBinding.LateGet(obj50, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj51, null, "SetValue", objArray, null, null, null, true);
				object obj52 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorBottom2" };
				object obj53 = NewLateBinding.LateGet(obj52, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj53, null, "SetValue", objArray, null, null, null, true);
				object obj54 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorSitagi" };
				object obj55 = NewLateBinding.LateGet(obj54, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj55, null, "SetValue", objArray, null, null, null, true);
				object obj56 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1" };
				object obj57 = NewLateBinding.LateGet(obj56, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj57, null, "SetValue", objArray, null, null, null, true);
				object obj58 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1Sitaji1" };
				object obj59 = NewLateBinding.LateGet(obj58, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj59, null, "SetValue", objArray, null, null, null, true);
				object obj60 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1Sitaji2" };
				object obj61 = NewLateBinding.LateGet(obj60, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj61, null, "SetValue", objArray, null, null, null, true);
				object obj62 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1Kage1" };
				object obj63 = NewLateBinding.LateGet(obj62, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj63, null, "SetValue", objArray, null, null, null, true);
				object obj64 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1Kage2" };
				object obj65 = NewLateBinding.LateGet(obj64, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj65, null, "SetValue", objArray, null, null, null, true);
				object obj66 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSitagi" };
				object obj67 = NewLateBinding.LateGet(obj66, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj67, null, "SetValue", objArray, null, null, null, true);
				object obj68 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSitagiSitaji1" };
				object obj69 = NewLateBinding.LateGet(obj68, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj69, null, "SetValue", objArray, null, null, null, true);
				object obj70 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSitagiSitaji2" };
				object obj71 = NewLateBinding.LateGet(obj70, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj71, null, "SetValue", objArray, null, null, null, true);
				object obj72 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSitagiKage1" };
				object obj73 = NewLateBinding.LateGet(obj72, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj73, null, "SetValue", objArray, null, null, null, true);
				object obj74 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSitagiKage2" };
				object obj75 = NewLateBinding.LateGet(obj74, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj75, null, "SetValue", objArray, null, null, null, true);
				object obj76 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSocks" };
				object obj77 = NewLateBinding.LateGet(obj76, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj77, null, "SetValue", objArray, null, null, null, true);
				object obj78 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numUtibaki" };
				object obj79 = NewLateBinding.LateGet(obj78, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 3 };
				NewLateBinding.LateCall(obj79, null, "SetValue", objArray, null, null, null, true);
				object obj80 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSotobaki" };
				object obj81 = NewLateBinding.LateGet(obj80, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 3 };
				NewLateBinding.LateCall(obj81, null, "SetValue", objArray, null, null, null, true);
				object obj82 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagJyoge" };
				object obj83 = NewLateBinding.LateGet(obj82, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj83, null, "SetValue", objArray, null, null, null, true);
				object obj84 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSitagi" };
				object obj85 = NewLateBinding.LateGet(obj84, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { true };
				NewLateBinding.LateCall(obj85, null, "SetValue", objArray, null, null, null, true);
				object obj86 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSkirt" };
				object obj87 = NewLateBinding.LateGet(obj86, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj87, null, "SetValue", objArray, null, null, null, true);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj1, 3, false))
			{
				object obj88 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop2" };
				object obj89 = NewLateBinding.LateGet(obj88, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj89, null, "SetValue", objArray, null, null, null, true);
				object obj90 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop3" };
				object obj91 = NewLateBinding.LateGet(obj90, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj91, null, "SetValue", objArray, null, null, null, true);
				object obj92 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop4" };
				object obj93 = NewLateBinding.LateGet(obj92, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj93, null, "SetValue", objArray, null, null, null, true);
				object obj94 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorBottom2" };
				object obj95 = NewLateBinding.LateGet(obj94, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj95, null, "SetValue", objArray, null, null, null, true);
				object obj96 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSocks" };
				object obj97 = NewLateBinding.LateGet(obj96, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 5 };
				NewLateBinding.LateCall(obj97, null, "SetValue", objArray, null, null, null, true);
				object obj98 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSkirt" };
				object obj99 = NewLateBinding.LateGet(obj98, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj99, null, "SetValue", objArray, null, null, null, true);
				object obj100 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numUtibaki" };
				object obj101 = NewLateBinding.LateGet(obj100, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 4 };
				NewLateBinding.LateCall(obj101, null, "SetValue", objArray, null, null, null, true);
				object obj102 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSotobaki" };
				object obj103 = NewLateBinding.LateGet(obj102, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 4 };
				NewLateBinding.LateCall(obj103, null, "SetValue", objArray, null, null, null, true);
				object obj104 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagJyoge" };
				object obj105 = NewLateBinding.LateGet(obj104, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj105, null, "SetValue", objArray, null, null, null, true);
				object obj106 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSitagi" };
				object obj107 = NewLateBinding.LateGet(obj106, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { true };
				NewLateBinding.LateCall(obj107, null, "SetValue", objArray, null, null, null, true);
				object obj108 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSkirt" };
				object obj109 = NewLateBinding.LateGet(obj108, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj109, null, "SetValue", objArray, null, null, null, true);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj1, 4, false))
			{
				object obj110 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numPansuto" };
				object obj111 = NewLateBinding.LateGet(obj110, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 255 };
				NewLateBinding.LateCall(obj111, null, "SetValue", objArray, null, null, null, true);
				object obj112 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorBottom2" };
				object obj113 = NewLateBinding.LateGet(obj112, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj113, null, "SetValue", objArray, null, null, null, true);
				object obj114 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSocks" };
				object obj115 = NewLateBinding.LateGet(obj114, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 1 };
				NewLateBinding.LateCall(obj115, null, "SetValue", objArray, null, null, null, true);
				object obj116 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSkirt" };
				object obj117 = NewLateBinding.LateGet(obj116, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj117, null, "SetValue", objArray, null, null, null, true);
				object obj118 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numUtibaki" };
				object obj119 = NewLateBinding.LateGet(obj118, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 1 };
				NewLateBinding.LateCall(obj119, null, "SetValue", objArray, null, null, null, true);
				object obj120 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSotobaki" };
				object obj121 = NewLateBinding.LateGet(obj120, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 2 };
				NewLateBinding.LateCall(obj121, null, "SetValue", objArray, null, null, null, true);
				object obj122 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagJyoge" };
				object obj123 = NewLateBinding.LateGet(obj122, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj123, null, "SetValue", objArray, null, null, null, true);
				object obj124 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSitagi" };
				object obj125 = NewLateBinding.LateGet(obj124, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { true };
				NewLateBinding.LateCall(obj125, null, "SetValue", objArray, null, null, null, true);
				object obj126 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSkirt" };
				object obj127 = NewLateBinding.LateGet(obj126, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { true };
				NewLateBinding.LateCall(obj127, null, "SetValue", objArray, null, null, null, true);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj1, 5, false))
			{
				object obj128 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numPansuto" };
				object obj129 = NewLateBinding.LateGet(obj128, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 255 };
				NewLateBinding.LateCall(obj129, null, "SetValue", objArray, null, null, null, true);
				object obj130 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSkirt" };
				object obj131 = NewLateBinding.LateGet(obj130, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj131, null, "SetValue", objArray, null, null, null, true);
				object obj132 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop1" };
				object obj133 = NewLateBinding.LateGet(obj132, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj133, null, "SetValue", objArray, null, null, null, true);
				object obj134 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop2" };
				object obj135 = NewLateBinding.LateGet(obj134, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj135, null, "SetValue", objArray, null, null, null, true);
				object obj136 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop3" };
				object obj137 = NewLateBinding.LateGet(obj136, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj137, null, "SetValue", objArray, null, null, null, true);
				object obj138 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop4" };
				object obj139 = NewLateBinding.LateGet(obj138, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj139, null, "SetValue", objArray, null, null, null, true);
				object obj140 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorBottom1" };
				object obj141 = NewLateBinding.LateGet(obj140, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj141, null, "SetValue", objArray, null, null, null, true);
				object obj142 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorBottom2" };
				object obj143 = NewLateBinding.LateGet(obj142, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj143, null, "SetValue", objArray, null, null, null, true);
				object obj144 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorSitagi" };
				object obj145 = NewLateBinding.LateGet(obj144, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj145, null, "SetValue", objArray, null, null, null, true);
				object obj146 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1" };
				object obj147 = NewLateBinding.LateGet(obj146, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj147, null, "SetValue", objArray, null, null, null, true);
				object obj148 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1Sitaji1" };
				object obj149 = NewLateBinding.LateGet(obj148, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj149, null, "SetValue", objArray, null, null, null, true);
				object obj150 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1Sitaji2" };
				object obj151 = NewLateBinding.LateGet(obj150, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj151, null, "SetValue", objArray, null, null, null, true);
				object obj152 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1Kage1" };
				object obj153 = NewLateBinding.LateGet(obj152, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj153, null, "SetValue", objArray, null, null, null, true);
				object obj154 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1Kage2" };
				object obj155 = NewLateBinding.LateGet(obj154, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj155, null, "SetValue", objArray, null, null, null, true);
				object obj156 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSitagi" };
				object obj157 = NewLateBinding.LateGet(obj156, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj157, null, "SetValue", objArray, null, null, null, true);
				object obj158 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSitagiSitaji1" };
				object obj159 = NewLateBinding.LateGet(obj158, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj159, null, "SetValue", objArray, null, null, null, true);
				object obj160 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSitagiSitaji2" };
				object obj161 = NewLateBinding.LateGet(obj160, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj161, null, "SetValue", objArray, null, null, null, true);
				object obj162 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSitagiKage1" };
				object obj163 = NewLateBinding.LateGet(obj162, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj163, null, "SetValue", objArray, null, null, null, true);
				object obj164 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSitagiKage2" };
				object obj165 = NewLateBinding.LateGet(obj164, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj165, null, "SetValue", objArray, null, null, null, true);
				object obj166 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSocks" };
				object obj167 = NewLateBinding.LateGet(obj166, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 1 };
				NewLateBinding.LateCall(obj167, null, "SetValue", objArray, null, null, null, true);
				object obj168 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numUtibaki" };
				object obj169 = NewLateBinding.LateGet(obj168, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 1 };
				NewLateBinding.LateCall(obj169, null, "SetValue", objArray, null, null, null, true);
				object obj170 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSotobaki" };
				object obj171 = NewLateBinding.LateGet(obj170, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 2 };
				NewLateBinding.LateCall(obj171, null, "SetValue", objArray, null, null, null, true);
				object obj172 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagJyoge" };
				object obj173 = NewLateBinding.LateGet(obj172, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj173, null, "SetValue", objArray, null, null, null, true);
				object obj174 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSitagi" };
				object obj175 = NewLateBinding.LateGet(obj174, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj175, null, "SetValue", objArray, null, null, null, true);
				object obj176 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSkirt" };
				object obj177 = NewLateBinding.LateGet(obj176, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj177, null, "SetValue", objArray, null, null, null, true);
			}
			else if (!Operators.ConditionalCompareObjectEqual(obj1, 6, false))
			{
				if (!Operators.ConditionalCompareObjectEqual(obj1, 7, false))
				{
					return;
				}
				object obj178 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numPansuto" };
				object obj179 = NewLateBinding.LateGet(obj178, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 255 };
				NewLateBinding.LateCall(obj179, null, "SetValue", objArray, null, null, null, true);
				object obj180 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorBottom2" };
				object obj181 = NewLateBinding.LateGet(obj180, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj181, null, "SetValue", objArray, null, null, null, true);
				object obj182 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSocks" };
				object obj183 = NewLateBinding.LateGet(obj182, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 1 };
				NewLateBinding.LateCall(obj183, null, "SetValue", objArray, null, null, null, true);
				object obj184 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSkirt" };
				object obj185 = NewLateBinding.LateGet(obj184, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj185, null, "SetValue", objArray, null, null, null, true);
				object obj186 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numUtibaki" };
				object obj187 = NewLateBinding.LateGet(obj186, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 1 };
				NewLateBinding.LateCall(obj187, null, "SetValue", objArray, null, null, null, true);
				object obj188 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSotobaki" };
				object obj189 = NewLateBinding.LateGet(obj188, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 2 };
				NewLateBinding.LateCall(obj189, null, "SetValue", objArray, null, null, null, true);
				object obj190 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagJyoge" };
				object obj191 = NewLateBinding.LateGet(obj190, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj191, null, "SetValue", objArray, null, null, null, true);
				object obj192 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSitagi" };
				object obj193 = NewLateBinding.LateGet(obj192, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { true };
				NewLateBinding.LateCall(obj193, null, "SetValue", objArray, null, null, null, true);
				object obj194 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSkirt" };
				object obj195 = NewLateBinding.LateGet(obj194, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { true };
				NewLateBinding.LateCall(obj195, null, "SetValue", objArray, null, null, null, true);
			}
			else
			{
				object obj196 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numPansuto" };
				object obj197 = NewLateBinding.LateGet(obj196, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 255 };
				NewLateBinding.LateCall(obj197, null, "SetValue", objArray, null, null, null, true);
				object obj198 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSkirt" };
				object obj199 = NewLateBinding.LateGet(obj198, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj199, null, "SetValue", objArray, null, null, null, true);
				object obj200 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop1" };
				object obj201 = NewLateBinding.LateGet(obj200, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj201, null, "SetValue", objArray, null, null, null, true);
				object obj202 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop2" };
				object obj203 = NewLateBinding.LateGet(obj202, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj203, null, "SetValue", objArray, null, null, null, true);
				object obj204 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop3" };
				object obj205 = NewLateBinding.LateGet(obj204, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj205, null, "SetValue", objArray, null, null, null, true);
				object obj206 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorTop4" };
				object obj207 = NewLateBinding.LateGet(obj206, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj207, null, "SetValue", objArray, null, null, null, true);
				object obj208 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorBottom1" };
				object obj209 = NewLateBinding.LateGet(obj208, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj209, null, "SetValue", objArray, null, null, null, true);
				object obj210 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "btnColorBottom2" };
				object obj211 = NewLateBinding.LateGet(obj210, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { Color.FromArgb(0, 0, 0, 0) };
				NewLateBinding.LateCall(obj211, null, "SetValue", objArray, null, null, null, true);
				object obj212 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1" };
				object obj213 = NewLateBinding.LateGet(obj212, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj213, null, "SetValue", objArray, null, null, null, true);
				object obj214 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1Sitaji1" };
				object obj215 = NewLateBinding.LateGet(obj214, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj215, null, "SetValue", objArray, null, null, null, true);
				object obj216 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1Sitaji2" };
				object obj217 = NewLateBinding.LateGet(obj216, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj217, null, "SetValue", objArray, null, null, null, true);
				object obj218 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1Kage1" };
				object obj219 = NewLateBinding.LateGet(obj218, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj219, null, "SetValue", objArray, null, null, null, true);
				object obj220 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numBottom1Kage2" };
				object obj221 = NewLateBinding.LateGet(obj220, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 0 };
				NewLateBinding.LateCall(obj221, null, "SetValue", objArray, null, null, null, true);
				object obj222 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSocks" };
				object obj223 = NewLateBinding.LateGet(obj222, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 1 };
				NewLateBinding.LateCall(obj223, null, "SetValue", objArray, null, null, null, true);
				object obj224 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numUtibaki" };
				object obj225 = NewLateBinding.LateGet(obj224, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 1 };
				NewLateBinding.LateCall(obj225, null, "SetValue", objArray, null, null, null, true);
				object obj226 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "numSotobaki" };
				object obj227 = NewLateBinding.LateGet(obj226, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { 2 };
				NewLateBinding.LateCall(obj227, null, "SetValue", objArray, null, null, null, true);
				object obj228 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagJyoge" };
				object obj229 = NewLateBinding.LateGet(obj228, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj229, null, "SetValue", objArray, null, null, null, true);
				object obj230 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSitagi" };
				object obj231 = NewLateBinding.LateGet(obj230, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { true };
				NewLateBinding.LateCall(obj231, null, "SetValue", objArray, null, null, null, true);
				object obj232 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objectValue = new object[] { "chkFlagSkirt" };
				object obj233 = NewLateBinding.LateGet(obj232, null, "Controls", objectValue, null, null, null);
				objArray = new object[] { false };
				NewLateBinding.LateCall(obj233, null, "SetValue", objArray, null, null, null, true);
			}
			object obj234 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
			objectValue = new object[] { "numSocks" };
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj234, null, "Controls", objectValue, null, null, null)), null);
			object obj235 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
			objectValue = new object[] { "numPansuto" };
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj235, null, "Controls", objectValue, null, null, null)), null);
			object obj236 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
			objectValue = new object[] { "numSkirt" };
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj236, null, "Controls", objectValue, null, null, null)), null);
			object obj237 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
			objectValue = new object[] { "numBottom1" };
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj237, null, "Controls", objectValue, null, null, null)), null);
			object obj238 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
			objectValue = new object[] { "numSitagi" };
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj238, null, "Controls", objectValue, null, null, null)), null);
			object obj239 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
			objectValue = new object[] { "numUtibaki" };
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj239, null, "Controls", objectValue, null, null, null)), null);
			object obj240 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
			objectValue = new object[] { "numSotobaki" };
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj240, null, "Controls", objectValue, null, null, null)), null);
			Button button = this.btnSubmit;
			object obj241 = sender;
			object objectValue1 = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj241, null, "Parent", new object[0], null, null, null));
			bool flag = this.CheckModified(ref objectValue1);
			objectValue = new object[] { RuntimeHelpers.GetObjectValue(objectValue1) };
			NewLateBinding.LateSetComplex(obj241, null, "Parent", objectValue, null, null, true, false);
			button.Enabled = flag;
		}

		private void btnSubmit_EnabledChanged(object sender, EventArgs e)
		{
			this.mnuSubmit.Enabled = Conversions.ToBoolean(NewLateBinding.LateGet(sender, null, "Enabled", new object[0], null, null, null));
		}

		private bool CheckModified(ref object argControl)
		{
			IEnumerator enumerator = null;
			bool modified = false;
			if (argControl == this.tbpTools)
			{
				modified = this.chkKousai.Modified;
			}
			else if (argControl is JG2NumericUpDown | argControl is JG2ComboBox | argControl is JG2Button | argControl is JG2CheckBox)
			{
				modified = Conversions.ToBoolean(NewLateBinding.LateGet(argControl, null, "Modified", new object[0], null, null, null));
			}
			else if (Operators.ConditionalCompareObjectGreater(NewLateBinding.LateGet(NewLateBinding.LateGet(argControl, null, "Controls", new object[0], null, null, null), null, "Count", new object[0], null, null, null), 0, false))
			{
				try
				{
					enumerator = ((IEnumerable)NewLateBinding.LateGet(argControl, null, "Controls", new object[0], null, null, null)).GetEnumerator();
					while (enumerator.MoveNext())
					{
						object objectValue = RuntimeHelpers.GetObjectValue(enumerator.Current);
						modified = this.CheckModified(ref objectValue);
						if (!modified)
						{
							continue;
						}
						return modified;
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
			}
			return modified;
		}

		private bool ClearAction(int argSeatNo)
		{
			long num;
			if (argSeatNo == JG2Importer.Common.MySaveData.Footer.SeatNoPlayer)
			{
				return false;
			}
			long index = (long)JG2Importer.Common.MySaveData.GetIndex(argSeatNo);
			if (index != (long)-1)
			{
				long num1 = (long)0;
				long koukandoCnt = (long)(checked(JG2Importer.Common.MySaveData.Seat[checked((int)index)].PlayData.KoukandoCnt - 1));
				num = num1;
				while (num <= koukandoCnt)
				{
					Seat.DefStruct2[] koukandoData = JG2Importer.Common.MySaveData.Seat[checked((int)index)].PlayData.KoukandoData;
					int num2 = checked((int)num);
					if (koukandoData[num2].SeatNo != JG2Importer.Common.MySaveData.Footer.SeatNoPlayer)
					{
						num = checked(num + (long)1);
					}
					else
					{
						koukandoData[num2].ActionCnt = 0;
						koukandoData[num2].ActionData = null;
						koukandoData[num2].PtsLove = 0;
						koukandoData[num2].PtsLike = 0;
						koukandoData[num2].PtsDislike = 0;
						koukandoData[num2].PtsHate = 0;
						this.ReCalcAction(ref JG2Importer.Common.MySaveData.Seat[checked((int)index)].PlayData.KoukandoData[checked((int)num)]);
						break;
					}
				}
			}
			index = (long)JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			if (index != (long)-1)
			{
				long num3 = (long)0;
				long koukandoCnt1 = (long)(checked(JG2Importer.Common.MySaveData.Seat[checked((int)index)].PlayData.KoukandoCnt - 1));
				num = num3;
				while (num <= koukandoCnt1)
				{
					Seat.DefStruct2[] defStruct2Array = JG2Importer.Common.MySaveData.Seat[checked((int)index)].PlayData.KoukandoData;
					int num4 = checked((int)num);
					if (defStruct2Array[num4].SeatNo != argSeatNo)
					{
						num = checked(num + (long)1);
					}
					else
					{
						defStruct2Array[num4].ActionCnt = 0;
						defStruct2Array[num4].ActionData = null;
						defStruct2Array[num4].PtsLove = 0;
						defStruct2Array[num4].PtsLike = 0;
						defStruct2Array[num4].PtsDislike = 0;
						defStruct2Array[num4].PtsHate = 0;
						this.ReCalcAction(ref JG2Importer.Common.MySaveData.Seat[checked((int)index)].PlayData.KoukandoData[checked((int)num)]);
						break;
					}
				}
			}
			return true;
		}

		public void Close(bool argCloseCheck)
		{
			this.CloseCheck = argCloseCheck;
			this.Close();
		}

		private void CreateCostumePanel()
		{
			int num = 30;
			this.tbpCostume.SuspendLayout();
			int num1 = 0;
			do
			{
				int num2 = 0;
				int num3 = -1;
				Panel panel = new Panel();
				panel.SuspendLayout();
				panel.Name = string.Concat("pnlCostume", Strings.Format(num1, ""));
				panel.TabIndex = checked(6 + num1);
				Point point = new Point(6, 41);
				panel.Location = point;
				System.Drawing.Size size = new System.Drawing.Size(448, 413);
				panel.Size = size;
				panel.Visible = false;
				num3 = checked(num3 + 1);
				Label label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(0, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionType;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				JG2NumericUpDown jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numType",
					TabIndex = num3
				};
				point = new Point(100, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)2147483647);
				jG2NumericUpDown.Minimum = new decimal((long)-2147483648);
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionType;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail.Item_Changed);
				frmDetail _frmDetail1 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail1.num_ValueChanged);
				panel.Controls.Add(jG2NumericUpDown);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					Name = "lblTypeName",
					TabIndex = num3
				};
				point = new Point(154, checked(num2 + 4));
				label.Location = point;
				size = new System.Drawing.Size(70, 18);
				label.Size = size;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				Button button = new Button()
				{
					Name = "btnDefault",
					TabIndex = num3,
					TabStop = false
				};
				point = new Point(checked(panel.Width - 80), num2);
				button.Location = point;
				size = new System.Drawing.Size(80, 26);
				button.Size = size;
				button.Text = Resources.ClothingDefValueText;
				button.UseVisualStyleBackColor = true;
				this.ToolTip.SetToolTip(button, Resources.HintDefaultButton);
				frmDetail _frmDetail2 = this;
				button.Click += new EventHandler(_frmDetail2.btnDefault_Click);
				panel.Controls.Add(button);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(0, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionSocks;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numSocks",
					TabIndex = num3
				};
				point = new Point(100, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)255);
				jG2NumericUpDown.Minimum = decimal.Zero;
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionSocks;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail3 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail3.Item_Changed);
				frmDetail _frmDetail4 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail4.num_ValueChanged);
				panel.Controls.Add(jG2NumericUpDown);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					Name = "lblSocksName",
					TabIndex = num3
				};
				point = new Point(154, checked(num2 + 4));
				label.Location = point;
				size = new System.Drawing.Size(100, 18);
				label.Size = size;
				panel.Controls.Add(label);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(0, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionPansuto;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numPansuto",
					TabIndex = num3
				};
				point = new Point(100, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)255);
				jG2NumericUpDown.Minimum = decimal.Zero;
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionPansuto;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail5 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail5.Item_Changed);
				frmDetail _frmDetail6 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail6.num_ValueChanged);
				panel.Controls.Add(jG2NumericUpDown);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					Name = "lblColorPansuto",
					TabIndex = num3
				};
				point = new Point(154, checked(num2 + 4));
				label.Location = point;
				size = new System.Drawing.Size(50, 18);
				label.Size = size;
				panel.Controls.Add(label);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(0, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionSkirt;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numSkirt",
					TabIndex = num3
				};
				point = new Point(100, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)255);
				jG2NumericUpDown.Minimum = decimal.Zero;
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionSkirt;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail7 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail7.Item_Changed);
				frmDetail _frmDetail8 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail8.num_ValueChanged);
				panel.Controls.Add(jG2NumericUpDown);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					Name = "lblSkirtName",
					TabIndex = num3
				};
				point = new Point(154, checked(num2 + 4));
				label.Location = point;
				size = new System.Drawing.Size(70, 18);
				label.Size = size;
				panel.Controls.Add(label);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(0, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionColorCostume;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				JG2Button jG2Button = new JG2Button()
				{
					Name = "btnColorTop1",
					TabIndex = num3
				};
				point = new Point(100, num2);
				jG2Button.Location = point;
				size = new System.Drawing.Size(50, 26);
				jG2Button.Size = size;
				jG2Button.Text = Strings.Left(Resources.CaptionColorTop1, 5);
				jG2Button.AllowDrop = true;
				jG2Button.UseVisualStyleBackColor = true;
				this.ToolTip.SetToolTip(jG2Button, Resources.HintColorButton);
				jG2Button.Caption = Resources.CaptionColorTop1;
				jG2Button.BatchButton = this.btnBatch;
				frmDetail _frmDetail9 = this;
				jG2Button.BackColorChanged += new EventHandler(_frmDetail9.Item_Changed);
				frmDetail _frmDetail10 = this;
				jG2Button.Click += new EventHandler(_frmDetail10.btnColor_Click);
				frmDetail _frmDetail11 = this;
				jG2Button.DragDrop += new DragEventHandler(_frmDetail11.btnColor_DragDrop);
				frmDetail _frmDetail12 = this;
				jG2Button.DragEnter += new DragEventHandler(_frmDetail12.btnColor_DragEnter);
				frmDetail _frmDetail13 = this;
				jG2Button.MouseDown += new MouseEventHandler(_frmDetail13.btnColor_MouseDown);
				panel.Controls.Add(jG2Button);
				num3 = checked(num3 + 1);
				jG2Button = new JG2Button()
				{
					Name = "btnColorTop2",
					TabIndex = num3
				};
				point = new Point(154, num2);
				jG2Button.Location = point;
				size = new System.Drawing.Size(50, 26);
				jG2Button.Size = size;
				jG2Button.Text = Strings.Left(Resources.CaptionColorTop2, 5);
				jG2Button.AllowDrop = true;
				jG2Button.UseVisualStyleBackColor = true;
				this.ToolTip.SetToolTip(jG2Button, Resources.HintColorButton);
				jG2Button.Caption = Resources.CaptionColorTop2;
				jG2Button.BatchButton = this.btnBatch;
				frmDetail _frmDetail14 = this;
				jG2Button.BackColorChanged += new EventHandler(_frmDetail14.Item_Changed);
				frmDetail _frmDetail15 = this;
				jG2Button.Click += new EventHandler(_frmDetail15.btnColor_Click);
				frmDetail _frmDetail16 = this;
				jG2Button.DragDrop += new DragEventHandler(_frmDetail16.btnColor_DragDrop);
				frmDetail _frmDetail17 = this;
				jG2Button.DragEnter += new DragEventHandler(_frmDetail17.btnColor_DragEnter);
				frmDetail _frmDetail18 = this;
				jG2Button.MouseDown += new MouseEventHandler(_frmDetail18.btnColor_MouseDown);
				panel.Controls.Add(jG2Button);
				num3 = checked(num3 + 1);
				jG2Button = new JG2Button()
				{
					Name = "btnColorTop3",
					TabIndex = num3
				};
				point = new Point(208, num2);
				jG2Button.Location = point;
				size = new System.Drawing.Size(50, 26);
				jG2Button.Size = size;
				jG2Button.Text = Strings.Left(Resources.CaptionColorTop3, 5);
				jG2Button.AllowDrop = true;
				jG2Button.UseVisualStyleBackColor = true;
				this.ToolTip.SetToolTip(jG2Button, Resources.HintColorButton);
				jG2Button.Caption = Resources.CaptionColorTop3;
				jG2Button.BatchButton = this.btnBatch;
				frmDetail _frmDetail19 = this;
				jG2Button.BackColorChanged += new EventHandler(_frmDetail19.Item_Changed);
				frmDetail _frmDetail20 = this;
				jG2Button.Click += new EventHandler(_frmDetail20.btnColor_Click);
				frmDetail _frmDetail21 = this;
				jG2Button.DragDrop += new DragEventHandler(_frmDetail21.btnColor_DragDrop);
				frmDetail _frmDetail22 = this;
				jG2Button.DragEnter += new DragEventHandler(_frmDetail22.btnColor_DragEnter);
				frmDetail _frmDetail23 = this;
				jG2Button.MouseDown += new MouseEventHandler(_frmDetail23.btnColor_MouseDown);
				panel.Controls.Add(jG2Button);
				num3 = checked(num3 + 1);
				jG2Button = new JG2Button()
				{
					Name = "btnColorTop4",
					TabIndex = num3
				};
				point = new Point(262, num2);
				jG2Button.Location = point;
				size = new System.Drawing.Size(50, 26);
				jG2Button.Size = size;
				jG2Button.Text = Strings.Left(Resources.CaptionColorTop4, 5);
				jG2Button.AllowDrop = true;
				jG2Button.UseVisualStyleBackColor = true;
				this.ToolTip.SetToolTip(jG2Button, Resources.HintColorButton);
				jG2Button.Caption = Resources.CaptionColorTop4;
				jG2Button.BatchButton = this.btnBatch;
				frmDetail _frmDetail24 = this;
				jG2Button.BackColorChanged += new EventHandler(_frmDetail24.Item_Changed);
				frmDetail _frmDetail25 = this;
				jG2Button.Click += new EventHandler(_frmDetail25.btnColor_Click);
				frmDetail _frmDetail26 = this;
				jG2Button.DragDrop += new DragEventHandler(_frmDetail26.btnColor_DragDrop);
				frmDetail _frmDetail27 = this;
				jG2Button.DragEnter += new DragEventHandler(_frmDetail27.btnColor_DragEnter);
				frmDetail _frmDetail28 = this;
				jG2Button.MouseDown += new MouseEventHandler(_frmDetail28.btnColor_MouseDown);
				panel.Controls.Add(jG2Button);
				num3 = checked(num3 + 1);
				jG2Button = new JG2Button()
				{
					Name = "btnColorBottom1",
					TabIndex = num3
				};
				point = new Point(316, num2);
				jG2Button.Location = point;
				size = new System.Drawing.Size(50, 26);
				jG2Button.Size = size;
				jG2Button.Text = Strings.Left(Resources.CaptionColorBottom1, 5);
				jG2Button.AllowDrop = true;
				jG2Button.UseVisualStyleBackColor = true;
				this.ToolTip.SetToolTip(jG2Button, Resources.HintColorButton);
				jG2Button.Caption = Resources.CaptionColorBottom1;
				jG2Button.BatchButton = this.btnBatch;
				frmDetail _frmDetail29 = this;
				jG2Button.BackColorChanged += new EventHandler(_frmDetail29.Item_Changed);
				frmDetail _frmDetail30 = this;
				jG2Button.Click += new EventHandler(_frmDetail30.btnColor_Click);
				frmDetail _frmDetail31 = this;
				jG2Button.DragDrop += new DragEventHandler(_frmDetail31.btnColor_DragDrop);
				frmDetail _frmDetail32 = this;
				jG2Button.DragEnter += new DragEventHandler(_frmDetail32.btnColor_DragEnter);
				frmDetail _frmDetail33 = this;
				jG2Button.MouseDown += new MouseEventHandler(_frmDetail33.btnColor_MouseDown);
				panel.Controls.Add(jG2Button);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				jG2Button = new JG2Button()
				{
					Name = "btnColorBottom2",
					TabIndex = num3
				};
				point = new Point(100, num2);
				jG2Button.Location = point;
				size = new System.Drawing.Size(50, 26);
				jG2Button.Size = size;
				jG2Button.Text = Strings.Left(Resources.CaptionColorBottom2, 5);
				jG2Button.AllowDrop = true;
				jG2Button.UseVisualStyleBackColor = true;
				this.ToolTip.SetToolTip(jG2Button, Resources.HintColorButton);
				jG2Button.Caption = Resources.CaptionColorBottom2;
				jG2Button.BatchButton = this.btnBatch;
				frmDetail _frmDetail34 = this;
				jG2Button.BackColorChanged += new EventHandler(_frmDetail34.Item_Changed);
				frmDetail _frmDetail35 = this;
				jG2Button.Click += new EventHandler(_frmDetail35.btnColor_Click);
				frmDetail _frmDetail36 = this;
				jG2Button.DragDrop += new DragEventHandler(_frmDetail36.btnColor_DragDrop);
				frmDetail _frmDetail37 = this;
				jG2Button.DragEnter += new DragEventHandler(_frmDetail37.btnColor_DragEnter);
				frmDetail _frmDetail38 = this;
				jG2Button.MouseDown += new MouseEventHandler(_frmDetail38.btnColor_MouseDown);
				panel.Controls.Add(jG2Button);
				num3 = checked(num3 + 1);
				jG2Button = new JG2Button()
				{
					Name = "btnColorSitagi",
					TabIndex = num3
				};
				point = new Point(154, num2);
				jG2Button.Location = point;
				size = new System.Drawing.Size(50, 26);
				jG2Button.Size = size;
				jG2Button.Text = Strings.Left(Resources.CaptionColorSitagi, 2);
				jG2Button.AllowDrop = true;
				jG2Button.UseVisualStyleBackColor = true;
				this.ToolTip.SetToolTip(jG2Button, Resources.HintColorButton);
				jG2Button.Caption = Resources.CaptionColorSitagi;
				jG2Button.BatchButton = this.btnBatch;
				frmDetail _frmDetail39 = this;
				jG2Button.BackColorChanged += new EventHandler(_frmDetail39.Item_Changed);
				frmDetail _frmDetail40 = this;
				jG2Button.Click += new EventHandler(_frmDetail40.btnColor_Click);
				frmDetail _frmDetail41 = this;
				jG2Button.DragDrop += new DragEventHandler(_frmDetail41.btnColor_DragDrop);
				frmDetail _frmDetail42 = this;
				jG2Button.DragEnter += new DragEventHandler(_frmDetail42.btnColor_DragEnter);
				frmDetail _frmDetail43 = this;
				jG2Button.MouseDown += new MouseEventHandler(_frmDetail43.btnColor_MouseDown);
				panel.Controls.Add(jG2Button);
				num3 = checked(num3 + 1);
				jG2Button = new JG2Button()
				{
					Name = "btnColorSocks",
					TabIndex = num3
				};
				point = new Point(208, num2);
				jG2Button.Location = point;
				size = new System.Drawing.Size(50, 26);
				jG2Button.Size = size;
				jG2Button.Text = Strings.Left(Resources.CaptionColorSocks, 2);
				jG2Button.AllowDrop = true;
				jG2Button.UseVisualStyleBackColor = true;
				this.ToolTip.SetToolTip(jG2Button, Resources.HintColorButton);
				jG2Button.Caption = Resources.CaptionColorSocks;
				jG2Button.BatchButton = this.btnBatch;
				frmDetail _frmDetail44 = this;
				jG2Button.BackColorChanged += new EventHandler(_frmDetail44.Item_Changed);
				frmDetail _frmDetail45 = this;
				jG2Button.Click += new EventHandler(_frmDetail45.btnColor_Click);
				frmDetail _frmDetail46 = this;
				jG2Button.DragDrop += new DragEventHandler(_frmDetail46.btnColor_DragDrop);
				frmDetail _frmDetail47 = this;
				jG2Button.DragEnter += new DragEventHandler(_frmDetail47.btnColor_DragEnter);
				frmDetail _frmDetail48 = this;
				jG2Button.MouseDown += new MouseEventHandler(_frmDetail48.btnColor_MouseDown);
				panel.Controls.Add(jG2Button);
				num3 = checked(num3 + 1);
				jG2Button = new JG2Button()
				{
					Name = "btnColorUtibaki",
					TabIndex = num3
				};
				point = new Point(262, num2);
				jG2Button.Location = point;
				size = new System.Drawing.Size(50, 26);
				jG2Button.Size = size;
				jG2Button.Text = Strings.Left(Resources.CaptionColorUtibaki, 2);
				jG2Button.AllowDrop = true;
				jG2Button.UseVisualStyleBackColor = true;
				this.ToolTip.SetToolTip(jG2Button, Resources.HintColorButton);
				jG2Button.Caption = Resources.CaptionColorUtibaki;
				jG2Button.BatchButton = this.btnBatch;
				frmDetail _frmDetail49 = this;
				jG2Button.BackColorChanged += new EventHandler(_frmDetail49.Item_Changed);
				frmDetail _frmDetail50 = this;
				jG2Button.Click += new EventHandler(_frmDetail50.btnColor_Click);
				frmDetail _frmDetail51 = this;
				jG2Button.DragDrop += new DragEventHandler(_frmDetail51.btnColor_DragDrop);
				frmDetail _frmDetail52 = this;
				jG2Button.DragEnter += new DragEventHandler(_frmDetail52.btnColor_DragEnter);
				frmDetail _frmDetail53 = this;
				jG2Button.MouseDown += new MouseEventHandler(_frmDetail53.btnColor_MouseDown);
				panel.Controls.Add(jG2Button);
				num3 = checked(num3 + 1);
				jG2Button = new JG2Button()
				{
					Name = "btnColorSotobaki",
					TabIndex = num3
				};
				point = new Point(316, num2);
				jG2Button.Location = point;
				size = new System.Drawing.Size(50, 26);
				jG2Button.Size = size;
				jG2Button.Text = Strings.Left(Resources.CaptionColorSotobaki, 2);
				jG2Button.AllowDrop = true;
				jG2Button.UseVisualStyleBackColor = true;
				this.ToolTip.SetToolTip(jG2Button, Resources.HintColorButton);
				jG2Button.Caption = Resources.CaptionColorSotobaki;
				jG2Button.BatchButton = this.btnBatch;
				frmDetail _frmDetail54 = this;
				jG2Button.BackColorChanged += new EventHandler(_frmDetail54.Item_Changed);
				frmDetail _frmDetail55 = this;
				jG2Button.Click += new EventHandler(_frmDetail55.btnColor_Click);
				frmDetail _frmDetail56 = this;
				jG2Button.DragDrop += new DragEventHandler(_frmDetail56.btnColor_DragDrop);
				frmDetail _frmDetail57 = this;
				jG2Button.DragEnter += new DragEventHandler(_frmDetail57.btnColor_DragEnter);
				frmDetail _frmDetail58 = this;
				jG2Button.MouseDown += new MouseEventHandler(_frmDetail58.btnColor_MouseDown);
				panel.Controls.Add(jG2Button);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(0, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionDesign;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(6, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionBottom1Sitaji;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numBottom1Sitaji1",
					TabIndex = num3
				};
				point = new Point(100, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)2147483647);
				jG2NumericUpDown.Minimum = new decimal((long)-2147483648);
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionSitagiSitaji1;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail59 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail59.Item_Changed);
				panel.Controls.Add(jG2NumericUpDown);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numBottom1Sitaji2",
					TabIndex = num3
				};
				point = new Point(154, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)2147483647);
				jG2NumericUpDown.Minimum = new decimal((long)-2147483648);
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionSitagiSitaji2;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail60 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail60.Item_Changed);
				panel.Controls.Add(jG2NumericUpDown);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(6, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionBottom1Kage;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numBottom1Kage1",
					TabIndex = num3
				};
				point = new Point(100, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)2147483647);
				jG2NumericUpDown.Minimum = new decimal((long)-2147483648);
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionSitagiKage1;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail61 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail61.Item_Changed);
				panel.Controls.Add(jG2NumericUpDown);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numBottom1Kage2",
					TabIndex = num3
				};
				point = new Point(154, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)2147483647);
				jG2NumericUpDown.Minimum = new decimal((long)-2147483648);
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionSitagiKage2;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail62 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail62.Item_Changed);
				panel.Controls.Add(jG2NumericUpDown);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(6, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionBottom1Design;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numBottom1",
					TabIndex = num3
				};
				point = new Point(100, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)2147483647);
				jG2NumericUpDown.Minimum = new decimal((long)-2147483648);
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionBottom1Design;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail63 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail63.Item_Changed);
				frmDetail _frmDetail64 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail64.num_ValueChanged);
				panel.Controls.Add(jG2NumericUpDown);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					Name = "lblBottom1Name",
					TabIndex = num3
				};
				point = new Point(154, checked(num2 + 4));
				label.Location = point;
				size = new System.Drawing.Size(70, 18);
				label.Size = size;
				panel.Controls.Add(label);
				num2 = checked(7 * num);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(230, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionSitagiSitaji;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numSitagiSitaji1",
					TabIndex = num3
				};
				point = new Point(324, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)2147483647);
				jG2NumericUpDown.Minimum = new decimal((long)-2147483648);
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionSitagiSitaji1;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail65 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail65.Item_Changed);
				panel.Controls.Add(jG2NumericUpDown);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numSitagiSitaji2",
					TabIndex = num3
				};
				point = new Point(378, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)2147483647);
				jG2NumericUpDown.Minimum = new decimal((long)-2147483648);
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionSitagiSitaji2;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail66 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail66.Item_Changed);
				panel.Controls.Add(jG2NumericUpDown);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(230, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionSitagiKage;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numSitagiKage1",
					TabIndex = num3
				};
				point = new Point(324, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)2147483647);
				jG2NumericUpDown.Minimum = new decimal((long)-2147483648);
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionSitagiKage1;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail67 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail67.Item_Changed);
				panel.Controls.Add(jG2NumericUpDown);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numSitagiKage2",
					TabIndex = num3
				};
				point = new Point(378, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)2147483647);
				jG2NumericUpDown.Minimum = new decimal((long)-2147483648);
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionSitagiKage2;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail68 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail68.Item_Changed);
				panel.Controls.Add(jG2NumericUpDown);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(230, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionSitagiDesign;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numSitagi",
					TabIndex = num3
				};
				point = new Point(324, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)2147483647);
				jG2NumericUpDown.Minimum = new decimal((long)-2147483648);
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionSitagiDesign;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail69 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail69.Item_Changed);
				frmDetail _frmDetail70 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail70.num_ValueChanged);
				panel.Controls.Add(jG2NumericUpDown);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					Name = "lblSitagiName",
					TabIndex = num3
				};
				point = new Point(378, checked(num2 + 4));
				label.Location = point;
				size = new System.Drawing.Size(70, 18);
				label.Size = size;
				panel.Controls.Add(label);
				num2 = checked(num2 + checked(2 * num));
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(0, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionUtibaki;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numUtibaki",
					TabIndex = num3
				};
				point = new Point(100, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)255);
				jG2NumericUpDown.Minimum = decimal.Zero;
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionUtibaki;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail71 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail71.Item_Changed);
				frmDetail _frmDetail72 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail72.num_ValueChanged);
				panel.Controls.Add(jG2NumericUpDown);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					Name = "lblUtibakiName",
					TabIndex = num3
				};
				point = new Point(154, checked(num2 + 4));
				label.Location = point;
				size = new System.Drawing.Size(70, 18);
				label.Size = size;
				panel.Controls.Add(label);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					TabIndex = num3
				};
				point = new Point(0, checked(num2 + 4));
				label.Location = point;
				label.Text = Resources.CaptionSotobaki;
				label.AutoSize = true;
				panel.Controls.Add(label);
				num3 = checked(num3 + 1);
				jG2NumericUpDown = new JG2NumericUpDown()
				{
					Name = "numSotobaki",
					TabIndex = num3
				};
				point = new Point(100, checked(num2 + 1));
				jG2NumericUpDown.Location = point;
				size = new System.Drawing.Size(50, 25);
				jG2NumericUpDown.Size = size;
				jG2NumericUpDown.Maximum = new decimal((long)255);
				jG2NumericUpDown.Minimum = decimal.Zero;
				jG2NumericUpDown.TextAlign = HorizontalAlignment.Right;
				jG2NumericUpDown.Caption = Resources.CaptionSotobaki;
				jG2NumericUpDown.BatchButton = this.btnBatch;
				frmDetail _frmDetail73 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail73.Item_Changed);
				frmDetail _frmDetail74 = this;
				jG2NumericUpDown.ValueChanged += new EventHandler(_frmDetail74.num_ValueChanged);
				panel.Controls.Add(jG2NumericUpDown);
				num3 = checked(num3 + 1);
				label = new Label()
				{
					Name = "lblSotobakiName",
					TabIndex = num3
				};
				point = new Point(154, checked(num2 + 4));
				label.Location = point;
				size = new System.Drawing.Size(70, 18);
				label.Size = size;
				panel.Controls.Add(label);
				num2 = checked(num2 + num);
				num3 = checked(num3 + 1);
				JG2CheckBox jG2CheckBox = new JG2CheckBox()
				{
					Name = "chkFlagJyoge",
					TabIndex = num3
				};
				point = new Point(3, checked(num2 + 3));
				jG2CheckBox.Location = point;
				size = new System.Drawing.Size(96, 20);
				jG2CheckBox.Size = size;
				jG2CheckBox.Text = Resources.CaptionFlagJyoge;
				jG2CheckBox.UseVisualStyleBackColor = true;
				jG2CheckBox.Caption = Resources.CaptionFlagJyoge;
				jG2CheckBox.BatchButton = this.btnBatch;
				frmDetail _frmDetail75 = this;
				jG2CheckBox.CheckedChanged += new EventHandler(_frmDetail75.Item_Changed);
				panel.Controls.Add(jG2CheckBox);
				num3 = checked(num3 + 1);
				jG2CheckBox = new JG2CheckBox()
				{
					Name = "chkFlagSitagi",
					TabIndex = num3
				};
				point = new Point(103, checked(num2 + 3));
				jG2CheckBox.Location = point;
				size = new System.Drawing.Size(96, 20);
				jG2CheckBox.Size = size;
				jG2CheckBox.Text = Resources.CaptionFlagSitagi;
				jG2CheckBox.UseVisualStyleBackColor = true;
				jG2CheckBox.Caption = Resources.CaptionFlagSitagi;
				jG2CheckBox.BatchButton = this.btnBatch;
				frmDetail _frmDetail76 = this;
				jG2CheckBox.CheckedChanged += new EventHandler(_frmDetail76.Item_Changed);
				panel.Controls.Add(jG2CheckBox);
				num3 = checked(num3 + 1);
				jG2CheckBox = new JG2CheckBox()
				{
					Name = "chkFlagSkirt",
					TabIndex = num3
				};
				point = new Point(203, checked(num2 + 3));
				jG2CheckBox.Location = point;
				size = new System.Drawing.Size(96, 20);
				jG2CheckBox.Size = size;
				jG2CheckBox.Text = Strings.Left(Resources.CaptionFlagSkirt, 4);
				jG2CheckBox.UseVisualStyleBackColor = true;
				jG2CheckBox.Caption = Resources.CaptionFlagSkirt;
				jG2CheckBox.BatchButton = this.btnBatch;
				frmDetail _frmDetail77 = this;
				jG2CheckBox.CheckedChanged += new EventHandler(_frmDetail77.Item_Changed);
				panel.Controls.Add(jG2CheckBox);
				panel.PerformLayout();
				panel.ResumeLayout();
				this.tbpCostume.Controls.Add(panel);
				num1 = checked(num1 + 1);
			}
			while (num1 <= 3);
			this.tbpCostume.PerformLayout();
			this.tbpCostume.ResumeLayout();
		}

		private void CreateSelectConditionEvent(ref ToolStripMenuItem argSelectCondition)
		{
			int count = checked(argSelectCondition.DropDownItems.Count - 1);
			for (int i = 0; i <= count; i = checked(i + 1))
			{
				bool flag = true;
				if (flag == argSelectCondition.DropDownItems[i] is JG2ToolStripMenuItem)
				{
					ToolStripMenuItem item = (ToolStripMenuItem)argSelectCondition.DropDownItems[i];
					if (item.DropDownItems.Count > 0)
					{
						((JG2ToolStripMenuItem)item.DropDownItems[0]).Checked = true;
						int num = checked(item.DropDownItems.Count - 1);
						for (int j = 0; j <= num; j = checked(j + 1))
						{
							frmDetail _frmDetail = this;
							item.DropDownItems[j].Click += new EventHandler(_frmDetail.mnuSelectAll_Click);
						}
					}
				}
				else if (flag == argSelectCondition.DropDownItems[i] is ToolStripMenuItem)
				{
					frmDetail _frmDetail1 = this;
					argSelectCondition.DropDownItems[i].Click += new EventHandler(_frmDetail1.mnuSelectAll_Click);
				}
			}
		}

		private bool DelAction(int argSeatNo, JG2Importer.JG2.Action argAction, bool argReverse)
		{
			int index;
			int num;
			if (argSeatNo == JG2Importer.Common.MySaveData.Footer.SeatNoPlayer)
			{
				return false;
			}
			if (!argReverse)
			{
				index = JG2Importer.Common.MySaveData.GetIndex(argSeatNo);
				num = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			}
			else
			{
				index = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
				num = JG2Importer.Common.MySaveData.GetIndex(argSeatNo);
			}
			if (!(index == -1 | num == -1))
			{
				int koukandoCnt = checked(JG2Importer.Common.MySaveData.Seat[index].PlayData.KoukandoCnt - 1);
				int num1 = 0;
				while (num1 <= koukandoCnt)
				{
					Seat.DefStruct2[] koukandoData = JG2Importer.Common.MySaveData.Seat[index].PlayData.KoukandoData;
					int num2 = num1;
					if (koukandoData[num2].SeatNo != JG2Importer.Common.MySaveData.Seat[num].SeatNo)
					{
						num1 = checked(num1 + 1);
					}
					else
					{
						int num3 = 0;
						int actionCnt = checked(koukandoData[num2].ActionCnt - 1);
						for (int i = 0; i <= actionCnt; i = checked(i + 1))
						{
							if (koukandoData[num2].ActionData[i] != (int)argAction)
							{
								num3 = checked(num3 + 1);
								Array.Copy(koukandoData[num2].ActionData, i, koukandoData[num2].ActionData, checked(num3 - 1), 1);
							}
						}
						if (num3 != 0)
						{
							Array.Resize<int>(ref koukandoData[num2].ActionData, num3);
						}
						else
						{
							koukandoData[num2].ActionData = null;
						}
						koukandoData[num2].ActionCnt = num3;
						switch (argAction)
						{
							case JG2Importer.JG2.Action.Love:
							{
								koukandoData[num2].PtsLove = 0;
								break;
							}
							case JG2Importer.JG2.Action.Like:
							{
								koukandoData[num2].PtsLike = 0;
								break;
							}
							case JG2Importer.JG2.Action.Dislike:
							{
								koukandoData[num2].PtsDislike = 0;
								break;
							}
							case JG2Importer.JG2.Action.Hate:
							{
								koukandoData[num2].PtsHate = 0;
								break;
							}
						}
						this.ReCalcAction(ref JG2Importer.Common.MySaveData.Seat[index].PlayData.KoukandoData[num1]);
						break;
					}
				}
			}
			return true;
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool DrawDetail(int argSeatNo)
		{
			bool flag;
			string str;
			string str1;
			this.PrevSeatNo = this.SeatNo;
			this.SeatNo = argSeatNo;
			this.btnSubmit.Enabled = false;
			int index = JG2Importer.Common.MySaveData.GetIndex(argSeatNo);
			int num = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			if (!(index == -1 | num == -1))
			{
				try
				{
					this.Icon = JG2Importer.JG2.Common.GetIcon((Bitmap)JG2Importer.Common.MySaveData.Seat[index].Character.ThumbnailImage);
					MyProject.Forms.frmClass.Icon = this.Icon;
					this.Text = JG2Importer.Common.MySaveData.Seat[index].Character.FullName;
					this.Sex = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sex;
					this.cmbHeight.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Height);
					this.cmbStyle.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Style);
					this.numAtama1.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Atama1));
					this.numAtama2.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Atama2));
					this.numWaist.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Waist));
					this.numMune1.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune1));
					this.cmbMune2.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune2);
					this.numMune3.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune3));
					this.numMune4.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune4));
					this.numMune5.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune5));
					this.numMune6.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune6));
					this.numMune7.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune7));
					this.cmbMune8.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune8);
					this.numNyurin.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Nyurin));
					this.btnColorSkin2.Initialize(Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorSkin2));
					this.cmbTikubi1.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tikubi1));
					this.cmbTikubi2.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tikubi2));
					this.numTikubi3.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tikubi3));
					this.cmbHiyake1.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hiyake1));
					this.numHiyake2.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hiyake2));
					this.cmbMosaic.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mosaic));
					this.cmbUnder1.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Under1));
					this.numUnder2.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Under2));
					this.btnColorUnder.Initialize(Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorUnder));
					this.cmbKao.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kao));
					this.numEye1.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Eye1));
					this.numEye2.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Eye2));
					this.numEye3.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Eye3));
					this.numEye4.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Eye4));
					this.numEye5.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Eye5));
					this.cmbPupil1.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Pupil1);
					this.numPupil2.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Pupil2));
					this.numPupil3.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Pupil3));
					this.numPupil4.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Pupil4));
					this.cmbPupil5.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Pupil5);
					this.cmbHighlight1.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Highlight1);
					this.btnColorEye1.Initialize(Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorEye1));
					this.btnColorEye2.Initialize(Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorEye2));
					this.cmbMayu1.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mayu1);
					this.numMayu2.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mayu2));
					this.btnColorMayu.Initialize(Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorMayu));
					this.cmbMabuta.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mabuta);
					this.cmbMatuge1.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Matuge1);
					this.cmbMatuge2.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Matuge2);
					this.cmbGlasses2.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Glasses2));
					this.btnColorGlasses.Initialize(Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorGlasses));
					this.chkHokuro1.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hokuro1 == 0 ? false : true));
					this.chkHokuro2.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hokuro2 == 0 ? false : true));
					this.chkHokuro3.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hokuro3 == 0 ? false : true));
					this.chkHokuro4.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hokuro4 == 0 ? false : true));
					this.cmbLip1.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Lip1));
					this.numLip2.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Lip2));
					this.numHair1.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hair1));
					this.numHairAdj1.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairAdj1));
					this.chkHairRev1.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairRev1 == 0 ? false : true));
					this.numHair2.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hair2));
					this.numHairAdj2.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairAdj2));
					this.chkHairRev2.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairRev2 == 0 ? false : true));
					this.numHair3.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hair3));
					this.numHairAdj3.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairAdj3));
					this.chkHairRev3.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairRev3 == 0 ? false : true));
					this.numHair4.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hair4));
					this.numHairAdj4.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairAdj4));
					this.chkHairRev4.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairRev4 == 0 ? false : true));
					this.btnColorHair.Initialize(Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorHair));
					this.numTiryoku3.Initialize(new decimal(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tiryoku3));
					this.numTairyoku3.Initialize(new decimal(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tairyoku3));
					this.cmbBukatu1.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bukatu1);
					this.numBukatu3.Initialize(new decimal(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bukatu3));
					this.cmbSyakousei.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Syakousei);
					this.cmbKenka.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kenka);
					this.cmbTeisou.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Teisou);
					this.cmbSeiai.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seiai);
					this.chkSeikou1.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikou1 == 0 ? false : true));
					this.chkSeikou2.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikou2 == 0 ? false : true));
					this.cmbVoice.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Voice);
					this.numSeikaku.Initialize(new decimal((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikaku));
					this.chkCyoroi.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Cyoroi == 0 ? false : true));
					this.chkNekketu.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Nekketu == 0 ? false : true));
					this.chkNigate1.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Nigate1 == 0 ? false : true));
					this.chkNigate2.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Nigate2 == 0 ? false : true));
					this.chkCharm.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Charm == 0 ? false : true));
					this.chkTundere.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tundere == 0 ? false : true));
					this.chkKyouki.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kyouki == 0 ? false : true));
					this.chkMiha.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Miha == 0 ? false : true));
					this.chkSunao.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sunao == 0 ? false : true));
					this.chkMaemuki.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Maemuki == 0 ? false : true));
					this.chkTereya.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tereya == 0 ? false : true));
					this.chkYakimochi.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Yakimochi == 0 ? false : true));
					this.chkToufu.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Toufu == 0 ? false : true));
					this.chkSukebe.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sukebe == 0 ? false : true));
					this.chkMajime.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Majime == 0 ? false : true));
					this.chkCool.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Cool == 0 ? false : true));
					this.chkTyokujyo.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tyokujyo == 0 ? false : true));
					this.chkPoyayan.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Poyayan == 0 ? false : true));
					this.chkBouryoku.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bouryoku == 0 ? false : true));
					this.chkSousyoku.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sousyoku == 0 ? false : true));
					this.chkSewayaki.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sewayaki == 0 ? false : true));
					this.chkIintyou.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Iintyou == 0 ? false : true));
					this.chkOsyaberi.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Osyaberi == 0 ? false : true));
					this.chkHarapeko.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Harapeko == 0 ? false : true));
					this.chkRenai.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Renai == 0 ? false : true));
					this.chkItizu.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Itizu == 0 ? false : true));
					this.chkYuujyufudan.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Yuujyufudan == 0 ? false : true));
					this.chkMakenki.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Makenki == 0 ? false : true));
					this.chkHaraguro.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Haraguro == 0 ? false : true));
					this.chkKinben.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kinben == 0 ? false : true));
					this.chkHonpou.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Honpou == 0 ? false : true));
					this.chkM.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.M == 0 ? false : true));
					this.chkAsekaki.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Asekaki == 0 ? false : true));
					this.chkYami.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Yami == 0 ? false : true));
					this.chkNantyo.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Nantyo == 0 ? false : true));
					this.chkYowami.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Yowami == 0 ? false : true));
					this.chkMiseityo.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Miseityo == 0 ? false : true));
					this.chkLuck.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Luck == 0 ? false : true));
					this.chkRare.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Rare == 0 ? false : true));
					this.chkKiss.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kiss == 0 ? false : true));
					this.chkBust.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bust == 0 ? false : true));
					this.chkSeiki1.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seiki1 == 0 ? false : true));
					this.chkSeiki2.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seiki2 == 0 ? false : true));
					this.chkCunnilingus.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Cunnilingus == 0 ? false : true));
					this.chkFellatio.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Fellatio == 0 ? false : true));
					this.chkBack.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Back == 0 ? false : true));
					this.chkOnna.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Onna == 0 ? false : true));
					this.chkAnal.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Anal == 0 ? false : true));
					this.chkNama.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Nama == 0 ? false : true));
					this.chkSeiin.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seiin == 0 ? false : true));
					this.chkNaka.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Naka == 0 ? false : true));
					this.chkKake.Initialize((JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kake == 0 ? false : true));
					this.cmbSunday1.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[0]);
					this.cmbMonday1.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[1]);
					this.cmbTuesday1.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[2]);
					this.cmbWednesday1.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[3]);
					this.cmbThursday1.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[4]);
					this.cmbFriday1.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[5]);
					this.cmbSaturday1.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[6]);
					this.cmbSunday2.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[7]);
					this.cmbMonday2.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[8]);
					this.cmbTuesday2.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[9]);
					this.cmbWednesday2.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[10]);
					this.cmbThursday2.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[11]);
					this.cmbFriday2.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[12]);
					this.cmbSaturday2.Initialize((int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[13]);
					int num1 = 0;
					do
					{
						object costumeControl = this.GetCostumeControl("numType", num1);
						object[] type = new object[1];
						Character.DefCostume[] costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						int num2 = num1;
						type[0] = costume[num2].Type;
						object[] socks = type;
						bool[] flagArray = new bool[] { true };
						NewLateBinding.LateCall(costumeControl, null, "Initialize", socks, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].Type = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(socks[0]), typeof(int));
						}
						object obj = this.GetCostumeControl("numSocks", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].Socks;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(obj, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].Socks = (byte)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(byte));
						}
						object costumeControl1 = this.GetCostumeControl("numPansuto", num1);
						socks = new object[1];
						byte[] colorSocks = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorSocks;
						num2 = 3;
						socks[0] = colorSocks[num2];
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(costumeControl1, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							colorSocks[num2] = (byte)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(byte));
						}
						object obj1 = this.GetCostumeControl("numSkirt", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].Skirt;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(obj1, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].Skirt = (byte)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(byte));
						}
						object costumeControl2 = this.GetCostumeControl("btnColorTop1", num1);
						socks = new object[] { Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorTop1) };
						NewLateBinding.LateCall(costumeControl2, null, "Initialize", socks, null, null, null, true);
						object obj2 = this.GetCostumeControl("btnColorTop2", num1);
						socks = new object[] { Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorTop2) };
						NewLateBinding.LateCall(obj2, null, "Initialize", socks, null, null, null, true);
						object costumeControl3 = this.GetCostumeControl("btnColorTop3", num1);
						socks = new object[] { Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorTop3) };
						NewLateBinding.LateCall(costumeControl3, null, "Initialize", socks, null, null, null, true);
						object obj3 = this.GetCostumeControl("btnColorTop4", num1);
						socks = new object[] { Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorTop4) };
						NewLateBinding.LateCall(obj3, null, "Initialize", socks, null, null, null, true);
						object costumeControl4 = this.GetCostumeControl("btnColorBottom1", num1);
						socks = new object[] { Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorBottom1) };
						NewLateBinding.LateCall(costumeControl4, null, "Initialize", socks, null, null, null, true);
						object obj4 = this.GetCostumeControl("btnColorBottom2", num1);
						socks = new object[] { Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorBottom2) };
						NewLateBinding.LateCall(obj4, null, "Initialize", socks, null, null, null, true);
						object costumeControl5 = this.GetCostumeControl("btnColorSitagi", num1);
						socks = new object[] { Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorSitagi) };
						NewLateBinding.LateCall(costumeControl5, null, "Initialize", socks, null, null, null, true);
						object obj5 = this.GetCostumeControl("btnColorSocks", num1);
						socks = new object[] { Color.FromArgb(255, (int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorSocks[2], (int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorSocks[1], (int)JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorSocks[0]) };
						NewLateBinding.LateCall(obj5, null, "Initialize", socks, null, null, null, true);
						object costumeControl6 = this.GetCostumeControl("btnColorUtibaki", num1);
						socks = new object[] { Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorUtibaki) };
						NewLateBinding.LateCall(costumeControl6, null, "Initialize", socks, null, null, null, true);
						object obj6 = this.GetCostumeControl("btnColorSotobaki", num1);
						socks = new object[] { Color.FromArgb(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorSotobaki) };
						NewLateBinding.LateCall(obj6, null, "Initialize", socks, null, null, null, true);
						object costumeControl7 = this.GetCostumeControl("numBottom1Sitaji1", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].Bottom1Sitaji1;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(costumeControl7, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].Bottom1Sitaji1 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
						}
						object obj7 = this.GetCostumeControl("numBottom1Sitaji2", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].Bottom1Sitaji2;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(obj7, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].Bottom1Sitaji2 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
						}
						object costumeControl8 = this.GetCostumeControl("numBottom1Kage1", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].Bottom1Kage1;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(costumeControl8, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].Bottom1Kage1 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
						}
						object obj8 = this.GetCostumeControl("numBottom1Kage2", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].Bottom1Kage2;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(obj8, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].Bottom1Kage2 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
						}
						object costumeControl9 = this.GetCostumeControl("numBottom1", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].Bottom1;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(costumeControl9, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].Bottom1 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
						}
						object obj9 = this.GetCostumeControl("numSitagiSitaji1", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].SitagiSitaji1;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(obj9, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].SitagiSitaji1 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
						}
						object costumeControl10 = this.GetCostumeControl("numSitagiSitaji2", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].SitagiSitaji2;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(costumeControl10, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].SitagiSitaji2 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
						}
						object obj10 = this.GetCostumeControl("numSitagiKage1", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].SitagiKage1;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(obj10, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].SitagiKage1 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
						}
						object costumeControl11 = this.GetCostumeControl("numSitagiKage2", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].SitagiKage2;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(costumeControl11, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].SitagiKage2 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
						}
						object obj11 = this.GetCostumeControl("numSitagi", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].Sitagi;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(obj11, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].Sitagi = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
						}
						object costumeControl12 = this.GetCostumeControl("numUtibaki", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].Utibaki;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(costumeControl12, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].Utibaki = (byte)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(byte));
						}
						object obj12 = this.GetCostumeControl("numSotobaki", num1);
						socks = new object[1];
						costume = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume;
						num2 = num1;
						socks[0] = costume[num2].Sotobaki;
						type = socks;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(obj12, null, "Initialize", type, null, null, flagArray, true);
						if (flagArray[0])
						{
							costume[num2].Sotobaki = (byte)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(byte));
						}
						object costumeControl13 = this.GetCostumeControl("chkFlagJyoge", num1);
						socks = new object[] { (JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].FlagJyoge == 0 ? false : true) };
						NewLateBinding.LateCall(costumeControl13, null, "Initialize", socks, null, null, null, true);
						object obj13 = this.GetCostumeControl("chkFlagSitagi", num1);
						socks = new object[] { (JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].FlagSitagi == 0 ? false : true) };
						NewLateBinding.LateCall(obj13, null, "Initialize", socks, null, null, null, true);
						object costumeControl14 = this.GetCostumeControl("chkFlagSkirt", num1);
						socks = new object[] { (JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].FlagSkirt == 0 ? false : true) };
						NewLateBinding.LateCall(costumeControl14, null, "Initialize", socks, null, null, null, true);
						num1 = checked(num1 + 1);
					}
					while (num1 <= 3);
					this.lblPersonal.Text = string.Concat(Resources.lblPersonalText, "/[", JG2Importer.Common.MySaveData.Seat[index].Character.FullName, "]");
					this.lblThumbnail.Image = JG2Importer.JG2.Common.ResizeBitmap((Bitmap)JG2Importer.Common.MySaveData.Seat[index].Character.PNGImage, this.lblThumbnail.Width, this.lblThumbnail.Height);
					this.lblCntKousai.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntKousai), Resources.ParticleTextPeopleCounter);
					this.lblCntSeikouAite.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntSeikouAite), Resources.ParticleTextPeopleCounter);
					this.lblCntFurareta.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntFurareta), Resources.GeneralCounterParticle);
					this.lblCntKenka.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntKenka), Resources.GeneralCounterParticle);
					this.lblCntSoudatu.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntSoudatu), Resources.GeneralCounterParticle);
					this.lblCntSabori.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntSabori), Resources.GeneralCounterParticle);
					this.lblTest1.Text = (JG2Importer.Common.MySaveData.Seat[index].PlayData.Test1 == -1 ? "－" : JG2Importer.JG2.Common.cnsTest1[JG2Importer.Common.MySaveData.Seat[index].PlayData.Test1]);
					this.lblTest2.Text = (JG2Importer.Common.MySaveData.Seat[index].PlayData.Test2 == -1 ? "－" : JG2Importer.JG2.Common.cnsTest2[JG2Importer.Common.MySaveData.Seat[index].PlayData.Test2]);
					Label label = this.lblTest3;
					if (JG2Importer.Common.MySaveData.Seat[index].PlayData.Test3 == 254)
					{
						str = "???";
					}
					else
					{
						str = (JG2Importer.Common.MySaveData.Seat[index].PlayData.Test3 == -1 ? "－" : JG2Importer.JG2.Common.cnsTest3[JG2Importer.Common.MySaveData.Seat[index].PlayData.Test3]);
					}
					label.Text = str;
					this.lblAite1.Text = (Information.IsNothing(JG2Importer.Common.MySaveData.Seat[index].PlayData.Aite1) ? "－" : JG2Importer.Common.MySaveData.Seat[index].PlayData.Aite1);
					this.lblAite2.Text = (Information.IsNothing(JG2Importer.Common.MySaveData.Seat[index].PlayData.Aite2) ? "－" : JG2Importer.Common.MySaveData.Seat[index].PlayData.Aite2);
					this.lblAite3.Text = (Information.IsNothing(JG2Importer.Common.MySaveData.Seat[index].PlayData.Aite3) ? "－" : JG2Importer.Common.MySaveData.Seat[index].PlayData.Aite3);
					this.lblGoods1.Text = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Goods1;
					this.lblGoods2.Text = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Goods2;
					this.lblGoods3.Text = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Goods3;
					System.Windows.Forms.ToolTip toolTip = this.ToolTip;
					Label label1 = this.lblThumbnail;
					string[] strArrays = new string[] { Resources.CaptionGoods1.PadRight(10, ' '), this.lblGoods1.Text, "\r\n", Resources.CaptionGoods2.PadRight(10, ' '), this.lblGoods2.Text, "\r\n", Resources.HItemText.PadRight(10, ' '), this.lblGoods3.Text, null };
					string[] strArrays1 = strArrays;
					if (Information.IsNothing(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Profile))
					{
						str1 = null;
					}
					else
					{
						str1 = string.Concat("\r\n\r\n", Resources.CaptionProfile, "\r\n", JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Profile);
					}
					strArrays1[8] = str1;
					toolTip.SetToolTip(label1, string.Concat(strArrays));
					this.pnlAction.Visible = this.SeatNo != JG2Importer.Common.MySaveData.Footer.SeatNoPlayer;
					this.lblAction.Text = string.Concat("[", JG2Importer.Common.MySaveData.Seat[num].Character.FullName, "]", Resources.CaptionRelationToChar, Resources.CaptionCodeAction);
					DefCodeAction codeAction = JG2Importer.Common.MySaveData.GetCodeAction(index, num);
					this.btnAction1.Text = Strings.Format(codeAction.CntLove, "00");
					this.btnAction2.Text = Strings.Format(codeAction.CntLike, "00");
					this.btnAction3.Text = Strings.Format(codeAction.CntDislike, "00");
					this.btnAction4.Text = Strings.Format(codeAction.CntHate, "00");
					this.pnlPair.Visible = this.SeatNo != JG2Importer.Common.MySaveData.Footer.SeatNoPlayer;
					this.lblPair.Text = string.Concat(Resources.PairRecordText, "/[", JG2Importer.Common.MySaveData.Seat[num].Character.FullName, "]");
					this.lblThumbnailPlayer.Image = JG2Importer.JG2.Common.ResizeBitmap((Bitmap)JG2Importer.Common.MySaveData.Seat[num].Character.PNGImage, this.lblThumbnailPlayer.Width, this.lblThumbnailPlayer.Height);
					this.chkKousai.Initialize((JG2Importer.Common.MySaveData.Seat[index].PlayData.KousaiData[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer].Status == 0 ? false : true));
					this.lblHAisyou.Text = (JG2Importer.Common.MySaveData.GetCodeHAisyou(index, num) == CodeHAisyou.None ? "－" : JG2Importer.JG2.Common.cnsCodeHAisyou[(int)JG2Importer.Common.MySaveData.GetCodeHAisyou(index, num)]);
					this.lblCntSeikou1.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntSeikou1[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer]), Resources.GeneralCounterParticle);
					this.lblCntSeikou2.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntSeikou2[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer]), Resources.GeneralCounterParticle);
					this.lblCntSeikou3.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntSeikou3[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer]), Resources.GeneralCounterParticle);
					this.lblCntCondom.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntCondom[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer]), Resources.GeneralCounterParticle);
					this.lblCntOrgasm.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntOrgasm[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer]), Resources.GeneralCounterParticle);
					this.lblCntSynchronize.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntSynchronize[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer]), Resources.GeneralCounterParticle);
					this.lblSyasei1.Text = string.Concat(Conversions.ToString(checked(JG2Importer.Common.MySaveData.Seat[index].PlayData.Syasei1[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer] * 3)), "㏄");
					this.lblSyasei2.Text = string.Concat(Conversions.ToString(checked(JG2Importer.Common.MySaveData.Seat[index].PlayData.Syasei2[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer] * 3)), "㏄");
					this.lblSyasei3.Text = string.Concat(Conversions.ToString(checked(JG2Importer.Common.MySaveData.Seat[index].PlayData.Syasei3[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer] * 3)), "㏄");
					this.lblSyasei4.Text = string.Concat(Conversions.ToString(checked(JG2Importer.Common.MySaveData.Seat[index].PlayData.Syasei4[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer] * 3)), "㏄");
					this.lblCntNakaKiken.Text = string.Concat(Conversions.ToString(checked(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntNakaKiken[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer] * 3)), "㏄");
					this.lblRenzoku.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.Renzoku[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer]), Resources.ParticleDays);
					this.lblCntTodaySeikou.Text = string.Concat(Conversions.ToString(JG2Importer.Common.MySaveData.Seat[index].PlayData.CntTodaySeikou[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer]), Resources.GeneralCounterParticle);
					this.pnlActionRev.Visible = this.SeatNo != JG2Importer.Common.MySaveData.Footer.SeatNoPlayer;
					this.lblActionRev.Text = string.Concat("[", JG2Importer.Common.MySaveData.Seat[index].Character.FullName, "]", Resources.CaptionRelationToChar, Resources.CaptionCodeAction);
					codeAction = JG2Importer.Common.MySaveData.GetCodeAction(num, index);
					this.btnActionRev1.Text = Strings.Format(codeAction.CntLove, "00");
					this.btnActionRev2.Text = Strings.Format(codeAction.CntLike, "00");
					this.btnActionRev3.Text = Strings.Format(codeAction.CntDislike, "00");
					this.btnActionRev4.Text = Strings.Format(codeAction.CntHate, "00");
					this.pnlTools.Visible = this.SeatNo != JG2Importer.Common.MySaveData.Footer.SeatNoPlayer;
					this.num_ValueChanged(this.numMune1, null);
					this.num_ValueChanged(this.numHair1, null);
					this.num_ValueChanged(this.numHair2, null);
					this.num_ValueChanged(this.numHair3, null);
					this.num_ValueChanged(this.numHair4, null);
					this.num_ValueChanged(this.numTiryoku3, null);
					this.num_ValueChanged(this.numTairyoku3, null);
					this.num_ValueChanged(this.numSeikaku, null);
					num1 = 0;
					do
					{
						this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numType", num1)), null);
						this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numSocks", num1)), null);
						this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numPansuto", num1)), null);
						this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numSkirt", num1)), null);
						this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numBottom1", num1)), null);
						this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numSitagi", num1)), null);
						this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numUtibaki", num1)), null);
						this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numSotobaki", num1)), null);
						num1 = checked(num1 + 1);
					}
					while (num1 <= 3);
					this.LockToolControl();
					return true;
				}
				catch (Exception exception1)
				{
					ProjectData.SetProjectError(exception1);
					Exception exception = exception1;
					Interaction.MsgBox(string.Concat("[", JG2Importer.Common.MySaveData.Seat[index].Character.FullName, "]", Resources.DetailedInfoCannotDisplayText, "\r\n\r\n", exception.Message), MsgBoxStyle.Critical, null);
					flag = false;
					ProjectData.ClearProjectError();
				}
				return flag;
			}
			return true;
		}

		private void frmDetail_FormClosed(object sender, FormClosedEventArgs e)
		{
			MyProject.Forms.frmClass.Activate();
			JG2Importer.Common.MyDetailLocation = this.Location;
			this.Dispose();
		}

		private void frmDetail_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (this.CloseCheck && e.CloseReason == System.Windows.Forms.CloseReason.UserClosing)
			{
				object obj = this.tabBase;
				bool flag = this.CheckModified(ref obj);
				this.tabBase = (TabControl)obj;
				if (flag)
				{
					switch (Interaction.MsgBox(string.Concat("[", JG2Importer.Common.MySaveData.Seat[JG2Importer.Common.MySaveData.GetIndex(this.SeatNo)].Character.FullName, "]", Resources.MenuTitleSaveChanges), MsgBoxStyle.OkCancel | MsgBoxStyle.AbortRetryIgnore | MsgBoxStyle.YesNoCancel | MsgBoxStyle.Critical | MsgBoxStyle.Question | MsgBoxStyle.Exclamation | MsgBoxStyle.DefaultButton3, null))
					{
						case MsgBoxResult.Cancel:
						{
							e.Cancel = true;
							break;
						}
						case MsgBoxResult.Yes:
						{
							this.mnuSubmit_Click(RuntimeHelpers.GetObjectValue(sender), e);
							break;
						}
					}
				}
			}
		}

		private void frmDetail_Load(object sender, EventArgs e)
		{
			this.tabBase.SelectedTab = this.tbpTools;
			this.radYouto0.Checked = true;
		}

		private object GetCostumeControl(string argName, int argIndex = -1)
		{
			object obj;
			obj = (argIndex != -1 ? this.tbpCostume.Controls[string.Concat("pnlCostume", Strings.Format(argIndex, ""))] : this.tbpCostume.Controls[string.Concat("pnlCostume", Strings.Format(RuntimeHelpers.GetObjectValue(this.pnlYouto.Tag), ""))]);
			object[] objArray = new object[] { argName };
			bool[] flagArray = new bool[] { true };
			object obj1 = NewLateBinding.LateGet(obj, null, "Controls", objArray, null, null, flagArray);
			if (!flagArray[0])
			{
				return obj1;
			}
			argName = (string)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(objArray[0]), typeof(string));
			return obj1;
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.pnlFooter = new Panel();
			this.btnSubmit = new Button();
			this.btnSelectAll = new Button();
			this.btnClearAll = new Button();
			this.btnBatch = new Button();
			this.tabBase = new TabControl();
			this.tbpBody = new TabPage();
			this.lblMune1Name = new Label();
			this.numTikubi3 = new JG2NumericUpDown();
			this.cmbTikubi1 = new JG2NumericUpDown();
			this.numHiyake2 = new JG2NumericUpDown();
			this.cmbTikubi2 = new JG2NumericUpDown();
			this.numUnder2 = new JG2NumericUpDown();
			this.cmbHiyake1 = new JG2NumericUpDown();
			this.cmbMosaic = new JG2NumericUpDown();
			this.cmbUnder1 = new JG2NumericUpDown();
			this.btnColorUnder = new JG2Button();
			this.lblColorUnder = new Label();
			this.lblUnder2 = new Label();
			this.lblUnder1 = new Label();
			this.lblMosaic = new Label();
			this.lblHiyake2 = new Label();
			this.lblHiyake1 = new Label();
			this.lblTikubi3 = new Label();
			this.lblTikubi2 = new Label();
			this.lblTikubi1 = new Label();
			this.btnColorSkin2 = new JG2Button();
			this.lblColorSkin2 = new Label();
			this.cmbMune8 = new JG2ComboBox();
			this.lblNyurin = new Label();
			this.numNyurin = new JG2NumericUpDown();
			this.lblMune8 = new Label();
			this.lblMune7 = new Label();
			this.numMune7 = new JG2NumericUpDown();
			this.lblMune6 = new Label();
			this.numMune6 = new JG2NumericUpDown();
			this.lblMune5 = new Label();
			this.numMune5 = new JG2NumericUpDown();
			this.lblMune4 = new Label();
			this.numMune4 = new JG2NumericUpDown();
			this.lblMune3 = new Label();
			this.numMune3 = new JG2NumericUpDown();
			this.cmbMune2 = new JG2ComboBox();
			this.lblMune2 = new Label();
			this.lblMune1 = new Label();
			this.numMune1 = new JG2NumericUpDown();
			this.lblWaist = new Label();
			this.numWaist = new JG2NumericUpDown();
			this.lblAtama2 = new Label();
			this.numAtama2 = new JG2NumericUpDown();
			this.lblAtama1 = new Label();
			this.numAtama1 = new JG2NumericUpDown();
			this.cmbStyle = new JG2ComboBox();
			this.lblStyle = new Label();
			this.cmbHeight = new JG2ComboBox();
			this.lblHeight = new Label();
			this.tbpFace = new TabPage();
			this.cmbLip1 = new JG2NumericUpDown();
			this.lblMabuta = new Label();
			this.pnlHokuro = new Panel();
			this.chkHokuro1 = new JG2CheckBox();
			this.chkHokuro4 = new JG2CheckBox();
			this.chkHokuro3 = new JG2CheckBox();
			this.chkHokuro2 = new JG2CheckBox();
			this.lblMatuge1 = new Label();
			this.lblMayu1 = new Label();
			this.lblMayu2 = new Label();
			this.lblMatuge2 = new Label();
			this.lblHighlight1 = new Label();
			this.lblColorMayu = new Label();
			this.lblGlasses2 = new Label();
			this.lblColorGlasses = new Label();
			this.lblPupil5 = new Label();
			this.lblHokuro = new Label();
			this.lblLip2 = new Label();
			this.lblPupil2 = new Label();
			this.lblLip1 = new Label();
			this.lblPupil4 = new Label();
			this.lblPupil3 = new Label();
			this.lblPupil1 = new Label();
			this.lblEye1 = new Label();
			this.lblEye5 = new Label();
			this.lblEye4 = new Label();
			this.lblEye3 = new Label();
			this.lblEye2 = new Label();
			this.lblKao = new Label();
			this.btnColorEye2Sub = new Button();
			this.btnColorEye1Sub = new Button();
			this.lblColorEye2 = new Label();
			this.lblColorEye1 = new Label();
			this.cmbMatuge1 = new JG2ComboBox();
			this.cmbHighlight1 = new JG2ComboBox();
			this.btnColorGlasses = new JG2Button();
			this.cmbGlasses2 = new JG2NumericUpDown();
			this.cmbPupil5 = new JG2ComboBox();
			this.cmbMatuge2 = new JG2ComboBox();
			this.numMayu2 = new JG2NumericUpDown();
			this.cmbMabuta = new JG2ComboBox();
			this.btnColorMayu = new JG2Button();
			this.numPupil2 = new JG2NumericUpDown();
			this.numLip2 = new JG2NumericUpDown();
			this.cmbMayu1 = new JG2ComboBox();
			this.numPupil4 = new JG2NumericUpDown();
			this.numPupil3 = new JG2NumericUpDown();
			this.cmbPupil1 = new JG2ComboBox();
			this.numEye1 = new JG2NumericUpDown();
			this.numEye5 = new JG2NumericUpDown();
			this.numEye4 = new JG2NumericUpDown();
			this.numEye3 = new JG2NumericUpDown();
			this.numEye2 = new JG2NumericUpDown();
			this.cmbKao = new JG2NumericUpDown();
			this.btnColorEye2 = new JG2Button();
			this.btnColorEye1 = new JG2Button();
			this.tbpHair = new TabPage();
			this.lblHair4Name = new Label();
			this.lblHair3Name = new Label();
			this.lblHair2Name = new Label();
			this.lblHair1Name = new Label();
			this.lblHair4 = new Label();
			this.lblHair3 = new Label();
			this.lblHair2 = new Label();
			this.lblHair1 = new Label();
			this.btnColorHairSub2 = new Button();
			this.btnColorHairSub1 = new Button();
			this.Label301 = new Label();
			this.lblColorHair = new Label();
			this.numHair4 = new JG2NumericUpDown();
			this.numHair3 = new JG2NumericUpDown();
			this.numHair2 = new JG2NumericUpDown();
			this.numHair1 = new JG2NumericUpDown();
			this.numHairAdj4 = new JG2NumericUpDown();
			this.chkHairRev4 = new JG2CheckBox();
			this.numHairAdj3 = new JG2NumericUpDown();
			this.chkHairRev3 = new JG2CheckBox();
			this.numHairAdj2 = new JG2NumericUpDown();
			this.chkHairRev2 = new JG2CheckBox();
			this.numHairAdj1 = new JG2NumericUpDown();
			this.chkHairRev1 = new JG2CheckBox();
			this.btnColorHair = new JG2Button();
			this.tbpJinkaku = new TabPage();
			this.lblTairyoku1Name = new Label();
			this.lblTiryoku1Name = new Label();
			this.lblSeikakuName = new Label();
			this.lblSyakousei = new Label();
			this.lblSeikaku = new Label();
			this.lblVoice = new Label();
			this.lblSeiai = new Label();
			this.lblTeisou = new Label();
			this.lblKenka = new Label();
			this.lblBukatu1 = new Label();
			this.lblTairyoku1 = new Label();
			this.lblTiryoku1 = new Label();
			this.numSeikaku = new JG2NumericUpDown();
			this.cmbBukatu1 = new JG2ComboBox();
			this.numBukatu3 = new JG2NumericUpDown();
			this.numTairyoku3 = new JG2NumericUpDown();
			this.numTiryoku3 = new JG2NumericUpDown();
			this.cmbVoice = new JG2ComboBox();
			this.chkSeikou2 = new JG2CheckBox();
			this.chkSeikou1 = new JG2CheckBox();
			this.cmbSeiai = new JG2ComboBox();
			this.cmbTeisou = new JG2ComboBox();
			this.cmbKenka = new JG2ComboBox();
			this.cmbSyakousei = new JG2ComboBox();
			this.tbpKosei = new TabPage();
			this.pnlH = new Panel();
			this.Label511 = new Label();
			this.chkKiss = new JG2CheckBox();
			this.chkKake = new JG2CheckBox();
			this.chkSeiki1 = new JG2CheckBox();
			this.chkNaka = new JG2CheckBox();
			this.chkBust = new JG2CheckBox();
			this.chkNama = new JG2CheckBox();
			this.chkSeiki2 = new JG2CheckBox();
			this.chkSeiin = new JG2CheckBox();
			this.chkCunnilingus = new JG2CheckBox();
			this.chkAnal = new JG2CheckBox();
			this.chkBack = new JG2CheckBox();
			this.chkOnna = new JG2CheckBox();
			this.chkFellatio = new JG2CheckBox();
			this.pnlKosei = new Panel();
			this.Label501 = new Label();
			this.chkLuck = new JG2CheckBox();
			this.chkRare = new JG2CheckBox();
			this.chkMiseityo = new JG2CheckBox();
			this.chkYowami = new JG2CheckBox();
			this.chkYami = new JG2CheckBox();
			this.chkNantyo = new JG2CheckBox();
			this.chkAsekaki = new JG2CheckBox();
			this.chkM = new JG2CheckBox();
			this.chkKinben = new JG2CheckBox();
			this.chkHonpou = new JG2CheckBox();
			this.chkHaraguro = new JG2CheckBox();
			this.chkMakenki = new JG2CheckBox();
			this.chkItizu = new JG2CheckBox();
			this.chkYuujyufudan = new JG2CheckBox();
			this.chkRenai = new JG2CheckBox();
			this.chkHarapeko = new JG2CheckBox();
			this.chkIintyou = new JG2CheckBox();
			this.chkOsyaberi = new JG2CheckBox();
			this.chkSewayaki = new JG2CheckBox();
			this.chkSousyoku = new JG2CheckBox();
			this.chkPoyayan = new JG2CheckBox();
			this.chkBouryoku = new JG2CheckBox();
			this.chkTyokujyo = new JG2CheckBox();
			this.chkCool = new JG2CheckBox();
			this.chkSukebe = new JG2CheckBox();
			this.chkMajime = new JG2CheckBox();
			this.chkToufu = new JG2CheckBox();
			this.chkYakimochi = new JG2CheckBox();
			this.chkMaemuki = new JG2CheckBox();
			this.chkTereya = new JG2CheckBox();
			this.chkSunao = new JG2CheckBox();
			this.chkMiha = new JG2CheckBox();
			this.chkTundere = new JG2CheckBox();
			this.chkKyouki = new JG2CheckBox();
			this.chkCharm = new JG2CheckBox();
			this.chkNigate2 = new JG2CheckBox();
			this.chkNekketu = new JG2CheckBox();
			this.chkNigate1 = new JG2CheckBox();
			this.chkCyoroi = new JG2CheckBox();
			this.tbpNinshin = new TabPage();
			this.cmbSaturday2 = new JG2ComboBox();
			this.lblSaturday2 = new Label();
			this.lblFriday2 = new Label();
			this.lblThursday2 = new Label();
			this.lblWednesday2 = new Label();
			this.lblTuesday2 = new Label();
			this.lblMonday2 = new Label();
			this.lblSaturday1 = new Label();
			this.lblFriday1 = new Label();
			this.lblThursday1 = new Label();
			this.lblWednesday1 = new Label();
			this.lblTuesday1 = new Label();
			this.lblSunday2 = new Label();
			this.lblMonday1 = new Label();
			this.lblSunday1 = new Label();
			this.cmbFriday2 = new JG2ComboBox();
			this.cmbThursday2 = new JG2ComboBox();
			this.cmbWednesday2 = new JG2ComboBox();
			this.cmbTuesday2 = new JG2ComboBox();
			this.cmbMonday2 = new JG2ComboBox();
			this.cmbSaturday1 = new JG2ComboBox();
			this.cmbFriday1 = new JG2ComboBox();
			this.cmbThursday1 = new JG2ComboBox();
			this.cmbWednesday1 = new JG2ComboBox();
			this.cmbTuesday1 = new JG2ComboBox();
			this.cmbSunday2 = new JG2ComboBox();
			this.cmbMonday1 = new JG2ComboBox();
			this.cmbSunday1 = new JG2ComboBox();
			this.tbpCostume = new TabPage();
			this.pnlYouto = new Panel();
			this.radYouto3 = new JG2RadioButton();
			this.radYouto2 = new JG2RadioButton();
			this.radYouto1 = new JG2RadioButton();
			this.radYouto0 = new JG2RadioButton();
			this.Label701 = new Label();
			this.tbpTools = new TabPage();
			this.pnlAction = new Panel();
			this.btnAction3 = new JG2Button();
			this.btnDelAction3 = new JG2Button();
			this.btnDelAction2 = new JG2Button();
			this.btnAction4 = new JG2Button();
			this.lblAction = new Label();
			this.btnDelAction4 = new JG2Button();
			this.btnAction2 = new JG2Button();
			this.btnDelAction1 = new JG2Button();
			this.btnAction1 = new JG2Button();
			this.pnlActionRev = new Panel();
			this.btnActionRev3 = new JG2Button();
			this.btnDelActionRev3 = new JG2Button();
			this.btnDelActionRev2 = new JG2Button();
			this.btnActionRev4 = new JG2Button();
			this.lblActionRev = new Label();
			this.btnDelActionRev4 = new JG2Button();
			this.btnActionRev2 = new JG2Button();
			this.btnDelActionRev1 = new JG2Button();
			this.btnActionRev1 = new JG2Button();
			this.pnlPair = new Panel();
			this.lblRenzoku = new Label();
			this.Label92 = new Label();
			this.lblCntTodaySeikou = new Label();
			this.Label89 = new Label();
			this.lblThumbnailPlayer = new Label();
			this.lblCntNakaKiken = new Label();
			this.lblHAisyou = new Label();
			this.chkKousai = new JG2CheckBox();
			this.Label83 = new Label();
			this.lblSyasei4 = new Label();
			this.Label129 = new Label();
			this.lblSyasei3 = new Label();
			this.Label131 = new Label();
			this.lblCntCondom = new Label();
			this.Label111 = new Label();
			this.lblSyasei2 = new Label();
			this.lblCntSeikou3 = new Label();
			this.Label114 = new Label();
			this.Label115 = new Label();
			this.lblSyasei1 = new Label();
			this.lblCntSeikou2 = new Label();
			this.Label118 = new Label();
			this.Label119 = new Label();
			this.lblCntSynchronize = new Label();
			this.lblCntSeikou1 = new Label();
			this.Label122 = new Label();
			this.Label123 = new Label();
			this.lblCntOrgasm = new Label();
			this.Label126 = new Label();
			this.Label127 = new Label();
			this.lblPair = new Label();
			this.pnlProfile = new Panel();
			this.lblGoods3 = new Label();
			this.lblGoods1 = new Label();
			this.lblGoods2 = new Label();
			this.lblAite3 = new Label();
			this.lblAite2 = new Label();
			this.lblAite1 = new Label();
			this.lblThumbnail = new Label();
			this.btnPlayer = new Button();
			this.Label90 = new Label();
			this.Label109 = new Label();
			this.Label91 = new Label();
			this.Label107 = new Label();
			this.lblCntSoudatu = new Label();
			this.Label26 = new Label();
			this.Label105 = new Label();
			this.lblTest3 = new Label();
			this.lblCntKenka = new Label();
			this.Label102 = new Label();
			this.Label103 = new Label();
			this.lblTest2 = new Label();
			this.lblCntFurareta = new Label();
			this.Label98 = new Label();
			this.Label99 = new Label();
			this.lblTest1 = new Label();
			this.lblCntSeikouAite = new Label();
			this.Label94 = new Label();
			this.Label95 = new Label();
			this.lblCntSabori = new Label();
			this.lblCntKousai = new Label();
			this.Label87 = new Label();
			this.Label85 = new Label();
			this.Label84 = new Label();
			this.lblPersonal = new Label();
			this.pnlTools = new Panel();
			this.btnClearAction = new JG2Button();
			this.btnHMax = new JG2Button();
			this.btnAfterPill = new JG2Button();
			this.btnGohoubi = new JG2Button();
			this.btnExport = new Button();
			this.btnImport = new Button();
			this.MenuStrip = new System.Windows.Forms.MenuStrip();
			this.mnuFile = new ToolStripMenuItem();
			this.mnuSubmit = new ToolStripMenuItem();
			this.toolStripSeparator1 = new ToolStripSeparator();
			this.mnuClose = new ToolStripMenuItem();
			this.mnuEdit = new JG2ToolStripMenuItem();
			this.mnuTabToolsImport = new ToolStripMenuItem();
			this.mnuTabToolsExport = new ToolStripMenuItem();
			this.ToolStripSeparator10 = new ToolStripSeparator();
			this.mnuOpen = new ToolStripMenuItem();
			this.ToolStripSeparator6 = new ToolStripSeparator();
			this.mnuTabToolsPlayer = new ToolStripMenuItem();
			this.mnuTabToolsSeparator = new ToolStripSeparator();
			this.mnuTabToolsAction = new ToolStripMenuItem();
			this.mnuTabToolsAction1 = new ToolStripMenuItem();
			this.mnuTabToolsAction2 = new ToolStripMenuItem();
			this.mnuTabToolsAction3 = new ToolStripMenuItem();
			this.mnuTabToolsAction4 = new ToolStripMenuItem();
			this.ToolStripSeparator11 = new ToolStripSeparator();
			this.mnuTabToolsDelAction1 = new ToolStripMenuItem();
			this.mnuTabToolsDelAction2 = new ToolStripMenuItem();
			this.mnuTabToolsDelAction3 = new ToolStripMenuItem();
			this.mnuTabToolsDelAction4 = new ToolStripMenuItem();
			this.mnuTabToolsActionRev = new ToolStripMenuItem();
			this.mnuTabToolsActionRev1 = new ToolStripMenuItem();
			this.mnuTabToolsActionRev2 = new ToolStripMenuItem();
			this.mnuTabToolsActionRev3 = new ToolStripMenuItem();
			this.mnuTabToolsActionRev4 = new ToolStripMenuItem();
			this.ToolStripSeparator4 = new ToolStripSeparator();
			this.mnuTabToolsDelActionRev1 = new ToolStripMenuItem();
			this.mnuTabToolsDelActionRev2 = new ToolStripMenuItem();
			this.mnuTabToolsDelActionRev3 = new ToolStripMenuItem();
			this.mnuTabToolsDelActionRev4 = new ToolStripMenuItem();
			this.mnuTabToolsClearAction = new ToolStripMenuItem();
			this.mnuTabToolsHMax = new ToolStripMenuItem();
			this.mnuTabToolsGohoubi = new ToolStripMenuItem();
			this.mnuTabToolsAfterPill = new ToolStripMenuItem();
			this.ToolStripSeparator9 = new ToolStripSeparator();
			this.mnuSelectCondition = new ToolStripMenuItem();
			this.ToolStripSeparator3 = new ToolStripSeparator();
			this.mnuSelectAll = new ToolStripMenuItem();
			this.mnuClearAll = new ToolStripMenuItem();
			this.mnuBatchSeparator = new ToolStripSeparator();
			this.mnuBatch = new ToolStripMenuItem();
			this.mnuTab = new ToolStripMenuItem();
			this.mnuTabBody = new ToolStripMenuItem();
			this.mnuTabFace = new ToolStripMenuItem();
			this.mnuTabHair = new ToolStripMenuItem();
			this.mnuTabJinkaku = new ToolStripMenuItem();
			this.mnuTabKosei = new ToolStripMenuItem();
			this.mnuTabNinshin = new ToolStripMenuItem();
			this.mnuTabCostume = new ToolStripMenuItem();
			this.mnuTabTools = new ToolStripMenuItem();
			this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
			this.pnlFooter.SuspendLayout();
			this.tabBase.SuspendLayout();
			this.tbpBody.SuspendLayout();
			((ISupportInitialize)this.numTikubi3).BeginInit();
			((ISupportInitialize)this.numHiyake2).BeginInit();
			((ISupportInitialize)this.numUnder2).BeginInit();
			((ISupportInitialize)this.numNyurin).BeginInit();
            ((ISupportInitialize)this.cmbKao).BeginInit();
            ((ISupportInitialize)this.cmbTikubi1).BeginInit();
            ((ISupportInitialize)this.cmbTikubi2).BeginInit();
            ((ISupportInitialize)this.cmbHiyake1).BeginInit();
            ((ISupportInitialize)this.cmbMosaic).BeginInit();
            ((ISupportInitialize)this.cmbUnder1).BeginInit();
            ((ISupportInitialize)this.cmbGlasses2).BeginInit();
            ((ISupportInitialize)this.numMune7).BeginInit();
			((ISupportInitialize)this.numMune6).BeginInit();
			((ISupportInitialize)this.numMune5).BeginInit();
			((ISupportInitialize)this.numMune4).BeginInit();
			((ISupportInitialize)this.numMune3).BeginInit();
			((ISupportInitialize)this.numMune1).BeginInit();
			((ISupportInitialize)this.numWaist).BeginInit();
			((ISupportInitialize)this.numAtama2).BeginInit();
			((ISupportInitialize)this.numAtama1).BeginInit();
			this.tbpFace.SuspendLayout();
			this.pnlHokuro.SuspendLayout();
			((ISupportInitialize)this.numMayu2).BeginInit();
			((ISupportInitialize)this.numPupil2).BeginInit();
			((ISupportInitialize)this.numLip2).BeginInit();
			((ISupportInitialize)this.numPupil4).BeginInit();
			((ISupportInitialize)this.numPupil3).BeginInit();
			((ISupportInitialize)this.numEye1).BeginInit();
			((ISupportInitialize)this.numEye5).BeginInit();
			((ISupportInitialize)this.numEye4).BeginInit();
			((ISupportInitialize)this.numEye3).BeginInit();
			((ISupportInitialize)this.numEye2).BeginInit();
			this.tbpHair.SuspendLayout();
			((ISupportInitialize)this.numHair4).BeginInit();
			((ISupportInitialize)this.numHair3).BeginInit();
			((ISupportInitialize)this.numHair2).BeginInit();
			((ISupportInitialize)this.numHair1).BeginInit();
			((ISupportInitialize)this.numHairAdj4).BeginInit();
			((ISupportInitialize)this.numHairAdj3).BeginInit();
			((ISupportInitialize)this.numHairAdj2).BeginInit();
			((ISupportInitialize)this.numHairAdj1).BeginInit();
			this.tbpJinkaku.SuspendLayout();
			((ISupportInitialize)this.numSeikaku).BeginInit();
			((ISupportInitialize)this.numBukatu3).BeginInit();
			((ISupportInitialize)this.numTairyoku3).BeginInit();
			((ISupportInitialize)this.numTiryoku3).BeginInit();
			this.tbpKosei.SuspendLayout();
			this.pnlH.SuspendLayout();
			this.pnlKosei.SuspendLayout();
			this.tbpNinshin.SuspendLayout();
			this.tbpCostume.SuspendLayout();
			this.pnlYouto.SuspendLayout();
			this.tbpTools.SuspendLayout();
			this.pnlAction.SuspendLayout();
			this.pnlActionRev.SuspendLayout();
			this.pnlPair.SuspendLayout();
			this.pnlProfile.SuspendLayout();
			this.pnlTools.SuspendLayout();
			this.MenuStrip.SuspendLayout();
			this.SuspendLayout();
			this.pnlFooter.Controls.Add(this.btnSubmit);
			this.pnlFooter.Controls.Add(this.btnSelectAll);
			this.pnlFooter.Controls.Add(this.btnClearAll);
			this.pnlFooter.Controls.Add(this.btnBatch);
			Panel panel = this.pnlFooter;
			Point point = new Point(8, 592);
			panel.Location = point;
			this.pnlFooter.Name = "pnlFooter";
			Panel panel1 = this.pnlFooter;
			System.Drawing.Size size = new System.Drawing.Size(468, 26);
			panel1.Size = size;
			this.pnlFooter.TabIndex = 4;
			this.btnSubmit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			Button button = this.btnSubmit;
			point = new Point(388, 0);
			button.Location = point;
			Button button1 = this.btnSubmit;
			System.Windows.Forms.Padding padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			button1.Margin = padding;
			this.btnSubmit.Name = "btnSubmit";
			Button button2 = this.btnSubmit;
			size = new System.Drawing.Size(80, 26);
			button2.Size = size;
			this.btnSubmit.TabIndex = 3;
			this.btnSubmit.TabStop = false;
            this.btnSubmit.Text = Resources.BtnTextSubmission;
			this.btnSubmit.UseVisualStyleBackColor = true;
			this.btnSelectAll.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			Button button3 = this.btnSelectAll;
			point = new Point(0, 0);
			button3.Location = point;
			Button button4 = this.btnSelectAll;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			button4.Margin = padding;
			this.btnSelectAll.Name = "btnSelectAll";
			Button button5 = this.btnSelectAll;
			size = new System.Drawing.Size(80, 26);
			button5.Size = size;
			this.btnSelectAll.TabIndex = 0;
			this.btnSelectAll.TabStop = false;
            this.btnSelectAll.Text = Resources.BtnTextSelectAll;
			this.btnSelectAll.UseVisualStyleBackColor = true;
			this.btnClearAll.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			Button button6 = this.btnClearAll;
			point = new Point(84, 0);
			button6.Location = point;
			Button button7 = this.btnClearAll;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			button7.Margin = padding;
			this.btnClearAll.Name = "btnClearAll";
			Button button8 = this.btnClearAll;
			size = new System.Drawing.Size(80, 26);
			button8.Size = size;
			this.btnClearAll.TabIndex = 1;
			this.btnClearAll.TabStop = false;
            this.btnClearAll.Text = Resources.ClearAllBtnText;
			this.btnClearAll.UseVisualStyleBackColor = true;
			this.btnBatch.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			Button button9 = this.btnBatch;
			point = new Point(168, 0);
			button9.Location = point;
			Button button10 = this.btnBatch;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			button10.Margin = padding;
			this.btnBatch.Name = "btnBatch";
			Button button11 = this.btnBatch;
			size = new System.Drawing.Size(190, 26);
			button11.Size = size;
			this.btnBatch.TabIndex = 2;
			this.btnBatch.TabStop = false;
			this.btnBatch.UseVisualStyleBackColor = true;
			this.tabBase.Controls.Add(this.tbpBody);
			this.tabBase.Controls.Add(this.tbpFace);
			this.tabBase.Controls.Add(this.tbpHair);
			this.tabBase.Controls.Add(this.tbpJinkaku);
			this.tabBase.Controls.Add(this.tbpKosei);
			this.tabBase.Controls.Add(this.tbpNinshin);
			this.tabBase.Controls.Add(this.tbpCostume);
			this.tabBase.Controls.Add(this.tbpTools);
			TabControl tabControl = this.tabBase;
			size = new System.Drawing.Size(66, 24);
			tabControl.ItemSize = size;
			TabControl tabControl1 = this.tabBase;
			point = new Point(8, 76);
			tabControl1.Location = point;
			this.tabBase.Name = "tabBase";
			this.tabBase.SelectedIndex = 0;
			TabControl tabControl2 = this.tabBase;
			size = new System.Drawing.Size(468, 508);
			tabControl2.Size = size;
			this.tabBase.SizeMode = TabSizeMode.Fixed;
			this.tabBase.TabIndex = 3;
			this.tbpBody.BackColor = SystemColors.Control;
			this.tbpBody.Controls.Add(this.lblMune1Name);
			this.tbpBody.Controls.Add(this.numTikubi3);
			this.tbpBody.Controls.Add(this.cmbTikubi1);
			this.tbpBody.Controls.Add(this.numHiyake2);
			this.tbpBody.Controls.Add(this.cmbTikubi2);
			this.tbpBody.Controls.Add(this.numUnder2);
			this.tbpBody.Controls.Add(this.cmbHiyake1);
			this.tbpBody.Controls.Add(this.cmbMosaic);
			this.tbpBody.Controls.Add(this.cmbUnder1);
			this.tbpBody.Controls.Add(this.btnColorUnder);
			this.tbpBody.Controls.Add(this.lblColorUnder);
			this.tbpBody.Controls.Add(this.lblUnder2);
			this.tbpBody.Controls.Add(this.lblUnder1);
			this.tbpBody.Controls.Add(this.lblMosaic);
			this.tbpBody.Controls.Add(this.lblHiyake2);
			this.tbpBody.Controls.Add(this.lblHiyake1);
			this.tbpBody.Controls.Add(this.lblTikubi3);
			this.tbpBody.Controls.Add(this.lblTikubi2);
			this.tbpBody.Controls.Add(this.lblTikubi1);
			this.tbpBody.Controls.Add(this.btnColorSkin2);
			this.tbpBody.Controls.Add(this.lblColorSkin2);
			this.tbpBody.Controls.Add(this.cmbMune8);
			this.tbpBody.Controls.Add(this.lblNyurin);
			this.tbpBody.Controls.Add(this.numNyurin);
			this.tbpBody.Controls.Add(this.lblMune8);
			this.tbpBody.Controls.Add(this.lblMune7);
			this.tbpBody.Controls.Add(this.numMune7);
			this.tbpBody.Controls.Add(this.lblMune6);
			this.tbpBody.Controls.Add(this.numMune6);
			this.tbpBody.Controls.Add(this.lblMune5);
			this.tbpBody.Controls.Add(this.numMune5);
			this.tbpBody.Controls.Add(this.lblMune4);
			this.tbpBody.Controls.Add(this.numMune4);
			this.tbpBody.Controls.Add(this.lblMune3);
			this.tbpBody.Controls.Add(this.numMune3);
			this.tbpBody.Controls.Add(this.cmbMune2);
			this.tbpBody.Controls.Add(this.lblMune2);
			this.tbpBody.Controls.Add(this.lblMune1);
			this.tbpBody.Controls.Add(this.numMune1);
			this.tbpBody.Controls.Add(this.lblWaist);
			this.tbpBody.Controls.Add(this.numWaist);
			this.tbpBody.Controls.Add(this.lblAtama2);
			this.tbpBody.Controls.Add(this.numAtama2);
			this.tbpBody.Controls.Add(this.lblAtama1);
			this.tbpBody.Controls.Add(this.numAtama1);
			this.tbpBody.Controls.Add(this.cmbStyle);
			this.tbpBody.Controls.Add(this.lblStyle);
			this.tbpBody.Controls.Add(this.cmbHeight);
			this.tbpBody.Controls.Add(this.lblHeight);
			TabPage tabPage = this.tbpBody;
			point = new Point(4, 28);
			tabPage.Location = point;
			this.tbpBody.Name = "tbpBody";
			TabPage tabPage1 = this.tbpBody;
			size = new System.Drawing.Size(460, 476);
			tabPage1.Size = size;
			this.tbpBody.TabIndex = 7;
			this.tbpBody.Text = "体";
			this.lblMune1Name.AutoEllipsis = true;
			this.lblMune1Name.BackColor = SystemColors.Control;
			Label label = this.lblMune1Name;
			point = new Point(160, 165);
			label.Location = point;
			this.lblMune1Name.Name = "lblMune1Name";
			Label label1 = this.lblMune1Name;
			size = new System.Drawing.Size(60, 18);
			label1.Size = size;
			this.lblMune1Name.TabIndex = 48;
			this.numTikubi3.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown = this.numTikubi3;
			point = new Point(333, 102);
			jG2NumericUpDown.Location = point;
			JG2NumericUpDown jG2NumericUpDown1 = this.numTikubi3;
			int[] numArray = new int[] { 255, 0, 0, 0 };
			decimal num = new decimal(numArray);
			jG2NumericUpDown1.Maximum = num;
			this.numTikubi3.Name = "numTikubi3";
			JG2NumericUpDown jG2NumericUpDown2 = this.numTikubi3;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown2.Size = size;
			this.numTikubi3.TabIndex = 35;
			this.numTikubi3.TextAlign = HorizontalAlignment.Right;
			this.cmbTikubi1.BatchButton = this.btnBatch;
            this.cmbTikubi1.Maximum = 255;
			JG2NumericUpDown jG2ComboBox = this.cmbTikubi1;
			point = new Point(333, 41);
			jG2ComboBox.Location = point;
			this.cmbTikubi1.Name = "cmbTikubi1";
			JG2NumericUpDown jG2ComboBox1 = this.cmbTikubi1;
			size = new System.Drawing.Size(104, 26);
			jG2ComboBox1.Size = size;
			this.cmbTikubi1.TabIndex = 31;
			this.numHiyake2.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown3 = this.numHiyake2;
			point = new Point(333, 162);
			jG2NumericUpDown3.Location = point;
			JG2NumericUpDown jG2NumericUpDown4 = this.numHiyake2;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown4.Maximum = num;
			this.numHiyake2.Name = "numHiyake2";
			JG2NumericUpDown jG2NumericUpDown5 = this.numHiyake2;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown5.Size = size;
			this.numHiyake2.TabIndex = 39;
			this.numHiyake2.TextAlign = HorizontalAlignment.Right;
			this.cmbTikubi2.BatchButton = this.btnBatch;
            this.cmbTikubi2.Maximum = num;
            jG2NumericUpDown5.Maximum = 255;
			JG2NumericUpDown jG2ComboBox2 = this.cmbTikubi2;
			point = new Point(333, 71);
			jG2ComboBox2.Location = point;
			this.cmbTikubi2.Name = "cmbTikubi2";
            jG2ComboBox2.Maximum = num;
            JG2NumericUpDown jG2ComboBox3 = this.cmbTikubi2;
			size = new System.Drawing.Size(104, 26);
			jG2ComboBox3.Size = size;
			this.cmbTikubi2.TabIndex = 33;
			this.numUnder2.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown6 = this.numUnder2;
			point = new Point(333, 252);
			jG2NumericUpDown6.Location = point;
			JG2NumericUpDown jG2NumericUpDown7 = this.numUnder2;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown7.Maximum = num;
			this.numUnder2.Name = "numUnder2";
			JG2NumericUpDown jG2NumericUpDown8 = this.numUnder2;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown8.Size = size;
			this.numUnder2.TabIndex = 45;
			this.numUnder2.TextAlign = HorizontalAlignment.Right;
			this.cmbHiyake1.BatchButton = this.btnBatch;
            JG2NumericUpDown jG2ComboBox4 = this.cmbHiyake1;
			point = new Point(333, 131);
			jG2ComboBox4.Location = point;
			this.cmbHiyake1.Name = "cmbHiyake1";
            jG2ComboBox4.Maximum = 255;
            JG2NumericUpDown jG2ComboBox5 = this.cmbHiyake1;
			size = new System.Drawing.Size(104, 26);
			jG2ComboBox5.Size = size;
			this.cmbHiyake1.TabIndex = 37;
			this.cmbMosaic.BatchButton = this.btnBatch;
            JG2NumericUpDown jG2ComboBox6 = this.cmbMosaic;
			point = new Point(333, 191);
			jG2ComboBox6.Location = point;
			this.cmbMosaic.Name = "cmbMosaic";
            jG2ComboBox6.Maximum = 255;
            JG2NumericUpDown jG2ComboBox7 = this.cmbMosaic;
			size = new System.Drawing.Size(104, 26);
			jG2ComboBox7.Size = size;
			this.cmbMosaic.TabIndex = 41;
			this.cmbUnder1.BatchButton = this.btnBatch;
            JG2NumericUpDown jG2ComboBox8 = this.cmbUnder1;
			point = new Point(333, 221);
			jG2ComboBox8.Location = point;
			this.cmbUnder1.Name = "cmbUnder1";
            jG2ComboBox8.Maximum = 255;
            JG2NumericUpDown jG2ComboBox9 = this.cmbUnder1;
			size = new System.Drawing.Size(104, 26);
			jG2ComboBox9.Size = size;
			this.cmbUnder1.TabIndex = 43;
			this.btnColorUnder.AllowDrop = true;
			this.btnColorUnder.BackColor = SystemColors.Control;
			this.btnColorUnder.BatchButton = this.btnBatch;
			JG2Button jG2Button = this.btnColorUnder;
			point = new Point(333, 281);
			jG2Button.Location = point;
			this.btnColorUnder.Name = "btnColorUnder";
			JG2Button jG2Button1 = this.btnColorUnder;
			size = new System.Drawing.Size(50, 26);
			jG2Button1.Size = size;
			this.btnColorUnder.TabIndex = 47;
			this.btnColorUnder.UseVisualStyleBackColor = true;
			Label label2 = this.lblColorUnder;
			point = new Point(233, 285);
			label2.Location = point;
			this.lblColorUnder.Name = "lblColorUnder";
			Label label3 = this.lblColorUnder;
			size = new System.Drawing.Size(92, 18);
			label3.Size = size;
			this.lblColorUnder.TabIndex = 46;
			Label label4 = this.lblUnder2;
			point = new Point(233, 255);
			label4.Location = point;
			this.lblUnder2.Name = "lblUnder2";
			Label label5 = this.lblUnder2;
			size = new System.Drawing.Size(92, 18);
			label5.Size = size;
			this.lblUnder2.TabIndex = 44;
			Label label6 = this.lblUnder1;
			point = new Point(233, 225);
			label6.Location = point;
			this.lblUnder1.Name = "lblUnder1";
			Label label7 = this.lblUnder1;
			size = new System.Drawing.Size(92, 18);
			label7.Size = size;
			this.lblUnder1.TabIndex = 42;
			Label label8 = this.lblMosaic;
			point = new Point(233, 195);
			label8.Location = point;
			this.lblMosaic.Name = "lblMosaic";
			Label label9 = this.lblMosaic;
			size = new System.Drawing.Size(92, 18);
			label9.Size = size;
			this.lblMosaic.TabIndex = 40;
			Label label10 = this.lblHiyake2;
			point = new Point(233, 165);
			label10.Location = point;
			this.lblHiyake2.Name = "lblHiyake2";
			Label label11 = this.lblHiyake2;
			size = new System.Drawing.Size(92, 18);
			label11.Size = size;
			this.lblHiyake2.TabIndex = 38;
			Label label12 = this.lblHiyake1;
			point = new Point(233, 135);
			label12.Location = point;
			this.lblHiyake1.Name = "lblHiyake1";
			Label label13 = this.lblHiyake1;
			size = new System.Drawing.Size(92, 18);
			label13.Size = size;
			this.lblHiyake1.TabIndex = 36;
			Label label14 = this.lblTikubi3;
			point = new Point(233, 105);
			label14.Location = point;
			this.lblTikubi3.Name = "lblTikubi3";
			Label label15 = this.lblTikubi3;
			size = new System.Drawing.Size(92, 18);
			label15.Size = size;
			this.lblTikubi3.TabIndex = 34;
			Label label16 = this.lblTikubi2;
			point = new Point(233, 75);
			label16.Location = point;
			this.lblTikubi2.Name = "lblTikubi2";
			Label label17 = this.lblTikubi2;
			size = new System.Drawing.Size(92, 18);
			label17.Size = size;
			this.lblTikubi2.TabIndex = 32;
			Label label18 = this.lblTikubi1;
			point = new Point(233, 45);
			label18.Location = point;
			this.lblTikubi1.Name = "lblTikubi1";
			Label label19 = this.lblTikubi1;
			size = new System.Drawing.Size(92, 18);
			label19.Size = size;
			this.lblTikubi1.TabIndex = 30;
			this.btnColorSkin2.AllowDrop = true;
			this.btnColorSkin2.BackColor = SystemColors.Control;
			this.btnColorSkin2.BatchButton = this.btnBatch;
			JG2Button jG2Button2 = this.btnColorSkin2;
			point = new Point(333, 11);
			jG2Button2.Location = point;
			this.btnColorSkin2.Name = "btnColorSkin2";
			JG2Button jG2Button3 = this.btnColorSkin2;
			size = new System.Drawing.Size(50, 26);
			jG2Button3.Size = size;
			this.btnColorSkin2.TabIndex = 29;
			this.btnColorSkin2.UseVisualStyleBackColor = true;
			Label label20 = this.lblColorSkin2;
			point = new Point(233, 15);
			label20.Location = point;
			this.lblColorSkin2.Name = "lblColorSkin2";
			Label label21 = this.lblColorSkin2;
			size = new System.Drawing.Size(92, 18);
			label21.Size = size;
			this.lblColorSkin2.TabIndex = 28;
			this.cmbMune8.BatchButton = this.btnBatch;
			this.cmbMune8.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox10 = this.cmbMune8;
			point = new Point(106, 371);
			jG2ComboBox10.Location = point;
			this.cmbMune8.Name = "cmbMune8";
			JG2ComboBox jG2ComboBox11 = this.cmbMune8;
			size = new System.Drawing.Size(50, 26);
			jG2ComboBox11.Size = size;
			this.cmbMune8.TabIndex = 25;
			Label label22 = this.lblNyurin;
			point = new Point(6, 405);
			label22.Location = point;
			this.lblNyurin.Name = "lblNyurin";
			Label label23 = this.lblNyurin;
			size = new System.Drawing.Size(92, 18);
			label23.Size = size;
			this.lblNyurin.TabIndex = 26;
			this.numNyurin.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown9 = this.numNyurin;
			point = new Point(106, 402);
			jG2NumericUpDown9.Location = point;
			JG2NumericUpDown jG2NumericUpDown10 = this.numNyurin;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown10.Maximum = num;
			this.numNyurin.Name = "numNyurin";
			JG2NumericUpDown jG2NumericUpDown11 = this.numNyurin;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown11.Size = size;
			this.numNyurin.TabIndex = 27;
			this.numNyurin.TextAlign = HorizontalAlignment.Right;
			Label label24 = this.lblMune8;
			point = new Point(6, 375);
			label24.Location = point;
			this.lblMune8.Name = "lblMune8";
			Label label25 = this.lblMune8;
			size = new System.Drawing.Size(92, 18);
			label25.Size = size;
			this.lblMune8.TabIndex = 24;
			Label label26 = this.lblMune7;
			point = new Point(6, 345);
			label26.Location = point;
			this.lblMune7.Name = "lblMune7";
			Label label27 = this.lblMune7;
			size = new System.Drawing.Size(92, 18);
			label27.Size = size;
			this.lblMune7.TabIndex = 22;
			this.numMune7.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown12 = this.numMune7;
			point = new Point(106, 342);
			jG2NumericUpDown12.Location = point;
			JG2NumericUpDown jG2NumericUpDown13 = this.numMune7;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown13.Maximum = num;
			this.numMune7.Name = "numMune7";
			JG2NumericUpDown jG2NumericUpDown14 = this.numMune7;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown14.Size = size;
			this.numMune7.TabIndex = 23;
			this.numMune7.TextAlign = HorizontalAlignment.Right;
			Label label28 = this.lblMune6;
			point = new Point(6, 315);
			label28.Location = point;
			this.lblMune6.Name = "lblMune6";
			Label label29 = this.lblMune6;
			size = new System.Drawing.Size(92, 18);
			label29.Size = size;
			this.lblMune6.TabIndex = 20;
			this.numMune6.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown15 = this.numMune6;
			point = new Point(106, 312);
			jG2NumericUpDown15.Location = point;
			JG2NumericUpDown jG2NumericUpDown16 = this.numMune6;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown16.Maximum = num;
			this.numMune6.Name = "numMune6";
			JG2NumericUpDown jG2NumericUpDown17 = this.numMune6;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown17.Size = size;
			this.numMune6.TabIndex = 21;
			this.numMune6.TextAlign = HorizontalAlignment.Right;
			Label label30 = this.lblMune5;
			point = new Point(6, 285);
			label30.Location = point;
			this.lblMune5.Name = "lblMune5";
			Label label31 = this.lblMune5;
			size = new System.Drawing.Size(92, 18);
			label31.Size = size;
			this.lblMune5.TabIndex = 18;
			this.numMune5.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown18 = this.numMune5;
			point = new Point(106, 282);
			jG2NumericUpDown18.Location = point;
			JG2NumericUpDown jG2NumericUpDown19 = this.numMune5;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown19.Maximum = num;
			this.numMune5.Name = "numMune5";
			JG2NumericUpDown jG2NumericUpDown20 = this.numMune5;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown20.Size = size;
			this.numMune5.TabIndex = 19;
			this.numMune5.TextAlign = HorizontalAlignment.Right;
			Label label32 = this.lblMune4;
			point = new Point(6, 255);
			label32.Location = point;
			this.lblMune4.Name = "lblMune4";
			Label label33 = this.lblMune4;
			size = new System.Drawing.Size(92, 18);
			label33.Size = size;
			this.lblMune4.TabIndex = 16;
			this.numMune4.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown21 = this.numMune4;
			point = new Point(106, 252);
			jG2NumericUpDown21.Location = point;
			JG2NumericUpDown jG2NumericUpDown22 = this.numMune4;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown22.Maximum = num;
			this.numMune4.Name = "numMune4";
			JG2NumericUpDown jG2NumericUpDown23 = this.numMune4;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown23.Size = size;
			this.numMune4.TabIndex = 17;
			this.numMune4.TextAlign = HorizontalAlignment.Right;
			Label label34 = this.lblMune3;
			point = new Point(6, 225);
			label34.Location = point;
			this.lblMune3.Name = "lblMune3";
			Label label35 = this.lblMune3;
			size = new System.Drawing.Size(92, 18);
			label35.Size = size;
			this.lblMune3.TabIndex = 14;
			this.numMune3.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown24 = this.numMune3;
			point = new Point(106, 222);
			jG2NumericUpDown24.Location = point;
			JG2NumericUpDown jG2NumericUpDown25 = this.numMune3;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown25.Maximum = num;
			this.numMune3.Name = "numMune3";
			JG2NumericUpDown jG2NumericUpDown26 = this.numMune3;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown26.Size = size;
			this.numMune3.TabIndex = 15;
			this.numMune3.TextAlign = HorizontalAlignment.Right;
			this.cmbMune2.BatchButton = this.btnBatch;
			this.cmbMune2.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox12 = this.cmbMune2;
			point = new Point(106, 191);
			jG2ComboBox12.Location = point;
			this.cmbMune2.Name = "cmbMune2";
			JG2ComboBox jG2ComboBox13 = this.cmbMune2;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox13.Size = size;
			this.cmbMune2.TabIndex = 13;
			Label label36 = this.lblMune2;
			point = new Point(6, 195);
			label36.Location = point;
			this.lblMune2.Name = "lblMune2";
			Label label37 = this.lblMune2;
			size = new System.Drawing.Size(92, 18);
			label37.Size = size;
			this.lblMune2.TabIndex = 12;
			Label label38 = this.lblMune1;
			point = new Point(6, 165);
			label38.Location = point;
			this.lblMune1.Name = "lblMune1";
			Label label39 = this.lblMune1;
			size = new System.Drawing.Size(92, 18);
			label39.Size = size;
			this.lblMune1.TabIndex = 10;
			this.numMune1.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown27 = this.numMune1;
			point = new Point(106, 162);
			jG2NumericUpDown27.Location = point;
			JG2NumericUpDown jG2NumericUpDown28 = this.numMune1;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown28.Maximum = num;
			this.numMune1.Name = "numMune1";
			JG2NumericUpDown jG2NumericUpDown29 = this.numMune1;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown29.Size = size;
			this.numMune1.TabIndex = 11;
			this.numMune1.TextAlign = HorizontalAlignment.Right;
			Label label40 = this.lblWaist;
			point = new Point(6, 135);
			label40.Location = point;
			this.lblWaist.Name = "lblWaist";
			Label label41 = this.lblWaist;
			size = new System.Drawing.Size(92, 18);
			label41.Size = size;
			this.lblWaist.TabIndex = 8;
			this.numWaist.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown30 = this.numWaist;
			point = new Point(106, 132);
			jG2NumericUpDown30.Location = point;
			JG2NumericUpDown jG2NumericUpDown31 = this.numWaist;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown31.Maximum = num;
			this.numWaist.Name = "numWaist";
			JG2NumericUpDown jG2NumericUpDown32 = this.numWaist;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown32.Size = size;
			this.numWaist.TabIndex = 9;
			this.numWaist.TextAlign = HorizontalAlignment.Right;
			Label label42 = this.lblAtama2;
			point = new Point(6, 105);
			label42.Location = point;
			this.lblAtama2.Name = "lblAtama2";
			Label label43 = this.lblAtama2;
			size = new System.Drawing.Size(92, 18);
			label43.Size = size;
			this.lblAtama2.TabIndex = 6;
			this.numAtama2.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown33 = this.numAtama2;
			point = new Point(106, 102);
			jG2NumericUpDown33.Location = point;
			JG2NumericUpDown jG2NumericUpDown34 = this.numAtama2;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown34.Maximum = num;
			this.numAtama2.Name = "numAtama2";
			JG2NumericUpDown jG2NumericUpDown35 = this.numAtama2;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown35.Size = size;
			this.numAtama2.TabIndex = 7;
			this.numAtama2.TextAlign = HorizontalAlignment.Right;
			Label label44 = this.lblAtama1;
			point = new Point(6, 75);
			label44.Location = point;
			this.lblAtama1.Name = "lblAtama1";
			Label label45 = this.lblAtama1;
			size = new System.Drawing.Size(92, 18);
			label45.Size = size;
			this.lblAtama1.TabIndex = 4;
			this.numAtama1.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown36 = this.numAtama1;
			point = new Point(106, 72);
			jG2NumericUpDown36.Location = point;
			JG2NumericUpDown jG2NumericUpDown37 = this.numAtama1;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown37.Maximum = num;
			this.numAtama1.Name = "numAtama1";
			JG2NumericUpDown jG2NumericUpDown38 = this.numAtama1;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown38.Size = size;
			this.numAtama1.TabIndex = 5;
			this.numAtama1.TextAlign = HorizontalAlignment.Right;
			this.cmbStyle.BatchButton = this.btnBatch;
			this.cmbStyle.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox14 = this.cmbStyle;
			point = new Point(106, 41);
			jG2ComboBox14.Location = point;
			this.cmbStyle.Name = "cmbStyle";
			JG2ComboBox jG2ComboBox15 = this.cmbStyle;
			size = new System.Drawing.Size(50, 26);
			jG2ComboBox15.Size = size;
			this.cmbStyle.TabIndex = 3;
			Label label46 = this.lblStyle;
			point = new Point(6, 45);
			label46.Location = point;
			this.lblStyle.Name = "lblStyle";
			Label label47 = this.lblStyle;
			size = new System.Drawing.Size(92, 18);
			label47.Size = size;
			this.lblStyle.TabIndex = 2;
			this.cmbHeight.BatchButton = this.btnBatch;
			this.cmbHeight.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox16 = this.cmbHeight;
			point = new Point(106, 11);
			jG2ComboBox16.Location = point;
			this.cmbHeight.Name = "cmbHeight";
			JG2ComboBox jG2ComboBox17 = this.cmbHeight;
			size = new System.Drawing.Size(50, 26);
			jG2ComboBox17.Size = size;
			this.cmbHeight.TabIndex = 1;
			Label label48 = this.lblHeight;
			point = new Point(6, 15);
			label48.Location = point;
			this.lblHeight.Name = "lblHeight";
			Label label49 = this.lblHeight;
			size = new System.Drawing.Size(92, 18);
			label49.Size = size;
			this.lblHeight.TabIndex = 0;
			this.tbpFace.BackColor = SystemColors.Control;
			this.tbpFace.Controls.Add(this.cmbLip1);
			this.tbpFace.Controls.Add(this.lblMabuta);
			this.tbpFace.Controls.Add(this.pnlHokuro);
			this.tbpFace.Controls.Add(this.lblMatuge1);
			this.tbpFace.Controls.Add(this.lblMayu1);
			this.tbpFace.Controls.Add(this.lblMayu2);
			this.tbpFace.Controls.Add(this.lblMatuge2);
			this.tbpFace.Controls.Add(this.lblHighlight1);
			this.tbpFace.Controls.Add(this.lblColorMayu);
			this.tbpFace.Controls.Add(this.lblGlasses2);
			this.tbpFace.Controls.Add(this.lblColorGlasses);
			this.tbpFace.Controls.Add(this.lblPupil5);
			this.tbpFace.Controls.Add(this.lblHokuro);
			this.tbpFace.Controls.Add(this.lblLip2);
			this.tbpFace.Controls.Add(this.lblPupil2);
			this.tbpFace.Controls.Add(this.lblLip1);
			this.tbpFace.Controls.Add(this.lblPupil4);
			this.tbpFace.Controls.Add(this.lblPupil3);
			this.tbpFace.Controls.Add(this.lblPupil1);
			this.tbpFace.Controls.Add(this.lblEye1);
			this.tbpFace.Controls.Add(this.lblEye5);
			this.tbpFace.Controls.Add(this.lblEye4);
			this.tbpFace.Controls.Add(this.lblEye3);
			this.tbpFace.Controls.Add(this.lblEye2);
			this.tbpFace.Controls.Add(this.lblKao);
			this.tbpFace.Controls.Add(this.btnColorEye2Sub);
			this.tbpFace.Controls.Add(this.btnColorEye1Sub);
			this.tbpFace.Controls.Add(this.lblColorEye2);
			this.tbpFace.Controls.Add(this.lblColorEye1);
			this.tbpFace.Controls.Add(this.cmbMatuge1);
			this.tbpFace.Controls.Add(this.cmbHighlight1);
			this.tbpFace.Controls.Add(this.btnColorGlasses);
			this.tbpFace.Controls.Add(this.cmbGlasses2);
			this.tbpFace.Controls.Add(this.cmbPupil5);
			this.tbpFace.Controls.Add(this.cmbMatuge2);
			this.tbpFace.Controls.Add(this.numMayu2);
			this.tbpFace.Controls.Add(this.cmbMabuta);
			this.tbpFace.Controls.Add(this.btnColorMayu);
			this.tbpFace.Controls.Add(this.numPupil2);
			this.tbpFace.Controls.Add(this.numLip2);
			this.tbpFace.Controls.Add(this.cmbMayu1);
			this.tbpFace.Controls.Add(this.numPupil4);
			this.tbpFace.Controls.Add(this.numPupil3);
			this.tbpFace.Controls.Add(this.cmbPupil1);
			this.tbpFace.Controls.Add(this.numEye1);
			this.tbpFace.Controls.Add(this.numEye5);
			this.tbpFace.Controls.Add(this.numEye4);
			this.tbpFace.Controls.Add(this.numEye3);
			this.tbpFace.Controls.Add(this.numEye2);
			this.tbpFace.Controls.Add(this.cmbKao);
			this.tbpFace.Controls.Add(this.btnColorEye2);
			this.tbpFace.Controls.Add(this.btnColorEye1);
			TabPage tabPage2 = this.tbpFace;
			point = new Point(4, 28);
			tabPage2.Location = point;
			this.tbpFace.Name = "tbpFace";
			TabPage tabPage3 = this.tbpFace;
			size = new System.Drawing.Size(460, 476);
			tabPage3.Size = size;
			this.tbpFace.TabIndex = 10;
            this.tbpFace.Text = Resources.TabFace;
			this.cmbLip1.BatchButton = this.btnBatch;
            JG2NumericUpDown jG2ComboBox18 = this.cmbLip1;
			point = new Point(333, 341);
			jG2ComboBox18.Location = point;
            jG2ComboBox18.Maximum = 255;
			this.cmbLip1.Name = "cmbLip1";
            JG2NumericUpDown jG2ComboBox19 = this.cmbLip1;
			size = new System.Drawing.Size(50, 26);
			jG2ComboBox19.Size = size;
			this.cmbLip1.TabIndex = 49;
			Label label50 = this.lblMabuta;
			point = new Point(233, 105);
			label50.Location = point;
			this.lblMabuta.Name = "lblMabuta";
			Label label51 = this.lblMabuta;
			size = new System.Drawing.Size(92, 18);
			label51.Size = size;
			this.lblMabuta.TabIndex = 36;
			this.pnlHokuro.Controls.Add(this.chkHokuro1);
			this.pnlHokuro.Controls.Add(this.chkHokuro4);
			this.pnlHokuro.Controls.Add(this.chkHokuro3);
			this.pnlHokuro.Controls.Add(this.chkHokuro2);
			Panel panel2 = this.pnlHokuro;
			point = new Point(333, 254);
			panel2.Location = point;
			this.pnlHokuro.Name = "pnlHokuro";
			Panel panel3 = this.pnlHokuro;
			size = new System.Drawing.Size(115, 88);
			panel3.Size = size;
			this.pnlHokuro.TabIndex = 47;
			this.chkHokuro1.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox = this.chkHokuro1;
			point = new Point(3, 0);
			jG2CheckBox.Location = point;
			this.chkHokuro1.Name = "chkHokuro1";
			JG2CheckBox jG2CheckBox1 = this.chkHokuro1;
			size = new System.Drawing.Size(80, 20);
			jG2CheckBox1.Size = size;
			this.chkHokuro1.TabIndex = 0;
			this.chkHokuro1.UseVisualStyleBackColor = true;
			this.chkHokuro4.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox2 = this.chkHokuro4;
			point = new Point(3, 66);
			jG2CheckBox2.Location = point;
			this.chkHokuro4.Name = "chkHokuro4";
			JG2CheckBox jG2CheckBox3 = this.chkHokuro4;
			size = new System.Drawing.Size(80, 20);
			jG2CheckBox3.Size = size;
			this.chkHokuro4.TabIndex = 3;
			this.chkHokuro4.UseVisualStyleBackColor = true;
			this.chkHokuro3.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox4 = this.chkHokuro3;
			point = new Point(3, 44);
			jG2CheckBox4.Location = point;
			this.chkHokuro3.Name = "chkHokuro3";
			JG2CheckBox jG2CheckBox5 = this.chkHokuro3;
			size = new System.Drawing.Size(80, 20);
			jG2CheckBox5.Size = size;
			this.chkHokuro3.TabIndex = 2;
			this.chkHokuro3.UseVisualStyleBackColor = true;
			this.chkHokuro2.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox6 = this.chkHokuro2;
			point = new Point(3, 22);
			jG2CheckBox6.Location = point;
			this.chkHokuro2.Name = "chkHokuro2";
			JG2CheckBox jG2CheckBox7 = this.chkHokuro2;
			size = new System.Drawing.Size(80, 20);
			jG2CheckBox7.Size = size;
			this.chkHokuro2.TabIndex = 1;
			this.chkHokuro2.UseVisualStyleBackColor = true;
			Label label52 = this.lblMatuge1;
			point = new Point(233, 135);
			label52.Location = point;
			this.lblMatuge1.Name = "lblMatuge1";
			Label label53 = this.lblMatuge1;
			size = new System.Drawing.Size(92, 18);
			label53.Size = size;
			this.lblMatuge1.TabIndex = 38;
			Label label54 = this.lblMayu1;
			point = new Point(233, 15);
			label54.Location = point;
			this.lblMayu1.Name = "lblMayu1";
			Label label55 = this.lblMayu1;
			size = new System.Drawing.Size(92, 18);
			label55.Size = size;
			this.lblMayu1.TabIndex = 30;
			Label label56 = this.lblMayu2;
			point = new Point(233, 45);
			label56.Location = point;
			this.lblMayu2.Name = "lblMayu2";
			Label label57 = this.lblMayu2;
			size = new System.Drawing.Size(92, 18);
			label57.Size = size;
			this.lblMayu2.TabIndex = 32;
			Label label58 = this.lblMatuge2;
			point = new Point(233, 165);
			label58.Location = point;
			this.lblMatuge2.Name = "lblMatuge2";
			Label label59 = this.lblMatuge2;
			size = new System.Drawing.Size(92, 18);
			label59.Size = size;
			this.lblMatuge2.TabIndex = 40;
			Label label60 = this.lblHighlight1;
			point = new Point(6, 345);
			label60.Location = point;
			this.lblHighlight1.Name = "lblHighlight1";
			Label label61 = this.lblHighlight1;
			size = new System.Drawing.Size(92, 18);
			label61.Size = size;
			this.lblHighlight1.TabIndex = 22;
			Label label62 = this.lblColorMayu;
			point = new Point(233, 75);
			label62.Location = point;
			this.lblColorMayu.Name = "lblColorMayu";
			Label label63 = this.lblColorMayu;
			size = new System.Drawing.Size(92, 18);
			label63.Size = size;
			this.lblColorMayu.TabIndex = 34;
			Label label64 = this.lblGlasses2;
			point = new Point(233, 195);
			label64.Location = point;
			this.lblGlasses2.Name = "lblGlasses2";
			Label label65 = this.lblGlasses2;
			size = new System.Drawing.Size(92, 18);
			label65.Size = size;
			this.lblGlasses2.TabIndex = 42;
			Label label66 = this.lblColorGlasses;
			point = new Point(233, 225);
			label66.Location = point;
			this.lblColorGlasses.Name = "lblColorGlasses";
			Label label67 = this.lblColorGlasses;
			size = new System.Drawing.Size(92, 18);
			label67.Size = size;
			this.lblColorGlasses.TabIndex = 44;
			Label label68 = this.lblPupil5;
			point = new Point(6, 315);
			label68.Location = point;
			this.lblPupil5.Name = "lblPupil5";
			Label label69 = this.lblPupil5;
			size = new System.Drawing.Size(92, 18);
			label69.Size = size;
			this.lblPupil5.TabIndex = 20;
			Label label70 = this.lblHokuro;
			point = new Point(233, 255);
			label70.Location = point;
			this.lblHokuro.Name = "lblHokuro";
			Label label71 = this.lblHokuro;
			size = new System.Drawing.Size(92, 18);
			label71.Size = size;
			this.lblHokuro.TabIndex = 46;
			Label label72 = this.lblLip2;
			point = new Point(233, 375);
			label72.Location = point;
			this.lblLip2.Name = "lblLip2";
			Label label73 = this.lblLip2;
			size = new System.Drawing.Size(92, 18);
			label73.Size = size;
			this.lblLip2.TabIndex = 50;
			Label label74 = this.lblPupil2;
			point = new Point(6, 225);
			label74.Location = point;
			this.lblPupil2.Name = "lblPupil2";
			Label label75 = this.lblPupil2;
			size = new System.Drawing.Size(92, 18);
			label75.Size = size;
			this.lblPupil2.TabIndex = 14;
			Label label76 = this.lblLip1;
			point = new Point(233, 345);
			label76.Location = point;
			this.lblLip1.Name = "lblLip1";
			Label label77 = this.lblLip1;
			size = new System.Drawing.Size(92, 18);
			label77.Size = size;
			this.lblLip1.TabIndex = 48;
			Label label78 = this.lblPupil4;
			point = new Point(6, 285);
			label78.Location = point;
			this.lblPupil4.Name = "lblPupil4";
			Label label79 = this.lblPupil4;
			size = new System.Drawing.Size(92, 18);
			label79.Size = size;
			this.lblPupil4.TabIndex = 18;
			Label label80 = this.lblPupil3;
			point = new Point(6, 255);
			label80.Location = point;
			this.lblPupil3.Name = "lblPupil3";
			Label label81 = this.lblPupil3;
			size = new System.Drawing.Size(92, 18);
			label81.Size = size;
			this.lblPupil3.TabIndex = 16;
			Label label82 = this.lblPupil1;
			point = new Point(6, 195);
			label82.Location = point;
			this.lblPupil1.Name = "lblPupil1";
			Label label83 = this.lblPupil1;
			size = new System.Drawing.Size(92, 18);
			label83.Size = size;
			this.lblPupil1.TabIndex = 12;
			Label label84 = this.lblEye1;
			point = new Point(6, 45);
			label84.Location = point;
			this.lblEye1.Name = "lblEye1";
			Label label85 = this.lblEye1;
			size = new System.Drawing.Size(92, 18);
			label85.Size = size;
			this.lblEye1.TabIndex = 2;
			Label label86 = this.lblEye5;
			point = new Point(6, 165);
			label86.Location = point;
			this.lblEye5.Name = "lblEye5";
			Label label87 = this.lblEye5;
			size = new System.Drawing.Size(92, 18);
			label87.Size = size;
			this.lblEye5.TabIndex = 10;
			Label label88 = this.lblEye4;
			point = new Point(6, 135);
			label88.Location = point;
			this.lblEye4.Name = "lblEye4";
			Label label89 = this.lblEye4;
			size = new System.Drawing.Size(92, 18);
			label89.Size = size;
			this.lblEye4.TabIndex = 8;
			Label label90 = this.lblEye3;
			point = new Point(6, 105);
			label90.Location = point;
			this.lblEye3.Name = "lblEye3";
			Label label91 = this.lblEye3;
			size = new System.Drawing.Size(92, 18);
			label91.Size = size;
			this.lblEye3.TabIndex = 6;
			Label label92 = this.lblEye2;
			point = new Point(6, 75);
			label92.Location = point;
			this.lblEye2.Name = "lblEye2";
			Label label93 = this.lblEye2;
			size = new System.Drawing.Size(92, 18);
			label93.Size = size;
			this.lblEye2.TabIndex = 4;
			Label label94 = this.lblKao;
			point = new Point(6, 15);
			label94.Location = point;
			this.lblKao.Name = "lblKao";
			Label label95 = this.lblKao;
			size = new System.Drawing.Size(92, 18);
			label95.Size = size;
			this.lblKao.TabIndex = 0;
			this.btnColorEye2Sub.BackColor = SystemColors.Control;
			Button button12 = this.btnColorEye2Sub;
			point = new Point(160, 401);
			button12.Location = point;
			this.btnColorEye2Sub.Name = "btnColorEye2Sub";
			Button button13 = this.btnColorEye2Sub;
			size = new System.Drawing.Size(50, 26);
			button13.Size = size;
			this.btnColorEye2Sub.TabIndex = 29;
			this.btnColorEye2Sub.TabStop = false;
            this.btnColorEye2Sub.Text = Resources.CaptionToLeftEye;
			this.btnColorEye2Sub.UseVisualStyleBackColor = true;
			this.btnColorEye1Sub.BackColor = SystemColors.Control;
			Button button14 = this.btnColorEye1Sub;
			point = new Point(160, 371);
			button14.Location = point;
			this.btnColorEye1Sub.Name = "btnColorEye1Sub";
			Button button15 = this.btnColorEye1Sub;
			size = new System.Drawing.Size(50, 26);
			button15.Size = size;
			this.btnColorEye1Sub.TabIndex = 26;
			this.btnColorEye1Sub.TabStop = false;
            this.btnColorEye1Sub.Text = Resources.CaptionToRightEye;
			this.btnColorEye1Sub.UseVisualStyleBackColor = true;
			Label label96 = this.lblColorEye2;
			point = new Point(6, 405);
			label96.Location = point;
			this.lblColorEye2.Name = "lblColorEye2";
			Label label97 = this.lblColorEye2;
			size = new System.Drawing.Size(92, 18);
			label97.Size = size;
			this.lblColorEye2.TabIndex = 27;
			Label label98 = this.lblColorEye1;
			point = new Point(6, 375);
			label98.Location = point;
			this.lblColorEye1.Name = "lblColorEye1";
			Label label99 = this.lblColorEye1;
			size = new System.Drawing.Size(92, 18);
			label99.Size = size;
			this.lblColorEye1.TabIndex = 24;
			this.cmbMatuge1.BatchButton = this.btnBatch;
			this.cmbMatuge1.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox20 = this.cmbMatuge1;
			point = new Point(333, 131);
			jG2ComboBox20.Location = point;
			this.cmbMatuge1.Name = "cmbMatuge1";
			JG2ComboBox jG2ComboBox21 = this.cmbMatuge1;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox21.Size = size;
			this.cmbMatuge1.TabIndex = 39;
			this.cmbHighlight1.BatchButton = this.btnBatch;
			this.cmbHighlight1.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox22 = this.cmbHighlight1;
			point = new Point(106, 341);
			jG2ComboBox22.Location = point;
			this.cmbHighlight1.Name = "cmbHighlight1";
			JG2ComboBox jG2ComboBox23 = this.cmbHighlight1;
			size = new System.Drawing.Size(104, 26);
			jG2ComboBox23.Size = size;
			this.cmbHighlight1.TabIndex = 23;
			this.btnColorGlasses.AllowDrop = true;
			this.btnColorGlasses.BackColor = SystemColors.Control;
			this.btnColorGlasses.BatchButton = this.btnBatch;
			JG2Button jG2Button4 = this.btnColorGlasses;
			point = new Point(333, 221);
			jG2Button4.Location = point;
			this.btnColorGlasses.Name = "btnColorGlasses";
			JG2Button jG2Button5 = this.btnColorGlasses;
			size = new System.Drawing.Size(50, 26);
			jG2Button5.Size = size;
			this.btnColorGlasses.TabIndex = 45;
			this.btnColorGlasses.UseVisualStyleBackColor = true;
			this.cmbGlasses2.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2ComboBox24 = this.cmbGlasses2;
			point = new Point(333, 191);
			jG2ComboBox24.Location = point;
			this.cmbGlasses2.Name = "cmbGlasses2";
            JG2NumericUpDown jG2ComboBox25 = this.cmbGlasses2;
			size = new System.Drawing.Size(104, 26);
			jG2ComboBox25.Size = size;
			this.cmbGlasses2.TabIndex = 43;
			this.cmbPupil5.BatchButton = this.btnBatch;
            this.cmbPupil5.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox26 = this.cmbPupil5;
			point = new Point(106, 311);
			jG2ComboBox26.Location = point;
			this.cmbPupil5.Name = "cmbPupil5";
			JG2ComboBox jG2ComboBox27 = this.cmbPupil5;
			size = new System.Drawing.Size(104, 26);
			jG2ComboBox27.Size = size;
			this.cmbPupil5.TabIndex = 21;
			this.cmbMatuge2.BatchButton = this.btnBatch;
			this.cmbMatuge2.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox28 = this.cmbMatuge2;
			point = new Point(333, 161);
			jG2ComboBox28.Location = point;
			this.cmbMatuge2.Name = "cmbMatuge2";
			JG2ComboBox jG2ComboBox29 = this.cmbMatuge2;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox29.Size = size;
			this.cmbMatuge2.TabIndex = 41;
			this.numMayu2.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown39 = this.numMayu2;
			point = new Point(333, 42);
			jG2NumericUpDown39.Location = point;
			JG2NumericUpDown jG2NumericUpDown40 = this.numMayu2;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown40.Maximum = num;
			this.numMayu2.Name = "numMayu2";
			JG2NumericUpDown jG2NumericUpDown41 = this.numMayu2;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown41.Size = size;
			this.numMayu2.TabIndex = 33;
			this.numMayu2.TextAlign = HorizontalAlignment.Right;
			this.cmbMabuta.BatchButton = this.btnBatch;
			this.cmbMabuta.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox30 = this.cmbMabuta;
			point = new Point(333, 101);
			jG2ComboBox30.Location = point;
			this.cmbMabuta.Name = "cmbMabuta";
			JG2ComboBox jG2ComboBox31 = this.cmbMabuta;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox31.Size = size;
			this.cmbMabuta.TabIndex = 37;
			this.btnColorMayu.AllowDrop = true;
			this.btnColorMayu.BackColor = SystemColors.Control;
			this.btnColorMayu.BatchButton = this.btnBatch;
			JG2Button jG2Button6 = this.btnColorMayu;
			point = new Point(333, 71);
			jG2Button6.Location = point;
			this.btnColorMayu.Name = "btnColorMayu";
			JG2Button jG2Button7 = this.btnColorMayu;
			size = new System.Drawing.Size(50, 26);
			jG2Button7.Size = size;
			this.btnColorMayu.TabIndex = 35;
			this.btnColorMayu.UseVisualStyleBackColor = true;
			this.numPupil2.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown42 = this.numPupil2;
			point = new Point(106, 222);
			jG2NumericUpDown42.Location = point;
			JG2NumericUpDown jG2NumericUpDown43 = this.numPupil2;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown43.Maximum = num;
			this.numPupil2.Name = "numPupil2";
			JG2NumericUpDown jG2NumericUpDown44 = this.numPupil2;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown44.Size = size;
			this.numPupil2.TabIndex = 15;
			this.numPupil2.TextAlign = HorizontalAlignment.Right;
			this.numLip2.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown45 = this.numLip2;
			point = new Point(333, 372);
			jG2NumericUpDown45.Location = point;
			JG2NumericUpDown jG2NumericUpDown46 = this.numLip2;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown46.Maximum = num;
			this.numLip2.Name = "numLip2";
			JG2NumericUpDown jG2NumericUpDown47 = this.numLip2;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown47.Size = size;
			this.numLip2.TabIndex = 51;
			this.numLip2.TextAlign = HorizontalAlignment.Right;
			this.cmbMayu1.BatchButton = this.btnBatch;
			this.cmbMayu1.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox32 = this.cmbMayu1;
			point = new Point(333, 11);
			jG2ComboBox32.Location = point;
			this.cmbMayu1.Name = "cmbMayu1";
			JG2ComboBox jG2ComboBox33 = this.cmbMayu1;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox33.Size = size;
			this.cmbMayu1.TabIndex = 31;
			this.numPupil4.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown48 = this.numPupil4;
			point = new Point(106, 282);
			jG2NumericUpDown48.Location = point;
			JG2NumericUpDown jG2NumericUpDown49 = this.numPupil4;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown49.Maximum = num;
			this.numPupil4.Name = "numPupil4";
			JG2NumericUpDown jG2NumericUpDown50 = this.numPupil4;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown50.Size = size;
			this.numPupil4.TabIndex = 19;
			this.numPupil4.TextAlign = HorizontalAlignment.Right;
			this.numPupil3.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown51 = this.numPupil3;
			point = new Point(106, 252);
			jG2NumericUpDown51.Location = point;
			JG2NumericUpDown jG2NumericUpDown52 = this.numPupil3;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown52.Maximum = num;
			this.numPupil3.Name = "numPupil3";
			JG2NumericUpDown jG2NumericUpDown53 = this.numPupil3;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown53.Size = size;
			this.numPupil3.TabIndex = 17;
			this.numPupil3.TextAlign = HorizontalAlignment.Right;
			this.cmbPupil1.BatchButton = this.btnBatch;
			this.cmbPupil1.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox34 = this.cmbPupil1;
			point = new Point(106, 191);
			jG2ComboBox34.Location = point;
			this.cmbPupil1.Name = "cmbPupil1";
			JG2ComboBox jG2ComboBox35 = this.cmbPupil1;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox35.Size = size;
			this.cmbPupil1.TabIndex = 13;
			this.numEye1.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown54 = this.numEye1;
			point = new Point(106, 42);
			jG2NumericUpDown54.Location = point;
			JG2NumericUpDown jG2NumericUpDown55 = this.numEye1;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown55.Maximum = num;
			this.numEye1.Name = "numEye1";
			JG2NumericUpDown jG2NumericUpDown56 = this.numEye1;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown56.Size = size;
			this.numEye1.TabIndex = 3;
			this.numEye1.TextAlign = HorizontalAlignment.Right;
			this.numEye5.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown57 = this.numEye5;
			point = new Point(106, 162);
			jG2NumericUpDown57.Location = point;
			JG2NumericUpDown jG2NumericUpDown58 = this.numEye5;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown58.Maximum = num;
			this.numEye5.Name = "numEye5";
			JG2NumericUpDown jG2NumericUpDown59 = this.numEye5;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown59.Size = size;
			this.numEye5.TabIndex = 11;
			this.numEye5.TextAlign = HorizontalAlignment.Right;
			this.numEye4.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown60 = this.numEye4;
			point = new Point(106, 132);
			jG2NumericUpDown60.Location = point;
			JG2NumericUpDown jG2NumericUpDown61 = this.numEye4;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown61.Maximum = num;
			this.numEye4.Name = "numEye4";
			JG2NumericUpDown jG2NumericUpDown62 = this.numEye4;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown62.Size = size;
			this.numEye4.TabIndex = 9;
			this.numEye4.TextAlign = HorizontalAlignment.Right;
			this.numEye3.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown63 = this.numEye3;
			point = new Point(106, 102);
			jG2NumericUpDown63.Location = point;
			JG2NumericUpDown jG2NumericUpDown64 = this.numEye3;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown64.Maximum = num;
			this.numEye3.Name = "numEye3";
			JG2NumericUpDown jG2NumericUpDown65 = this.numEye3;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown65.Size = size;
			this.numEye3.TabIndex = 7;
			this.numEye3.TextAlign = HorizontalAlignment.Right;
			this.numEye2.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown66 = this.numEye2;
			point = new Point(106, 72);
			jG2NumericUpDown66.Location = point;
			JG2NumericUpDown jG2NumericUpDown67 = this.numEye2;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown67.Maximum = num;
			this.numEye2.Name = "numEye2";
			JG2NumericUpDown jG2NumericUpDown68 = this.numEye2;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown68.Size = size;
			this.numEye2.TabIndex = 5;
			this.numEye2.TextAlign = HorizontalAlignment.Right;
			this.cmbKao.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2ComboBox36 = this.cmbKao;
			point = new Point(106, 11);
			jG2ComboBox36.Location = point;
			this.cmbKao.Name = "cmbKao";
            jG2ComboBox36.Maximum = 255;
			JG2NumericUpDown jG2ComboBox37 = this.cmbKao;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox37.Size = size;
			this.cmbKao.TabIndex = 1;
			this.btnColorEye2.AllowDrop = true;
			this.btnColorEye2.BackColor = SystemColors.Control;
			this.btnColorEye2.BatchButton = this.btnBatch;
			JG2Button jG2Button8 = this.btnColorEye2;
			point = new Point(106, 401);
			jG2Button8.Location = point;
			this.btnColorEye2.Name = "btnColorEye2";
			JG2Button jG2Button9 = this.btnColorEye2;
			size = new System.Drawing.Size(50, 26);
			jG2Button9.Size = size;
			this.btnColorEye2.TabIndex = 28;
			this.btnColorEye2.UseVisualStyleBackColor = true;
			this.btnColorEye1.AllowDrop = true;
			this.btnColorEye1.BackColor = SystemColors.Control;
			this.btnColorEye1.BatchButton = this.btnBatch;
			JG2Button jG2Button10 = this.btnColorEye1;
			point = new Point(106, 371);
			jG2Button10.Location = point;
			this.btnColorEye1.Name = "btnColorEye1";
			JG2Button jG2Button11 = this.btnColorEye1;
			size = new System.Drawing.Size(50, 26);
			jG2Button11.Size = size;
			this.btnColorEye1.TabIndex = 25;
			this.btnColorEye1.UseVisualStyleBackColor = true;
			this.tbpHair.BackColor = SystemColors.Control;
			this.tbpHair.Controls.Add(this.lblHair4Name);
			this.tbpHair.Controls.Add(this.lblHair3Name);
			this.tbpHair.Controls.Add(this.lblHair2Name);
			this.tbpHair.Controls.Add(this.lblHair1Name);
			this.tbpHair.Controls.Add(this.lblHair4);
			this.tbpHair.Controls.Add(this.lblHair3);
			this.tbpHair.Controls.Add(this.lblHair2);
			this.tbpHair.Controls.Add(this.lblHair1);
			this.tbpHair.Controls.Add(this.btnColorHairSub2);
			this.tbpHair.Controls.Add(this.btnColorHairSub1);
			this.tbpHair.Controls.Add(this.Label301);
			this.tbpHair.Controls.Add(this.lblColorHair);
			this.tbpHair.Controls.Add(this.numHair4);
			this.tbpHair.Controls.Add(this.numHair3);
			this.tbpHair.Controls.Add(this.numHair2);
			this.tbpHair.Controls.Add(this.numHair1);
			this.tbpHair.Controls.Add(this.numHairAdj4);
			this.tbpHair.Controls.Add(this.chkHairRev4);
			this.tbpHair.Controls.Add(this.numHairAdj3);
			this.tbpHair.Controls.Add(this.chkHairRev3);
			this.tbpHair.Controls.Add(this.numHairAdj2);
			this.tbpHair.Controls.Add(this.chkHairRev2);
			this.tbpHair.Controls.Add(this.numHairAdj1);
			this.tbpHair.Controls.Add(this.chkHairRev1);
			this.tbpHair.Controls.Add(this.btnColorHair);
			TabPage tabPage4 = this.tbpHair;
			point = new Point(4, 28);
			tabPage4.Location = point;
			this.tbpHair.Name = "tbpHair";
			TabPage tabPage5 = this.tbpHair;
			size = new System.Drawing.Size(460, 476);
			tabPage5.Size = size;
			this.tbpHair.TabIndex = 6;
            this.tbpHair.Text = Resources.TabHair;
			Label label100 = this.lblHair4Name;
			point = new Point(160, 105);
			label100.Location = point;
			this.lblHair4Name.Name = "lblHair4Name";
			Label label101 = this.lblHair4Name;
			size = new System.Drawing.Size(60, 18);
			label101.Size = size;
			this.lblHair4Name.TabIndex = 17;
			Label label102 = this.lblHair3Name;
			point = new Point(160, 75);
			label102.Location = point;
			this.lblHair3Name.Name = "lblHair3Name";
			Label label103 = this.lblHair3Name;
			size = new System.Drawing.Size(60, 18);
			label103.Size = size;
			this.lblHair3Name.TabIndex = 12;
			Label label104 = this.lblHair2Name;
			point = new Point(160, 45);
			label104.Location = point;
			this.lblHair2Name.Name = "lblHair2Name";
			Label label105 = this.lblHair2Name;
			size = new System.Drawing.Size(60, 18);
			label105.Size = size;
			this.lblHair2Name.TabIndex = 7;
			Label label106 = this.lblHair1Name;
			point = new Point(160, 15);
			label106.Location = point;
			this.lblHair1Name.Name = "lblHair1Name";
			Label label107 = this.lblHair1Name;
			size = new System.Drawing.Size(60, 18);
			label107.Size = size;
			this.lblHair1Name.TabIndex = 2;
			Label label108 = this.lblHair4;
			point = new Point(6, 105);
			label108.Location = point;
			this.lblHair4.Name = "lblHair4";
			Label label109 = this.lblHair4;
			size = new System.Drawing.Size(92, 18);
			label109.Size = size;
			this.lblHair4.TabIndex = 15;
			Label label110 = this.lblHair3;
			point = new Point(6, 75);
			label110.Location = point;
			this.lblHair3.Name = "lblHair3";
			Label label111 = this.lblHair3;
			size = new System.Drawing.Size(92, 18);
			label111.Size = size;
			this.lblHair3.TabIndex = 10;
			Label label112 = this.lblHair2;
			point = new Point(6, 45);
			label112.Location = point;
			this.lblHair2.Name = "lblHair2";
			Label label113 = this.lblHair2;
			size = new System.Drawing.Size(92, 18);
			label113.Size = size;
			this.lblHair2.TabIndex = 5;
			Label label114 = this.lblHair1;
			point = new Point(6, 15);
			label114.Location = point;
			this.lblHair1.Name = "lblHair1";
			Label label115 = this.lblHair1;
			size = new System.Drawing.Size(92, 18);
			label115.Size = size;
			this.lblHair1.TabIndex = 0;
			this.btnColorHairSub2.BackColor = SystemColors.Control;
			Button button16 = this.btnColorHairSub2;
			point = new Point(190, 161);
			button16.Location = point;
			this.btnColorHairSub2.Name = "btnColorHairSub2";
			Button button17 = this.btnColorHairSub2;
			size = new System.Drawing.Size(80, 26);
			button17.Size = size;
			this.btnColorHairSub2.TabIndex = 24;
			this.btnColorHairSub2.TabStop = false;
            this.btnColorHairSub2.Text = Resources.HairSub2Text;
			this.btnColorHairSub2.UseVisualStyleBackColor = true;
			this.btnColorHairSub1.BackColor = SystemColors.Control;
			Button button18 = this.btnColorHairSub1;
			point = new Point(106, 161);
			button18.Location = point;
			this.btnColorHairSub1.Name = "btnColorHairSub1";
			Button button19 = this.btnColorHairSub1;
			size = new System.Drawing.Size(80, 26);
			button19.Size = size;
			this.btnColorHairSub1.TabIndex = 23;
			this.btnColorHairSub1.TabStop = false;
            this.btnColorHairSub1.Text = Resources.HairSub1Text;
			this.btnColorHairSub1.UseVisualStyleBackColor = true;
			Label label301 = this.Label301;
			point = new Point(6, 165);
			label301.Location = point;
			this.Label301.Name = "Label301";
			Label label3011 = this.Label301;
			size = new System.Drawing.Size(92, 18);
			label3011.Size = size;
			this.Label301.TabIndex = 22;
			this.Label301.Text = Resources.ColourSynchText;
			Label label116 = this.lblColorHair;
			point = new Point(6, 135);
			label116.Location = point;
			this.lblColorHair.Name = "lblColorHair";
			Label label117 = this.lblColorHair;
			size = new System.Drawing.Size(92, 18);
			label117.Size = size;
			this.lblColorHair.TabIndex = 20;
			this.numHair4.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown69 = this.numHair4;
			point = new Point(106, 102);
			jG2NumericUpDown69.Location = point;
			JG2NumericUpDown jG2NumericUpDown70 = this.numHair4;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown70.Maximum = num;
			this.numHair4.Name = "numHair4";
			JG2NumericUpDown jG2NumericUpDown71 = this.numHair4;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown71.Size = size;
			this.numHair4.TabIndex = 16;
			this.numHair4.TextAlign = HorizontalAlignment.Right;
			this.numHair3.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown72 = this.numHair3;
			point = new Point(106, 72);
			jG2NumericUpDown72.Location = point;
			JG2NumericUpDown jG2NumericUpDown73 = this.numHair3;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown73.Maximum = num;
			this.numHair3.Name = "numHair3";
			JG2NumericUpDown jG2NumericUpDown74 = this.numHair3;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown74.Size = size;
			this.numHair3.TabIndex = 11;
			this.numHair3.TextAlign = HorizontalAlignment.Right;
			this.numHair2.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown75 = this.numHair2;
			point = new Point(106, 42);
			jG2NumericUpDown75.Location = point;
			JG2NumericUpDown jG2NumericUpDown76 = this.numHair2;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown76.Maximum = num;
			this.numHair2.Name = "numHair2";
			JG2NumericUpDown jG2NumericUpDown77 = this.numHair2;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown77.Size = size;
			this.numHair2.TabIndex = 6;
			this.numHair2.TextAlign = HorizontalAlignment.Right;
			this.numHair1.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown78 = this.numHair1;
			point = new Point(106, 12);
			jG2NumericUpDown78.Location = point;
			JG2NumericUpDown jG2NumericUpDown79 = this.numHair1;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown79.Maximum = num;
			this.numHair1.Name = "numHair1";
			JG2NumericUpDown jG2NumericUpDown80 = this.numHair1;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown80.Size = size;
			this.numHair1.TabIndex = 1;
			this.numHair1.TextAlign = HorizontalAlignment.Right;
			this.numHairAdj4.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown81 = this.numHairAdj4;
			point = new Point(224, 102);
			jG2NumericUpDown81.Location = point;
			JG2NumericUpDown jG2NumericUpDown82 = this.numHairAdj4;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown82.Maximum = num;
			this.numHairAdj4.Name = "numHairAdj4";
			JG2NumericUpDown jG2NumericUpDown83 = this.numHairAdj4;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown83.Size = size;
			this.numHairAdj4.TabIndex = 18;
			this.numHairAdj4.TextAlign = HorizontalAlignment.Right;
			this.chkHairRev4.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox8 = this.chkHairRev4;
			point = new Point(281, 104);
			jG2CheckBox8.Location = point;
			this.chkHairRev4.Name = "chkHairRev4";
			JG2CheckBox jG2CheckBox9 = this.chkHairRev4;
			size = new System.Drawing.Size(80, 20);
			jG2CheckBox9.Size = size;
			this.chkHairRev4.TabIndex = 19;
			this.chkHairRev4.UseVisualStyleBackColor = true;
			this.numHairAdj3.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown84 = this.numHairAdj3;
			point = new Point(224, 72);
			jG2NumericUpDown84.Location = point;
			JG2NumericUpDown jG2NumericUpDown85 = this.numHairAdj3;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown85.Maximum = num;
			this.numHairAdj3.Name = "numHairAdj3";
			JG2NumericUpDown jG2NumericUpDown86 = this.numHairAdj3;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown86.Size = size;
			this.numHairAdj3.TabIndex = 13;
			this.numHairAdj3.TextAlign = HorizontalAlignment.Right;
			this.chkHairRev3.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox10 = this.chkHairRev3;
			point = new Point(281, 74);
			jG2CheckBox10.Location = point;
			this.chkHairRev3.Name = "chkHairRev3";
			JG2CheckBox jG2CheckBox11 = this.chkHairRev3;
			size = new System.Drawing.Size(80, 20);
			jG2CheckBox11.Size = size;
			this.chkHairRev3.TabIndex = 14;
			this.chkHairRev3.UseVisualStyleBackColor = true;
			this.numHairAdj2.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown87 = this.numHairAdj2;
			point = new Point(224, 42);
			jG2NumericUpDown87.Location = point;
			JG2NumericUpDown jG2NumericUpDown88 = this.numHairAdj2;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown88.Maximum = num;
			this.numHairAdj2.Name = "numHairAdj2";
			JG2NumericUpDown jG2NumericUpDown89 = this.numHairAdj2;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown89.Size = size;
			this.numHairAdj2.TabIndex = 8;
			this.numHairAdj2.TextAlign = HorizontalAlignment.Right;
			this.chkHairRev2.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox12 = this.chkHairRev2;
			point = new Point(281, 44);
			jG2CheckBox12.Location = point;
			this.chkHairRev2.Name = "chkHairRev2";
			JG2CheckBox jG2CheckBox13 = this.chkHairRev2;
			size = new System.Drawing.Size(80, 20);
			jG2CheckBox13.Size = size;
			this.chkHairRev2.TabIndex = 9;
			this.chkHairRev2.UseVisualStyleBackColor = true;
			this.numHairAdj1.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown90 = this.numHairAdj1;
			point = new Point(224, 12);
			jG2NumericUpDown90.Location = point;
			JG2NumericUpDown jG2NumericUpDown91 = this.numHairAdj1;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown91.Maximum = num;
			this.numHairAdj1.Name = "numHairAdj1";
			JG2NumericUpDown jG2NumericUpDown92 = this.numHairAdj1;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown92.Size = size;
			this.numHairAdj1.TabIndex = 3;
			this.numHairAdj1.TextAlign = HorizontalAlignment.Right;
			this.chkHairRev1.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox14 = this.chkHairRev1;
			point = new Point(281, 14);
			jG2CheckBox14.Location = point;
			this.chkHairRev1.Name = "chkHairRev1";
			JG2CheckBox jG2CheckBox15 = this.chkHairRev1;
			size = new System.Drawing.Size(80, 20);
			jG2CheckBox15.Size = size;
			this.chkHairRev1.TabIndex = 4;
			this.chkHairRev1.UseVisualStyleBackColor = true;
			this.btnColorHair.AllowDrop = true;
			this.btnColorHair.BackColor = SystemColors.Control;
			this.btnColorHair.BatchButton = this.btnBatch;
			JG2Button jG2Button12 = this.btnColorHair;
			point = new Point(106, 131);
			jG2Button12.Location = point;
			this.btnColorHair.Name = "btnColorHair";
			JG2Button jG2Button13 = this.btnColorHair;
			size = new System.Drawing.Size(50, 26);
			jG2Button13.Size = size;
			this.btnColorHair.TabIndex = 21;
			this.btnColorHair.UseVisualStyleBackColor = true;
			this.tbpJinkaku.BackColor = SystemColors.Control;
			this.tbpJinkaku.Controls.Add(this.lblTairyoku1Name);
			this.tbpJinkaku.Controls.Add(this.lblTiryoku1Name);
			this.tbpJinkaku.Controls.Add(this.lblSeikakuName);
			this.tbpJinkaku.Controls.Add(this.lblSyakousei);
			this.tbpJinkaku.Controls.Add(this.lblSeikaku);
			this.tbpJinkaku.Controls.Add(this.lblVoice);
			this.tbpJinkaku.Controls.Add(this.lblSeiai);
			this.tbpJinkaku.Controls.Add(this.lblTeisou);
			this.tbpJinkaku.Controls.Add(this.lblKenka);
			this.tbpJinkaku.Controls.Add(this.lblBukatu1);
			this.tbpJinkaku.Controls.Add(this.lblTairyoku1);
			this.tbpJinkaku.Controls.Add(this.lblTiryoku1);
			this.tbpJinkaku.Controls.Add(this.numSeikaku);
			this.tbpJinkaku.Controls.Add(this.cmbBukatu1);
			this.tbpJinkaku.Controls.Add(this.numBukatu3);
			this.tbpJinkaku.Controls.Add(this.numTairyoku3);
			this.tbpJinkaku.Controls.Add(this.numTiryoku3);
			this.tbpJinkaku.Controls.Add(this.cmbVoice);
			this.tbpJinkaku.Controls.Add(this.chkSeikou2);
			this.tbpJinkaku.Controls.Add(this.chkSeikou1);
			this.tbpJinkaku.Controls.Add(this.cmbSeiai);
			this.tbpJinkaku.Controls.Add(this.cmbTeisou);
			this.tbpJinkaku.Controls.Add(this.cmbKenka);
			this.tbpJinkaku.Controls.Add(this.cmbSyakousei);
			TabPage tabPage6 = this.tbpJinkaku;
			point = new Point(4, 28);
			tabPage6.Location = point;
			this.tbpJinkaku.Name = "tbpJinkaku";
			TabPage tabPage7 = this.tbpJinkaku;
			padding = new System.Windows.Forms.Padding(3);
			tabPage7.Padding = padding;
			TabPage tabPage8 = this.tbpJinkaku;
			size = new System.Drawing.Size(460, 476);
			tabPage8.Size = size;
			this.tbpJinkaku.TabIndex = 1;
            this.tbpJinkaku.Text = Resources.TabPersonality;
			this.lblTairyoku1Name.AutoEllipsis = true;
			this.lblTairyoku1Name.BackColor = SystemColors.Control;
			Label label118 = this.lblTairyoku1Name;
			point = new Point(160, 45);
			label118.Location = point;
			this.lblTairyoku1Name.Name = "lblTairyoku1Name";
			Label label119 = this.lblTairyoku1Name;
			size = new System.Drawing.Size(80, 18);
			label119.Size = size;
			this.lblTairyoku1Name.TabIndex = 5;
			this.lblTiryoku1Name.AutoEllipsis = true;
			this.lblTiryoku1Name.BackColor = SystemColors.Control;
			Label label120 = this.lblTiryoku1Name;
			point = new Point(160, 15);
			label120.Location = point;
			this.lblTiryoku1Name.Name = "lblTiryoku1Name";
			Label label121 = this.lblTiryoku1Name;
			size = new System.Drawing.Size(80, 18);
			label121.Size = size;
			this.lblTiryoku1Name.TabIndex = 2;
			Label label122 = this.lblSeikakuName;
			point = new Point(160, 345);
			label122.Location = point;
			this.lblSeikakuName.Name = "lblSeikakuName";
			Label label123 = this.lblSeikakuName;
			size = new System.Drawing.Size(80, 18);
			label123.Size = size;
			this.lblSeikakuName.TabIndex = 23;
			Label label124 = this.lblSyakousei;
			point = new Point(6, 105);
			label124.Location = point;
			this.lblSyakousei.Name = "lblSyakousei";
			Label label125 = this.lblSyakousei;
			size = new System.Drawing.Size(92, 18);
			label125.Size = size;
			this.lblSyakousei.TabIndex = 9;
			Label label126 = this.lblSeikaku;
			point = new Point(6, 345);
			label126.Location = point;
			this.lblSeikaku.Name = "lblSeikaku";
			Label label127 = this.lblSeikaku;
			size = new System.Drawing.Size(92, 18);
			label127.Size = size;
			this.lblSeikaku.TabIndex = 21;
			Label label128 = this.lblVoice;
			point = new Point(6, 315);
			label128.Location = point;
			this.lblVoice.Name = "lblVoice";
			Label label129 = this.lblVoice;
			size = new System.Drawing.Size(92, 18);
			label129.Size = size;
			this.lblVoice.TabIndex = 19;
			Label label130 = this.lblSeiai;
			point = new Point(6, 195);
			label130.Location = point;
			this.lblSeiai.Name = "lblSeiai";
			Label label131 = this.lblSeiai;
			size = new System.Drawing.Size(92, 18);
			label131.Size = size;
			this.lblSeiai.TabIndex = 15;
			Label label132 = this.lblTeisou;
			point = new Point(6, 165);
			label132.Location = point;
			this.lblTeisou.Name = "lblTeisou";
			Label label133 = this.lblTeisou;
			size = new System.Drawing.Size(92, 18);
			label133.Size = size;
			this.lblTeisou.TabIndex = 13;
			Label label134 = this.lblKenka;
			point = new Point(6, 135);
			label134.Location = point;
			this.lblKenka.Name = "lblKenka";
			Label label135 = this.lblKenka;
			size = new System.Drawing.Size(92, 18);
			label135.Size = size;
			this.lblKenka.TabIndex = 11;
			Label label136 = this.lblBukatu1;
			point = new Point(6, 75);
			label136.Location = point;
			this.lblBukatu1.Name = "lblBukatu1";
			Label label137 = this.lblBukatu1;
			size = new System.Drawing.Size(92, 18);
			label137.Size = size;
			this.lblBukatu1.TabIndex = 6;
			Label label138 = this.lblTairyoku1;
			point = new Point(6, 45);
			label138.Location = point;
			this.lblTairyoku1.Name = "lblTairyoku1";
			Label label139 = this.lblTairyoku1;
			size = new System.Drawing.Size(92, 18);
			label139.Size = size;
			this.lblTairyoku1.TabIndex = 3;
			Label label140 = this.lblTiryoku1;
			point = new Point(6, 15);
			label140.Location = point;
			this.lblTiryoku1.Name = "lblTiryoku1";
			Label label141 = this.lblTiryoku1;
			size = new System.Drawing.Size(92, 18);
			label141.Size = size;
			this.lblTiryoku1.TabIndex = 0;
			this.numSeikaku.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown93 = this.numSeikaku;
			point = new Point(106, 342);
			jG2NumericUpDown93.Location = point;
			JG2NumericUpDown jG2NumericUpDown94 = this.numSeikaku;
			numArray = new int[] { 255, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown94.Maximum = num;
			this.numSeikaku.Name = "numSeikaku";
			JG2NumericUpDown jG2NumericUpDown95 = this.numSeikaku;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown95.Size = size;
			this.numSeikaku.TabIndex = 22;
			this.numSeikaku.TextAlign = HorizontalAlignment.Right;
			this.cmbBukatu1.BatchButton = this.btnBatch;
			this.cmbBukatu1.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox38 = this.cmbBukatu1;
			point = new Point(160, 71);
			jG2ComboBox38.Location = point;
			this.cmbBukatu1.Name = "cmbBukatu1";
			JG2ComboBox jG2ComboBox39 = this.cmbBukatu1;
			size = new System.Drawing.Size(104, 26);
			jG2ComboBox39.Size = size;
			this.cmbBukatu1.TabIndex = 8;
			this.numBukatu3.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown96 = this.numBukatu3;
			point = new Point(106, 72);
			jG2NumericUpDown96.Location = point;
			JG2NumericUpDown jG2NumericUpDown97 = this.numBukatu3;
			numArray = new int[] { 999, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown97.Maximum = num;
			this.numBukatu3.Name = "numBukatu3";
			JG2NumericUpDown jG2NumericUpDown98 = this.numBukatu3;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown98.Size = size;
			this.numBukatu3.TabIndex = 7;
			this.numBukatu3.TextAlign = HorizontalAlignment.Right;
			this.numTairyoku3.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown99 = this.numTairyoku3;
			point = new Point(106, 42);
			jG2NumericUpDown99.Location = point;
			JG2NumericUpDown jG2NumericUpDown100 = this.numTairyoku3;
			numArray = new int[] { 999, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown100.Maximum = num;
			this.numTairyoku3.Name = "numTairyoku3";
			JG2NumericUpDown jG2NumericUpDown101 = this.numTairyoku3;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown101.Size = size;
			this.numTairyoku3.TabIndex = 4;
			this.numTairyoku3.TextAlign = HorizontalAlignment.Right;
			this.numTiryoku3.BatchButton = this.btnBatch;
			JG2NumericUpDown jG2NumericUpDown102 = this.numTiryoku3;
			point = new Point(106, 12);
			jG2NumericUpDown102.Location = point;
			JG2NumericUpDown jG2NumericUpDown103 = this.numTiryoku3;
			numArray = new int[] { 999, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown103.Maximum = num;
			this.numTiryoku3.Name = "numTiryoku3";
			JG2NumericUpDown jG2NumericUpDown104 = this.numTiryoku3;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown104.Size = size;
			this.numTiryoku3.TabIndex = 1;
			this.numTiryoku3.TextAlign = HorizontalAlignment.Right;
			this.cmbVoice.BatchButton = this.btnBatch;
			this.cmbVoice.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox40 = this.cmbVoice;
			point = new Point(106, 311);
			jG2ComboBox40.Location = point;
			this.cmbVoice.Name = "cmbVoice";
			JG2ComboBox jG2ComboBox41 = this.cmbVoice;
			size = new System.Drawing.Size(50, 26);
			jG2ComboBox41.Size = size;
			this.cmbVoice.TabIndex = 20;
			this.chkSeikou2.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox16 = this.chkSeikou2;
			point = new Point(9, 254);
			jG2CheckBox16.Location = point;
			this.chkSeikou2.Name = "chkSeikou2";
			JG2CheckBox jG2CheckBox17 = this.chkSeikou2;
			size = new System.Drawing.Size(80, 20);
			jG2CheckBox17.Size = size;
			this.chkSeikou2.TabIndex = 18;
			this.chkSeikou2.UseVisualStyleBackColor = true;
			this.chkSeikou1.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox18 = this.chkSeikou1;
			point = new Point(9, 224);
			jG2CheckBox18.Location = point;
			this.chkSeikou1.Name = "chkSeikou1";
			JG2CheckBox jG2CheckBox19 = this.chkSeikou1;
			size = new System.Drawing.Size(80, 20);
			jG2CheckBox19.Size = size;
			this.chkSeikou1.TabIndex = 17;
			this.chkSeikou1.UseVisualStyleBackColor = true;
			this.cmbSeiai.BatchButton = this.btnBatch;
			this.cmbSeiai.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox42 = this.cmbSeiai;
			point = new Point(106, 191);
			jG2ComboBox42.Location = point;
			this.cmbSeiai.Name = "cmbSeiai";
			JG2ComboBox jG2ComboBox43 = this.cmbSeiai;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox43.Size = size;
			this.cmbSeiai.TabIndex = 16;
			this.cmbTeisou.BatchButton = this.btnBatch;
			this.cmbTeisou.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox44 = this.cmbTeisou;
			point = new Point(106, 161);
			jG2ComboBox44.Location = point;
			this.cmbTeisou.Name = "cmbTeisou";
			JG2ComboBox jG2ComboBox45 = this.cmbTeisou;
			size = new System.Drawing.Size(50, 26);
			jG2ComboBox45.Size = size;
			this.cmbTeisou.TabIndex = 14;
			this.cmbKenka.BatchButton = this.btnBatch;
			this.cmbKenka.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox46 = this.cmbKenka;
			point = new Point(106, 131);
			jG2ComboBox46.Location = point;
			this.cmbKenka.Name = "cmbKenka";
			JG2ComboBox jG2ComboBox47 = this.cmbKenka;
			size = new System.Drawing.Size(50, 26);
			jG2ComboBox47.Size = size;
			this.cmbKenka.TabIndex = 12;
			this.cmbSyakousei.BatchButton = this.btnBatch;
			this.cmbSyakousei.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox48 = this.cmbSyakousei;
			point = new Point(106, 101);
			jG2ComboBox48.Location = point;
			this.cmbSyakousei.Name = "cmbSyakousei";
			JG2ComboBox jG2ComboBox49 = this.cmbSyakousei;
			size = new System.Drawing.Size(50, 26);
			jG2ComboBox49.Size = size;
			this.cmbSyakousei.TabIndex = 10;
			this.tbpKosei.BackColor = SystemColors.Control;
			this.tbpKosei.Controls.Add(this.pnlH);
			this.tbpKosei.Controls.Add(this.pnlKosei);
			TabPage tabPage9 = this.tbpKosei;
			point = new Point(4, 28);
			tabPage9.Location = point;
			this.tbpKosei.Name = "tbpKosei";
			TabPage tabPage10 = this.tbpKosei;
			size = new System.Drawing.Size(460, 476);
			tabPage10.Size = size;
			this.tbpKosei.TabIndex = 3;
            this.tbpKosei.Text = Resources.CaptionPersonality;
			this.pnlH.Controls.Add(this.Label511);
			this.pnlH.Controls.Add(this.chkKiss);
			this.pnlH.Controls.Add(this.chkKake);
			this.pnlH.Controls.Add(this.chkSeiki1);
			this.pnlH.Controls.Add(this.chkNaka);
			this.pnlH.Controls.Add(this.chkBust);
			this.pnlH.Controls.Add(this.chkNama);
			this.pnlH.Controls.Add(this.chkSeiki2);
			this.pnlH.Controls.Add(this.chkSeiin);
			this.pnlH.Controls.Add(this.chkCunnilingus);
			this.pnlH.Controls.Add(this.chkAnal);
			this.pnlH.Controls.Add(this.chkBack);
			this.pnlH.Controls.Add(this.chkOnna);
			this.pnlH.Controls.Add(this.chkFellatio);
			Panel panel4 = this.pnlH;
			point = new Point(6, 285);
			panel4.Location = point;
			this.pnlH.Name = "pnlH";
			Panel panel5 = this.pnlH;
			size = new System.Drawing.Size(448, 110);
			panel5.Size = size;
			this.pnlH.TabIndex = 1;
			Label label511 = this.Label511;
			point = new Point(0, 0);
			label511.Location = point;
			this.Label511.Name = "Label511";
			Label label5111 = this.Label511;
			size = new System.Drawing.Size(50, 18);
			label5111.Size = size;
			this.Label511.TabIndex = 0;
            this.Label511.Text = Resources.CaptionHFavorite;
			this.chkKiss.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox20 = this.chkKiss;
			point = new Point(9, 22);
			jG2CheckBox20.Location = point;
			this.chkKiss.Name = "chkKiss";
			JG2CheckBox jG2CheckBox21 = this.chkKiss;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox21.Size = size;
			this.chkKiss.TabIndex = 1;
			this.chkKiss.UseVisualStyleBackColor = true;
			this.chkKake.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox22 = this.chkKake;
			point = new Point(9, 88);
			jG2CheckBox22.Location = point;
			this.chkKake.Name = "chkKake";
			JG2CheckBox jG2CheckBox23 = this.chkKake;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox23.Size = size;
			this.chkKake.TabIndex = 13;
			this.chkKake.UseVisualStyleBackColor = true;
			this.chkSeiki1.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox24 = this.chkSeiki1;
			point = new Point(227, 22);
			jG2CheckBox24.Location = point;
			this.chkSeiki1.Name = "chkSeiki1";
			JG2CheckBox jG2CheckBox25 = this.chkSeiki1;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox25.Size = size;
			this.chkSeiki1.TabIndex = 3;
			this.chkSeiki1.UseVisualStyleBackColor = true;
			this.chkNaka.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox26 = this.chkNaka;
			point = new Point(336, 66);
			jG2CheckBox26.Location = point;
			this.chkNaka.Name = "chkNaka";
			JG2CheckBox jG2CheckBox27 = this.chkNaka;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox27.Size = size;
			this.chkNaka.TabIndex = 12;
			this.chkNaka.UseVisualStyleBackColor = true;
			this.chkBust.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox28 = this.chkBust;
			point = new Point(118, 22);
			jG2CheckBox28.Location = point;
			this.chkBust.Name = "chkBust";
			JG2CheckBox jG2CheckBox29 = this.chkBust;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox29.Size = size;
			this.chkBust.TabIndex = 2;
			this.chkBust.UseVisualStyleBackColor = true;
			this.chkNama.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox30 = this.chkNama;
			point = new Point(118, 66);
			jG2CheckBox30.Location = point;
			this.chkNama.Name = "chkNama";
			JG2CheckBox jG2CheckBox31 = this.chkNama;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox31.Size = size;
			this.chkNama.TabIndex = 10;
			this.chkNama.UseVisualStyleBackColor = true;
			this.chkSeiki2.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox32 = this.chkSeiki2;
			point = new Point(336, 22);
			jG2CheckBox32.Location = point;
			this.chkSeiki2.Name = "chkSeiki2";
			JG2CheckBox jG2CheckBox33 = this.chkSeiki2;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox33.Size = size;
			this.chkSeiki2.TabIndex = 4;
			this.chkSeiki2.UseVisualStyleBackColor = true;
			this.chkSeiin.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox34 = this.chkSeiin;
			point = new Point(227, 66);
			jG2CheckBox34.Location = point;
			this.chkSeiin.Name = "chkSeiin";
			JG2CheckBox jG2CheckBox35 = this.chkSeiin;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox35.Size = size;
			this.chkSeiin.TabIndex = 11;
			this.chkSeiin.UseVisualStyleBackColor = true;
			this.chkCunnilingus.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox36 = this.chkCunnilingus;
			point = new Point(9, 44);
			jG2CheckBox36.Location = point;
			this.chkCunnilingus.Name = "chkCunnilingus";
			JG2CheckBox jG2CheckBox37 = this.chkCunnilingus;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox37.Size = size;
			this.chkCunnilingus.TabIndex = 5;
			this.chkCunnilingus.UseVisualStyleBackColor = true;
			this.chkAnal.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox38 = this.chkAnal;
			point = new Point(9, 66);
			jG2CheckBox38.Location = point;
			this.chkAnal.Name = "chkAnal";
			JG2CheckBox jG2CheckBox39 = this.chkAnal;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox39.Size = size;
			this.chkAnal.TabIndex = 9;
			this.chkAnal.UseVisualStyleBackColor = true;
			this.chkBack.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox40 = this.chkBack;
			point = new Point(227, 44);
			jG2CheckBox40.Location = point;
			this.chkBack.Name = "chkBack";
			JG2CheckBox jG2CheckBox41 = this.chkBack;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox41.Size = size;
			this.chkBack.TabIndex = 7;
			this.chkBack.UseVisualStyleBackColor = true;
			this.chkOnna.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox42 = this.chkOnna;
			point = new Point(336, 44);
			jG2CheckBox42.Location = point;
			this.chkOnna.Name = "chkOnna";
			JG2CheckBox jG2CheckBox43 = this.chkOnna;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox43.Size = size;
			this.chkOnna.TabIndex = 8;
			this.chkOnna.UseVisualStyleBackColor = true;
			this.chkFellatio.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox44 = this.chkFellatio;
			point = new Point(118, 44);
			jG2CheckBox44.Location = point;
			this.chkFellatio.Name = "chkFellatio";
			JG2CheckBox jG2CheckBox45 = this.chkFellatio;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox45.Size = size;
			this.chkFellatio.TabIndex = 6;
			this.chkFellatio.UseVisualStyleBackColor = true;
			this.pnlKosei.Controls.Add(this.Label501);
			this.pnlKosei.Controls.Add(this.chkLuck);
			this.pnlKosei.Controls.Add(this.chkRare);
			this.pnlKosei.Controls.Add(this.chkMiseityo);
			this.pnlKosei.Controls.Add(this.chkYowami);
			this.pnlKosei.Controls.Add(this.chkYami);
			this.pnlKosei.Controls.Add(this.chkNantyo);
			this.pnlKosei.Controls.Add(this.chkAsekaki);
			this.pnlKosei.Controls.Add(this.chkM);
			this.pnlKosei.Controls.Add(this.chkKinben);
			this.pnlKosei.Controls.Add(this.chkHonpou);
			this.pnlKosei.Controls.Add(this.chkHaraguro);
			this.pnlKosei.Controls.Add(this.chkMakenki);
			this.pnlKosei.Controls.Add(this.chkItizu);
			this.pnlKosei.Controls.Add(this.chkYuujyufudan);
			this.pnlKosei.Controls.Add(this.chkRenai);
			this.pnlKosei.Controls.Add(this.chkHarapeko);
			this.pnlKosei.Controls.Add(this.chkIintyou);
			this.pnlKosei.Controls.Add(this.chkOsyaberi);
			this.pnlKosei.Controls.Add(this.chkSewayaki);
			this.pnlKosei.Controls.Add(this.chkSousyoku);
			this.pnlKosei.Controls.Add(this.chkPoyayan);
			this.pnlKosei.Controls.Add(this.chkBouryoku);
			this.pnlKosei.Controls.Add(this.chkTyokujyo);
			this.pnlKosei.Controls.Add(this.chkCool);
			this.pnlKosei.Controls.Add(this.chkSukebe);
			this.pnlKosei.Controls.Add(this.chkMajime);
			this.pnlKosei.Controls.Add(this.chkToufu);
			this.pnlKosei.Controls.Add(this.chkYakimochi);
			this.pnlKosei.Controls.Add(this.chkMaemuki);
			this.pnlKosei.Controls.Add(this.chkTereya);
			this.pnlKosei.Controls.Add(this.chkSunao);
			this.pnlKosei.Controls.Add(this.chkMiha);
			this.pnlKosei.Controls.Add(this.chkTundere);
			this.pnlKosei.Controls.Add(this.chkKyouki);
			this.pnlKosei.Controls.Add(this.chkCharm);
			this.pnlKosei.Controls.Add(this.chkNigate2);
			this.pnlKosei.Controls.Add(this.chkNekketu);
			this.pnlKosei.Controls.Add(this.chkNigate1);
			this.pnlKosei.Controls.Add(this.chkCyoroi);
			Panel panel6 = this.pnlKosei;
			point = new Point(6, 15);
			panel6.Location = point;
			this.pnlKosei.Name = "pnlKosei";
			Panel panel7 = this.pnlKosei;
			size = new System.Drawing.Size(448, 242);
			panel7.Size = size;
			this.pnlKosei.TabIndex = 0;
			Label label501 = this.Label501;
			point = new Point(0, 0);
			label501.Location = point;
			this.Label501.Name = "Label501";
			Label label5011 = this.Label501;
			size = new System.Drawing.Size(50, 18);
			label5011.Size = size;
			this.Label501.TabIndex = 0;
            this.Label501.Text = Resources.CaptionPersonality;
			this.chkLuck.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox46 = this.chkLuck;
			point = new Point(118, 220);
			jG2CheckBox46.Location = point;
			this.chkLuck.Name = "chkLuck";
			JG2CheckBox jG2CheckBox47 = this.chkLuck;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox47.Size = size;
			this.chkLuck.TabIndex = 38;
			this.chkLuck.UseVisualStyleBackColor = true;
			this.chkRare.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox48 = this.chkRare;
			point = new Point(227, 220);
			jG2CheckBox48.Location = point;
			this.chkRare.Name = "chkRare";
			JG2CheckBox jG2CheckBox49 = this.chkRare;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox49.Size = size;
			this.chkRare.TabIndex = 39;
			this.chkRare.UseVisualStyleBackColor = true;
			this.chkMiseityo.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox50 = this.chkMiseityo;
			point = new Point(9, 220);
			jG2CheckBox50.Location = point;
			this.chkMiseityo.Name = "chkMiseityo";
			JG2CheckBox jG2CheckBox51 = this.chkMiseityo;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox51.Size = size;
			this.chkMiseityo.TabIndex = 37;
			this.chkMiseityo.UseVisualStyleBackColor = true;
			this.chkYowami.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox52 = this.chkYowami;
			point = new Point(336, 198);
			jG2CheckBox52.Location = point;
			this.chkYowami.Name = "chkYowami";
			JG2CheckBox jG2CheckBox53 = this.chkYowami;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox53.Size = size;
			this.chkYowami.TabIndex = 36;
			this.chkYowami.UseVisualStyleBackColor = true;
			this.chkYami.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox54 = this.chkYami;
			point = new Point(118, 198);
			jG2CheckBox54.Location = point;
			this.chkYami.Name = "chkYami";
			JG2CheckBox jG2CheckBox55 = this.chkYami;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox55.Size = size;
			this.chkYami.TabIndex = 34;
			this.chkYami.UseVisualStyleBackColor = true;
			this.chkNantyo.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox56 = this.chkNantyo;
			point = new Point(227, 198);
			jG2CheckBox56.Location = point;
			this.chkNantyo.Name = "chkNantyo";
			JG2CheckBox jG2CheckBox57 = this.chkNantyo;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox57.Size = size;
			this.chkNantyo.TabIndex = 35;
			this.chkNantyo.UseVisualStyleBackColor = true;
			this.chkAsekaki.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox58 = this.chkAsekaki;
			point = new Point(9, 198);
			jG2CheckBox58.Location = point;
			this.chkAsekaki.Name = "chkAsekaki";
			JG2CheckBox jG2CheckBox59 = this.chkAsekaki;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox59.Size = size;
			this.chkAsekaki.TabIndex = 33;
			this.chkAsekaki.UseVisualStyleBackColor = true;
			this.chkM.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox60 = this.chkM;
			point = new Point(336, 176);
			jG2CheckBox60.Location = point;
			this.chkM.Name = "chkM";
			JG2CheckBox jG2CheckBox61 = this.chkM;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox61.Size = size;
			this.chkM.TabIndex = 32;
			this.chkM.UseVisualStyleBackColor = true;
			this.chkKinben.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox62 = this.chkKinben;
			point = new Point(118, 176);
			jG2CheckBox62.Location = point;
			this.chkKinben.Name = "chkKinben";
			JG2CheckBox jG2CheckBox63 = this.chkKinben;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox63.Size = size;
			this.chkKinben.TabIndex = 30;
			this.chkKinben.UseVisualStyleBackColor = true;
			this.chkHonpou.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox64 = this.chkHonpou;
			point = new Point(227, 176);
			jG2CheckBox64.Location = point;
			this.chkHonpou.Name = "chkHonpou";
			JG2CheckBox jG2CheckBox65 = this.chkHonpou;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox65.Size = size;
			this.chkHonpou.TabIndex = 31;
			this.chkHonpou.UseVisualStyleBackColor = true;
			this.chkHaraguro.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox66 = this.chkHaraguro;
			point = new Point(9, 176);
			jG2CheckBox66.Location = point;
			this.chkHaraguro.Name = "chkHaraguro";
			JG2CheckBox jG2CheckBox67 = this.chkHaraguro;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox67.Size = size;
			this.chkHaraguro.TabIndex = 29;
			this.chkHaraguro.UseVisualStyleBackColor = true;
			this.chkMakenki.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox68 = this.chkMakenki;
			point = new Point(336, 154);
			jG2CheckBox68.Location = point;
			this.chkMakenki.Name = "chkMakenki";
			JG2CheckBox jG2CheckBox69 = this.chkMakenki;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox69.Size = size;
			this.chkMakenki.TabIndex = 28;
			this.chkMakenki.UseVisualStyleBackColor = true;
			this.chkItizu.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox70 = this.chkItizu;
			point = new Point(118, 154);
			jG2CheckBox70.Location = point;
			this.chkItizu.Name = "chkItizu";
			JG2CheckBox jG2CheckBox71 = this.chkItizu;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox71.Size = size;
			this.chkItizu.TabIndex = 26;
			this.chkItizu.UseVisualStyleBackColor = true;
			this.chkYuujyufudan.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox72 = this.chkYuujyufudan;
			point = new Point(227, 154);
			jG2CheckBox72.Location = point;
			this.chkYuujyufudan.Name = "chkYuujyufudan";
			JG2CheckBox jG2CheckBox73 = this.chkYuujyufudan;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox73.Size = size;
			this.chkYuujyufudan.TabIndex = 27;
			this.chkYuujyufudan.UseVisualStyleBackColor = true;
			this.chkRenai.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox74 = this.chkRenai;
			point = new Point(9, 154);
			jG2CheckBox74.Location = point;
			this.chkRenai.Name = "chkRenai";
			JG2CheckBox jG2CheckBox75 = this.chkRenai;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox75.Size = size;
			this.chkRenai.TabIndex = 25;
			this.chkRenai.UseVisualStyleBackColor = true;
			this.chkHarapeko.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox76 = this.chkHarapeko;
			point = new Point(336, 132);
			jG2CheckBox76.Location = point;
			this.chkHarapeko.Name = "chkHarapeko";
			JG2CheckBox jG2CheckBox77 = this.chkHarapeko;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox77.Size = size;
			this.chkHarapeko.TabIndex = 24;
			this.chkHarapeko.UseVisualStyleBackColor = true;
			this.chkIintyou.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox78 = this.chkIintyou;
			point = new Point(118, 132);
			jG2CheckBox78.Location = point;
			this.chkIintyou.Name = "chkIintyou";
			JG2CheckBox jG2CheckBox79 = this.chkIintyou;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox79.Size = size;
			this.chkIintyou.TabIndex = 22;
			this.chkIintyou.UseVisualStyleBackColor = true;
			this.chkOsyaberi.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox80 = this.chkOsyaberi;
			point = new Point(227, 132);
			jG2CheckBox80.Location = point;
			this.chkOsyaberi.Name = "chkOsyaberi";
			JG2CheckBox jG2CheckBox81 = this.chkOsyaberi;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox81.Size = size;
			this.chkOsyaberi.TabIndex = 23;
			this.chkOsyaberi.UseVisualStyleBackColor = true;
			this.chkSewayaki.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox82 = this.chkSewayaki;
			point = new Point(9, 132);
			jG2CheckBox82.Location = point;
			this.chkSewayaki.Name = "chkSewayaki";
			JG2CheckBox jG2CheckBox83 = this.chkSewayaki;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox83.Size = size;
			this.chkSewayaki.TabIndex = 21;
			this.chkSewayaki.UseVisualStyleBackColor = true;
			this.chkSousyoku.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox84 = this.chkSousyoku;
			point = new Point(336, 110);
			jG2CheckBox84.Location = point;
			this.chkSousyoku.Name = "chkSousyoku";
			JG2CheckBox jG2CheckBox85 = this.chkSousyoku;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox85.Size = size;
			this.chkSousyoku.TabIndex = 20;
			this.chkSousyoku.UseVisualStyleBackColor = true;
			this.chkPoyayan.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox86 = this.chkPoyayan;
			point = new Point(118, 110);
			jG2CheckBox86.Location = point;
			this.chkPoyayan.Name = "chkPoyayan";
			JG2CheckBox jG2CheckBox87 = this.chkPoyayan;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox87.Size = size;
			this.chkPoyayan.TabIndex = 18;
			this.chkPoyayan.UseVisualStyleBackColor = true;
			this.chkBouryoku.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox88 = this.chkBouryoku;
			point = new Point(227, 110);
			jG2CheckBox88.Location = point;
			this.chkBouryoku.Name = "chkBouryoku";
			JG2CheckBox jG2CheckBox89 = this.chkBouryoku;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox89.Size = size;
			this.chkBouryoku.TabIndex = 19;
			this.chkBouryoku.UseVisualStyleBackColor = true;
			this.chkTyokujyo.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox90 = this.chkTyokujyo;
			point = new Point(9, 110);
			jG2CheckBox90.Location = point;
			this.chkTyokujyo.Name = "chkTyokujyo";
			JG2CheckBox jG2CheckBox91 = this.chkTyokujyo;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox91.Size = size;
			this.chkTyokujyo.TabIndex = 17;
			this.chkTyokujyo.UseVisualStyleBackColor = true;
			this.chkCool.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox92 = this.chkCool;
			point = new Point(336, 88);
			jG2CheckBox92.Location = point;
			this.chkCool.Name = "chkCool";
			JG2CheckBox jG2CheckBox93 = this.chkCool;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox93.Size = size;
			this.chkCool.TabIndex = 16;
			this.chkCool.UseVisualStyleBackColor = true;
			this.chkSukebe.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox94 = this.chkSukebe;
			point = new Point(118, 88);
			jG2CheckBox94.Location = point;
			this.chkSukebe.Name = "chkSukebe";
			JG2CheckBox jG2CheckBox95 = this.chkSukebe;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox95.Size = size;
			this.chkSukebe.TabIndex = 14;
			this.chkSukebe.UseVisualStyleBackColor = true;
			this.chkMajime.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox96 = this.chkMajime;
			point = new Point(227, 88);
			jG2CheckBox96.Location = point;
			this.chkMajime.Name = "chkMajime";
			JG2CheckBox jG2CheckBox97 = this.chkMajime;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox97.Size = size;
			this.chkMajime.TabIndex = 15;
			this.chkMajime.UseVisualStyleBackColor = true;
			this.chkToufu.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox98 = this.chkToufu;
			point = new Point(9, 88);
			jG2CheckBox98.Location = point;
			this.chkToufu.Name = "chkToufu";
			JG2CheckBox jG2CheckBox99 = this.chkToufu;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox99.Size = size;
			this.chkToufu.TabIndex = 13;
			this.chkToufu.UseVisualStyleBackColor = true;
			this.chkYakimochi.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox100 = this.chkYakimochi;
			point = new Point(336, 66);
			jG2CheckBox100.Location = point;
			this.chkYakimochi.Name = "chkYakimochi";
			JG2CheckBox jG2CheckBox101 = this.chkYakimochi;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox101.Size = size;
			this.chkYakimochi.TabIndex = 12;
			this.chkYakimochi.UseVisualStyleBackColor = true;
			this.chkMaemuki.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox102 = this.chkMaemuki;
			point = new Point(118, 66);
			jG2CheckBox102.Location = point;
			this.chkMaemuki.Name = "chkMaemuki";
			JG2CheckBox jG2CheckBox103 = this.chkMaemuki;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox103.Size = size;
			this.chkMaemuki.TabIndex = 10;
			this.chkMaemuki.UseVisualStyleBackColor = true;
			this.chkTereya.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox104 = this.chkTereya;
			point = new Point(227, 66);
			jG2CheckBox104.Location = point;
			this.chkTereya.Name = "chkTereya";
			JG2CheckBox jG2CheckBox105 = this.chkTereya;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox105.Size = size;
			this.chkTereya.TabIndex = 11;
			this.chkTereya.UseVisualStyleBackColor = true;
			this.chkSunao.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox106 = this.chkSunao;
			point = new Point(9, 66);
			jG2CheckBox106.Location = point;
			this.chkSunao.Name = "chkSunao";
			JG2CheckBox jG2CheckBox107 = this.chkSunao;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox107.Size = size;
			this.chkSunao.TabIndex = 9;
			this.chkSunao.UseVisualStyleBackColor = true;
			this.chkMiha.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox108 = this.chkMiha;
			point = new Point(336, 44);
			jG2CheckBox108.Location = point;
			this.chkMiha.Name = "chkMiha";
			JG2CheckBox jG2CheckBox109 = this.chkMiha;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox109.Size = size;
			this.chkMiha.TabIndex = 8;
			this.chkMiha.UseVisualStyleBackColor = true;
			this.chkTundere.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox110 = this.chkTundere;
			point = new Point(118, 44);
			jG2CheckBox110.Location = point;
			this.chkTundere.Name = "chkTundere";
			JG2CheckBox jG2CheckBox111 = this.chkTundere;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox111.Size = size;
			this.chkTundere.TabIndex = 6;
			this.chkTundere.UseVisualStyleBackColor = true;
			this.chkKyouki.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox112 = this.chkKyouki;
			point = new Point(227, 44);
			jG2CheckBox112.Location = point;
			this.chkKyouki.Name = "chkKyouki";
			JG2CheckBox jG2CheckBox113 = this.chkKyouki;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox113.Size = size;
			this.chkKyouki.TabIndex = 7;
			this.chkKyouki.UseVisualStyleBackColor = true;
			this.chkCharm.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox114 = this.chkCharm;
			point = new Point(9, 44);
			jG2CheckBox114.Location = point;
			this.chkCharm.Name = "chkCharm";
			JG2CheckBox jG2CheckBox115 = this.chkCharm;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox115.Size = size;
			this.chkCharm.TabIndex = 5;
			this.chkCharm.UseVisualStyleBackColor = true;
			this.chkNigate2.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox116 = this.chkNigate2;
			point = new Point(336, 22);
			jG2CheckBox116.Location = point;
			this.chkNigate2.Name = "chkNigate2";
			JG2CheckBox jG2CheckBox117 = this.chkNigate2;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox117.Size = size;
			this.chkNigate2.TabIndex = 4;
			this.chkNigate2.UseVisualStyleBackColor = true;
			this.chkNekketu.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox118 = this.chkNekketu;
			point = new Point(118, 22);
			jG2CheckBox118.Location = point;
			this.chkNekketu.Name = "chkNekketu";
			JG2CheckBox jG2CheckBox119 = this.chkNekketu;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox119.Size = size;
			this.chkNekketu.TabIndex = 2;
			this.chkNekketu.UseVisualStyleBackColor = true;
			this.chkNigate1.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox120 = this.chkNigate1;
			point = new Point(227, 22);
			jG2CheckBox120.Location = point;
			this.chkNigate1.Name = "chkNigate1";
			JG2CheckBox jG2CheckBox121 = this.chkNigate1;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox121.Size = size;
			this.chkNigate1.TabIndex = 3;
			this.chkNigate1.UseVisualStyleBackColor = true;
			this.chkCyoroi.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox122 = this.chkCyoroi;
			point = new Point(9, 22);
			jG2CheckBox122.Location = point;
			this.chkCyoroi.Name = "chkCyoroi";
			JG2CheckBox jG2CheckBox123 = this.chkCyoroi;
			size = new System.Drawing.Size(100, 20);
			jG2CheckBox123.Size = size;
			this.chkCyoroi.TabIndex = 1;
			this.chkCyoroi.UseVisualStyleBackColor = true;
			this.tbpNinshin.BackColor = SystemColors.Control;
			this.tbpNinshin.Controls.Add(this.cmbSaturday2);
			this.tbpNinshin.Controls.Add(this.lblSaturday2);
			this.tbpNinshin.Controls.Add(this.lblFriday2);
			this.tbpNinshin.Controls.Add(this.lblThursday2);
			this.tbpNinshin.Controls.Add(this.lblWednesday2);
			this.tbpNinshin.Controls.Add(this.lblTuesday2);
			this.tbpNinshin.Controls.Add(this.lblMonday2);
			this.tbpNinshin.Controls.Add(this.lblSaturday1);
			this.tbpNinshin.Controls.Add(this.lblFriday1);
			this.tbpNinshin.Controls.Add(this.lblThursday1);
			this.tbpNinshin.Controls.Add(this.lblWednesday1);
			this.tbpNinshin.Controls.Add(this.lblTuesday1);
			this.tbpNinshin.Controls.Add(this.lblSunday2);
			this.tbpNinshin.Controls.Add(this.lblMonday1);
			this.tbpNinshin.Controls.Add(this.lblSunday1);
			this.tbpNinshin.Controls.Add(this.cmbFriday2);
			this.tbpNinshin.Controls.Add(this.cmbThursday2);
			this.tbpNinshin.Controls.Add(this.cmbWednesday2);
			this.tbpNinshin.Controls.Add(this.cmbTuesday2);
			this.tbpNinshin.Controls.Add(this.cmbMonday2);
			this.tbpNinshin.Controls.Add(this.cmbSaturday1);
			this.tbpNinshin.Controls.Add(this.cmbFriday1);
			this.tbpNinshin.Controls.Add(this.cmbThursday1);
			this.tbpNinshin.Controls.Add(this.cmbWednesday1);
			this.tbpNinshin.Controls.Add(this.cmbTuesday1);
			this.tbpNinshin.Controls.Add(this.cmbSunday2);
			this.tbpNinshin.Controls.Add(this.cmbMonday1);
			this.tbpNinshin.Controls.Add(this.cmbSunday1);
			TabPage tabPage11 = this.tbpNinshin;
			point = new Point(4, 28);
			tabPage11.Location = point;
			this.tbpNinshin.Name = "tbpNinshin";
			TabPage tabPage12 = this.tbpNinshin;
			size = new System.Drawing.Size(460, 476);
			tabPage12.Size = size;
			this.tbpNinshin.TabIndex = 4;
            this.tbpNinshin.Text = Resources.CaptionCodeNinshin;
			this.cmbSaturday2.BatchButton = this.btnBatch;
			this.cmbSaturday2.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox50 = this.cmbSaturday2;
			point = new Point(333, 191);
			jG2ComboBox50.Location = point;
			this.cmbSaturday2.Name = "cmbSaturday2";
			JG2ComboBox jG2ComboBox51 = this.cmbSaturday2;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox51.Size = size;
			this.cmbSaturday2.TabIndex = 33;
			Label label142 = this.lblSaturday2;
			point = new Point(233, 195);
			label142.Location = point;
			this.lblSaturday2.Name = "lblSaturday2";
			Label label143 = this.lblSaturday2;
			size = new System.Drawing.Size(92, 18);
			label143.Size = size;
			this.lblSaturday2.TabIndex = 32;
			Label label144 = this.lblFriday2;
			point = new Point(233, 165);
			label144.Location = point;
			this.lblFriday2.Name = "lblFriday2";
			Label label145 = this.lblFriday2;
			size = new System.Drawing.Size(92, 18);
			label145.Size = size;
			this.lblFriday2.TabIndex = 30;
			Label label146 = this.lblThursday2;
			point = new Point(233, 135);
			label146.Location = point;
			this.lblThursday2.Name = "lblThursday2";
			Label label147 = this.lblThursday2;
			size = new System.Drawing.Size(92, 18);
			label147.Size = size;
			this.lblThursday2.TabIndex = 28;
			Label label148 = this.lblWednesday2;
			point = new Point(233, 105);
			label148.Location = point;
			this.lblWednesday2.Name = "lblWednesday2";
			Label label149 = this.lblWednesday2;
			size = new System.Drawing.Size(92, 18);
			label149.Size = size;
			this.lblWednesday2.TabIndex = 20;
			Label label150 = this.lblTuesday2;
			point = new Point(233, 75);
			label150.Location = point;
			this.lblTuesday2.Name = "lblTuesday2";
			Label label151 = this.lblTuesday2;
			size = new System.Drawing.Size(92, 18);
			label151.Size = size;
			this.lblTuesday2.TabIndex = 18;
			Label label152 = this.lblMonday2;
			point = new Point(233, 45);
			label152.Location = point;
			this.lblMonday2.Name = "lblMonday2";
			Label label153 = this.lblMonday2;
			size = new System.Drawing.Size(92, 18);
			label153.Size = size;
			this.lblMonday2.TabIndex = 16;
			Label label154 = this.lblSaturday1;
			point = new Point(6, 195);
			label154.Location = point;
			this.lblSaturday1.Name = "lblSaturday1";
			Label label155 = this.lblSaturday1;
			size = new System.Drawing.Size(92, 18);
			label155.Size = size;
			this.lblSaturday1.TabIndex = 12;
			Label label156 = this.lblFriday1;
			point = new Point(6, 165);
			label156.Location = point;
			this.lblFriday1.Name = "lblFriday1";
			Label label157 = this.lblFriday1;
			size = new System.Drawing.Size(92, 18);
			label157.Size = size;
			this.lblFriday1.TabIndex = 10;
			Label label158 = this.lblThursday1;
			point = new Point(6, 135);
			label158.Location = point;
			this.lblThursday1.Name = "lblThursday1";
			Label label159 = this.lblThursday1;
			size = new System.Drawing.Size(92, 18);
			label159.Size = size;
			this.lblThursday1.TabIndex = 8;
			Label label160 = this.lblWednesday1;
			point = new Point(6, 105);
			label160.Location = point;
			this.lblWednesday1.Name = "lblWednesday1";
			Label label161 = this.lblWednesday1;
			size = new System.Drawing.Size(92, 18);
			label161.Size = size;
			this.lblWednesday1.TabIndex = 6;
			Label label162 = this.lblTuesday1;
			point = new Point(6, 75);
			label162.Location = point;
			this.lblTuesday1.Name = "lblTuesday1";
			Label label163 = this.lblTuesday1;
			size = new System.Drawing.Size(92, 18);
			label163.Size = size;
			this.lblTuesday1.TabIndex = 4;
			Label label164 = this.lblSunday2;
			point = new Point(233, 15);
			label164.Location = point;
			this.lblSunday2.Name = "lblSunday2";
			Label label165 = this.lblSunday2;
			size = new System.Drawing.Size(92, 18);
			label165.Size = size;
			this.lblSunday2.TabIndex = 14;
			Label label166 = this.lblMonday1;
			point = new Point(6, 45);
			label166.Location = point;
			this.lblMonday1.Name = "lblMonday1";
			Label label167 = this.lblMonday1;
			size = new System.Drawing.Size(92, 18);
			label167.Size = size;
			this.lblMonday1.TabIndex = 2;
			Label label168 = this.lblSunday1;
			point = new Point(6, 15);
			label168.Location = point;
			this.lblSunday1.Name = "lblSunday1";
			Label label169 = this.lblSunday1;
			size = new System.Drawing.Size(92, 18);
			label169.Size = size;
			this.lblSunday1.TabIndex = 0;
			this.cmbFriday2.BatchButton = this.btnBatch;
			this.cmbFriday2.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox52 = this.cmbFriday2;
			point = new Point(333, 161);
			jG2ComboBox52.Location = point;
			this.cmbFriday2.Name = "cmbFriday2";
			JG2ComboBox jG2ComboBox53 = this.cmbFriday2;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox53.Size = size;
			this.cmbFriday2.TabIndex = 31;
			this.cmbThursday2.BatchButton = this.btnBatch;
			this.cmbThursday2.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox54 = this.cmbThursday2;
			point = new Point(333, 131);
			jG2ComboBox54.Location = point;
			this.cmbThursday2.Name = "cmbThursday2";
			JG2ComboBox jG2ComboBox55 = this.cmbThursday2;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox55.Size = size;
			this.cmbThursday2.TabIndex = 29;
			this.cmbWednesday2.BatchButton = this.btnBatch;
			this.cmbWednesday2.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox56 = this.cmbWednesday2;
			point = new Point(333, 101);
			jG2ComboBox56.Location = point;
			this.cmbWednesday2.Name = "cmbWednesday2";
			JG2ComboBox jG2ComboBox57 = this.cmbWednesday2;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox57.Size = size;
			this.cmbWednesday2.TabIndex = 21;
			this.cmbTuesday2.BatchButton = this.btnBatch;
			this.cmbTuesday2.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox58 = this.cmbTuesday2;
			point = new Point(333, 71);
			jG2ComboBox58.Location = point;
			this.cmbTuesday2.Name = "cmbTuesday2";
			JG2ComboBox jG2ComboBox59 = this.cmbTuesday2;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox59.Size = size;
			this.cmbTuesday2.TabIndex = 19;
			this.cmbMonday2.BatchButton = this.btnBatch;
			this.cmbMonday2.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox60 = this.cmbMonday2;
			point = new Point(333, 41);
			jG2ComboBox60.Location = point;
			this.cmbMonday2.Name = "cmbMonday2";
			JG2ComboBox jG2ComboBox61 = this.cmbMonday2;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox61.Size = size;
			this.cmbMonday2.TabIndex = 17;
			this.cmbSaturday1.BatchButton = this.btnBatch;
			this.cmbSaturday1.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox62 = this.cmbSaturday1;
			point = new Point(106, 191);
			jG2ComboBox62.Location = point;
			this.cmbSaturday1.Name = "cmbSaturday1";
			JG2ComboBox jG2ComboBox63 = this.cmbSaturday1;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox63.Size = size;
			this.cmbSaturday1.TabIndex = 13;
			this.cmbFriday1.BatchButton = this.btnBatch;
			this.cmbFriday1.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox64 = this.cmbFriday1;
			point = new Point(106, 161);
			jG2ComboBox64.Location = point;
			this.cmbFriday1.Name = "cmbFriday1";
			JG2ComboBox jG2ComboBox65 = this.cmbFriday1;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox65.Size = size;
			this.cmbFriday1.TabIndex = 11;
			this.cmbThursday1.BatchButton = this.btnBatch;
			this.cmbThursday1.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox66 = this.cmbThursday1;
			point = new Point(106, 131);
			jG2ComboBox66.Location = point;
			this.cmbThursday1.Name = "cmbThursday1";
			JG2ComboBox jG2ComboBox67 = this.cmbThursday1;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox67.Size = size;
			this.cmbThursday1.TabIndex = 9;
			this.cmbWednesday1.BatchButton = this.btnBatch;
			this.cmbWednesday1.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox68 = this.cmbWednesday1;
			point = new Point(106, 101);
			jG2ComboBox68.Location = point;
			this.cmbWednesday1.Name = "cmbWednesday1";
			JG2ComboBox jG2ComboBox69 = this.cmbWednesday1;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox69.Size = size;
			this.cmbWednesday1.TabIndex = 7;
			this.cmbTuesday1.BatchButton = this.btnBatch;
			this.cmbTuesday1.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox70 = this.cmbTuesday1;
			point = new Point(106, 71);
			jG2ComboBox70.Location = point;
			this.cmbTuesday1.Name = "cmbTuesday1";
			JG2ComboBox jG2ComboBox71 = this.cmbTuesday1;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox71.Size = size;
			this.cmbTuesday1.TabIndex = 5;
			this.cmbSunday2.BatchButton = this.btnBatch;
			this.cmbSunday2.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox72 = this.cmbSunday2;
			point = new Point(333, 11);
			jG2ComboBox72.Location = point;
			this.cmbSunday2.Name = "cmbSunday2";
			JG2ComboBox jG2ComboBox73 = this.cmbSunday2;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox73.Size = size;
			this.cmbSunday2.TabIndex = 15;
			this.cmbMonday1.BatchButton = this.btnBatch;
			this.cmbMonday1.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox74 = this.cmbMonday1;
			point = new Point(106, 41);
			jG2ComboBox74.Location = point;
			this.cmbMonday1.Name = "cmbMonday1";
			JG2ComboBox jG2ComboBox75 = this.cmbMonday1;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox75.Size = size;
			this.cmbMonday1.TabIndex = 3;
			this.cmbSunday1.BatchButton = this.btnBatch;
			this.cmbSunday1.DropDownStyle = ComboBoxStyle.DropDownList;
			JG2ComboBox jG2ComboBox76 = this.cmbSunday1;
			point = new Point(106, 11);
			jG2ComboBox76.Location = point;
			this.cmbSunday1.Name = "cmbSunday1";
			JG2ComboBox jG2ComboBox77 = this.cmbSunday1;
			size = new System.Drawing.Size(80, 26);
			jG2ComboBox77.Size = size;
			this.cmbSunday1.TabIndex = 1;
			this.tbpCostume.BackColor = SystemColors.Control;
			this.tbpCostume.Controls.Add(this.pnlYouto);
			this.tbpCostume.Controls.Add(this.Label701);
			TabPage tabPage13 = this.tbpCostume;
			point = new Point(4, 28);
			tabPage13.Location = point;
			this.tbpCostume.Name = "tbpCostume";
			TabPage tabPage14 = this.tbpCostume;
			size = new System.Drawing.Size(460, 476);
			tabPage14.Size = size;
			this.tbpCostume.TabIndex = 11;
            this.tbpCostume.Text = Resources.TabOutfit;
			this.pnlYouto.Controls.Add(this.radYouto3);
			this.pnlYouto.Controls.Add(this.radYouto2);
			this.pnlYouto.Controls.Add(this.radYouto1);
			this.pnlYouto.Controls.Add(this.radYouto0);
			Panel panel8 = this.pnlYouto;
			point = new Point(106, 11);
			panel8.Location = point;
			this.pnlYouto.Name = "pnlYouto";
			Panel panel9 = this.pnlYouto;
			size = new System.Drawing.Size(212, 26);
			panel9.Size = size;
			this.pnlYouto.TabIndex = 1;
			this.radYouto3.AllowDrop = true;
			this.radYouto3.Appearance = Appearance.Button;
			this.radYouto3.BatchButton = this.btnBatch;
			JG2RadioButton jG2RadioButton = this.radYouto3;
			point = new Point(162, 0);
			jG2RadioButton.Location = point;
			this.radYouto3.Name = "radYouto3";
			JG2RadioButton jG2RadioButton1 = this.radYouto3;
			size = new System.Drawing.Size(50, 26);
			jG2RadioButton1.Size = size;
			this.radYouto3.TabIndex = 3;
			this.radYouto3.TextAlign = ContentAlignment.MiddleCenter;
			this.radYouto3.UseVisualStyleBackColor = true;
			this.radYouto2.AllowDrop = true;
			this.radYouto2.Appearance = Appearance.Button;
			this.radYouto2.BatchButton = this.btnBatch;
			JG2RadioButton jG2RadioButton2 = this.radYouto2;
			point = new Point(108, 0);
			jG2RadioButton2.Location = point;
			this.radYouto2.Name = "radYouto2";
			JG2RadioButton jG2RadioButton3 = this.radYouto2;
			size = new System.Drawing.Size(50, 26);
			jG2RadioButton3.Size = size;
			this.radYouto2.TabIndex = 2;
			this.radYouto2.TextAlign = ContentAlignment.MiddleCenter;
			this.radYouto2.UseVisualStyleBackColor = true;
			this.radYouto1.AllowDrop = true;
			this.radYouto1.Appearance = Appearance.Button;
			this.radYouto1.BatchButton = this.btnBatch;
			JG2RadioButton jG2RadioButton4 = this.radYouto1;
			point = new Point(54, 0);
			jG2RadioButton4.Location = point;
			this.radYouto1.Name = "radYouto1";
			JG2RadioButton jG2RadioButton5 = this.radYouto1;
			size = new System.Drawing.Size(50, 26);
			jG2RadioButton5.Size = size;
			this.radYouto1.TabIndex = 1;
			this.radYouto1.TextAlign = ContentAlignment.MiddleCenter;
			this.radYouto1.UseVisualStyleBackColor = true;
			this.radYouto0.AllowDrop = true;
			this.radYouto0.Appearance = Appearance.Button;
			this.radYouto0.BatchButton = this.btnBatch;
			JG2RadioButton jG2RadioButton6 = this.radYouto0;
			point = new Point(0, 0);
			jG2RadioButton6.Location = point;
			this.radYouto0.Name = "radYouto0";
			JG2RadioButton jG2RadioButton7 = this.radYouto0;
			size = new System.Drawing.Size(50, 26);
			jG2RadioButton7.Size = size;
			this.radYouto0.TabIndex = 0;
			this.radYouto0.TextAlign = ContentAlignment.MiddleCenter;
			this.radYouto0.UseVisualStyleBackColor = true;
			Label label701 = this.Label701;
			point = new Point(6, 15);
			label701.Location = point;
			this.Label701.Name = "Label701";
			Label label7011 = this.Label701;
			size = new System.Drawing.Size(92, 18);
			label7011.Size = size;
			this.Label701.TabIndex = 0;
			this.Label701.Text = Resources.Label701Text;
			this.tbpTools.BackColor = SystemColors.Control;
			this.tbpTools.Controls.Add(this.pnlAction);
			this.tbpTools.Controls.Add(this.pnlActionRev);
			this.tbpTools.Controls.Add(this.pnlPair);
			this.tbpTools.Controls.Add(this.pnlProfile);
			this.tbpTools.Controls.Add(this.pnlTools);
			TabPage tabPage15 = this.tbpTools;
			point = new Point(4, 28);
			tabPage15.Location = point;
			this.tbpTools.Name = "tbpTools";
			TabPage tabPage16 = this.tbpTools;
			padding = new System.Windows.Forms.Padding(3);
			tabPage16.Padding = padding;
			TabPage tabPage17 = this.tbpTools;
			size = new System.Drawing.Size(460, 476);
			tabPage17.Size = size;
			this.tbpTools.TabIndex = 0;
            this.tbpTools.Text = Resources.CaptiontbpTools;
			this.pnlAction.Controls.Add(this.btnAction3);
			this.pnlAction.Controls.Add(this.btnDelAction3);
			this.pnlAction.Controls.Add(this.btnDelAction2);
			this.pnlAction.Controls.Add(this.btnAction4);
			this.pnlAction.Controls.Add(this.lblAction);
			this.pnlAction.Controls.Add(this.btnDelAction4);
			this.pnlAction.Controls.Add(this.btnAction2);
			this.pnlAction.Controls.Add(this.btnDelAction1);
			this.pnlAction.Controls.Add(this.btnAction1);
			Panel panel10 = this.pnlAction;
			point = new Point(14, 195);
			panel10.Location = point;
			this.pnlAction.Name = "pnlAction";
			Panel panel11 = this.pnlAction;
			size = new System.Drawing.Size(432, 26);
			panel11.Size = size;
			this.pnlAction.TabIndex = 1;
			this.btnAction3.BatchButton = this.btnBatch;
			this.btnAction3.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button14 = this.btnAction3;
			point = new Point(217, 0);
			jG2Button14.Location = point;
			JG2Button jG2Button15 = this.btnAction3;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button15.Margin = padding;
			this.btnAction3.Name = "btnAction3";
			JG2Button jG2Button16 = this.btnAction3;
			size = new System.Drawing.Size(34, 26);
			jG2Button16.Size = size;
			this.btnAction3.TabIndex = 3;
			this.btnAction3.UseVisualStyleBackColor = true;
			this.btnDelAction3.BatchButton = this.btnBatch;
			this.btnDelAction3.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button17 = this.btnDelAction3;
			point = new Point(361, 0);
			jG2Button17.Location = point;
			JG2Button jG2Button18 = this.btnDelAction3;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button18.Margin = padding;
			this.btnDelAction3.Name = "btnDelAction3";
			JG2Button jG2Button19 = this.btnDelAction3;
			size = new System.Drawing.Size(34, 26);
			jG2Button19.Size = size;
			this.btnDelAction3.TabIndex = 7;
            this.btnDelAction3.Text = Resources.BtnTextDelete;
			this.btnDelAction3.UseVisualStyleBackColor = true;
			this.btnDelAction2.BatchButton = this.btnBatch;
			this.btnDelAction2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button20 = this.btnDelAction2;
			point = new Point(325, 0);
			jG2Button20.Location = point;
			JG2Button jG2Button21 = this.btnDelAction2;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button21.Margin = padding;
			this.btnDelAction2.Name = "btnDelAction2";
			JG2Button jG2Button22 = this.btnDelAction2;
			size = new System.Drawing.Size(34, 26);
			jG2Button22.Size = size;
			this.btnDelAction2.TabIndex = 6;
			this.btnDelAction2.Text = Resources.BtnTextDelete;
            this.btnDelAction2.UseVisualStyleBackColor = true;
			this.btnAction4.BatchButton = this.btnBatch;
			this.btnAction4.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button23 = this.btnAction4;
			point = new Point(253, 0);
			jG2Button23.Location = point;
			JG2Button jG2Button24 = this.btnAction4;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button24.Margin = padding;
			this.btnAction4.Name = "btnAction4";
			JG2Button jG2Button25 = this.btnAction4;
			size = new System.Drawing.Size(34, 26);
			jG2Button25.Size = size;
			this.btnAction4.TabIndex = 4;
			this.btnAction4.UseVisualStyleBackColor = true;
			this.lblAction.AutoEllipsis = true;
			Label label170 = this.lblAction;
			point = new Point(0, 4);
			label170.Location = point;
			this.lblAction.Name = "lblAction";
			Label label171 = this.lblAction;
			size = new System.Drawing.Size(144, 18);
			label171.Size = size;
			this.lblAction.TabIndex = 0;
			this.btnDelAction4.BatchButton = this.btnBatch;
			this.btnDelAction4.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button26 = this.btnDelAction4;
			point = new Point(397, 0);
			jG2Button26.Location = point;
			JG2Button jG2Button27 = this.btnDelAction4;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button27.Margin = padding;
			this.btnDelAction4.Name = "btnDelAction4";
			JG2Button jG2Button28 = this.btnDelAction4;
			size = new System.Drawing.Size(34, 26);
			jG2Button28.Size = size;
			this.btnDelAction4.TabIndex = 8;
			this.btnDelAction4.Text = Resources.BtnTextDelete;
            this.btnDelAction4.UseVisualStyleBackColor = true;
			this.btnAction2.BatchButton = this.btnBatch;
			this.btnAction2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button29 = this.btnAction2;
			point = new Point(181, 0);
			jG2Button29.Location = point;
			JG2Button jG2Button30 = this.btnAction2;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button30.Margin = padding;
			this.btnAction2.Name = "btnAction2";
			JG2Button jG2Button31 = this.btnAction2;
			size = new System.Drawing.Size(34, 26);
			jG2Button31.Size = size;
			this.btnAction2.TabIndex = 2;
			this.btnAction2.UseVisualStyleBackColor = true;
			this.btnDelAction1.BatchButton = this.btnBatch;
			this.btnDelAction1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button32 = this.btnDelAction1;
			point = new Point(289, 0);
			jG2Button32.Location = point;
			JG2Button jG2Button33 = this.btnDelAction1;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button33.Margin = padding;
			this.btnDelAction1.Name = "btnDelAction1";
			JG2Button jG2Button34 = this.btnDelAction1;
			size = new System.Drawing.Size(34, 26);
			jG2Button34.Size = size;
			this.btnDelAction1.TabIndex = 5;
			this.btnDelAction1.Text = Resources.BtnTextDelete;
            this.btnDelAction1.UseVisualStyleBackColor = true;
			this.btnAction1.BatchButton = this.btnBatch;
			this.btnAction1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button35 = this.btnAction1;
			point = new Point(145, 0);
			jG2Button35.Location = point;
			JG2Button jG2Button36 = this.btnAction1;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button36.Margin = padding;
			this.btnAction1.Name = "btnAction1";
			JG2Button jG2Button37 = this.btnAction1;
			size = new System.Drawing.Size(34, 26);
			jG2Button37.Size = size;
			this.btnAction1.TabIndex = 1;
			this.btnAction1.UseVisualStyleBackColor = true;
			this.pnlActionRev.Controls.Add(this.btnActionRev3);
			this.pnlActionRev.Controls.Add(this.btnDelActionRev3);
			this.pnlActionRev.Controls.Add(this.btnDelActionRev2);
			this.pnlActionRev.Controls.Add(this.btnActionRev4);
			this.pnlActionRev.Controls.Add(this.lblActionRev);
			this.pnlActionRev.Controls.Add(this.btnDelActionRev4);
			this.pnlActionRev.Controls.Add(this.btnActionRev2);
			this.pnlActionRev.Controls.Add(this.btnDelActionRev1);
			this.pnlActionRev.Controls.Add(this.btnActionRev1);
			Panel panel12 = this.pnlActionRev;
			point = new Point(14, 396);
			panel12.Location = point;
			this.pnlActionRev.Name = "pnlActionRev";
			Panel panel13 = this.pnlActionRev;
			size = new System.Drawing.Size(432, 26);
			panel13.Size = size;
			this.pnlActionRev.TabIndex = 3;
			this.btnActionRev3.BatchButton = this.btnBatch;
			this.btnActionRev3.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button38 = this.btnActionRev3;
			point = new Point(217, 0);
			jG2Button38.Location = point;
			JG2Button jG2Button39 = this.btnActionRev3;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button39.Margin = padding;
			this.btnActionRev3.Name = "btnActionRev3";
			JG2Button jG2Button40 = this.btnActionRev3;
			size = new System.Drawing.Size(34, 26);
			jG2Button40.Size = size;
			this.btnActionRev3.TabIndex = 3;
			this.btnActionRev3.UseVisualStyleBackColor = true;
			this.btnDelActionRev3.BatchButton = this.btnBatch;
			this.btnDelActionRev3.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button41 = this.btnDelActionRev3;
			point = new Point(361, 0);
			jG2Button41.Location = point;
			JG2Button jG2Button42 = this.btnDelActionRev3;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button42.Margin = padding;
			this.btnDelActionRev3.Name = "btnDelActionRev3";
			JG2Button jG2Button43 = this.btnDelActionRev3;
			size = new System.Drawing.Size(34, 26);
			jG2Button43.Size = size;
			this.btnDelActionRev3.TabIndex = 7;
			this.btnDelActionRev3.Text = Resources.BtnTextDelete;
            this.btnDelActionRev3.UseVisualStyleBackColor = true;
			this.btnDelActionRev2.BatchButton = this.btnBatch;
			this.btnDelActionRev2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button44 = this.btnDelActionRev2;
			point = new Point(325, 0);
			jG2Button44.Location = point;
			JG2Button jG2Button45 = this.btnDelActionRev2;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button45.Margin = padding;
			this.btnDelActionRev2.Name = "btnDelActionRev2";
			JG2Button jG2Button46 = this.btnDelActionRev2;
			size = new System.Drawing.Size(34, 26);
			jG2Button46.Size = size;
			this.btnDelActionRev2.TabIndex = 6;
			this.btnDelActionRev2.Text = Resources.BtnTextDelete;
            this.btnDelActionRev2.UseVisualStyleBackColor = true;
			this.btnActionRev4.BatchButton = this.btnBatch;
			this.btnActionRev4.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button47 = this.btnActionRev4;
			point = new Point(253, 0);
			jG2Button47.Location = point;
			JG2Button jG2Button48 = this.btnActionRev4;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button48.Margin = padding;
			this.btnActionRev4.Name = "btnActionRev4";
			JG2Button jG2Button49 = this.btnActionRev4;
			size = new System.Drawing.Size(34, 26);
			jG2Button49.Size = size;
			this.btnActionRev4.TabIndex = 4;
			this.btnActionRev4.UseVisualStyleBackColor = true;
			this.lblActionRev.AutoEllipsis = true;
			Label label172 = this.lblActionRev;
			point = new Point(0, 4);
			label172.Location = point;
			this.lblActionRev.Name = "lblActionRev";
			Label label173 = this.lblActionRev;
			size = new System.Drawing.Size(144, 18);
			label173.Size = size;
			this.lblActionRev.TabIndex = 0;
			this.btnDelActionRev4.BatchButton = this.btnBatch;
			this.btnDelActionRev4.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button50 = this.btnDelActionRev4;
			point = new Point(397, 0);
			jG2Button50.Location = point;
			JG2Button jG2Button51 = this.btnDelActionRev4;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button51.Margin = padding;
			this.btnDelActionRev4.Name = "btnDelActionRev4";
			JG2Button jG2Button52 = this.btnDelActionRev4;
			size = new System.Drawing.Size(34, 26);
			jG2Button52.Size = size;
			this.btnDelActionRev4.TabIndex = 8;
			this.btnDelActionRev4.Text = Resources.BtnTextDelete;
            this.btnDelActionRev4.UseVisualStyleBackColor = true;
			this.btnActionRev2.BatchButton = this.btnBatch;
			this.btnActionRev2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button53 = this.btnActionRev2;
			point = new Point(181, 0);
			jG2Button53.Location = point;
			JG2Button jG2Button54 = this.btnActionRev2;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button54.Margin = padding;
			this.btnActionRev2.Name = "btnActionRev2";
			JG2Button jG2Button55 = this.btnActionRev2;
			size = new System.Drawing.Size(34, 26);
			jG2Button55.Size = size;
			this.btnActionRev2.TabIndex = 2;
			this.btnActionRev2.UseVisualStyleBackColor = true;
			this.btnDelActionRev1.BatchButton = this.btnBatch;
			this.btnDelActionRev1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button56 = this.btnDelActionRev1;
			point = new Point(289, 0);
			jG2Button56.Location = point;
			JG2Button jG2Button57 = this.btnDelActionRev1;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button57.Margin = padding;
			this.btnDelActionRev1.Name = "btnDelActionRev1";
			JG2Button jG2Button58 = this.btnDelActionRev1;
			size = new System.Drawing.Size(34, 26);
			jG2Button58.Size = size;
			this.btnDelActionRev1.TabIndex = 5;
			this.btnDelActionRev1.Text = Resources.BtnTextDelete;
            this.btnDelActionRev1.UseVisualStyleBackColor = true;
			this.btnActionRev1.BatchButton = this.btnBatch;
			this.btnActionRev1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button59 = this.btnActionRev1;
			point = new Point(145, 0);
			jG2Button59.Location = point;
			JG2Button jG2Button60 = this.btnActionRev1;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button60.Margin = padding;
			this.btnActionRev1.Name = "btnActionRev1";
			JG2Button jG2Button61 = this.btnActionRev1;
			size = new System.Drawing.Size(34, 26);
			jG2Button61.Size = size;
			this.btnActionRev1.TabIndex = 1;
			this.btnActionRev1.UseVisualStyleBackColor = true;
			this.pnlPair.Controls.Add(this.lblRenzoku);
			this.pnlPair.Controls.Add(this.Label92);
			this.pnlPair.Controls.Add(this.lblCntTodaySeikou);
			this.pnlPair.Controls.Add(this.Label89);
			this.pnlPair.Controls.Add(this.lblThumbnailPlayer);
			this.pnlPair.Controls.Add(this.lblCntNakaKiken);
			this.pnlPair.Controls.Add(this.lblHAisyou);
			this.pnlPair.Controls.Add(this.chkKousai);
			this.pnlPair.Controls.Add(this.Label83);
			this.pnlPair.Controls.Add(this.lblSyasei4);
			this.pnlPair.Controls.Add(this.Label129);
			this.pnlPair.Controls.Add(this.lblSyasei3);
			this.pnlPair.Controls.Add(this.Label131);
			this.pnlPair.Controls.Add(this.lblCntCondom);
			this.pnlPair.Controls.Add(this.Label111);
			this.pnlPair.Controls.Add(this.lblSyasei2);
			this.pnlPair.Controls.Add(this.lblCntSeikou3);
			this.pnlPair.Controls.Add(this.Label114);
			this.pnlPair.Controls.Add(this.Label115);
			this.pnlPair.Controls.Add(this.lblSyasei1);
			this.pnlPair.Controls.Add(this.lblCntSeikou2);
			this.pnlPair.Controls.Add(this.Label118);
			this.pnlPair.Controls.Add(this.Label119);
			this.pnlPair.Controls.Add(this.lblCntSynchronize);
			this.pnlPair.Controls.Add(this.lblCntSeikou1);
			this.pnlPair.Controls.Add(this.Label122);
			this.pnlPair.Controls.Add(this.Label123);
			this.pnlPair.Controls.Add(this.lblCntOrgasm);
			this.pnlPair.Controls.Add(this.Label126);
			this.pnlPair.Controls.Add(this.Label127);
			this.pnlPair.Controls.Add(this.lblPair);
			Panel panel14 = this.pnlPair;
			point = new Point(6, 236);
			panel14.Location = point;
			this.pnlPair.Name = "pnlPair";
			Panel panel15 = this.pnlPair;
			size = new System.Drawing.Size(448, 160);
			panel15.Size = size;
			this.pnlPair.TabIndex = 2;
			Label label174 = this.lblRenzoku;
			point = new Point(180, 140);
			label174.Location = point;
			this.lblRenzoku.Name = "lblRenzoku";
			Label label175 = this.lblRenzoku;
			size = new System.Drawing.Size(77, 18);
			label175.Size = size;
			this.lblRenzoku.TabIndex = 28;
			this.lblRenzoku.TextAlign = ContentAlignment.TopRight;
			this.Label92.AutoSize = true;
			Label label921 = this.Label92;
			point = new Point(90, 140);
			label921.Location = point;
			this.Label92.Name = "Label92";
			Label label922 = this.Label92;
			size = new System.Drawing.Size(80, 18);
			label922.Size = size;
			this.Label92.TabIndex = 27;
			this.Label92.Text = "連続性交日数";
			Label label176 = this.lblCntTodaySeikou;
			point = new Point(363, 140);
			label176.Location = point;
			this.lblCntTodaySeikou.Name = "lblCntTodaySeikou";
			Label label177 = this.lblCntTodaySeikou;
			size = new System.Drawing.Size(77, 18);
			label177.Size = size;
			this.lblCntTodaySeikou.TabIndex = 30;
			this.lblCntTodaySeikou.TextAlign = ContentAlignment.TopRight;
			this.Label89.AutoSize = true;
			Label label891 = this.Label89;
			point = new Point(273, 140);
			label891.Location = point;
			this.Label89.Name = "Label89";
			Label label892 = this.Label89;
			size = new System.Drawing.Size(80, 18);
			label892.Size = size;
			this.Label89.TabIndex = 29;
			this.Label89.Text = "当日性交回数";
			Label label178 = this.lblThumbnailPlayer;
			point = new Point(8, 20);
			label178.Location = point;
			this.lblThumbnailPlayer.Name = "lblThumbnailPlayer";
			Label label179 = this.lblThumbnailPlayer;
			size = new System.Drawing.Size(74, 111);
			label179.Size = size;
			this.lblThumbnailPlayer.TabIndex = 1;
			Label label180 = this.lblCntNakaKiken;
			point = new Point(363, 120);
			label180.Location = point;
			this.lblCntNakaKiken.Name = "lblCntNakaKiken";
			Label label181 = this.lblCntNakaKiken;
			size = new System.Drawing.Size(77, 18);
			label181.Size = size;
			this.lblCntNakaKiken.TabIndex = 26;
			this.lblCntNakaKiken.TextAlign = ContentAlignment.TopRight;
			Label label182 = this.lblHAisyou;
			point = new Point(180, 20);
			label182.Location = point;
			this.lblHAisyou.Name = "lblHAisyou";
			Label label183 = this.lblHAisyou;
			size = new System.Drawing.Size(77, 18);
			label183.Size = size;
			this.lblHAisyou.TabIndex = 4;
			this.lblHAisyou.TextAlign = ContentAlignment.TopRight;
			this.chkKousai.BatchButton = this.btnBatch;
			JG2CheckBox jG2CheckBox124 = this.chkKousai;
			point = new Point(11, 139);
			jG2CheckBox124.Location = point;
			this.chkKousai.Name = "chkKousai";
			JG2CheckBox jG2CheckBox125 = this.chkKousai;
			size = new System.Drawing.Size(74, 20);
			jG2CheckBox125.Size = size;
			this.chkKousai.TabIndex = 2;
			this.chkKousai.UseVisualStyleBackColor = true;
			this.Label83.AutoSize = true;
			Label label831 = this.Label83;
			point = new Point(273, 120);
			label831.Location = point;
			this.Label83.Name = "Label83";
			Label label832 = this.Label83;
			size = new System.Drawing.Size(80, 18);
			label832.Size = size;
			this.Label83.TabIndex = 25;
            this.Label83.Text = Resources.CaptionDangerousDayAmt;
			Label label184 = this.lblSyasei4;
			point = new Point(363, 100);
			label184.Location = point;
			this.lblSyasei4.Name = "lblSyasei4";
			Label label185 = this.lblSyasei4;
			size = new System.Drawing.Size(77, 18);
			label185.Size = size;
			this.lblSyasei4.TabIndex = 24;
			this.lblSyasei4.TextAlign = ContentAlignment.TopRight;
			this.Label129.AutoSize = true;
			Label label1291 = this.Label129;
			point = new Point(273, 100);
			label1291.Location = point;
			this.Label129.Name = "Label129";
			Label label1292 = this.Label129;
			size = new System.Drawing.Size(44, 18);
			label1292.Size = size;
			this.Label129.TabIndex = 23;
            this.Label129.Text = Resources.CaptionSwallowAmt;
			Label label186 = this.lblSyasei3;
			point = new Point(363, 80);
			label186.Location = point;
			this.lblSyasei3.Name = "lblSyasei3";
			Label label187 = this.lblSyasei3;
			size = new System.Drawing.Size(77, 18);
			label187.Size = size;
			this.lblSyasei3.TabIndex = 22;
			this.lblSyasei3.TextAlign = ContentAlignment.TopRight;
			this.Label131.AutoSize = true;
			Label label1311 = this.Label131;
			point = new Point(273, 80);
			label1311.Location = point;
			this.Label131.Name = "Label131";
			Label label1312 = this.Label131;
			size = new System.Drawing.Size(80, 18);
			label1312.Size = size;
			this.Label131.TabIndex = 21;
            this.Label131.Text = Resources.CaptionAnalAmt;
			Label label188 = this.lblCntCondom;
			point = new Point(180, 100);
			label188.Location = point;
			this.lblCntCondom.Name = "lblCntCondom";
			Label label189 = this.lblCntCondom;
			size = new System.Drawing.Size(77, 18);
			label189.Size = size;
			this.lblCntCondom.TabIndex = 12;
			this.lblCntCondom.TextAlign = ContentAlignment.TopRight;
			this.Label111.AutoSize = true;
			Label label1111 = this.Label111;
			point = new Point(90, 100);
			label1111.Location = point;
			this.Label111.Name = "Label111";
			Label label1112 = this.Label111;
			size = new System.Drawing.Size(92, 18);
			label1112.Size = size;
			this.Label111.TabIndex = 11;
            this.Label111.Text = Resources.CaptionCondomUseFreq;
			Label label190 = this.lblSyasei2;
			point = new Point(363, 60);
			label190.Location = point;
			this.lblSyasei2.Name = "lblSyasei2";
			Label label191 = this.lblSyasei2;
			size = new System.Drawing.Size(77, 18);
			label191.Size = size;
			this.lblSyasei2.TabIndex = 20;
			this.lblSyasei2.TextAlign = ContentAlignment.TopRight;
			Label label192 = this.lblCntSeikou3;
			point = new Point(180, 80);
			label192.Location = point;
			this.lblCntSeikou3.Name = "lblCntSeikou3";
			Label label193 = this.lblCntSeikou3;
			size = new System.Drawing.Size(77, 18);
			label193.Size = size;
			this.lblCntSeikou3.TabIndex = 10;
			this.lblCntSeikou3.TextAlign = ContentAlignment.TopRight;
			this.Label114.AutoSize = true;
			Label label1141 = this.Label114;
			point = new Point(273, 60);
			label1141.Location = point;
			this.Label114.Name = "Label114";
			Label label1142 = this.Label114;
			size = new System.Drawing.Size(68, 18);
			label1142.Size = size;
			this.Label114.TabIndex = 19;
            this.Label114.Text = Resources.CaptionGenitalEjaFreq;
			this.Label115.AutoSize = true;
			Label label1151 = this.Label115;
			point = new Point(90, 80);
			label1151.Location = point;
			this.Label115.Name = "Label115";
			Label label1152 = this.Label115;
			size = new System.Drawing.Size(92, 18);
			label1152.Size = size;
			this.Label115.TabIndex = 9;
            this.Label115.Text = Resources.CaptionAnalAmt;
			Label label194 = this.lblSyasei1;
			point = new Point(363, 40);
			label194.Location = point;
			this.lblSyasei1.Name = "lblSyasei1";
			Label label195 = this.lblSyasei1;
			size = new System.Drawing.Size(77, 18);
			label195.Size = size;
			this.lblSyasei1.TabIndex = 18;
			this.lblSyasei1.TextAlign = ContentAlignment.TopRight;
			Label label196 = this.lblCntSeikou2;
			point = new Point(180, 60);
			label196.Location = point;
			this.lblCntSeikou2.Name = "lblCntSeikou2";
			Label label197 = this.lblCntSeikou2;
			size = new System.Drawing.Size(77, 18);
			label197.Size = size;
			this.lblCntSeikou2.TabIndex = 8;
			this.lblCntSeikou2.TextAlign = ContentAlignment.TopRight;
			this.Label118.AutoSize = true;
			Label label1181 = this.Label118;
			point = new Point(273, 40);
			label1181.Location = point;
			this.Label118.Name = "Label118";
			Label label1182 = this.Label118;
			size = new System.Drawing.Size(56, 18);
			label1182.Size = size;
			this.Label118.TabIndex = 17;
            this.Label118.Text = Resources.CaptionEjaAmt;
			this.Label119.AutoSize = true;
			Label label1191 = this.Label119;
			point = new Point(90, 60);
			label1191.Location = point;
			this.Label119.Name = "Label119";
			Label label1192 = this.Label119;
			size = new System.Drawing.Size(80, 18);
			label1192.Size = size;
			this.Label119.TabIndex = 7;
            this.Label119.Text = Resources.CaptionSexCount;
			Label label198 = this.lblCntSynchronize;
			point = new Point(363, 20);
			label198.Location = point;
			this.lblCntSynchronize.Name = "lblCntSynchronize";
			Label label199 = this.lblCntSynchronize;
			size = new System.Drawing.Size(77, 18);
			label199.Size = size;
			this.lblCntSynchronize.TabIndex = 16;
			this.lblCntSynchronize.TextAlign = ContentAlignment.TopRight;
			Label label200 = this.lblCntSeikou1;
			point = new Point(180, 40);
			label200.Location = point;
			this.lblCntSeikou1.Name = "lblCntSeikou1";
			Label label201 = this.lblCntSeikou1;
			size = new System.Drawing.Size(77, 18);
			label201.Size = size;
			this.lblCntSeikou1.TabIndex = 6;
			this.lblCntSeikou1.TextAlign = ContentAlignment.TopRight;
			this.Label122.AutoSize = true;
			Label label1221 = this.Label122;
			point = new Point(273, 20);
			label1221.Location = point;
			this.Label122.Name = "Label122";
			Label label1222 = this.Label122;
			size = new System.Drawing.Size(80, 18);
			label1222.Size = size;
			this.Label122.TabIndex = 15;
            this.Label122.Text = Resources.CaptionMultiMaxNumber;
			this.Label123.AutoSize = true;
			Label label1231 = this.Label123;
			point = new Point(90, 40);
			label1231.Location = point;
			this.Label123.Name = "Label123";
			Label label1232 = this.Label123;
			size = new System.Drawing.Size(68, 18);
			label1232.Size = size;
			this.Label123.TabIndex = 5;
            this.Label123.Text = Resources.CaptionHFrequency;
			Label label202 = this.lblCntOrgasm;
			point = new Point(180, 120);
			label202.Location = point;
			this.lblCntOrgasm.Name = "lblCntOrgasm";
			Label label203 = this.lblCntOrgasm;
			size = new System.Drawing.Size(77, 18);
			label203.Size = size;
			this.lblCntOrgasm.TabIndex = 14;
			this.lblCntOrgasm.TextAlign = ContentAlignment.TopRight;
			this.Label126.AutoSize = true;
			Label label1261 = this.Label126;
			point = new Point(90, 120);
			label1261.Location = point;
			this.Label126.Name = "Label126";
			Label label1262 = this.Label126;
			size = new System.Drawing.Size(56, 18);
			label1262.Size = size;
			this.Label126.TabIndex = 13;
            this.Label126.Text = Resources.CaptionOrgasmCount;
			this.Label127.AutoSize = true;
			Label label1271 = this.Label127;
			point = new Point(90, 20);
			label1271.Location = point;
			this.Label127.Name = "Label127";
			Label label1272 = this.Label127;
			size = new System.Drawing.Size(68, 18);
			label1272.Size = size;
			this.Label127.TabIndex = 3;
            this.Label127.Text = Resources.CaptionCodeHAisyou;
			Label label204 = this.lblPair;
			point = new Point(0, 0);
			label204.Location = point;
			this.lblPair.Name = "lblPair";
			Label label205 = this.lblPair;
			size = new System.Drawing.Size(200, 18);
			label205.Size = size;
			this.lblPair.TabIndex = 0;
			this.pnlProfile.Controls.Add(this.lblGoods3);
			this.pnlProfile.Controls.Add(this.lblGoods1);
			this.pnlProfile.Controls.Add(this.lblGoods2);
			this.pnlProfile.Controls.Add(this.lblAite3);
			this.pnlProfile.Controls.Add(this.lblAite2);
			this.pnlProfile.Controls.Add(this.lblAite1);
			this.pnlProfile.Controls.Add(this.lblThumbnail);
			this.pnlProfile.Controls.Add(this.btnPlayer);
			this.pnlProfile.Controls.Add(this.Label90);
			this.pnlProfile.Controls.Add(this.Label109);
			this.pnlProfile.Controls.Add(this.Label91);
			this.pnlProfile.Controls.Add(this.Label107);
			this.pnlProfile.Controls.Add(this.lblCntSoudatu);
			this.pnlProfile.Controls.Add(this.Label26);
			this.pnlProfile.Controls.Add(this.Label105);
			this.pnlProfile.Controls.Add(this.lblTest3);
			this.pnlProfile.Controls.Add(this.lblCntKenka);
			this.pnlProfile.Controls.Add(this.Label102);
			this.pnlProfile.Controls.Add(this.Label103);
			this.pnlProfile.Controls.Add(this.lblTest2);
			this.pnlProfile.Controls.Add(this.lblCntFurareta);
			this.pnlProfile.Controls.Add(this.Label98);
			this.pnlProfile.Controls.Add(this.Label99);
			this.pnlProfile.Controls.Add(this.lblTest1);
			this.pnlProfile.Controls.Add(this.lblCntSeikouAite);
			this.pnlProfile.Controls.Add(this.Label94);
			this.pnlProfile.Controls.Add(this.Label95);
			this.pnlProfile.Controls.Add(this.lblCntSabori);
			this.pnlProfile.Controls.Add(this.lblCntKousai);
			this.pnlProfile.Controls.Add(this.Label87);
			this.pnlProfile.Controls.Add(this.Label85);
			this.pnlProfile.Controls.Add(this.Label84);
			this.pnlProfile.Controls.Add(this.lblPersonal);
			Panel panel16 = this.pnlProfile;
			point = new Point(6, 15);
			panel16.Location = point;
			this.pnlProfile.Name = "pnlProfile";
			Panel panel17 = this.pnlProfile;
			size = new System.Drawing.Size(448, 180);
			panel17.Size = size;
			this.pnlProfile.TabIndex = 0;
			this.lblGoods3.AutoEllipsis = true;
			Label label206 = this.lblGoods3;
			point = new Point(345, 160);
			label206.Location = point;
			this.lblGoods3.Name = "lblGoods3";
			Label label207 = this.lblGoods3;
			size = new System.Drawing.Size(95, 18);
			label207.Size = size;
			this.lblGoods3.TabIndex = 32;
			this.lblGoods3.TextAlign = ContentAlignment.TopRight;
			this.lblGoods1.AutoEllipsis = true;
			Label label208 = this.lblGoods1;
			point = new Point(345, 120);
			label208.Location = point;
			this.lblGoods1.Name = "lblGoods1";
			Label label209 = this.lblGoods1;
			size = new System.Drawing.Size(95, 18);
			label209.Size = size;
			this.lblGoods1.TabIndex = 28;
			this.lblGoods1.TextAlign = ContentAlignment.TopRight;
			this.lblGoods2.AutoEllipsis = true;
			Label label210 = this.lblGoods2;
			point = new Point(345, 140);
			label210.Location = point;
			this.lblGoods2.Name = "lblGoods2";
			Label label211 = this.lblGoods2;
			size = new System.Drawing.Size(95, 18);
			label211.Size = size;
			this.lblGoods2.TabIndex = 30;
			this.lblGoods2.TextAlign = ContentAlignment.TopRight;
			this.lblAite3.AutoEllipsis = true;
			Label label212 = this.lblAite3;
			point = new Point(180, 160);
			label212.Location = point;
			this.lblAite3.Name = "lblAite3";
			Label label213 = this.lblAite3;
			size = new System.Drawing.Size(77, 18);
			label213.Size = size;
			this.lblAite3.TabIndex = 26;
			this.lblAite3.TextAlign = ContentAlignment.TopRight;
			this.lblAite2.AutoEllipsis = true;
			Label label214 = this.lblAite2;
			point = new Point(180, 140);
			label214.Location = point;
			this.lblAite2.Name = "lblAite2";
			Label label215 = this.lblAite2;
			size = new System.Drawing.Size(77, 18);
			label215.Size = size;
			this.lblAite2.TabIndex = 24;
			this.lblAite2.TextAlign = ContentAlignment.TopRight;
			this.lblAite1.AutoEllipsis = true;
			Label label216 = this.lblAite1;
			point = new Point(180, 120);
			label216.Location = point;
			this.lblAite1.Name = "lblAite1";
			Label label217 = this.lblAite1;
			size = new System.Drawing.Size(77, 18);
			label217.Size = size;
			this.lblAite1.TabIndex = 22;
			this.lblAite1.TextAlign = ContentAlignment.TopRight;
			Label label218 = this.lblThumbnail;
			point = new Point(8, 20);
			label218.Location = point;
			this.lblThumbnail.Name = "lblThumbnail";
			Label label219 = this.lblThumbnail;
			size = new System.Drawing.Size(74, 111);
			label219.Size = size;
			this.lblThumbnail.TabIndex = 1;
			this.btnPlayer.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			Button button20 = this.btnPlayer;
			point = new Point(8, 136);
			button20.Location = point;
			Button button21 = this.btnPlayer;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			button21.Margin = padding;
			this.btnPlayer.Name = "btnPlayer";
			Button button22 = this.btnPlayer;
			size = new System.Drawing.Size(74, 26);
			button22.Size = size;
			this.btnPlayer.TabIndex = 2;
            this.btnPlayer.Text = Resources.BtnTxtSetPlayerChar;
			this.btnPlayer.UseVisualStyleBackColor = true;
			this.Label90.AutoSize = true;
			Label label901 = this.Label90;
			point = new Point(273, 160);
			label901.Location = point;
			this.Label90.Name = "Label90";
			Label label902 = this.Label90;
			size = new System.Drawing.Size(63, 18);
			label902.Size = size;
			this.Label90.TabIndex = 31;
            this.Label90.Text = Resources.CaptionGoods3;
			this.Label109.AutoSize = true;
			Label label1091 = this.Label109;
			point = new Point(90, 160);
			label1091.Location = point;
			this.Label109.Name = "Label109";
			Label label1092 = this.Label109;
			size = new System.Drawing.Size(80, 18);
			label1092.Size = size;
			this.Label109.TabIndex = 25;
            this.Label109.Text = Resources.CaptionLastHPartner;
			this.Label91.AutoSize = true;
			Label label911 = this.Label91;
			point = new Point(273, 140);
			label911.Location = point;
			this.Label91.Name = "Label91";
			Label label912 = this.Label91;
			size = new System.Drawing.Size(66, 18);
			label912.Size = size;
			this.Label91.TabIndex = 29;
            this.Label91.Text = Resources.CaptionGoods2;
			this.Label107.AutoSize = true;
			Label label1071 = this.Label107;
			point = new Point(90, 140);
			label1071.Location = point;
			this.Label107.Name = "Label107";
			Label label1072 = this.Label107;
			size = new System.Drawing.Size(86, 18);
			label1072.Size = size;
			this.Label107.TabIndex = 23;
            this.Label107.Text = Resources.CaptionAnalFirstPartner;
			Label label220 = this.lblCntSoudatu;
			point = new Point(180, 100);
			label220.Location = point;
			this.lblCntSoudatu.Name = "lblCntSoudatu";
			Label label221 = this.lblCntSoudatu;
			size = new System.Drawing.Size(77, 18);
			label221.Size = size;
			this.lblCntSoudatu.TabIndex = 12;
			this.lblCntSoudatu.TextAlign = ContentAlignment.TopRight;
			this.Label26.AutoSize = true;
			Label label261 = this.Label26;
			point = new Point(273, 120);
			label261.Location = point;
			this.Label26.Name = "Label26";
			Label label262 = this.Label26;
			size = new System.Drawing.Size(66, 18);
			label262.Size = size;
			this.Label26.TabIndex = 27;
            this.Label26.Text = Resources.CaptionGoods1;
			this.Label105.AutoSize = true;
			Label label1051 = this.Label105;
			point = new Point(90, 100);
			label1051.Location = point;
			this.Label105.Name = "Label105";
			Label label1052 = this.Label105;
			size = new System.Drawing.Size(80, 18);
			label1052.Size = size;
			this.Label105.TabIndex = 11;
            this.Label105.Text = Resources.CaptionFightWins;
			Label label222 = this.lblTest3;
			point = new Point(363, 80);
			label222.Location = point;
			this.lblTest3.Name = "lblTest3";
			Label label223 = this.lblTest3;
			size = new System.Drawing.Size(77, 18);
			label223.Size = size;
			this.lblTest3.TabIndex = 20;
			this.lblTest3.TextAlign = ContentAlignment.TopRight;
			Label label224 = this.lblCntKenka;
			point = new Point(180, 80);
			label224.Location = point;
			this.lblCntKenka.Name = "lblCntKenka";
			Label label225 = this.lblCntKenka;
			size = new System.Drawing.Size(77, 18);
			label225.Size = size;
			this.lblCntKenka.TabIndex = 10;
			this.lblCntKenka.TextAlign = ContentAlignment.TopRight;
			this.Label102.AutoSize = true;
			Label label1021 = this.Label102;
			point = new Point(273, 80);
			label1021.Location = point;
			this.Label102.Name = "Label102";
			Label label1022 = this.Label102;
			size = new System.Drawing.Size(56, 18);
			label1022.Size = size;
			this.Label102.TabIndex = 19;
            this.Label102.Text = Resources.CaptionClubCompetition;
			this.Label103.AutoSize = true;
			Label label1031 = this.Label103;
			point = new Point(90, 80);
			label1031.Location = point;
			this.Label103.Name = "Label103";
			Label label1032 = this.Label103;
			size = new System.Drawing.Size(68, 18);
			label1032.Size = size;
			this.Label103.TabIndex = 9;
            this.Label103.Text = Resources.CaptionArgumentWins;
			Label label226 = this.lblTest2;
			point = new Point(345, 60);
			label226.Location = point;
			this.lblTest2.Name = "lblTest2";
			Label label227 = this.lblTest2;
			size = new System.Drawing.Size(95, 18);
			label227.Size = size;
			this.lblTest2.TabIndex = 18;
			this.lblTest2.TextAlign = ContentAlignment.TopRight;
			Label label228 = this.lblCntFurareta;
			point = new Point(180, 60);
			label228.Location = point;
			this.lblCntFurareta.Name = "lblCntFurareta";
			Label label229 = this.lblCntFurareta;
			size = new System.Drawing.Size(77, 18);
			label229.Size = size;
			this.lblCntFurareta.TabIndex = 8;
			this.lblCntFurareta.TextAlign = ContentAlignment.TopRight;
			this.Label98.AutoSize = true;
			Label label981 = this.Label98;
			point = new Point(273, 60);
			label981.Location = point;
			this.Label98.Name = "Label98";
			Label label982 = this.Label98;
			size = new System.Drawing.Size(68, 18);
			label982.Size = size;
			this.Label98.TabIndex = 17;
            this.Label98.Text = Resources.CaptionFitnessTest;
			this.Label99.AutoSize = true;
			Label label991 = this.Label99;
			point = new Point(90, 60);
			label991.Location = point;
			this.Label99.Name = "Label99";
			Label label992 = this.Label99;
			size = new System.Drawing.Size(80, 18);
			label992.Size = size;
			this.Label99.TabIndex = 7;
            this.Label99.Text = Resources.CaptionFellatioCount;
			Label label230 = this.lblTest1;
			point = new Point(345, 40);
			label230.Location = point;
			this.lblTest1.Name = "lblTest1";
			Label label231 = this.lblTest1;
			size = new System.Drawing.Size(95, 18);
			label231.Size = size;
			this.lblTest1.TabIndex = 16;
			this.lblTest1.TextAlign = ContentAlignment.TopRight;
			Label label232 = this.lblCntSeikouAite;
			point = new Point(180, 40);
			label232.Location = point;
			this.lblCntSeikouAite.Name = "lblCntSeikouAite";
			Label label233 = this.lblCntSeikouAite;
			size = new System.Drawing.Size(77, 18);
			label233.Size = size;
			this.lblCntSeikouAite.TabIndex = 6;
			this.lblCntSeikouAite.TextAlign = ContentAlignment.TopRight;
			this.Label94.AutoSize = true;
			Label label941 = this.Label94;
			point = new Point(273, 40);
			label941.Location = point;
			this.Label94.Name = "Label94";
			Label label942 = this.Label94;
			size = new System.Drawing.Size(68, 18);
			label942.Size = size;
			this.Label94.TabIndex = 15;
            this.Label94.Text = Resources.CaptionExams;
			this.Label95.AutoSize = true;
			Label label951 = this.Label95;
			point = new Point(90, 40);
			label951.Location = point;
			this.Label95.Name = "Label95";
			Label label952 = this.Label95;
			size = new System.Drawing.Size(56, 18);
			label952.Size = size;
			this.Label95.TabIndex = 5;
            this.Label95.Text = Resources.CaptionHPartner;
			Label label234 = this.lblCntSabori;
			point = new Point(363, 20);
			label234.Location = point;
			this.lblCntSabori.Name = "lblCntSabori";
			Label label235 = this.lblCntSabori;
			size = new System.Drawing.Size(77, 18);
			label235.Size = size;
			this.lblCntSabori.TabIndex = 14;
			this.lblCntSabori.TextAlign = ContentAlignment.TopRight;
			Label label236 = this.lblCntKousai;
			point = new Point(180, 20);
			label236.Location = point;
			this.lblCntKousai.Name = "lblCntKousai";
			Label label237 = this.lblCntKousai;
			size = new System.Drawing.Size(77, 18);
			label237.Size = size;
			this.lblCntKousai.TabIndex = 4;
			this.lblCntKousai.TextAlign = ContentAlignment.TopRight;
			this.Label87.AutoSize = true;
			Label label871 = this.Label87;
			point = new Point(273, 20);
			label871.Location = point;
			this.Label87.Name = "Label87";
			Label label872 = this.Label87;
			size = new System.Drawing.Size(68, 18);
			label872.Size = size;
			this.Label87.TabIndex = 13;
            this.Label87.Text = Resources.CaptionSkipClassCount;
			this.Label85.AutoSize = true;
			Label label851 = this.Label85;
			point = new Point(90, 120);
			label851.Location = point;
			this.Label85.Name = "Label85";
			Label label852 = this.Label85;
			size = new System.Drawing.Size(68, 18);
			label852.Size = size;
			this.Label85.TabIndex = 21;
            this.Label85.Text = Resources.CaptionHFirstPartner;
			this.Label84.AutoSize = true;
			Label label841 = this.Label84;
			point = new Point(90, 20);
			label841.Location = point;
			this.Label84.Name = "Label84";
			Label label842 = this.Label84;
			size = new System.Drawing.Size(56, 18);
			label842.Size = size;
			this.Label84.TabIndex = 3;
            this.Label84.Text = Resources.CaptionContactsCount;
			Label label238 = this.lblPersonal;
			point = new Point(0, 0);
			label238.Location = point;
			this.lblPersonal.Name = "lblPersonal";
			Label label239 = this.lblPersonal;
			size = new System.Drawing.Size(200, 18);
			label239.Size = size;
			this.lblPersonal.TabIndex = 0;
			this.pnlTools.Controls.Add(this.btnClearAction);
			this.pnlTools.Controls.Add(this.btnHMax);
			this.pnlTools.Controls.Add(this.btnAfterPill);
			this.pnlTools.Controls.Add(this.btnGohoubi);
			Panel panel18 = this.pnlTools;
			point = new Point(14, 437);
			panel18.Location = point;
			this.pnlTools.Name = "pnlTools";
			Panel panel19 = this.pnlTools;
			size = new System.Drawing.Size(432, 26);
			panel19.Size = size;
			this.pnlTools.TabIndex = 4;
			this.btnClearAction.BatchButton = this.btnBatch;
			this.btnClearAction.Caption = Resources.PanelCapInitLikeability;
			this.btnClearAction.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button62 = this.btnClearAction;
			point = new Point(1, 0);
			jG2Button62.Location = point;
			JG2Button jG2Button63 = this.btnClearAction;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button63.Margin = padding;
			this.btnClearAction.Name = "btnClearAction";
			JG2Button jG2Button64 = this.btnClearAction;
			size = new System.Drawing.Size(106, 26);
			jG2Button64.Size = size;
			this.btnClearAction.TabIndex = 0;
			this.btnClearAction.Text = Resources.PanelCapInitLikeability;
            this.btnClearAction.UseVisualStyleBackColor = true;
			this.btnHMax.BatchButton = this.btnBatch;
            this.btnHMax.Caption = Resources.CaptionCodeHAisyou;
            this.btnHMax.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button65 = this.btnHMax;
			point = new Point(109, 0);
			jG2Button65.Location = point;
			JG2Button jG2Button66 = this.btnHMax;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button66.Margin = padding;
			this.btnHMax.Name = "btnHMax";
			JG2Button jG2Button67 = this.btnHMax;
			size = new System.Drawing.Size(106, 26);
			jG2Button67.Size = size;
			this.btnHMax.TabIndex = 1;
			this.btnHMax.Text = "[" + Resources.CaptionCodeHAisyou + "]";
			this.btnHMax.UseVisualStyleBackColor = true;
			this.btnAfterPill.BatchButton = this.btnBatch;
            this.btnAfterPill.Caption = Resources.CaptionAfterPill;
			this.btnAfterPill.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button68 = this.btnAfterPill;
			point = new Point(325, 0);
			jG2Button68.Location = point;
			JG2Button jG2Button69 = this.btnAfterPill;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button69.Margin = padding;
			this.btnAfterPill.Name = "btnAfterPill";
			JG2Button jG2Button70 = this.btnAfterPill;
			size = new System.Drawing.Size(106, 26);
			jG2Button70.Size = size;
			this.btnAfterPill.TabIndex = 3;
            this.btnAfterPill.Text = Resources.CaptionAfterPill;
			this.btnAfterPill.UseVisualStyleBackColor = true;
			this.btnGohoubi.BatchButton = this.btnBatch;
            this.btnGohoubi.Caption = Resources.CaptionCodeGohoubi;
			this.btnGohoubi.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			JG2Button jG2Button71 = this.btnGohoubi;
			point = new Point(217, 0);
			jG2Button71.Location = point;
			JG2Button jG2Button72 = this.btnGohoubi;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			jG2Button72.Margin = padding;
			this.btnGohoubi.Name = "btnGohoubi";
			JG2Button jG2Button73 = this.btnGohoubi;
			size = new System.Drawing.Size(106, 26);
			jG2Button73.Size = size;
			this.btnGohoubi.TabIndex = 2;
            this.btnGohoubi.Text = "'" + Resources.CaptionCodeGohoubi + "'";
			this.btnGohoubi.UseVisualStyleBackColor = true;
			this.btnExport.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			Button button23 = this.btnExport;
			point = new Point(243, 38);
			button23.Location = point;
			Button button24 = this.btnExport;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			button24.Margin = padding;
			this.btnExport.Name = "btnExport";
			Button button25 = this.btnExport;
			size = new System.Drawing.Size(233, 26);
			button25.Size = size;
			this.btnExport.TabIndex = 2;
            this.btnExport.Text = Resources.BtnTxtExporttoPNG;
			this.btnExport.UseVisualStyleBackColor = true;
			this.btnImport.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			Button button26 = this.btnImport;
			point = new Point(8, 38);
			button26.Location = point;
			Button button27 = this.btnImport;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			button27.Margin = padding;
			this.btnImport.Name = "btnImport";
			Button button28 = this.btnImport;
			size = new System.Drawing.Size(233, 26);
			button28.Size = size;
			this.btnImport.TabIndex = 1;
            this.btnImport.Text = Resources.BtnTxtExporttoPNG;
			this.btnImport.UseVisualStyleBackColor = true;
			this.MenuStrip.BackColor = SystemColors.Window;
			ToolStripItemCollection items = this.MenuStrip.Items;
			ToolStripItem[] toolStripSeparator10 = new ToolStripItem[] { this.mnuFile, this.mnuEdit, this.mnuTab };
			items.AddRange(toolStripSeparator10);
			System.Windows.Forms.MenuStrip menuStrip = this.MenuStrip;
			point = new Point(0, 0);
			menuStrip.Location = point;
			this.MenuStrip.Name = "MenuStrip";
			System.Windows.Forms.MenuStrip menuStrip1 = this.MenuStrip;
			size = new System.Drawing.Size(484, 26);
			menuStrip1.Size = size;
			this.MenuStrip.TabIndex = 0;
			ToolStripItemCollection dropDownItems = this.mnuFile.DropDownItems;
			toolStripSeparator10 = new ToolStripItem[] { this.mnuSubmit, this.toolStripSeparator1, this.mnuClose };
			dropDownItems.AddRange(toolStripSeparator10);
			this.mnuFile.Name = "mnuFile";
			ToolStripMenuItem toolStripMenuItem = this.mnuFile;
			size = new System.Drawing.Size(85, 22);
			toolStripMenuItem.Size = size;
            this.mnuFile.Text = Resources.MenuTxtFile;
			this.mnuSubmit.Image = Resources.IconUpdate;
			this.mnuSubmit.ImageTransparentColor = Color.Magenta;
			this.mnuSubmit.Name = "mnuSubmit";
			this.mnuSubmit.ShortcutKeys = Keys.LButton | Keys.RButton | Keys.Cancel | Keys.ShiftKey | Keys.ControlKey | Keys.Menu | Keys.Pause | Keys.A | Keys.B | Keys.C | Keys.P | Keys.Q | Keys.R | Keys.S | Keys.Control;
			ToolStripMenuItem toolStripMenuItem1 = this.mnuSubmit;
			size = new System.Drawing.Size(213, 22);
			toolStripMenuItem1.Size = size;
            this.mnuSubmit.Text = Resources.MenuTxtUpdateSeatTable;
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			ToolStripSeparator toolStripSeparator = this.toolStripSeparator1;
			size = new System.Drawing.Size(210, 6);
			toolStripSeparator.Size = size;
			this.mnuClose.Name = "mnuClose";
			ToolStripMenuItem toolStripMenuItem2 = this.mnuClose;
			size = new System.Drawing.Size(213, 22);
			toolStripMenuItem2.Size = size;
            this.mnuClose.Text = Resources.MenuTxtClose;
			this.mnuEdit.CheckOnClick = true;
			ToolStripItemCollection toolStripItemCollections = this.mnuEdit.DropDownItems;
			toolStripSeparator10 = new ToolStripItem[] { this.mnuTabToolsImport, this.mnuTabToolsExport, this.ToolStripSeparator10, this.mnuOpen, this.ToolStripSeparator6, this.mnuTabToolsPlayer, this.mnuTabToolsSeparator, this.mnuTabToolsAction, this.mnuTabToolsActionRev, this.mnuTabToolsClearAction, this.mnuTabToolsHMax, this.mnuTabToolsGohoubi, this.mnuTabToolsAfterPill, this.ToolStripSeparator9, this.mnuSelectCondition, this.ToolStripSeparator3, this.mnuSelectAll, this.mnuClearAll, this.mnuBatchSeparator, this.mnuBatch };
			toolStripItemCollections.AddRange(toolStripSeparator10);
			this.mnuEdit.Name = "mnuEdit";
			JG2ToolStripMenuItem jG2ToolStripMenuItem = this.mnuEdit;
			size = new System.Drawing.Size(61, 22);
			jG2ToolStripMenuItem.Size = size;
            this.mnuEdit.Text = Resources.MenuTxtEdit;
			this.mnuTabToolsImport.Name = "mnuTabToolsImport";
			this.mnuTabToolsImport.ShortcutKeys = Keys.LButton | Keys.Back | Keys.Tab | Keys.A | Keys.H | Keys.I | Keys.Control;
			ToolStripMenuItem toolStripMenuItem3 = this.mnuTabToolsImport;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem3.Size = size;
            this.mnuTabToolsImport.Text = Resources.MenuTxtPNGImport;
			this.mnuTabToolsExport.Name = "mnuTabToolsExport";
			this.mnuTabToolsExport.ShortcutKeys = Keys.LButton | Keys.MButton | Keys.XButton1 | Keys.A | Keys.D | Keys.E | Keys.Control;
			ToolStripMenuItem toolStripMenuItem4 = this.mnuTabToolsExport;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem4.Size = size;
            this.mnuTabToolsExport.Text = Resources.BtnTxtExporttoPNG;
			this.ToolStripSeparator10.Name = "ToolStripSeparator10";
			ToolStripSeparator toolStripSeparator101 = this.ToolStripSeparator10;
			size = new System.Drawing.Size(300, 6);
			toolStripSeparator101.Size = size;
			this.mnuOpen.Image = Resources.IconOpen;
			this.mnuOpen.ImageTransparentColor = Color.Magenta;
			this.mnuOpen.Name = "mnuOpen";
			this.mnuOpen.ShortcutKeys = Keys.LButton | Keys.RButton | Keys.Cancel | Keys.MButton | Keys.XButton1 | Keys.XButton2 | Keys.Back | Keys.Tab | Keys.LineFeed | Keys.Clear | Keys.Return | Keys.Enter | Keys.A | Keys.B | Keys.C | Keys.D | Keys.E | Keys.F | Keys.G | Keys.H | Keys.I | Keys.J | Keys.K | Keys.L | Keys.M | Keys.N | Keys.O | Keys.Control;
			ToolStripMenuItem toolStripMenuItem5 = this.mnuOpen;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem5.Size = size;
            this.mnuOpen.Text = Resources.MenuTxtOpenClothingFile;
			this.ToolStripSeparator6.Name = "ToolStripSeparator6";
			ToolStripSeparator toolStripSeparator6 = this.ToolStripSeparator6;
			size = new System.Drawing.Size(300, 6);
			toolStripSeparator6.Size = size;
			this.mnuTabToolsPlayer.Name = "mnuTabToolsPlayer";
			this.mnuTabToolsPlayer.ShortcutKeys = Keys.ShiftKey | Keys.P | Keys.Control;
			ToolStripMenuItem toolStripMenuItem6 = this.mnuTabToolsPlayer;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem6.Size = size;
            this.mnuTabToolsPlayer.Text = Resources.MenuTxtSetPlayerChar;
			this.mnuTabToolsSeparator.Name = "mnuTabToolsSeparator";
			ToolStripSeparator toolStripSeparator1 = this.mnuTabToolsSeparator;
			size = new System.Drawing.Size(300, 6);
			toolStripSeparator1.Size = size;
			ToolStripItemCollection dropDownItems1 = this.mnuTabToolsAction.DropDownItems;
			toolStripSeparator10 = new ToolStripItem[] { this.mnuTabToolsAction1, this.mnuTabToolsAction2, this.mnuTabToolsAction3, this.mnuTabToolsAction4, this.ToolStripSeparator11, this.mnuTabToolsDelAction1, this.mnuTabToolsDelAction2, this.mnuTabToolsDelAction3, this.mnuTabToolsDelAction4 };
			dropDownItems1.AddRange(toolStripSeparator10);
			this.mnuTabToolsAction.Name = "mnuTabToolsAction";
			ToolStripMenuItem toolStripMenuItem7 = this.mnuTabToolsAction;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem7.Size = size;
			this.mnuTabToolsAction1.Name = "mnuTabToolsAction1";
			ToolStripMenuItem toolStripMenuItem8 = this.mnuTabToolsAction1;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem8.Size = size;
			this.mnuTabToolsAction2.Name = "mnuTabToolsAction2";
			ToolStripMenuItem toolStripMenuItem9 = this.mnuTabToolsAction2;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem9.Size = size;
			this.mnuTabToolsAction3.Name = "mnuTabToolsAction3";
			ToolStripMenuItem toolStripMenuItem10 = this.mnuTabToolsAction3;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem10.Size = size;
			this.mnuTabToolsAction4.Name = "mnuTabToolsAction4";
			ToolStripMenuItem toolStripMenuItem11 = this.mnuTabToolsAction4;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem11.Size = size;
			this.ToolStripSeparator11.Name = "ToolStripSeparator11";
			ToolStripSeparator toolStripSeparator11 = this.ToolStripSeparator11;
			size = new System.Drawing.Size(65, 6);
			toolStripSeparator11.Size = size;
			this.mnuTabToolsDelAction1.Name = "mnuTabToolsDelAction1";
			ToolStripMenuItem toolStripMenuItem12 = this.mnuTabToolsDelAction1;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem12.Size = size;
			this.mnuTabToolsDelAction2.Name = "mnuTabToolsDelAction2";
			ToolStripMenuItem toolStripMenuItem13 = this.mnuTabToolsDelAction2;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem13.Size = size;
			this.mnuTabToolsDelAction3.Name = "mnuTabToolsDelAction3";
			ToolStripMenuItem toolStripMenuItem14 = this.mnuTabToolsDelAction3;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem14.Size = size;
			this.mnuTabToolsDelAction4.Name = "mnuTabToolsDelAction4";
			ToolStripMenuItem toolStripMenuItem15 = this.mnuTabToolsDelAction4;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem15.Size = size;
			ToolStripItemCollection toolStripItemCollections1 = this.mnuTabToolsActionRev.DropDownItems;
			toolStripSeparator10 = new ToolStripItem[] { this.mnuTabToolsActionRev1, this.mnuTabToolsActionRev2, this.mnuTabToolsActionRev3, this.mnuTabToolsActionRev4, this.ToolStripSeparator4, this.mnuTabToolsDelActionRev1, this.mnuTabToolsDelActionRev2, this.mnuTabToolsDelActionRev3, this.mnuTabToolsDelActionRev4 };
			toolStripItemCollections1.AddRange(toolStripSeparator10);
			this.mnuTabToolsActionRev.Name = "mnuTabToolsActionRev";
			ToolStripMenuItem toolStripMenuItem16 = this.mnuTabToolsActionRev;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem16.Size = size;
			this.mnuTabToolsActionRev1.Name = "mnuTabToolsActionRev1";
			ToolStripMenuItem toolStripMenuItem17 = this.mnuTabToolsActionRev1;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem17.Size = size;
			this.mnuTabToolsActionRev2.Name = "mnuTabToolsActionRev2";
			ToolStripMenuItem toolStripMenuItem18 = this.mnuTabToolsActionRev2;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem18.Size = size;
			this.mnuTabToolsActionRev3.Name = "mnuTabToolsActionRev3";
			ToolStripMenuItem toolStripMenuItem19 = this.mnuTabToolsActionRev3;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem19.Size = size;
			this.mnuTabToolsActionRev4.Name = "mnuTabToolsActionRev4";
			ToolStripMenuItem toolStripMenuItem20 = this.mnuTabToolsActionRev4;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem20.Size = size;
			this.ToolStripSeparator4.Name = "ToolStripSeparator4";
			ToolStripSeparator toolStripSeparator4 = this.ToolStripSeparator4;
			size = new System.Drawing.Size(65, 6);
			toolStripSeparator4.Size = size;
			this.mnuTabToolsDelActionRev1.Name = "mnuTabToolsDelActionRev1";
			ToolStripMenuItem toolStripMenuItem21 = this.mnuTabToolsDelActionRev1;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem21.Size = size;
			this.mnuTabToolsDelActionRev2.Name = "mnuTabToolsDelActionRev2";
			ToolStripMenuItem toolStripMenuItem22 = this.mnuTabToolsDelActionRev2;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem22.Size = size;
			this.mnuTabToolsDelActionRev3.Name = "mnuTabToolsDelActionRev3";
			ToolStripMenuItem toolStripMenuItem23 = this.mnuTabToolsDelActionRev3;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem23.Size = size;
			this.mnuTabToolsDelActionRev4.Name = "mnuTabToolsDelActionRev4";
			ToolStripMenuItem toolStripMenuItem24 = this.mnuTabToolsDelActionRev4;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem24.Size = size;
			this.mnuTabToolsClearAction.Name = "mnuTabToolsClearAction";
			ToolStripMenuItem toolStripMenuItem25 = this.mnuTabToolsClearAction;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem25.Size = size;
            this.mnuTabToolsClearAction.Text = Resources.MenuTxtInitLiklihood;
			this.mnuTabToolsHMax.Name = "mnuTabToolsHMax";
			ToolStripMenuItem toolStripMenuItem26 = this.mnuTabToolsHMax;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem26.Size = size;
            this.mnuTabToolsHMax.Text = Resources.MenuTxtPerfectHCompat;
			this.mnuTabToolsGohoubi.Name = "mnuTabToolsGohoubi";
			ToolStripMenuItem toolStripMenuItem27 = this.mnuTabToolsGohoubi;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem27.Size = size;
            this.mnuTabToolsGohoubi.Text = Resources.MenuTxtMakeHPromise;
			this.mnuTabToolsAfterPill.Name = "mnuTabToolsAfterPill";
			ToolStripMenuItem toolStripMenuItem28 = this.mnuTabToolsAfterPill;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem28.Size = size;
            this.mnuTabToolsAfterPill.Text = Resources.MenuTxtAfterPillThings;
			this.ToolStripSeparator9.Name = "ToolStripSeparator9";
			ToolStripSeparator toolStripSeparator9 = this.ToolStripSeparator9;
			size = new System.Drawing.Size(300, 6);
			toolStripSeparator9.Size = size;
			this.mnuSelectCondition.Image = Resources.IconFilter;
			this.mnuSelectCondition.Name = "mnuSelectCondition";
			ToolStripMenuItem toolStripMenuItem29 = this.mnuSelectCondition;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem29.Size = size;
            this.mnuSelectCondition.Text = Resources.MenuTxtFilter;
			this.ToolStripSeparator3.Name = "ToolStripSeparator3";
			ToolStripSeparator toolStripSeparator3 = this.ToolStripSeparator3;
			size = new System.Drawing.Size(300, 6);
			toolStripSeparator3.Size = size;
			this.mnuSelectAll.Name = "mnuSelectAll";
			this.mnuSelectAll.ShortcutKeys = Keys.LButton | Keys.A | Keys.Control;
			ToolStripMenuItem toolStripMenuItem30 = this.mnuSelectAll;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem30.Size = size;
            this.mnuSelectAll.Text = Resources.MenuTxtSelectTarget;
			this.mnuClearAll.Name = "mnuClearAll";
			this.mnuClearAll.ShortcutKeys = Keys.RButton | Keys.ShiftKey | Keys.Menu | Keys.B | Keys.P | Keys.R | Keys.Control;
			ToolStripMenuItem toolStripMenuItem31 = this.mnuClearAll;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem31.Size = size;
            this.mnuClearAll.Text = Resources.MenuTxtUnselect;
			this.mnuBatchSeparator.Name = "mnuBatchSeparator";
			ToolStripSeparator toolStripSeparator2 = this.mnuBatchSeparator;
			size = new System.Drawing.Size(300, 6);
			toolStripSeparator2.Size = size;
			this.mnuBatch.Name = "mnuBatch";
			ToolStripMenuItem toolStripMenuItem32 = this.mnuBatch;
			size = new System.Drawing.Size(303, 22);
			toolStripMenuItem32.Size = size;
			ToolStripItemCollection dropDownItems2 = this.mnuTab.DropDownItems;
			toolStripSeparator10 = new ToolStripItem[] { this.mnuTabBody, this.mnuTabFace, this.mnuTabHair, this.mnuTabJinkaku, this.mnuTabKosei, this.mnuTabNinshin, this.mnuTabCostume, this.mnuTabTools };
			dropDownItems2.AddRange(toolStripSeparator10);
			this.mnuTab.Name = "mnuTab";
			this.mnuTab.ShortcutKeys = Keys.F5;
			ToolStripMenuItem toolStripMenuItem33 = this.mnuTab;
			size = new System.Drawing.Size(62, 22);
			toolStripMenuItem33.Size = size;
            this.mnuTab.Text = Resources.MenuTxtTab;
			this.mnuTabBody.Name = "mnuTabBody";
			this.mnuTabBody.ShortcutKeys = Keys.ShiftKey | Keys.Space | Keys.D0 | Keys.P | Keys.NumPad0 | Keys.F1 | Keys.Control;
			ToolStripMenuItem toolStripMenuItem34 = this.mnuTabBody;
			size = new System.Drawing.Size(177, 22);
			toolStripMenuItem34.Size = size;
            this.mnuTabBody.Text = Resources.TabBody;
			this.mnuTabFace.Name = "mnuTabFace";
			this.mnuTabFace.ShortcutKeys = Keys.LButton | Keys.ShiftKey | Keys.ControlKey | Keys.Space | Keys.Prior | Keys.PageUp | Keys.D0 | Keys.D1 | Keys.A | Keys.P | Keys.Q | Keys.NumPad0 | Keys.NumPad1 | Keys.F1 | Keys.F2 | Keys.Control;
			ToolStripMenuItem toolStripMenuItem35 = this.mnuTabFace;
			size = new System.Drawing.Size(177, 22);
			toolStripMenuItem35.Size = size;
            this.mnuTabFace.Text = Resources.TabFace;
			this.mnuTabHair.Name = "mnuTabHair";
			this.mnuTabHair.ShortcutKeys = Keys.RButton | Keys.ShiftKey | Keys.Menu | Keys.Space | Keys.Next | Keys.PageDown | Keys.D0 | Keys.D2 | Keys.B | Keys.P | Keys.R | Keys.NumPad0 | Keys.NumPad2 | Keys.F1 | Keys.F3 | Keys.Control;
			ToolStripMenuItem toolStripMenuItem36 = this.mnuTabHair;
			size = new System.Drawing.Size(177, 22);
			toolStripMenuItem36.Size = size;
            this.mnuTabHair.Text = Resources.TabHair;
			this.mnuTabJinkaku.Name = "mnuTabJinkaku";
			this.mnuTabJinkaku.ShortcutKeys = Keys.LButton | Keys.RButton | Keys.Cancel | Keys.ShiftKey | Keys.ControlKey | Keys.Menu | Keys.Pause | Keys.Space | Keys.Prior | Keys.PageUp | Keys.Next | Keys.PageDown | Keys.End | Keys.D0 | Keys.D1 | Keys.D2 | Keys.D3 | Keys.A | Keys.B | Keys.C | Keys.P | Keys.Q | Keys.R | Keys.S | Keys.NumPad0 | Keys.NumPad1 | Keys.NumPad2 | Keys.NumPad3 | Keys.F1 | Keys.F2 | Keys.F3 | Keys.F4 | Keys.Control;
			ToolStripMenuItem toolStripMenuItem37 = this.mnuTabJinkaku;
			size = new System.Drawing.Size(177, 22);
			toolStripMenuItem37.Size = size;
            this.mnuTabJinkaku.Text = Resources.TabPersonality;
			this.mnuTabKosei.Name = "mnuTabKosei";
			this.mnuTabKosei.ShortcutKeys = Keys.MButton | Keys.ShiftKey | Keys.Capital | Keys.CapsLock | Keys.Space | Keys.Home | Keys.D0 | Keys.D4 | Keys.D | Keys.P | Keys.T | Keys.NumPad0 | Keys.NumPad4 | Keys.F1 | Keys.F5 | Keys.Control;
			ToolStripMenuItem toolStripMenuItem38 = this.mnuTabKosei;
			size = new System.Drawing.Size(177, 22);
			toolStripMenuItem38.Size = size;
            this.mnuTabKosei.Text = Resources.CaptionPersonality;
			this.mnuTabNinshin.Name = "mnuTabNinshin";
			this.mnuTabNinshin.ShortcutKeys = Keys.LButton | Keys.MButton | Keys.XButton1 | Keys.ShiftKey | Keys.ControlKey | Keys.Capital | Keys.CapsLock | Keys.KanaMode | Keys.HanguelMode | Keys.HangulMode | Keys.Space | Keys.Prior | Keys.PageUp | Keys.Home | Keys.Left | Keys.D0 | Keys.D1 | Keys.D4 | Keys.D5 | Keys.A | Keys.D | Keys.E | Keys.P | Keys.Q | Keys.T | Keys.U | Keys.NumPad0 | Keys.NumPad1 | Keys.NumPad4 | Keys.NumPad5 | Keys.F1 | Keys.F2 | Keys.F5 | Keys.F6 | Keys.Control;
			ToolStripMenuItem toolStripMenuItem39 = this.mnuTabNinshin;
			size = new System.Drawing.Size(177, 22);
			toolStripMenuItem39.Size = size;
            this.mnuTabNinshin.Text = Resources.CaptionCodeNinshin;
			this.mnuTabCostume.Name = "mnuTabCostume";
			this.mnuTabCostume.ShortcutKeys = Keys.RButton | Keys.MButton | Keys.XButton2 | Keys.ShiftKey | Keys.Menu | Keys.Capital | Keys.CapsLock | Keys.Space | Keys.Next | Keys.PageDown | Keys.Home | Keys.Up | Keys.D0 | Keys.D2 | Keys.D4 | Keys.D6 | Keys.B | Keys.D | Keys.F | Keys.P | Keys.R | Keys.T | Keys.V | Keys.NumPad0 | Keys.NumPad2 | Keys.NumPad4 | Keys.NumPad6 | Keys.F1 | Keys.F3 | Keys.F5 | Keys.F7 | Keys.Control;
			ToolStripMenuItem toolStripMenuItem40 = this.mnuTabCostume;
			size = new System.Drawing.Size(177, 22);
			toolStripMenuItem40.Size = size;
            this.mnuTabCostume.Text = Resources.TabOutfit;
			this.mnuTabTools.Name = "mnuTabTools";
			this.mnuTabTools.ShortcutKeys = Keys.MButton | Keys.ShiftKey | Keys.Capital | Keys.CapsLock | Keys.D | Keys.P | Keys.T | Keys.Control;
			ToolStripMenuItem toolStripMenuItem41 = this.mnuTabTools;
			size = new System.Drawing.Size(177, 22);
			toolStripMenuItem41.Size = size;
            this.mnuTabTools.Text = Resources.CaptiontbpTools;
			this.ToolTip.AutoPopDelay = 10000;
			this.ToolTip.InitialDelay = 500;
			this.ToolTip.ReshowDelay = 100;
			this.ToolTip.ShowAlways = true;
			this.AutoScaleDimensions = new SizeF(7f, 18f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = SystemColors.Control;
			size = new System.Drawing.Size(484, 626);
			this.ClientSize = size;
			this.Controls.Add(this.btnExport);
			this.Controls.Add(this.MenuStrip);
			this.Controls.Add(this.pnlFooter);
			this.Controls.Add(this.btnImport);
			this.Controls.Add(this.tabBase);
            this.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Margin = padding;
			this.MaximizeBox = false;
			size = new System.Drawing.Size(490, 654);
			this.MaximumSize = size;
			this.MinimizeBox = false;
			size = new System.Drawing.Size(490, 654);
			this.MinimumSize = size;
			this.Name = "frmDetail";
			this.StartPosition = FormStartPosition.Manual;
			this.pnlFooter.ResumeLayout(false);
			this.tabBase.ResumeLayout(false);
			this.tbpBody.ResumeLayout(false);
			((ISupportInitialize)this.numTikubi3).EndInit();
			((ISupportInitialize)this.numHiyake2).EndInit();
			((ISupportInitialize)this.numUnder2).EndInit();
			((ISupportInitialize)this.numNyurin).EndInit();
            ((ISupportInitialize)this.cmbKao).EndInit();
            ((ISupportInitialize)this.cmbTikubi1).EndInit();
            ((ISupportInitialize)this.cmbTikubi2).EndInit();
            ((ISupportInitialize)this.cmbHiyake1).EndInit();
            ((ISupportInitialize)this.cmbMosaic).EndInit();
            ((ISupportInitialize)this.cmbUnder1).EndInit();
            ((ISupportInitialize)this.cmbGlasses2).EndInit();
            ((ISupportInitialize)this.numMune7).EndInit();
			((ISupportInitialize)this.numMune6).EndInit();
			((ISupportInitialize)this.numMune5).EndInit();
			((ISupportInitialize)this.numMune4).EndInit();
			((ISupportInitialize)this.numMune3).EndInit();
			((ISupportInitialize)this.numMune1).EndInit();
			((ISupportInitialize)this.numWaist).EndInit();
			((ISupportInitialize)this.numAtama2).EndInit();
			((ISupportInitialize)this.numAtama1).EndInit();
			this.tbpFace.ResumeLayout(false);
			this.pnlHokuro.ResumeLayout(false);
			((ISupportInitialize)this.numMayu2).EndInit();
			((ISupportInitialize)this.numPupil2).EndInit();
			((ISupportInitialize)this.numLip2).EndInit();
			((ISupportInitialize)this.numPupil4).EndInit();
			((ISupportInitialize)this.numPupil3).EndInit();
			((ISupportInitialize)this.numEye1).EndInit();
			((ISupportInitialize)this.numEye5).EndInit();
			((ISupportInitialize)this.numEye4).EndInit();
			((ISupportInitialize)this.numEye3).EndInit();
			((ISupportInitialize)this.numEye2).EndInit();
			this.tbpHair.ResumeLayout(false);
			((ISupportInitialize)this.numHair4).EndInit();
			((ISupportInitialize)this.numHair3).EndInit();
			((ISupportInitialize)this.numHair2).EndInit();
			((ISupportInitialize)this.numHair1).EndInit();
			((ISupportInitialize)this.numHairAdj4).EndInit();
			((ISupportInitialize)this.numHairAdj3).EndInit();
			((ISupportInitialize)this.numHairAdj2).EndInit();
			((ISupportInitialize)this.numHairAdj1).EndInit();
			this.tbpJinkaku.ResumeLayout(false);
			((ISupportInitialize)this.numSeikaku).EndInit();
			((ISupportInitialize)this.numBukatu3).EndInit();
			((ISupportInitialize)this.numTairyoku3).EndInit();
			((ISupportInitialize)this.numTiryoku3).EndInit();
			this.tbpKosei.ResumeLayout(false);
			this.pnlH.ResumeLayout(false);
			this.pnlKosei.ResumeLayout(false);
			this.tbpNinshin.ResumeLayout(false);
			this.tbpCostume.ResumeLayout(false);
			this.pnlYouto.ResumeLayout(false);
			this.tbpTools.ResumeLayout(false);
			this.pnlAction.ResumeLayout(false);
			this.pnlActionRev.ResumeLayout(false);
			this.pnlPair.ResumeLayout(false);
			this.pnlPair.PerformLayout();
			this.pnlProfile.ResumeLayout(false);
			this.pnlProfile.PerformLayout();
			this.pnlTools.ResumeLayout(false);
			this.MenuStrip.ResumeLayout(false);
			this.MenuStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void Item_Changed(object sender, EventArgs e)
		{
			Button button = this.btnSubmit;
			object obj = this.tabBase;
			bool flag = this.CheckModified(ref obj);
			this.tabBase = (TabControl)obj;
			button.Enabled = flag;
		}

		private void LockToolControl()
		{
			this.mnuTabToolsPlayer.Enabled = this.SeatNo != JG2Importer.Common.MySaveData.Footer.SeatNoPlayer;
			this.btnPlayer.Enabled = this.mnuTabToolsPlayer.Enabled;
			if (this.tabBase.SelectedTab != this.tbpTools)
			{
				this.mnuTabToolsAction.Enabled = false;
				this.mnuTabToolsActionRev.Enabled = false;
				this.mnuTabToolsClearAction.Enabled = false;
				this.mnuTabToolsHMax.Enabled = false;
				this.mnuTabToolsGohoubi.Enabled = false;
				this.mnuTabToolsAfterPill.Enabled = false;
			}
			else if (this.SeatNo != JG2Importer.Common.MySaveData.Footer.SeatNoPlayer)
			{
				this.mnuTabToolsAction.Enabled = true;
				this.mnuTabToolsActionRev.Enabled = true;
				this.mnuTabToolsClearAction.Enabled = true;
				this.mnuTabToolsHMax.Enabled = true;
				this.mnuTabToolsGohoubi.Enabled = true;
				this.mnuTabToolsAfterPill.Enabled = true;
			}
			else
			{
				this.mnuTabToolsAction.Enabled = false;
				this.mnuTabToolsActionRev.Enabled = false;
				this.mnuTabToolsClearAction.Enabled = false;
				this.mnuTabToolsHMax.Enabled = false;
				this.mnuTabToolsGohoubi.Enabled = false;
				this.mnuTabToolsAfterPill.Enabled = false;
			}
			if (this.tabBase.SelectedTab != this.tbpTools)
			{
				this.mnuBatch.Enabled = true;
				this.btnBatch.Enabled = true;
			}
			else
			{
				this.mnuBatch.Enabled = this.SeatNo != JG2Importer.Common.MySaveData.Footer.SeatNoPlayer;
				this.btnBatch.Enabled = this.mnuBatch.Enabled;
			}
			if (this.tabBase.SelectedTab == this.tbpTools && this.PrevSeatNo == JG2Importer.Common.MySaveData.Footer.SeatNoPlayer && this.SeatNo != JG2Importer.Common.MySaveData.Footer.SeatNoPlayer && this.ActiveControl.Parent == this.pnlPair)
			{
				this.SelectNextControl(this.pnlPair, true, true, true, true);
			}
		}

		private void mnuBatch_Click(object sender, EventArgs e)
		{
			int selectedIndex;
			bool flag = true;
			if (flag == this.btnBatch.Tag is JG2ComboBox)
			{
				selectedIndex = ((JG2ComboBox)this.btnBatch.Tag).SelectedIndex;
			}
			else if (flag == this.btnBatch.Tag is JG2CheckBox)
			{
				selectedIndex = (((JG2CheckBox)this.btnBatch.Tag).Checked ? 1 : 0);
			}
			else if (flag != this.btnBatch.Tag is JG2Button)
			{
				selectedIndex = (flag != this.btnBatch.Tag is JG2NumericUpDown ? 0 : Convert.ToInt32(((JG2NumericUpDown)this.btnBatch.Tag).Value));
			}
			else
			{
				selectedIndex = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.btnBatch.Tag, null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
			}
			int integer = Conversions.ToInteger(this.pnlYouto.Tag);
			int index = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			if (index != -1)
			{
				int length = checked(checked((int)JG2Importer.Common.MySaveData.Seat.Length) - 1);
				for (int i = 0; i <= length; i = checked(i + 1))
				{
					if (((usrSeat)MyProject.Forms.frmClass.pnlClass.Controls[string.Concat("usrSeat", Strings.Format(JG2Importer.Common.MySaveData.Seat[i].SeatNo, "00"))]).Checked)
					{
						object obj = NewLateBinding.LateGet(this.btnBatch.Tag, null, "Name", new object[0], null, null, null);
						if (Operators.ConditionalCompareObjectEqual(obj, "cmbHeight", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Height = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbStyle", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Style = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numAtama1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Atama1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numAtama2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Atama2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numWaist", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Waist = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numMune1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Mune1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbMune2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Mune2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numMune3", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Mune3 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numMune4", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Mune4 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numMune5", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Mune5 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numMune6", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Mune6 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numMune7", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Mune7 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbMune8", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Mune8 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numNyurin", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Nyurin = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorSkin2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.ColorSkin2 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbTikubi1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Tikubi1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbTikubi2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Tikubi2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numTikubi3", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Tikubi3 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbHiyake1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Hiyake1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numHiyake2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Hiyake2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbMosaic", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Mosaic = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbUnder1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Under1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numUnder2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Under2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorUnder", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.ColorUnder = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbKao", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Kao = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numEye1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Eye1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numEye2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Eye2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numEye3", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Eye3 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numEye4", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Eye4 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numEye5", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Eye5 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbPupil1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Pupil1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numPupil2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Pupil2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numPupil3", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Pupil3 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numPupil4", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Pupil4 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbPupil5", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Pupil5 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbHighlight1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Highlight1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorEye1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.ColorEye1 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorEye2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.ColorEye2 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbMayu1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Mayu1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numMayu2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Mayu2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorMayu", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.ColorMayu = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbMabuta", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Mabuta = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbMatuge1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Matuge1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbMatuge2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Matuge2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbGlasses2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Glasses2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorGlasses", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.ColorGlasses = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkHokuro1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Hokuro1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkHokuro2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Hokuro2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkHokuro3", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Hokuro3 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkHokuro4", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Hokuro4 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbLip1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Lip1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numLip2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Lip2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numHair1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Hair1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numHairAdj1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.HairAdj1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkHairRev1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.HairRev1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numHair2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Hair2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numHairAdj2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.HairAdj2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkHairRev2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.HairRev2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numHair3", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Hair3 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numHairAdj3", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.HairAdj3 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkHairRev3", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.HairRev3 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numHair4", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Hair4 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numHairAdj4", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.HairAdj4 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkHairRev4", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.HairRev4 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorHair", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.ColorHair = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numTiryoku3", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Tiryoku3 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numTairyoku3", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Tairyoku3 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbBukatu1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Bukatu1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numBukatu3", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Bukatu3 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbSyakousei", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Syakousei = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbKenka", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Kenka = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbTeisou", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Teisou = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbSeiai", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Seiai = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkSeikou1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Seikou1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkSeikou2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Seikou2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbVoice", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Voice = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numSeikaku", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Seikaku = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkCyoroi", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Cyoroi = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkNekketu", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Nekketu = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkNigate1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Nigate1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkNigate2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Nigate2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkCharm", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Charm = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkTundere", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Tundere = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkKyouki", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Kyouki = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkMiha", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Miha = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkSunao", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Sunao = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkMaemuki", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Maemuki = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkTereya", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Tereya = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkYakimochi", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Yakimochi = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkToufu", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Toufu = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkSukebe", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Sukebe = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkMajime", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Majime = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkCool", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Cool = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkTyokujyo", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Tyokujyo = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkPoyayan", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Poyayan = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkBouryoku", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Bouryoku = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkSousyoku", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Sousyoku = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkSewayaki", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Sewayaki = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkIintyou", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Iintyou = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkOsyaberi", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Osyaberi = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkHarapeko", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Harapeko = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkRenai", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Renai = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkItizu", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Itizu = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkYuujyufudan", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Yuujyufudan = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkMakenki", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Makenki = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkHaraguro", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Haraguro = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkKinben", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Kinben = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkHonpou", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Honpou = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkM", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.M = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkAsekaki", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Asekaki = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkYami", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Yami = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkNantyo", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Nantyo = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkYowami", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Yowami = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkMiseityo", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Miseityo = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkLuck", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Luck = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkRare", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Rare = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkKiss", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Kiss = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkBust", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Bust = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkSeiki1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Seiki1 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkSeiki2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Seiki2 = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkCunnilingus", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Cunnilingus = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkFellatio", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Fellatio = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkBack", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Back = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkOnna", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Onna = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkAnal", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Anal = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkNama", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Nama = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkSeiin", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Seiin = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkNaka", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Naka = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkKake", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Kake = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbSunday1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[0] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbMonday1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[1] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbTuesday1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[2] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbWednesday1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[3] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbThursday1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[4] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbFriday1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[5] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbSaturday1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[6] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbSunday2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[7] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbMonday2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[8] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbTuesday2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[9] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbWednesday2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[10] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbThursday2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[11] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbFriday2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[12] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "cmbSaturday2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Ninshin[13] = checked((byte)selectedIndex);
						}
						else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "radYouto0", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "radYouto1", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "radYouto2", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "radYouto3", false)) ? true : false)))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Type = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numType", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Socks = Conversions.ToByte(NewLateBinding.LateGet(this.GetCostumeControl("numSocks", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorSocks[3] = Conversions.ToByte(NewLateBinding.LateGet(this.GetCostumeControl("numPansuto", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Skirt = Conversions.ToByte(NewLateBinding.LateGet(this.GetCostumeControl("numSkirt", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorTop1 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorTop1", integer), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorTop2 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorTop2", integer), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorTop3 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorTop3", integer), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorTop4 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorTop4", integer), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorBottom1 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorBottom1", integer), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorBottom2 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorBottom2", integer), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorSitagi = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSitagi", integer), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorSocks[0] = Conversions.ToByte(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSocks", integer), null, "BackColor", new object[0], null, null, null), null, "B", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorSocks[1] = Conversions.ToByte(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSocks", integer), null, "BackColor", new object[0], null, null, null), null, "G", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorSocks[2] = Conversions.ToByte(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSocks", integer), null, "BackColor", new object[0], null, null, null), null, "R", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorUtibaki = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorUtibaki", integer), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorSotobaki = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSotobaki", integer), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Bottom1Sitaji1 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numBottom1Sitaji1", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Bottom1Sitaji2 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numBottom1Sitaji2", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Bottom1Kage1 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numBottom1Kage1", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Bottom1Kage2 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numBottom1Kage2", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Bottom1 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numBottom1", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].SitagiSitaji1 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numSitagiSitaji1", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].SitagiSitaji2 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numSitagiSitaji2", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].SitagiKage1 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numSitagiKage1", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].SitagiKage2 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numSitagiKage2", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Sitagi = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numSitagi", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Utibaki = Conversions.ToByte(NewLateBinding.LateGet(this.GetCostumeControl("numUtibaki", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Sotobaki = Conversions.ToByte(NewLateBinding.LateGet(this.GetCostumeControl("numSotobaki", integer), null, "Value", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].FlagJyoge = (byte)((Conversions.ToBoolean(NewLateBinding.LateGet(this.GetCostumeControl("chkFlagJyoge", integer), null, "Checked", new object[0], null, null, null)) ? 1 : 0));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].FlagSitagi = (byte)((Conversions.ToBoolean(NewLateBinding.LateGet(this.GetCostumeControl("chkFlagSitagi", integer), null, "Checked", new object[0], null, null, null)) ? 1 : 0));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].FlagSkirt = (byte)((Conversions.ToBoolean(NewLateBinding.LateGet(this.GetCostumeControl("chkFlagSkirt", integer), null, "Checked", new object[0], null, null, null)) ? 1 : 0));
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numType", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Type = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numSocks", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Socks = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numPansuto", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorSocks[3] = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numSkirt", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Skirt = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorTop1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorTop1 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorTop2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorTop2 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorTop3", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorTop3 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorTop4", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorTop4 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorBottom1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorBottom1 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorBottom2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorBottom2 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorSitagi", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorSitagi = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorSocks", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorSocks[0] = Conversions.ToByte(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSocks", integer), null, "BackColor", new object[0], null, null, null), null, "B", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorSocks[1] = Conversions.ToByte(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSocks", integer), null, "BackColor", new object[0], null, null, null), null, "G", new object[0], null, null, null));
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorSocks[2] = Conversions.ToByte(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSocks", integer), null, "BackColor", new object[0], null, null, null), null, "R", new object[0], null, null, null));
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorUtibaki", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorUtibaki = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnColorSotobaki", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].ColorSotobaki = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numBottom1Sitaji1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Bottom1Sitaji1 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numBottom1Sitaji2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Bottom1Sitaji2 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numBottom1Kage1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Bottom1Kage1 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numBottom1Kage2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Bottom1Kage2 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numBottom1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Bottom1 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numSitagiSitaji1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].SitagiSitaji1 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numSitagiSitaji2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].SitagiSitaji2 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numSitagiKage1", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].SitagiKage1 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numSitagiKage2", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].SitagiKage2 = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numSitagi", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Sitagi = selectedIndex;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numUtibaki", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Utibaki = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "numSotobaki", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].Sotobaki = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkFlagJyoge", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].FlagJyoge = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkFlagSitagi", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].FlagSitagi = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkFlagSkirt", false))
						{
							JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Costume[integer].FlagSkirt = checked((byte)selectedIndex);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnAction1", false))
						{
							this.SetAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Love, Conversions.ToInteger(this.btnAction1.Text), false);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnAction2", false))
						{
							this.SetAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Like, Conversions.ToInteger(this.btnAction2.Text), false);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnAction3", false))
						{
							this.SetAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Dislike, Conversions.ToInteger(this.btnAction3.Text), false);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnAction4", false))
						{
							this.SetAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Hate, Conversions.ToInteger(this.btnAction4.Text), false);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnDelAction1", false))
						{
							this.DelAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Love, false);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnDelAction2", false))
						{
							this.DelAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Like, false);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnDelAction3", false))
						{
							this.DelAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Dislike, false);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnDelAction4", false))
						{
							this.DelAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Hate, false);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "chkKousai", false))
						{
							this.ReCalcKousai((this.chkKousai.Checked ? Flags.ON : Flags.OFF), i, index);
							JG2Importer.Common.MySaveData.Seat[i].PlayData.KousaiData[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer].Status = (byte)((this.chkKousai.Checked ? 1 : 0));
							JG2Importer.Common.MySaveData.Seat[index].PlayData.KousaiData[JG2Importer.Common.MySaveData.Seat[i].SeatNo].Status = (byte)((this.chkKousai.Checked ? 1 : 0));
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnActionRev1", false))
						{
							this.SetAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Love, Conversions.ToInteger(this.btnActionRev1.Text), true);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnActionRev2", false))
						{
							this.SetAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Like, Conversions.ToInteger(this.btnActionRev2.Text), true);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnActionRev3", false))
						{
							this.SetAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Dislike, Conversions.ToInteger(this.btnActionRev3.Text), true);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnActionRev4", false))
						{
							this.SetAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Hate, Conversions.ToInteger(this.btnActionRev4.Text), true);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnDelActionRev1", false))
						{
							this.DelAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Love, true);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnDelActionRev2", false))
						{
							this.DelAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Like, true);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnDelActionRev3", false))
						{
							this.DelAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Dislike, true);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnDelActionRev4", false))
						{
							this.DelAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo, JG2Importer.JG2.Action.Hate, true);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnClearAction", false))
						{
							this.ClearAction(JG2Importer.Common.MySaveData.Seat[i].SeatNo);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnHMax", false))
						{
							this.SetHMax(JG2Importer.Common.MySaveData.Seat[i].SeatNo);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnGohoubi", false))
						{
							this.SetGohoubi(JG2Importer.Common.MySaveData.Seat[i].SeatNo);
						}
						else if (Operators.ConditionalCompareObjectEqual(obj, "btnAfterPill", false))
						{
							this.SetAfterPill(JG2Importer.Common.MySaveData.Seat[i].SeatNo);
						}
						JG2Importer.Common.MySaveData.Seat[i].ReCalc();
					}
				}
				MyProject.Forms.frmClass.DrawClass();
				this.DrawDetail(this.SeatNo);
				NewLateBinding.LateCall(this.btnBatch.Tag, null, "Select", new object[0], null, null, null, true);
			}
		}

		private void mnuClearAll_Click(object sender, EventArgs e)
		{
			MyProject.Forms.frmClass.ClearSeatCheck();
		}

		private void mnuClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void mnuOpen_Click(object sender, EventArgs e)
		{
			int index = JG2Importer.Common.MySaveData.GetIndex(this.SeatNo);
			if (index != -1)
			{
				string str = JG2Importer.Common.BrowseOpenFile(this, "CLOTH", Resources.MenuTxtClothFile + " (*.cloth)|*.cloth", Resources.MenuTxtClothFileDesignation, Path.Combine(JG2Importer.Common.INSTALLDIR_PLAY, "data\\save\\cloth\\"));
				if (Operators.CompareString(str, null, false) == 0)
				{
					return;
				}
				Cloth cloth = new Cloth(str);
				if (cloth.Attr.Sex == JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sex)
				{
					object costumeControl = this.GetCostumeControl("numType", -1);
					object[] type = new object[1];
					Cloth cloth1 = cloth;
					type[0] = cloth1.Attr.Type;
					object[] socks = type;
					bool[] flagArray = new bool[] { true };
					NewLateBinding.LateCall(costumeControl, null, "SetValue", socks, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.Type = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(socks[0]), typeof(int));
					}
					object obj = this.GetCostumeControl("numSocks", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.Socks;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(obj, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.Socks = (byte)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(byte));
					}
					object costumeControl1 = this.GetCostumeControl("numPansuto", -1);
					socks = new object[1];
					Color color = Color.FromArgb(cloth.Attr.ColorSocks);
					socks[0] = color.A;
					NewLateBinding.LateCall(costumeControl1, null, "SetValue", socks, null, null, null, true);
					object obj1 = this.GetCostumeControl("numSkirt", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.Skirt;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(obj1, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.Skirt = (byte)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(byte));
					}
					object costumeControl2 = this.GetCostumeControl("btnColorTop1", -1);
					socks = new object[] { Color.FromArgb(cloth.Attr.ColorTop1) };
					NewLateBinding.LateCall(costumeControl2, null, "SetValue", socks, null, null, null, true);
					object obj2 = this.GetCostumeControl("btnColorTop2", -1);
					socks = new object[] { Color.FromArgb(cloth.Attr.ColorTop2) };
					NewLateBinding.LateCall(obj2, null, "SetValue", socks, null, null, null, true);
					object costumeControl3 = this.GetCostumeControl("btnColorTop3", -1);
					socks = new object[] { Color.FromArgb(cloth.Attr.ColorTop3) };
					NewLateBinding.LateCall(costumeControl3, null, "SetValue", socks, null, null, null, true);
					object obj3 = this.GetCostumeControl("btnColorTop4", -1);
					socks = new object[] { Color.FromArgb(cloth.Attr.ColorTop4) };
					NewLateBinding.LateCall(obj3, null, "SetValue", socks, null, null, null, true);
					object costumeControl4 = this.GetCostumeControl("btnColorBottom1", -1);
					socks = new object[] { Color.FromArgb(cloth.Attr.ColorBottom1) };
					NewLateBinding.LateCall(costumeControl4, null, "SetValue", socks, null, null, null, true);
					object obj4 = this.GetCostumeControl("btnColorBottom2", -1);
					socks = new object[] { Color.FromArgb(cloth.Attr.ColorBottom2) };
					NewLateBinding.LateCall(obj4, null, "SetValue", socks, null, null, null, true);
					object costumeControl5 = this.GetCostumeControl("btnColorSitagi", -1);
					socks = new object[] { Color.FromArgb(cloth.Attr.ColorSitagi) };
					NewLateBinding.LateCall(costumeControl5, null, "SetValue", socks, null, null, null, true);
					object obj5 = this.GetCostumeControl("btnColorSocks", -1);
					socks = new object[1];
					color = Color.FromArgb(cloth.Attr.ColorSocks);
					byte r = color.R;
					byte g = Color.FromArgb(cloth.Attr.ColorSocks).G;
					Color color1 = Color.FromArgb(cloth.Attr.ColorSocks);
					socks[0] = Color.FromArgb(255, (int)r, (int)g, (int)color1.B);
					NewLateBinding.LateCall(obj5, null, "SetValue", socks, null, null, null, true);
					object costumeControl6 = this.GetCostumeControl("btnColorUtibaki", -1);
					socks = new object[] { Color.FromArgb(cloth.Attr.ColorUtibaki) };
					NewLateBinding.LateCall(costumeControl6, null, "SetValue", socks, null, null, null, true);
					object obj6 = this.GetCostumeControl("btnColorSotobaki", -1);
					socks = new object[] { Color.FromArgb(cloth.Attr.ColorSotobaki) };
					NewLateBinding.LateCall(obj6, null, "SetValue", socks, null, null, null, true);
					object costumeControl7 = this.GetCostumeControl("numBottom1Sitaji1", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.Bottom1Sitaji1;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(costumeControl7, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.Bottom1Sitaji1 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
					}
					object obj7 = this.GetCostumeControl("numBottom1Sitaji2", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.Bottom1Sitaji2;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(obj7, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.Bottom1Sitaji2 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
					}
					object costumeControl8 = this.GetCostumeControl("numBottom1Kage1", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.Bottom1Kage1;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(costumeControl8, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.Bottom1Kage1 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
					}
					object obj8 = this.GetCostumeControl("numBottom1Kage2", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.Bottom1Kage2;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(obj8, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.Bottom1Kage2 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
					}
					object costumeControl9 = this.GetCostumeControl("numBottom1", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.Bottom1;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(costumeControl9, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.Bottom1 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
					}
					object obj9 = this.GetCostumeControl("numSitagiSitaji1", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.SitagiSitaji1;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(obj9, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.SitagiSitaji1 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
					}
					object costumeControl10 = this.GetCostumeControl("numSitagiSitaji2", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.SitagiSitaji2;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(costumeControl10, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.SitagiSitaji2 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
					}
					object obj10 = this.GetCostumeControl("numSitagiKage1", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.SitagiKage1;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(obj10, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.SitagiKage1 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
					}
					object costumeControl11 = this.GetCostumeControl("numSitagiKage2", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.SitagiKage2;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(costumeControl11, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.SitagiKage2 = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
					}
					object obj11 = this.GetCostumeControl("numSitagi", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.Sitagi;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(obj11, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.Sitagi = (int)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(int));
					}
					object costumeControl12 = this.GetCostumeControl("numUtibaki", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.Utibaki;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(costumeControl12, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.Utibaki = (byte)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(byte));
					}
					object obj12 = this.GetCostumeControl("numSotobaki", -1);
					socks = new object[1];
					cloth1 = cloth;
					socks[0] = cloth1.Attr.Sotobaki;
					type = socks;
					flagArray = new bool[] { true };
					NewLateBinding.LateCall(obj12, null, "SetValue", type, null, null, flagArray, true);
					if (flagArray[0])
					{
						cloth1.Attr.Sotobaki = (byte)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(type[0]), typeof(byte));
					}
					object costumeControl13 = this.GetCostumeControl("chkFlagJyoge", -1);
					socks = new object[] { (cloth.Attr.FlagJyoge == 0 ? false : true) };
					NewLateBinding.LateCall(costumeControl13, null, "SetValue", socks, null, null, null, true);
					object obj13 = this.GetCostumeControl("chkFlagSitagi", -1);
					socks = new object[] { (cloth.Attr.FlagSitagi == 0 ? false : true) };
					NewLateBinding.LateCall(obj13, null, "SetValue", socks, null, null, null, true);
					object costumeControl14 = this.GetCostumeControl("chkFlagSkirt", -1);
					socks = new object[] { (cloth.Attr.FlagSkirt == 0 ? false : true) };
					NewLateBinding.LateCall(costumeControl14, null, "SetValue", socks, null, null, null, true);
					this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numType", -1)), null);
					this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numSocks", -1)), null);
					this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numPansuto", -1)), null);
					this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numSkirt", -1)), null);
					this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numBottom1", -1)), null);
					this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numSitagi", -1)), null);
					this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numUtibaki", -1)), null);
					this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numSotobaki", -1)), null);
					Button button = this.btnSubmit;
					object obj14 = this.tabBase;
					bool flag = this.CheckModified(ref obj14);
					this.tabBase = (TabControl)obj14;
					button.Enabled = flag;
				}
				else
				{
					Interaction.MsgBox(Resources.ErrorMessage203, MsgBoxStyle.Critical, null);
				}
				cloth = null;
			}
		}

		private void mnuSelectAll_Click(object sender, EventArgs e)
		{
			MyProject.Forms.frmClass.SelectSeatCheck(this.mnuSelectCondition);
		}

		private void mnuSubmit_Click(object sender, EventArgs e)
		{
			int index = JG2Importer.Common.MySaveData.GetIndex(this.SeatNo);
			int num = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			if (!(index == -1 | num == -1))
			{
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Height = checked((byte)this.cmbHeight.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Style = checked((byte)this.cmbStyle.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Atama1 = Convert.ToByte(this.numAtama1.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Atama2 = Convert.ToByte(this.numAtama2.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Waist = Convert.ToByte(this.numWaist.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune1 = Convert.ToByte(this.numMune1.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune2 = checked((byte)this.cmbMune2.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune3 = Convert.ToByte(this.numMune3.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune4 = Convert.ToByte(this.numMune4.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune5 = Convert.ToByte(this.numMune5.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune6 = Convert.ToByte(this.numMune6.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune7 = Convert.ToByte(this.numMune7.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune8 = checked((byte)this.cmbMune8.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Nyurin = Convert.ToByte(this.numNyurin.Value);
				Color backColor = this.btnColorSkin2.BackColor;
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorSkin2 = backColor.ToArgb();
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tikubi1 = Convert.ToByte(this.cmbTikubi1.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tikubi2 = Convert.ToByte(this.cmbTikubi2.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tikubi3 = Convert.ToByte(this.numTikubi3.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hiyake1 = Convert.ToByte(this.cmbHiyake1.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hiyake2 = Convert.ToByte(this.numHiyake2.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mosaic = Convert.ToByte(this.cmbMosaic.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Under1 = Convert.ToByte(this.cmbUnder1.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Under2 = Convert.ToByte(this.numUnder2.Value);
				backColor = this.btnColorUnder.BackColor;
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorUnder = backColor.ToArgb();
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kao = Convert.ToByte(this.cmbKao.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Eye1 = Convert.ToByte(this.numEye1.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Eye2 = Convert.ToByte(this.numEye2.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Eye3 = Convert.ToByte(this.numEye3.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Eye4 = Convert.ToByte(this.numEye4.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Eye5 = Convert.ToByte(this.numEye5.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Pupil1 = checked((byte)this.cmbPupil1.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Pupil2 = Convert.ToByte(this.numPupil2.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Pupil3 = Convert.ToByte(this.numPupil3.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Pupil4 = Convert.ToByte(this.numPupil4.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Pupil5 = checked((byte)this.cmbPupil5.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Highlight1 = checked((byte)this.cmbHighlight1.SelectedIndex);
				backColor = this.btnColorEye1.BackColor;
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorEye1 = backColor.ToArgb();
				backColor = this.btnColorEye2.BackColor;
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorEye2 = backColor.ToArgb();
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mayu1 = checked((byte)this.cmbMayu1.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mayu2 = Convert.ToByte(this.numMayu2.Value);
				backColor = this.btnColorMayu.BackColor;
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorMayu = backColor.ToArgb();
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mabuta = checked((byte)this.cmbMabuta.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Matuge1 = checked((byte)this.cmbMatuge1.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Matuge2 = checked((byte)this.cmbMatuge2.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Glasses2 = Convert.ToByte(this.cmbGlasses2.Value);
				backColor = this.btnColorGlasses.BackColor;
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorGlasses = backColor.ToArgb();
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hokuro1 = (byte)((this.chkHokuro1.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hokuro2 = (byte)((this.chkHokuro2.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hokuro3 = (byte)((this.chkHokuro3.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hokuro4 = (byte)((this.chkHokuro4.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Lip1 = Convert.ToByte(this.cmbLip1.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Lip2 = Convert.ToByte(this.numLip2.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hair1 = Convert.ToByte(this.numHair1.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairAdj1 = Convert.ToByte(this.numHairAdj1.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairRev1 = (byte)((this.chkHairRev1.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hair2 = Convert.ToByte(this.numHair2.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairAdj2 = Convert.ToByte(this.numHairAdj2.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairRev2 = (byte)((this.chkHairRev2.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hair3 = Convert.ToByte(this.numHair3.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairAdj3 = Convert.ToByte(this.numHairAdj3.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairRev3 = (byte)((this.chkHairRev3.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Hair4 = Convert.ToByte(this.numHair4.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairAdj4 = Convert.ToByte(this.numHairAdj4.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HairRev4 = (byte)((this.chkHairRev4.Checked ? 1 : 0));
				backColor = this.btnColorHair.BackColor;
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.ColorHair = backColor.ToArgb();
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tiryoku3 = Convert.ToInt32(this.numTiryoku3.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tairyoku3 = Convert.ToInt32(this.numTairyoku3.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bukatu1 = checked((byte)this.cmbBukatu1.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bukatu3 = Convert.ToInt32(this.numBukatu3.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Syakousei = checked((byte)this.cmbSyakousei.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kenka = checked((byte)this.cmbKenka.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Teisou = checked((byte)this.cmbTeisou.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seiai = checked((byte)this.cmbSeiai.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikou1 = (byte)((this.chkSeikou1.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikou2 = (byte)((this.chkSeikou2.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Voice = checked((byte)this.cmbVoice.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikaku = Convert.ToByte(this.numSeikaku.Value);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Cyoroi = (byte)((this.chkCyoroi.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Nekketu = (byte)((this.chkNekketu.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Nigate1 = (byte)((this.chkNigate1.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Nigate2 = (byte)((this.chkNigate2.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Charm = (byte)((this.chkCharm.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tundere = (byte)((this.chkTundere.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kyouki = (byte)((this.chkKyouki.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Miha = (byte)((this.chkMiha.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sunao = (byte)((this.chkSunao.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Maemuki = (byte)((this.chkMaemuki.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tereya = (byte)((this.chkTereya.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Yakimochi = (byte)((this.chkYakimochi.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Toufu = (byte)((this.chkToufu.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sukebe = (byte)((this.chkSukebe.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Majime = (byte)((this.chkMajime.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Cool = (byte)((this.chkCool.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Tyokujyo = (byte)((this.chkTyokujyo.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Poyayan = (byte)((this.chkPoyayan.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bouryoku = (byte)((this.chkBouryoku.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sousyoku = (byte)((this.chkSousyoku.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sewayaki = (byte)((this.chkSewayaki.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Iintyou = (byte)((this.chkIintyou.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Osyaberi = (byte)((this.chkOsyaberi.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Harapeko = (byte)((this.chkHarapeko.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Renai = (byte)((this.chkRenai.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Itizu = (byte)((this.chkItizu.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Yuujyufudan = (byte)((this.chkYuujyufudan.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Makenki = (byte)((this.chkMakenki.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Haraguro = (byte)((this.chkHaraguro.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kinben = (byte)((this.chkKinben.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Honpou = (byte)((this.chkHonpou.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.M = (byte)((this.chkM.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Asekaki = (byte)((this.chkAsekaki.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Yami = (byte)((this.chkYami.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Nantyo = (byte)((this.chkNantyo.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Yowami = (byte)((this.chkYowami.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Miseityo = (byte)((this.chkMiseityo.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Luck = (byte)((this.chkLuck.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Rare = (byte)((this.chkRare.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kiss = (byte)((this.chkKiss.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bust = (byte)((this.chkBust.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seiki1 = (byte)((this.chkSeiki1.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seiki2 = (byte)((this.chkSeiki2.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Cunnilingus = (byte)((this.chkCunnilingus.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Fellatio = (byte)((this.chkFellatio.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Back = (byte)((this.chkBack.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Onna = (byte)((this.chkOnna.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Anal = (byte)((this.chkAnal.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Nama = (byte)((this.chkNama.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seiin = (byte)((this.chkSeiin.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Naka = (byte)((this.chkNaka.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kake = (byte)((this.chkKake.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[0] = checked((byte)this.cmbSunday1.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[1] = checked((byte)this.cmbMonday1.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[2] = checked((byte)this.cmbTuesday1.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[3] = checked((byte)this.cmbWednesday1.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[4] = checked((byte)this.cmbThursday1.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[5] = checked((byte)this.cmbFriday1.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[6] = checked((byte)this.cmbSaturday1.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[7] = checked((byte)this.cmbSunday2.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[8] = checked((byte)this.cmbMonday2.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[9] = checked((byte)this.cmbTuesday2.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[10] = checked((byte)this.cmbWednesday2.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[11] = checked((byte)this.cmbThursday2.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[12] = checked((byte)this.cmbFriday2.SelectedIndex);
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Ninshin[13] = checked((byte)this.cmbSaturday2.SelectedIndex);
				int num1 = 0;
				do
				{
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].Type = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numType", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].Skirt = Conversions.ToByte(NewLateBinding.LateGet(this.GetCostumeControl("numSkirt", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].Socks = Conversions.ToByte(NewLateBinding.LateGet(this.GetCostumeControl("numSocks", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].Utibaki = Conversions.ToByte(NewLateBinding.LateGet(this.GetCostumeControl("numUtibaki", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].Sotobaki = Conversions.ToByte(NewLateBinding.LateGet(this.GetCostumeControl("numSotobaki", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorTop1 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorTop1", num1), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorTop2 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorTop2", num1), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorTop3 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorTop3", num1), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorTop4 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorTop4", num1), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorBottom1 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorBottom1", num1), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorBottom2 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorBottom2", num1), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorSitagi = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSitagi", num1), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorSocks[0] = Conversions.ToByte(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSocks", num1), null, "BackColor", new object[0], null, null, null), null, "B", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorSocks[1] = Conversions.ToByte(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSocks", num1), null, "BackColor", new object[0], null, null, null), null, "G", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorSocks[2] = Conversions.ToByte(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSocks", num1), null, "BackColor", new object[0], null, null, null), null, "R", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorUtibaki = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorUtibaki", num1), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorSotobaki = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(this.GetCostumeControl("btnColorSotobaki", num1), null, "BackColor", new object[0], null, null, null), null, "ToArgb", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].Bottom1 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numBottom1", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].Sitagi = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numSitagi", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].Bottom1Sitaji1 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numBottom1Sitaji1", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].Bottom1Sitaji2 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numBottom1Sitaji2", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].SitagiSitaji1 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numSitagiSitaji1", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].SitagiSitaji2 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numSitagiSitaji2", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].FlagJyoge = (byte)((Conversions.ToBoolean(NewLateBinding.LateGet(this.GetCostumeControl("chkFlagJyoge", num1), null, "Checked", new object[0], null, null, null)) ? 1 : 0));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].FlagSitagi = (byte)((Conversions.ToBoolean(NewLateBinding.LateGet(this.GetCostumeControl("chkFlagSitagi", num1), null, "Checked", new object[0], null, null, null)) ? 1 : 0));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].FlagSkirt = (byte)((Conversions.ToBoolean(NewLateBinding.LateGet(this.GetCostumeControl("chkFlagSkirt", num1), null, "Checked", new object[0], null, null, null)) ? 1 : 0));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].Bottom1Kage1 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numBottom1Kage1", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].Bottom1Kage2 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numBottom1Kage2", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].SitagiKage1 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numSitagiKage1", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].SitagiKage2 = Conversions.ToInteger(NewLateBinding.LateGet(this.GetCostumeControl("numSitagiKage2", num1), null, "Value", new object[0], null, null, null));
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Costume[num1].ColorSocks[3] = Conversions.ToByte(NewLateBinding.LateGet(this.GetCostumeControl("numPansuto", num1), null, "Value", new object[0], null, null, null));
					num1 = checked(num1 + 1);
				}
				while (num1 <= 3);
				this.ReCalcKousai((this.chkKousai.Checked ? Flags.ON : Flags.OFF), index, num);
				JG2Importer.Common.MySaveData.Seat[index].PlayData.KousaiData[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer].Status = (byte)((this.chkKousai.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[num].PlayData.KousaiData[JG2Importer.Common.MySaveData.Seat[index].SeatNo].Status = (byte)((this.chkKousai.Checked ? 1 : 0));
				JG2Importer.Common.MySaveData.Seat[index].ReCalc();
				MyProject.Forms.frmClass.DrawSeat(this.SeatNo);
				this.DrawDetail(this.SeatNo);
			}
		}

		private void mnuTab_Click(object sender, EventArgs e)
		{
			this.tabBase.SelectedTab = this.tabBase.TabPages[string.Concat("tbp", Strings.Mid(Conversions.ToString(NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null)), 7))];
		}

		private void mnuTabToolsAction_Click(object sender, EventArgs e)
		{
			JG2Importer.JG2.Action action = (JG2Importer.JG2.Action)0;
			int integer = 0;
			bool flag = false;
			string str = null;
			object obj = NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null);
			if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsAction1", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnAction1", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Love;
				integer = Conversions.ToInteger(this.btnAction1.Text);
				flag = false;
				str = JG2Importer.JG2.Common.cnsCodeAction[1];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsAction2", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnAction2", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Like;
				integer = Conversions.ToInteger(this.btnAction2.Text);
				flag = false;
				str = JG2Importer.JG2.Common.cnsCodeAction[2];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsAction3", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnAction3", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Dislike;
				integer = Conversions.ToInteger(this.btnAction3.Text);
				flag = false;
				str = JG2Importer.JG2.Common.cnsCodeAction[3];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsAction4", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnAction4", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Hate;
				integer = Conversions.ToInteger(this.btnAction4.Text);
				flag = false;
				str = JG2Importer.JG2.Common.cnsCodeAction[4];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsActionRev1", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnActionRev1", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Love;
				integer = Conversions.ToInteger(this.btnActionRev1.Text);
				flag = true;
				str = JG2Importer.JG2.Common.cnsCodeAction[1];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsActionRev2", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnActionRev2", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Like;
				integer = Conversions.ToInteger(this.btnActionRev2.Text);
				flag = true;
				str = JG2Importer.JG2.Common.cnsCodeAction[2];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsActionRev3", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnActionRev3", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Dislike;
				integer = Conversions.ToInteger(this.btnActionRev3.Text);
				flag = true;
				str = JG2Importer.JG2.Common.cnsCodeAction[3];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsActionRev4", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnActionRev4", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Hate;
				integer = Conversions.ToInteger(this.btnActionRev4.Text);
				flag = true;
				str = JG2Importer.JG2.Common.cnsCodeAction[4];
			}
			switch (integer)
			{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				{
					integer = 10;
					break;
				}
				case 10:
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				{
					integer = 20;
					break;
				}
				case 20:
				case 21:
				case 22:
				case 23:
				case 24:
				case 25:
				case 26:
				case 27:
				case 28:
				case 29:
				{
					integer = 30;
					break;
				}
				default:
				{
					integer = 10;
					break;
				}
			}
			if (!this.SetAction(this.SeatNo, action, integer, flag))
			{
				Interaction.MsgBox(Resources.ErrorMessage201, MsgBoxStyle.Critical, null);
			}
			else
			{
				MyProject.Forms.frmClass.DrawSeat(this.SeatNo);
				this.DrawDetail(this.SeatNo);
			}
		}

		private void mnuTabToolsAfterPill_Click(object sender, EventArgs e)
		{
			if (!this.SetAfterPill(this.SeatNo))
			{
				Interaction.MsgBox(Resources.ErrorMessage201, MsgBoxStyle.Critical, null);
			}
			else
			{
				MyProject.Forms.frmClass.DrawSeat(this.SeatNo);
				this.DrawDetail(this.SeatNo);
			}
		}

		private void mnuTabToolsClearAction_Click(object sender, EventArgs e)
		{
			if (!this.ClearAction(this.SeatNo))
			{
				Interaction.MsgBox(Resources.ErrorMessage201, MsgBoxStyle.Critical, null);
			}
			else
			{
				MyProject.Forms.frmClass.DrawSeat(this.SeatNo);
				this.DrawDetail(this.SeatNo);
			}
		}

		private void mnuTabToolsDelAction_Click(object sender, EventArgs e)
		{
			JG2Importer.JG2.Action action = (JG2Importer.JG2.Action)0;
			bool flag = false;
			string str = null;
			object obj = NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null);
			if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsDelAction1", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnDelAction1", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Love;
				flag = false;
				str = JG2Importer.JG2.Common.cnsCodeAction[1];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsDelAction2", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnDelAction2", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Like;
				flag = false;
				str = JG2Importer.JG2.Common.cnsCodeAction[2];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsDelAction3", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnDelAction3", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Dislike;
				flag = false;
				str = JG2Importer.JG2.Common.cnsCodeAction[3];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsDelAction4", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnDelAction4", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Hate;
				flag = false;
				str = JG2Importer.JG2.Common.cnsCodeAction[4];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsDelActionRev1", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnDelActionRev1", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Love;
				flag = true;
				str = JG2Importer.JG2.Common.cnsCodeAction[1];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsDelActionRev2", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnDelActionRev2", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Like;
				flag = true;
				str = JG2Importer.JG2.Common.cnsCodeAction[2];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsDelActionRev3", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnDelActionRev3", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Dislike;
				flag = true;
				str = JG2Importer.JG2.Common.cnsCodeAction[3];
			}
			else if (Conversions.ToBoolean((Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "mnuTabToolsDelActionRev4", false)) || Conversions.ToBoolean(Operators.CompareObjectEqual(obj, "btnDelActionRev4", false)) ? true : false)))
			{
				action = JG2Importer.JG2.Action.Hate;
				flag = true;
				str = JG2Importer.JG2.Common.cnsCodeAction[4];
			}
			if (!this.DelAction(this.SeatNo, action, flag))
			{
				Interaction.MsgBox(Resources.ErrorMessage201, MsgBoxStyle.Critical, null);
			}
			else
			{
				MyProject.Forms.frmClass.DrawSeat(this.SeatNo);
				this.DrawDetail(this.SeatNo);
			}
		}

		private void mnuTabToolsExport_Click(object sender, EventArgs e)
		{
			long index = (long)JG2Importer.Common.MySaveData.GetIndex(this.SeatNo);
			if (index != (long)-1)
			{
				string str = JG2Importer.Common.BrowseSaveFile(this, "PNG", Resources.MenuTxtCharFile + " (*.png)|*.png", Resources.MenuTxtCharFileDesignation, string.Concat(JG2Importer.Common.MySaveData.Seat[checked((int)index)].Character.FullName, ".png"), Path.Combine(JG2Importer.Common.INSTALLDIR_EDIT, "data\\save\\Female\\"));
                if (Operators.CompareString(str, null, false) == 0)
                {
                    return;
                }
                else
                {
                    JG2Importer.Common.MySaveData.Seat[checked((int)index)].Export(str);
                }
			}
		}

		private void mnuTabToolsGohoubi_Click(object sender, EventArgs e)
		{
			switch (this.SetGohoubi(this.SeatNo))
			{
				case 0:
				{
					MyProject.Forms.frmClass.DrawSeat(this.SeatNo);
					this.DrawDetail(this.SeatNo);
					return;
				}
				case 1:
				{
					Interaction.MsgBox(Resources.ErrorMessage202, MsgBoxStyle.Critical, null);
					return;
				}
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				{
					return;
				}
				case 9:
				{
					Interaction.MsgBox(Resources.ErrorMessage201, MsgBoxStyle.Critical, null);
					return;
				}
				default:
				{
					return;
				}
			}
		}

		private void mnuTabToolsHMax_Click(object sender, EventArgs e)
		{
			if (!this.SetHMax(this.SeatNo))
			{
				Interaction.MsgBox(Resources.ErrorMessage201, MsgBoxStyle.Critical, null);
			}
			else
			{
				MyProject.Forms.frmClass.DrawSeat(this.SeatNo);
				this.DrawDetail(this.SeatNo);
			}
		}

		private void mnuTabToolsImport_Click(object sender, EventArgs e)
		{
			long index = (long)JG2Importer.Common.MySaveData.GetIndex(this.SeatNo);
			if (index != (long)-1)
			{
				string str = JG2Importer.Common.BrowseOpenFile(this, "PNG", Resources.MenuTxtCharFile + " (*.png)|*.png", Resources.MenuTxtCharFileDesignation, Path.Combine(JG2Importer.Common.INSTALLDIR_EDIT, "data\\save\\Female\\"));
				if (Operators.CompareString(str, null, false) == 0)
				{
					return;
				}
				string name1 = JG2Importer.Common.MySaveData.Seat[checked((int)index)].Character.Attr.Name1;
				string name2 = JG2Importer.Common.MySaveData.Seat[checked((int)index)].Character.Attr.Name2;
				Seat seat = JG2Importer.Common.MySaveData.Seat[checked((int)index)];
				Character character = new Character(str);
				seat.Import(ref character);
				if (!(Operators.CompareString(name1, JG2Importer.Common.MySaveData.Seat[checked((int)index)].Character.Attr.Name1, false) == 0 & Operators.CompareString(name2, JG2Importer.Common.MySaveData.Seat[checked((int)index)].Character.Attr.Name2, false) == 0))
				{
					this.ReName(name1, name2, JG2Importer.Common.MySaveData.Seat[checked((int)index)].Character.Attr.Name1, JG2Importer.Common.MySaveData.Seat[checked((int)index)].Character.Attr.Name2);
				}
				MyProject.Forms.frmClass.DrawSeat(this.SeatNo);
				this.DrawDetail(this.SeatNo);
			}
		}

		private void mnuTabToolsPlayer_Click(object sender, EventArgs e)
		{
			if ((long)JG2Importer.Common.MySaveData.GetIndex(this.SeatNo) != (long)-1)
			{
				JG2Importer.Common.MySaveData.Footer.SeatNoPlayer = this.SeatNo;
				MyProject.Forms.frmClass.DrawClass();
				this.DrawDetail(this.SeatNo);
			}
		}

		private void num_ValueChanged(object sender, EventArgs e)
		{
			object[] objArray;
			object[] objArray1;
			object[] objArray2;
			object[] objArray3;
			object obj;
			object obj1;
			object obj2;
			object obj3;
			object obj4;
			object obj5;
			object obj6;
			string str;
			string str1;
			string str2;
			string str3;
			string str4;
			object obj7 = NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null);
			if (Operators.ConditionalCompareObjectEqual(obj7, "numMune1", false))
			{
				this.lblMune1Name.Text = JG2Importer.JG2.Common.cnsMune[(int)JG2Importer.JG2.Common.GetMune(this.Sex, Conversions.ToByte(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null)))];
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numHair1", false))
			{
				Label label = this.lblHair1Name;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectGreaterEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 0, false), Operators.CompareObjectLessEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 58, false))))
				{
					str4 = string.Concat(Resources.CaptionHair1, Strings.Format(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null)), "0"));
				}
				else
				{
					str4 = null;
				}
				label.Text = str4;
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numHair2", false))
			{
				Label label1 = this.lblHair2Name;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectGreaterEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 0, false), Operators.CompareObjectLessEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 32, false))))
				{
					str3 = string.Concat(Resources.CaptionHair2, Strings.Format(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null)), "0"));
				}
				else
				{
					str3 = null;
				}
				label1.Text = str3;
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numHair3", false))
			{
				Label label2 = this.lblHair3Name;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectGreaterEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 0, false), Operators.CompareObjectLessEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 65, false))))
				{
					str2 = string.Concat(Resources.CaptionHair3, Strings.Format(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null)), "0"));
				}
				else
				{
					str2 = null;
				}
				label2.Text = str2;
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numHair4", false))
			{
				Label label3 = this.lblHair4Name;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectGreaterEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 0, false), Operators.CompareObjectLessEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 32, false))))
				{
					str1 = string.Concat(Resources.CaptionHair4, Strings.Format(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null)), "0"));
				}
				else
				{
					str1 = null;
				}
				label3.Text = str1;
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numTiryoku3", false))
			{
				this.lblTiryoku1Name.Text = JG2Importer.JG2.Common.cnsTiryoku1[JG2Importer.JG2.Common.GetNouryoku1(Conversions.ToInteger(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null)))];
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numTairyoku3", false))
			{
				this.lblTairyoku1Name.Text = JG2Importer.JG2.Common.cnsTairyoku1[JG2Importer.JG2.Common.GetNouryoku1(Conversions.ToInteger(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null)))];
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numSeikaku", false))
			{
				Label label4 = this.lblSeikakuName;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectGreaterEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 0, false), Operators.CompareObjectLessEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), checked(checked((int)JG2Importer.JG2.Common.cnsSeikaku.Length) - 1), false))))
				{
					str = JG2Importer.JG2.Common.cnsSeikaku[Conversions.ToInteger(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null))];
				}
				else
				{
					str = null;
				}
				label4.Text = str;
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numType", false))
			{
				object obj8 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray = new object[] { "lblTypeName" };
				object obj9 = NewLateBinding.LateGet(obj8, null, "Controls", objArray, null, null, null);
				objArray1 = new object[1];
				object[] objArray4 = objArray1;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectGreaterEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 0, false), Operators.CompareObjectLessEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), checked(checked((int)JG2Importer.JG2.Common.cnsType.Length) - 1), false))))
				{
					obj6 = JG2Importer.JG2.Common.cnsType[Conversions.ToInteger(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null))];
				}
				else
				{
					obj6 = null;
				}
				objArray4[0] = obj6;
				NewLateBinding.LateSetComplex(obj9, null, "Text", objArray1, null, null, false, true);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numSocks", false))
			{
				object obj10 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray1 = new object[] { "lblSocksName" };
				object obj11 = NewLateBinding.LateGet(obj10, null, "Controls", objArray1, null, null, null);
				objArray = new object[1];
				object[] objArray5 = objArray;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectGreaterEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 0, false), Operators.CompareObjectLessEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), checked(checked((int)JG2Importer.JG2.Common.cnsSocks.Length) - 1), false))))
				{
					obj5 = JG2Importer.JG2.Common.cnsSocks[Conversions.ToInteger(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null))];
				}
				else
				{
					obj5 = null;
				}
				objArray5[0] = obj5;
				NewLateBinding.LateSetComplex(obj11, null, "Text", objArray, null, null, false, true);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numPansuto", false))
			{
				object obj12 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray1 = new object[] { "lblColorPansuto" };
				object obj13 = NewLateBinding.LateGet(obj12, null, "Controls", objArray1, null, null, null);
				objArray3 = new object[1];
				int integer = Conversions.ToInteger(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null));
				object obj14 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray = new object[] { "btnColorSocks" };
				int num = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(NewLateBinding.LateGet(obj14, null, "Controls", objArray, null, null, null), null, "BackColor", new object[0], null, null, null), null, "R", new object[0], null, null, null));
				object obj15 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				object[] objArray6 = new object[] { "btnColorSocks" };
				int integer1 = Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(NewLateBinding.LateGet(obj15, null, "Controls", objArray6, null, null, null), null, "BackColor", new object[0], null, null, null), null, "G", new object[0], null, null, null));
				object obj16 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray2 = new object[] { "btnColorSocks" };
				objArray3[0] = Color.FromArgb(integer, num, integer1, Conversions.ToInteger(NewLateBinding.LateGet(NewLateBinding.LateGet(NewLateBinding.LateGet(obj16, null, "Controls", objArray2, null, null, null), null, "BackColor", new object[0], null, null, null), null, "B", new object[0], null, null, null)));
				NewLateBinding.LateSetComplex(obj13, null, "BackColor", objArray3, null, null, false, true);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numSkirt", false))
			{
				object obj17 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray3 = new object[] { "lblSkirtName" };
				object obj18 = NewLateBinding.LateGet(obj17, null, "Controls", objArray3, null, null, null);
				objArray2 = new object[1];
				object[] objArray7 = objArray2;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectGreaterEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 0, false), Operators.CompareObjectLessEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), checked(checked((int)JG2Importer.JG2.Common.cnsSkirt.Length) - 1), false))))
				{
					obj4 = JG2Importer.JG2.Common.cnsSkirt[Conversions.ToInteger(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null))];
				}
				else
				{
					obj4 = null;
				}
				objArray7[0] = obj4;
				NewLateBinding.LateSetComplex(obj18, null, "Text", objArray2, null, null, false, true);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numBottom1", false))
			{
				object obj19 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray3 = new object[] { "lblBottom1Name" };
				object obj20 = NewLateBinding.LateGet(obj19, null, "Controls", objArray3, null, null, null);
				objArray2 = new object[1];
				object[] objArray8 = objArray2;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectGreaterEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 0, false), Operators.CompareObjectLessEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), checked(checked((int)JG2Importer.JG2.Common.cnsBottom1.Length) - 1), false))))
				{
					obj3 = JG2Importer.JG2.Common.cnsBottom1[Conversions.ToInteger(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null))];
				}
				else
				{
					obj3 = null;
				}
				objArray8[0] = obj3;
				NewLateBinding.LateSetComplex(obj20, null, "Text", objArray2, null, null, false, true);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numSitagi", false))
			{
				object obj21 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray3 = new object[] { "lblSitagiName" };
				object obj22 = NewLateBinding.LateGet(obj21, null, "Controls", objArray3, null, null, null);
				objArray2 = new object[1];
				object[] objArray9 = objArray2;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectGreaterEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 0, false), Operators.CompareObjectLessEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), checked(checked((int)JG2Importer.JG2.Common.cnsSitagi.Length) - 1), false))))
				{
					obj2 = JG2Importer.JG2.Common.cnsSitagi[Conversions.ToInteger(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null))];
				}
				else
				{
					obj2 = null;
				}
				objArray9[0] = obj2;
				NewLateBinding.LateSetComplex(obj22, null, "Text", objArray2, null, null, false, true);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numUtibaki", false))
			{
				object obj23 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray3 = new object[] { "lblUtibakiName" };
				object obj24 = NewLateBinding.LateGet(obj23, null, "Controls", objArray3, null, null, null);
				objArray2 = new object[1];
				object[] objArray10 = objArray2;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectGreaterEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 0, false), Operators.CompareObjectLessEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), checked(checked((int)JG2Importer.JG2.Common.cnsUtibaki.Length) - 1), false))))
				{
					obj1 = JG2Importer.JG2.Common.cnsUtibaki[Conversions.ToInteger(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null))];
				}
				else
				{
					obj1 = null;
				}
				objArray10[0] = obj1;
				NewLateBinding.LateSetComplex(obj24, null, "Text", objArray2, null, null, false, true);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj7, "numSotobaki", false))
			{
				object obj25 = NewLateBinding.LateGet(sender, null, "Parent", new object[0], null, null, null);
				objArray3 = new object[] { "lblSotobakiName" };
				object obj26 = NewLateBinding.LateGet(obj25, null, "Controls", objArray3, null, null, null);
				objArray2 = new object[1];
				object[] objArray11 = objArray2;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectGreaterEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), 0, false), Operators.CompareObjectLessEqual(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null), checked(checked((int)JG2Importer.JG2.Common.cnsSotobaki.Length) - 1), false))))
				{
					obj = JG2Importer.JG2.Common.cnsSotobaki[Conversions.ToInteger(NewLateBinding.LateGet(sender, null, "Value", new object[0], null, null, null))];
				}
				else
				{
					obj = null;
				}
				objArray11[0] = obj;
				NewLateBinding.LateSetComplex(obj26, null, "Text", objArray2, null, null, false, true);
			}
		}

		private void radYouto_CheckedChanged(object sender, EventArgs e)
		{
			if (Conversions.ToBoolean(NewLateBinding.LateGet(sender, null, "Checked", new object[0], null, null, null)))
			{
				object obj = NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null);
				if (Operators.ConditionalCompareObjectEqual(obj, "radYouto0", false))
				{
					this.pnlYouto.Tag = 0;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "radYouto1", false))
				{
					this.pnlYouto.Tag = 1;
				}
				else if (!Operators.ConditionalCompareObjectEqual(obj, "radYouto2", false))
				{
					if (!Operators.ConditionalCompareObjectEqual(obj, "radYouto3", false))
					{
						return;
					}
					this.pnlYouto.Tag = 3;
				}
				else
				{
					this.pnlYouto.Tag = 2;
				}
				this.tbpCostume.Controls[string.Concat("pnlCostume", Strings.Format(RuntimeHelpers.GetObjectValue(this.pnlYouto.Tag), ""))].Visible = true;
				this.tbpCostume.Controls[string.Concat("pnlCostume", Strings.Format(RuntimeHelpers.GetObjectValue(this.pnlYouto.Tag), ""))].BringToFront();
				int num = 0;
				do
				{
					if (!Operators.ConditionalCompareObjectEqual(num, this.pnlYouto.Tag, false))
					{
						if (this.tbpCostume.Controls[string.Concat("pnlCostume", Strings.Format(num, ""))].Visible)
						{
							this.tbpCostume.Controls[string.Concat("pnlCostume", Strings.Format(num, ""))].Visible = false;
						}
					}
					num = checked(num + 1);
				}
				while (num <= 3);
			}
		}

		private void radYouto_DragDrop(object sender, DragEventArgs e)
		{
			IEnumerator enumerator = null;
			object obj;
			object[] objArray;
			bool[] flagArray;
			object[] objectValue;
			int integer = Conversions.ToInteger(Strings.Mid(Conversions.ToString(NewLateBinding.LateGet(e.Data.GetData(typeof(JG2RadioButton)), null, "Name", new object[0], null, null, null)), 9));
			int num = Conversions.ToInteger(Strings.Mid(Conversions.ToString(NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null)), 9));
			try
			{
				enumerator = this.tbpCostume.Controls[string.Concat("pnlCostume", Strings.Format(integer, ""))].Controls.GetEnumerator();
				while (enumerator.MoveNext())
				{
					object objectValue1 = RuntimeHelpers.GetObjectValue(enumerator.Current);
					Control.ControlCollection controls = this.tbpCostume.Controls[string.Concat("pnlCostume", Strings.Format(num, ""))].Controls;
					object[] objArray1 = new object[] { RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(objectValue1, null, "Name", new object[0], null, null, null)) };
					object obj1 = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(controls, null, "Item", objArray1, null, null, null));
					bool flag = true;
					if (flag == objectValue1 is JG2NumericUpDown)
					{
						objArray1 = new object[1];
						obj = objectValue1;
						objArray1[0] = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj, null, "Value", new object[0], null, null, null));
						objArray = objArray1;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(obj1, null, "SetValue", objArray, null, null, flagArray, true);
						if (!flagArray[0])
						{
							continue;
						}
						objectValue = new object[] { RuntimeHelpers.GetObjectValue(objArray[0]) };
						NewLateBinding.LateSetComplex(obj, null, "Value", objectValue, null, null, true, false);
					}
					else if (flag == objectValue1 is JG2ComboBox)
					{
						objectValue = new object[1];
						obj = objectValue1;
						objectValue[0] = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj, null, "SelectedIndex", new object[0], null, null, null));
						objArray = objectValue;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(obj1, null, "SetValue", objArray, null, null, flagArray, true);
						if (!flagArray[0])
						{
							continue;
						}
						objArray1 = new object[] { RuntimeHelpers.GetObjectValue(objArray[0]) };
						NewLateBinding.LateSetComplex(obj, null, "SelectedIndex", objArray1, null, null, true, false);
					}
					else if (flag != objectValue1 is JG2Button)
					{
						if (flag != objectValue1 is JG2CheckBox)
						{
							continue;
						}
						objectValue = new object[1];
						obj = objectValue1;
						objectValue[0] = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj, null, "Checked", new object[0], null, null, null));
						objArray = objectValue;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(obj1, null, "SetValue", objArray, null, null, flagArray, true);
						if (!flagArray[0])
						{
							continue;
						}
						objArray1 = new object[] { RuntimeHelpers.GetObjectValue(objArray[0]) };
						NewLateBinding.LateSetComplex(obj, null, "Checked", objArray1, null, null, true, false);
					}
					else
					{
						objectValue = new object[1];
						obj = objectValue1;
						objectValue[0] = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(obj, null, "BackColor", new object[0], null, null, null));
						objArray = objectValue;
						flagArray = new bool[] { true };
						NewLateBinding.LateCall(obj1, null, "SetValue", objArray, null, null, flagArray, true);
						if (!flagArray[0])
						{
							continue;
						}
						objArray1 = new object[] { RuntimeHelpers.GetObjectValue(objArray[0]) };
						NewLateBinding.LateSetComplex(obj, null, "BackColor", objArray1, null, null, true, false);
					}
				}
			}
			finally
			{
				if (enumerator is IDisposable)
				{
					(enumerator as IDisposable).Dispose();
				}
			}
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numType", num)), null);
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numSocks", num)), null);
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numPansuto", num)), null);
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numSkirt", num)), null);
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numBottom1", num)), null);
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numSitagi", num)), null);
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numUtibaki", num)), null);
			this.num_ValueChanged(RuntimeHelpers.GetObjectValue(this.GetCostumeControl("numSotobaki", num)), null);
			Button button = this.btnSubmit;
			obj = this.tabBase;
			bool flag1 = this.CheckModified(ref obj);
			this.tabBase = (TabControl)obj;
			button.Enabled = flag1;
		}

		private void radYouto_DragEnter(object sender, DragEventArgs e)
		{
			e.Effect = (e.Data.GetDataPresent(typeof(JG2RadioButton)) ? DragDropEffects.Copy : DragDropEffects.None);
		}

		private void radYouto_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Right)
			{
				object[] objectValue = new object[] { RuntimeHelpers.GetObjectValue(sender), DragDropEffects.Copy };
				object[] objArray = objectValue;
				bool[] flagArray = new bool[] { true, false };
				NewLateBinding.LateCall(sender, null, "DoDragDrop", objArray, null, null, flagArray, true);
				if (flagArray[0])
				{
					sender = RuntimeHelpers.GetObjectValue(objArray[0]);
				}
			}
		}

		private void ReCalcAction(ref Seat.DefStruct2 argKoukando)
		{
			argKoukando.CntLove = 0;
			argKoukando.CntLike = 0;
			argKoukando.CntDislike = 0;
			argKoukando.CntHate = 0;
			argKoukando.DirLove = 0;
			argKoukando.DirLike = 0;
			argKoukando.DirDislike = 0;
			argKoukando.DirHate = 0;
			argKoukando.MaxActionCnt = 0;
			argKoukando.MaxActionData = null;
			argKoukando.MaxAction = 0;
			if (argKoukando.ActionCnt > 0)
			{
				int actionCnt = checked(argKoukando.ActionCnt - 1);
				for (int i = 0; i <= actionCnt; i = checked(i + 1))
				{
					switch (argKoukando.ActionData[i])
					{
						case 0:
						{
							argKoukando.CntLove = checked(argKoukando.CntLove + 1);
							break;
						}
						case 1:
						{
							argKoukando.CntLike = checked(argKoukando.CntLike + 1);
							break;
						}
						case 2:
						{
							argKoukando.CntDislike = checked(argKoukando.CntDislike + 1);
							break;
						}
						case 3:
						{
							argKoukando.CntHate = checked(argKoukando.CntHate + 1);
							break;
						}
					}
				}
			}
			switch (argKoukando.CntLove)
			{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				{
					argKoukando.DirLove = 2;
					break;
				}
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				case 20:
				{
					argKoukando.DirLove = 1;
					break;
				}
				default:
				{
					argKoukando.DirLove = 0;
					break;
				}
			}
			switch (argKoukando.CntLike)
			{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				{
					argKoukando.DirLike = 2;
					break;
				}
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				case 20:
				{
					argKoukando.DirLike = 1;
					break;
				}
				default:
				{
					argKoukando.DirLike = 0;
					break;
				}
			}
			switch (argKoukando.CntDislike)
			{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				{
					argKoukando.DirDislike = 2;
					break;
				}
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				case 20:
				{
					argKoukando.DirDislike = 1;
					break;
				}
				default:
				{
					argKoukando.DirDislike = 0;
					break;
				}
			}
			switch (argKoukando.CntHate)
			{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				{
					argKoukando.DirHate = 2;
					break;
				}
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				case 20:
				{
					argKoukando.DirHate = 1;
					break;
				}
				default:
				{
					argKoukando.DirHate = 0;
					break;
				}
			}
			argKoukando.MaxAction = argKoukando.CntLove;
			if (argKoukando.MaxAction <= argKoukando.CntLike)
			{
				argKoukando.MaxAction = argKoukando.CntLike;
			}
			if (argKoukando.MaxAction <= argKoukando.CntDislike)
			{
				argKoukando.MaxAction = argKoukando.CntDislike;
			}
			if (argKoukando.MaxAction <= argKoukando.CntHate)
			{
				argKoukando.MaxAction = argKoukando.CntHate;
			}
			if (argKoukando.MaxAction > 0)
			{
				if (argKoukando.MaxAction == argKoukando.CntLove)
				{
					argKoukando.MaxActionCnt = checked(argKoukando.MaxActionCnt + 1);
					Array.Resize<int>(ref argKoukando.MaxActionData, argKoukando.MaxActionCnt);
					argKoukando.MaxActionData[checked(argKoukando.MaxActionCnt - 1)] = 0;
				}
				if (argKoukando.MaxAction == argKoukando.CntLike)
				{
					argKoukando.MaxActionCnt = checked(argKoukando.MaxActionCnt + 1);
					Array.Resize<int>(ref argKoukando.MaxActionData, argKoukando.MaxActionCnt);
					argKoukando.MaxActionData[checked(argKoukando.MaxActionCnt - 1)] = 1;
				}
				if (argKoukando.MaxAction == argKoukando.CntDislike)
				{
					argKoukando.MaxActionCnt = checked(argKoukando.MaxActionCnt + 1);
					Array.Resize<int>(ref argKoukando.MaxActionData, argKoukando.MaxActionCnt);
					argKoukando.MaxActionData[checked(argKoukando.MaxActionCnt - 1)] = 2;
				}
				if (argKoukando.MaxAction == argKoukando.CntHate)
				{
					argKoukando.MaxActionCnt = checked(argKoukando.MaxActionCnt + 1);
					Array.Resize<int>(ref argKoukando.MaxActionData, argKoukando.MaxActionCnt);
					argKoukando.MaxActionData[checked(argKoukando.MaxActionCnt - 1)] = 3;
				}
			}
		}

		private bool ReCalcKousai(Flags argKousai, int argFromIndex, int argToIndex)
		{
			Seat[] seat = JG2Importer.Common.MySaveData.Seat;
			int num = argFromIndex;
			if ((byte)argKousai != seat[num].PlayData.KousaiData[JG2Importer.Common.MySaveData.Seat[argToIndex].SeatNo].Status)
			{
				if (JG2Importer.Common.MySaveData.GetCodeFormer(argFromIndex, argToIndex) == Flags.ON)
				{
					if (argKousai != Flags.ON)
					{
						seat[num].PlayData.KousaiData[JG2Importer.Common.MySaveData.Seat[argToIndex].SeatNo].Nissu = 0;
						seat[num].PlayData.CntFurareta = checked(seat[num].PlayData.CntFurareta + 1);
					}
				}
				else if (argKousai != Flags.ON)
				{
					seat[num].PlayData.KousaiData[JG2Importer.Common.MySaveData.Seat[argToIndex].SeatNo].Nissu = 0;
					seat[num].PlayData.CntFurareta = checked(seat[num].PlayData.CntFurareta + 1);
					if (seat[num].PlayData.DataSet16Data[6].DataSet1Cnt == 0)
					{
						seat[num].PlayData.DataSet16Data[6].DataSet1Cnt = 1;
						Array.Resize<Seat.DefStruct7>(ref seat[num].PlayData.DataSet16Data[6].DataSet1Data, 1);
						seat[num].PlayData.DataSet16Data[6].DataSet1Data[0] = new Seat.DefStruct7();
						seat[num].PlayData.DataSet16Data[6].DataSet1Data[0].Kouban = 6;
						seat[num].PlayData.DataSet16Data[6].DataSet1Data[0].Data2 = 0;
						seat[num].PlayData.DataSet16Data[6].DataSet1Data[0].Data3 = 3;
					}
					seat[num].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoCnt = checked((short)(checked(seat[num].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoCnt + 1)));
					Array.Resize<byte>(ref seat[num].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoData, seat[num].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoCnt);
					seat[num].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoData[checked(seat[num].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoCnt - 1)] = 0;
					seat[num].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoData[checked(seat[num].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoCnt - 1)] = checked((byte)JG2Importer.Common.MySaveData.Seat[argToIndex].SeatNo);
				}
				else
				{
					seat[num].PlayData.CntKousai = checked(seat[num].PlayData.CntKousai + 1);
				}
			}
			Seat[] seatNoCnt = JG2Importer.Common.MySaveData.Seat;
			int num1 = argToIndex;
			if ((byte)argKousai != seatNoCnt[num1].PlayData.KousaiData[JG2Importer.Common.MySaveData.Seat[argFromIndex].SeatNo].Status)
			{
				if (JG2Importer.Common.MySaveData.GetCodeFormer(argToIndex, argFromIndex) == Flags.ON)
				{
					if (argKousai != Flags.ON)
					{
						seatNoCnt[num1].PlayData.KousaiData[JG2Importer.Common.MySaveData.Seat[argFromIndex].SeatNo].Nissu = 0;
					}
				}
				else if (argKousai != Flags.ON)
				{
					seatNoCnt[num1].PlayData.KousaiData[JG2Importer.Common.MySaveData.Seat[argFromIndex].SeatNo].Nissu = 0;
					if (seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Cnt == 0)
					{
						seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Cnt = 1;
						Array.Resize<Seat.DefStruct7>(ref seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data, 1);
						seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data[0] = new Seat.DefStruct7();
						seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data[0].Kouban = 6;
						seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data[0].Data2 = 0;
						seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data[0].Data3 = 3;
					}
					seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoCnt = checked((short)(checked(seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoCnt + 1)));
					Array.Resize<byte>(ref seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoData, seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoCnt);
					seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoData[checked(seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoCnt - 1)] = 0;
					seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoData[checked(seatNoCnt[num1].PlayData.DataSet16Data[6].DataSet1Data[0].SeatNoCnt - 1)] = checked((byte)JG2Importer.Common.MySaveData.Seat[argFromIndex].SeatNo);
				}
				else
				{
					seatNoCnt[num1].PlayData.CntKousai = checked(seatNoCnt[num1].PlayData.CntKousai + 1);
				}
			}
			return true;
		}

		private void ReName(string argOldName1, string argOldName2, string argNewName1, string argNewName2)
		{
			int length = checked(checked((int)JG2Importer.Common.MySaveData.Seat.Length) - 1);
			for (int i = 0; i <= length; i = checked(i + 1))
			{
				string aite1 = JG2Importer.Common.MySaveData.Seat[i].PlayData.Aite1;
				int num = Strings.InStr(aite1, argOldName1, CompareMethod.Text);
				if (num > 0 && Strings.InStr(num, aite1, argOldName2, CompareMethod.Text) > 0)
				{
					aite1 = Strings.Replace(aite1, argOldName1, argNewName1, 1, 1, CompareMethod.Text);
					aite1 = Strings.Replace(aite1, argOldName2, argNewName2, 1, 1, CompareMethod.Text);
					JG2Importer.Common.MySaveData.Seat[i].PlayData.Aite1 = aite1;
				}
				aite1 = JG2Importer.Common.MySaveData.Seat[i].PlayData.Aite2;
				num = Strings.InStr(aite1, argOldName1, CompareMethod.Text);
				if (num > 0 && Strings.InStr(num, aite1, argOldName2, CompareMethod.Text) > 0)
				{
					aite1 = Strings.Replace(aite1, argOldName1, argNewName1, 1, 1, CompareMethod.Text);
					aite1 = Strings.Replace(aite1, argOldName2, argNewName2, 1, 1, CompareMethod.Text);
					JG2Importer.Common.MySaveData.Seat[i].PlayData.Aite2 = aite1;
				}
				aite1 = JG2Importer.Common.MySaveData.Seat[i].PlayData.Aite3;
				num = Strings.InStr(aite1, argOldName1, CompareMethod.Text);
				if (num > 0 && Strings.InStr(num, aite1, argOldName2, CompareMethod.Text) > 0)
				{
					aite1 = Strings.Replace(aite1, argOldName1, argNewName1, 1, 1, CompareMethod.Text);
					aite1 = Strings.Replace(aite1, argOldName2, argNewName2, 1, 1, CompareMethod.Text);
					JG2Importer.Common.MySaveData.Seat[i].PlayData.Aite3 = aite1;
				}
				aite1 = JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Goods1;
				num = Strings.InStr(aite1, argOldName1, CompareMethod.Text);
				if (num > 0 && Strings.InStr(num, aite1, argOldName2, CompareMethod.Text) > 0)
				{
					aite1 = Strings.Replace(aite1, argOldName1, argNewName1, 1, 1, CompareMethod.Text);
					aite1 = Strings.Replace(aite1, argOldName2, argNewName2, 1, 1, CompareMethod.Text);
					JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Goods1 = aite1;
				}
				aite1 = JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Goods2;
				num = Strings.InStr(aite1, argOldName1, CompareMethod.Text);
				if (num > 0 && Strings.InStr(num, aite1, argOldName2, CompareMethod.Text) > 0)
				{
					aite1 = Strings.Replace(aite1, argOldName1, argNewName1, 1, 1, CompareMethod.Text);
					aite1 = Strings.Replace(aite1, argOldName2, argNewName2, 1, 1, CompareMethod.Text);
					JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Goods2 = aite1;
				}
				aite1 = JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Goods3;
				num = Strings.InStr(aite1, argOldName1, CompareMethod.Text);
				if (num > 0 && Strings.InStr(num, aite1, argOldName2, CompareMethod.Text) > 0)
				{
					aite1 = Strings.Replace(aite1, argOldName1, argNewName1, 1, 1, CompareMethod.Text);
					aite1 = Strings.Replace(aite1, argOldName2, argNewName2, 1, 1, CompareMethod.Text);
					JG2Importer.Common.MySaveData.Seat[i].Character.Attr.Goods3 = aite1;
				}
			}
		}

		private bool SetAction(int argSeatNo, JG2Importer.JG2.Action argAction, int argCount, bool argReverse)
		{
			int index;
			int num;
			if (argSeatNo == JG2Importer.Common.MySaveData.Footer.SeatNoPlayer)
			{
				return false;
			}
			if (!argReverse)
			{
				index = JG2Importer.Common.MySaveData.GetIndex(argSeatNo);
				num = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			}
			else
			{
				index = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
				num = JG2Importer.Common.MySaveData.GetIndex(argSeatNo);
			}
			if (!(index == -1 | num == -1))
			{
				int koukandoCnt = checked(JG2Importer.Common.MySaveData.Seat[index].PlayData.KoukandoCnt - 1);
				int num1 = 0;
				while (num1 <= koukandoCnt)
				{
					Seat.DefStruct2[] koukandoData = JG2Importer.Common.MySaveData.Seat[index].PlayData.KoukandoData;
					int num2 = num1;
					if (koukandoData[num2].SeatNo != JG2Importer.Common.MySaveData.Seat[num].SeatNo)
					{
						num1 = checked(num1 + 1);
					}
					else
					{
						koukandoData[num2].ActionCnt = argCount;
						Array.Resize<int>(ref koukandoData[num2].ActionData, koukandoData[num2].ActionCnt);
						int length = checked(checked((int)koukandoData[num2].ActionData.Length) - 1);
						for (int i = 0; i <= length; i = checked(i + 1))
						{
							koukandoData[num2].ActionData[i] = (int)argAction;
						}
						koukandoData[num2].PtsLove = (argAction == JG2Importer.JG2.Action.Love ? 30 : 0);
						koukandoData[num2].PtsLike = (argAction == JG2Importer.JG2.Action.Like ? 30 : 0);
						koukandoData[num2].PtsDislike = (argAction == JG2Importer.JG2.Action.Dislike ? 30 : 0);
						koukandoData[num2].PtsHate = (argAction == JG2Importer.JG2.Action.Hate ? 30 : 0);
						this.ReCalcAction(ref JG2Importer.Common.MySaveData.Seat[index].PlayData.KoukandoData[num1]);
						break;
					}
				}
			}
			return true;
		}

		private bool SetAfterPill(int argSeatNo)
		{
			if (argSeatNo == JG2Importer.Common.MySaveData.Footer.SeatNoPlayer)
			{
				return false;
			}
			int index = JG2Importer.Common.MySaveData.GetIndex(argSeatNo);
			if (!(index == -1 | JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer) == -1))
			{
				JG2Importer.Common.MySaveData.Seat[index].PlayData.CntNakaKiken[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer] = 0;
			}
			return true;
		}

		private int SetGohoubi(int argSeatNo)
		{
			int gohoubi2Cnt;
			if (argSeatNo == JG2Importer.Common.MySaveData.Footer.SeatNoPlayer)
			{
				return 9;
			}
			int index = JG2Importer.Common.MySaveData.GetIndex(argSeatNo);
			int num = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			if (!(index == -1 | num == -1))
			{
				if (JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Cnt > 0)
				{
					bool flag = false;
					int gohoubi2Cnt1 = checked(JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Cnt - 1);
					gohoubi2Cnt = 0;
					while (gohoubi2Cnt <= gohoubi2Cnt1)
					{
						if (JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Data[gohoubi2Cnt].SeatNo != JG2Importer.Common.MySaveData.Footer.SeatNoPlayer)
						{
							gohoubi2Cnt = checked(gohoubi2Cnt + 1);
						}
						else
						{
							flag = true;
							break;
						}
					}
					if (flag)
					{
						return 1;
					}
				}
				int num1 = checked(argSeatNo * 100);
				DateTime now = DateAndTime.Now;
				byte millisecond = checked((byte)((checked(num1 + now.Millisecond)) % 256));
				JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Cnt = checked(JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Cnt + 1);
				gohoubi2Cnt = checked(JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Cnt - 1);
				JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Data = (Seat.DefStruct11[])Utils.CopyArray((Array)JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Data, new Seat.DefStruct11[checked(gohoubi2Cnt + 1)]);
				JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Data[gohoubi2Cnt].SeatNo = JG2Importer.Common.MySaveData.Footer.SeatNoPlayer;
				JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Data[gohoubi2Cnt].Syubetu = 2;
				JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Data[gohoubi2Cnt].Status = 1;
				JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Data[gohoubi2Cnt].ID = millisecond;
				byte[] numArray = new byte[] { 136, 32 };
				JG2Importer.Common.MySaveData.Seat[index].PlayData.Gohoubi2Data[gohoubi2Cnt].Filler = numArray;
				JG2Importer.Common.MySaveData.Seat[num].PlayData.Gohoubi1Cnt = checked(JG2Importer.Common.MySaveData.Seat[num].PlayData.Gohoubi1Cnt + 1);
				gohoubi2Cnt = checked(JG2Importer.Common.MySaveData.Seat[num].PlayData.Gohoubi1Cnt - 1);
				JG2Importer.Common.MySaveData.Seat[num].PlayData.Gohoubi1Data = (Seat.DefStruct11[])Utils.CopyArray((Array)JG2Importer.Common.MySaveData.Seat[num].PlayData.Gohoubi1Data, new Seat.DefStruct11[checked(gohoubi2Cnt + 1)]);
				JG2Importer.Common.MySaveData.Seat[num].PlayData.Gohoubi1Data[gohoubi2Cnt].SeatNo = argSeatNo;
				JG2Importer.Common.MySaveData.Seat[num].PlayData.Gohoubi1Data[gohoubi2Cnt].Syubetu = 2;
				JG2Importer.Common.MySaveData.Seat[num].PlayData.Gohoubi1Data[gohoubi2Cnt].Status = 1;
				JG2Importer.Common.MySaveData.Seat[num].PlayData.Gohoubi1Data[gohoubi2Cnt].ID = millisecond;
				numArray = new byte[] { 136, 32 };
				JG2Importer.Common.MySaveData.Seat[num].PlayData.Gohoubi1Data[gohoubi2Cnt].Filler = numArray;
			}
			return 0;
		}

		private bool SetHMax(int argSeatNo)
		{
			if (argSeatNo == JG2Importer.Common.MySaveData.Footer.SeatNoPlayer)
			{
				return false;
			}
			int index = JG2Importer.Common.MySaveData.GetIndex(argSeatNo);
			int num = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			if (!(index == -1 | num == -1))
			{
				JG2Importer.Common.MySaveData.Seat[index].Character.Attr.HAisyou[JG2Importer.Common.MySaveData.Footer.SeatNoPlayer] = 999;
				JG2Importer.Common.MySaveData.Seat[num].Character.Attr.HAisyou[argSeatNo] = 999;
			}
			return true;
		}

		private void tabBase_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.LockToolControl();
			this.mnuOpen.Enabled = NewLateBinding.LateGet(sender, null, "SelectedTab", new object[0], null, null, null) == this.tbpCostume;
			if (NewLateBinding.LateGet(sender, null, "SelectedTab", new object[0], null, null, null) != this.tbpTools)
			{
				this.SelectNextControl((Control)sender, true, true, true, true);
			}
			else if (this.SeatNo != JG2Importer.Common.MySaveData.Footer.SeatNoPlayer)
			{
				this.SelectNextControl(this.pnlPair, true, true, true, true);
			}
			else
			{
				this.chkKousai.SetBatchButton();
			}
		}
	}
}