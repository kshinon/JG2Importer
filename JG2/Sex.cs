using System;

namespace JG2Importer.JG2
{
	public enum Sex : byte
	{
		Male,
		Female
	}
}