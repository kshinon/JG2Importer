using System;

namespace JG2Importer.JG2
{
	public enum Flags : byte
	{
		OFF,
		ON
	}
}