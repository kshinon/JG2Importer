using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.IO;

namespace JG2Importer.JG2
{
	public class Seat
	{
		public byte Sex;

		public int SeatNo;

		public JG2Importer.JG2.Character Character;

		public Seat.DefPlayData PlayData;

		public Seat()
		{
			this.Clear();
		}

		public void Clear()
		{
			this.Sex = 0;
			this.SeatNo = 0;
			this.Character = new JG2Importer.JG2.Character();
			this.PlayData = new Seat.DefPlayData();
			Array.Resize<int>(ref this.PlayData.Renzoku, 25);
			Array.Resize<int>(ref this.PlayData.CntTodaySeikou, 25);
			Array.Resize<int>(ref this.PlayData.CntSeikou1, 25);
			Array.Resize<int>(ref this.PlayData.CntSeikou2, 25);
			Array.Resize<int>(ref this.PlayData.CntSeikou3, 25);
			Array.Resize<int>(ref this.PlayData.CntCondom, 25);
			Array.Resize<int>(ref this.PlayData.CntOrgasm, 25);
			Array.Resize<int>(ref this.PlayData.CntSynchronize, 25);
			Array.Resize<int>(ref this.PlayData.Syasei1, 25);
			Array.Resize<int>(ref this.PlayData.Syasei2, 25);
			Array.Resize<int>(ref this.PlayData.Syasei3, 25);
			Array.Resize<int>(ref this.PlayData.Syasei4, 25);
			Array.Resize<int>(ref this.PlayData.CntNakaKiken, 25);
		}

		public bool Export(string argPathName)
		{
			bool flag = false;
			if (!File.Exists(argPathName))
			{
				flag = this.Character.Update(argPathName);
			}
			else
			{
				JG2Importer.JG2.Character character = new JG2Importer.JG2.Character(argPathName);
				if (this.Character.Attr.Sex == character.Attr.Sex)
				{
					if (Operators.CompareString(this.Character.FullName, character.FullName, false) != 0)
					{
						string[] fullName = new string[] { Resources.MenuTxtExportSource + " [", this.Character.FullName, "]\r\n" + Resources.MenuTxtExportDest + " [", character.FullName, "]\r\n\r\n" + Resources.MenuTxtOverwriteQuestion };
						if (Interaction.MsgBox(string.Concat(fullName), MsgBoxStyle.YesNo | MsgBoxStyle.Critical | MsgBoxStyle.Question | MsgBoxStyle.Exclamation, null) != MsgBoxResult.Yes)
						{
							goto Label0;
						}
					}
					character.Replace(ref this.Character);
					flag = character.Update();
				}
				else
				{
					Interaction.MsgBox(Resources.ErrorMessage203, MsgBoxStyle.Critical, null);
				}
			Label0:
				character = null;
			}
			return flag;
		}

		public byte[] Get(int argVersion)
		{
			int i;
			byte[] numArray = null;
			JG2Importer.JG2.Common.ToRawData(ref this.Sex, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.SeatNo, ref numArray, true);
			byte[] koukando = this.Character.Get();
			JG2Importer.JG2.Common.ToRawData(ref koukando, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Filler01, ref numArray);
			koukando = this.GetKoukando();
			JG2Importer.JG2.Common.ToRawData(ref koukando, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Date1Cnt, ref numArray, true);
			if (this.PlayData.Date1Cnt > 0)
			{
				int date1Cnt = checked(this.PlayData.Date1Cnt - 1);
				for (i = 0; i <= date1Cnt; i = checked(i + 1))
				{
					JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Date1Data[i], ref numArray, true);
				}
			}
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Date2Cnt, ref numArray, true);
			if (this.PlayData.Date2Cnt > 0)
			{
				int date2Cnt = checked(this.PlayData.Date2Cnt - 1);
				for (i = 0; i <= date2Cnt; i = checked(i + 1))
				{
					JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Date2Data[i], ref numArray, true);
				}
			}
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Filler97, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Gohoubi1Cnt, ref numArray, true);
			if (this.PlayData.Gohoubi1Cnt > 0)
			{
				int gohoubi1Cnt = checked(this.PlayData.Gohoubi1Cnt - 1);
				for (i = 0; i <= gohoubi1Cnt; i = checked(i + 1))
				{
					Seat.DefStruct11[] gohoubi1Data = this.PlayData.Gohoubi1Data;
					int num = i;
					JG2Importer.JG2.Common.ToRawData(ref gohoubi1Data[num].SeatNo, ref numArray, true);
					JG2Importer.JG2.Common.ToRawData(ref gohoubi1Data[num].Syubetu, ref numArray, true);
					JG2Importer.JG2.Common.ToRawData(ref gohoubi1Data[num].Status, ref numArray);
					JG2Importer.JG2.Common.ToRawData(ref gohoubi1Data[num].ID, ref numArray);
					JG2Importer.JG2.Common.ToRawData(ref gohoubi1Data[num].Filler, ref numArray);
				}
			}
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Gohoubi2Cnt, ref numArray, true);
			if (this.PlayData.Gohoubi2Cnt > 0)
			{
				int gohoubi2Cnt = checked(this.PlayData.Gohoubi2Cnt - 1);
				for (i = 0; i <= gohoubi2Cnt; i = checked(i + 1))
				{
					Seat.DefStruct11[] gohoubi2Data = this.PlayData.Gohoubi2Data;
					int num1 = i;
					JG2Importer.JG2.Common.ToRawData(ref gohoubi2Data[num1].SeatNo, ref numArray, true);
					JG2Importer.JG2.Common.ToRawData(ref gohoubi2Data[num1].Syubetu, ref numArray, true);
					JG2Importer.JG2.Common.ToRawData(ref gohoubi2Data[num1].Status, ref numArray);
					JG2Importer.JG2.Common.ToRawData(ref gohoubi2Data[num1].ID, ref numArray);
					JG2Importer.JG2.Common.ToRawData(ref gohoubi2Data[num1].Filler, ref numArray);
				}
			}
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Filler99, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntKenka, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntSabori, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntSoudatu, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntKousai, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntFurareta, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Test1, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Test2, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Test3, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Rank1, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Rank2, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntTotalSeikou, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntSeikouAite, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Aite1, ref numArray, (long)32, false);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Aite2, ref numArray, (long)32, false);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Aite3, ref numArray, (long)32, false);
			koukando = this.GetPairData();
			JG2Importer.JG2.Common.ToRawData(ref koukando, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Filler98, ref numArray);
			koukando = this.GetDataSet16(argVersion);
			JG2Importer.JG2.Common.ToRawData(ref koukando, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Filler95, ref numArray);
			koukando = this.GetKousaiData();
			JG2Importer.JG2.Common.ToRawData(ref koukando, ref numArray);
			return numArray;
		}

		private byte[] GetDataSet16(int argVersion)
		{
			byte[] numArray = null;
			if (argVersion != 100)
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.DataSet16Cnt, ref numArray, true);
			}
			if (this.PlayData.DataSet16Cnt > 0)
			{
				byte[] numArray1 = null;
				int dataSet16Cnt = checked(this.PlayData.DataSet16Cnt - 1);
				for (int i = 0; i <= dataSet16Cnt; i = checked(i + 1))
				{
					Seat.DefStruct8[] dataSet16Data = this.PlayData.DataSet16Data;
					int num = i;
					JG2Importer.JG2.Common.ToRawData(ref dataSet16Data[num].DataSet1Cnt, ref numArray1, true);
					if (dataSet16Data[num].DataSet1Cnt > 0)
					{
						byte[] numArray2 = null;
						int dataSet1Cnt = checked(dataSet16Data[num].DataSet1Cnt - 1);
						for (int j = 0; j <= dataSet1Cnt; j = checked(j + 1))
						{
							Seat.DefStruct7[] dataSet1Data = dataSet16Data[num].DataSet1Data;
							int num1 = j;
							JG2Importer.JG2.Common.ToRawData(ref dataSet1Data[num1].Kouban, ref numArray2);
							JG2Importer.JG2.Common.ToRawData(ref dataSet1Data[num1].Data2, ref numArray2, true);
							JG2Importer.JG2.Common.ToRawData(ref dataSet1Data[num1].Data3, ref numArray2);
							JG2Importer.JG2.Common.ToRawData(ref dataSet1Data[num1].SeatNoCnt, ref numArray2, true);
							if (dataSet1Data[num1].SeatNoCnt > 0)
							{
								byte[] numArray3 = null;
								int seatNoCnt = checked(dataSet1Data[num1].SeatNoCnt - 1);
								for (int k = 0; k <= seatNoCnt; k = checked(k + 1))
								{
									JG2Importer.JG2.Common.ToRawData(ref dataSet1Data[num1].SeatNoData[k], ref numArray3);
								}
								JG2Importer.JG2.Common.ToRawData(ref numArray3, ref numArray2);
							}
						}
						JG2Importer.JG2.Common.ToRawData(ref numArray2, ref numArray1);
					}
				}
				JG2Importer.JG2.Common.ToRawData(ref numArray1, ref numArray);
			}
			return numArray;
		}

		private byte[] GetKoukando()
		{
			int j;
			byte[] numArray;
			byte[] numArray1 = null;
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.KoukandoCnt, ref numArray1, true);
			if (this.PlayData.KoukandoCnt > 0)
			{
				byte[] numArray2 = null;
				int koukandoCnt = checked(this.PlayData.KoukandoCnt - 1);
				for (int i = 0; i <= koukandoCnt; i = checked(i + 1))
				{
					Seat.DefStruct2[] koukandoData = this.PlayData.KoukandoData;
					int num = i;
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].SeatNo, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].PtsLove, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].PtsLike, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].PtsDislike, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].PtsHate, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].ActionCnt, ref numArray2, true);
					if (koukandoData[num].ActionCnt > 0)
					{
						numArray = null;
						int actionCnt = checked(koukandoData[num].ActionCnt - 1);
						for (j = 0; j <= actionCnt; j = checked(j + 1))
						{
							JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].ActionData[j], ref numArray, true);
						}
						JG2Importer.JG2.Common.ToRawData(ref numArray, ref numArray2);
					}
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].CntLove, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].CntLike, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].CntDislike, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].CntHate, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].DirLove, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].DirLike, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].DirDislike, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].DirHate, ref numArray2, true);
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].MaxActionCnt, ref numArray2, true);
					if (koukandoData[num].MaxActionCnt > 0)
					{
						numArray = null;
						int maxActionCnt = checked(koukandoData[num].MaxActionCnt - 1);
						for (j = 0; j <= maxActionCnt; j = checked(j + 1))
						{
							JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].MaxActionData[j], ref numArray, true);
						}
						JG2Importer.JG2.Common.ToRawData(ref numArray, ref numArray2);
					}
					JG2Importer.JG2.Common.ToRawData(ref koukandoData[num].MaxAction, ref numArray2, true);
				}
				JG2Importer.JG2.Common.ToRawData(ref numArray2, ref numArray1);
			}
			return numArray1;
		}

		private byte[] GetKousaiData()
		{
			byte[] numArray = null;
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.KousaiCnt, ref numArray, true);
			if (this.PlayData.KousaiCnt > 0)
			{
				int kousaiCnt = checked(this.PlayData.KousaiCnt - 1);
				for (int i = 0; i <= kousaiCnt; i = checked(i + 1))
				{
					JG2Importer.JG2.Common.ToRawData(ref this.PlayData.KousaiData[i].Status, ref numArray);
					JG2Importer.JG2.Common.ToRawData(ref this.PlayData.KousaiData[i].Nissu, ref numArray, true);
				}
			}
			return numArray;
		}

		private byte[] GetPairData()
		{
			byte[] numArray = null;
			int num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Renzoku[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntTodaySeikou[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntSeikou1[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntSeikou2[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntSeikou3[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntCondom[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntOrgasm[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntSynchronize[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Syasei1[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Syasei2[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Syasei3[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Syasei4[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			JG2Importer.JG2.Common.ToRawData(ref this.PlayData.Filler96, ref numArray);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.PlayData.CntNakaKiken[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			return numArray;
		}

		public void Import(ref JG2Importer.JG2.Character argImportCharacter)
		{
			byte sex = this.Character.Attr.Sex;
			this.Sex = argImportCharacter.Attr.Sex;
			this.Character.Replace(ref argImportCharacter);
			if (sex != argImportCharacter.Attr.Sex)
			{
				int num = 0;
				do
				{
					this.Character.Attr.Costume[num] = argImportCharacter.Attr.Costume[num];
					num = checked(num + 1);
				}
				while (num <= 3);
			}
		}

		public void ReCalc()
		{
			this.Sex = this.Character.Attr.Sex;
			this.Character.ReCalc();
		}

		public long Set(ref byte[] argData, long argStartIndex, int argVersion)
		{
			int i;
			int j;
			long dWord = this.Character.Set(ref argData, argStartIndex);
			if (dWord == (long)-1)
			{
				return (long)-1;
			}
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.Character.IxStart - (long)5), ref this.Sex);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.Character.IxStart - (long)4), ref this.SeatNo, true);
			long num = dWord;
			dWord = checked(checked(dWord + (long)4) + (long)(checked(JG2Importer.JG2.Common.ToDWord(ref argData, dWord, true) * 4)));
			dWord = checked(checked(dWord + (long)4) + (long)(checked(JG2Importer.JG2.Common.ToDWord(ref argData, dWord, true) * 4)));
			JG2Importer.JG2.Common.FromRawData(ref argData, num, ref this.PlayData.Filler01, checked(dWord - num));
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.KoukandoCnt, true);
			dWord = checked(dWord + (long)4);
			if (this.PlayData.KoukandoCnt > 0)
			{
				this.PlayData.KoukandoData = (Seat.DefStruct2[])Utils.CopyArray((Array)this.PlayData.KoukandoData, new Seat.DefStruct2[checked(checked(this.PlayData.KoukandoCnt - 1) + 1)]);
				int koukandoCnt = checked(this.PlayData.KoukandoCnt - 1);
				for (i = 0; i <= koukandoCnt; i = checked(i + 1))
				{
					Seat.DefStruct2[] koukandoData = this.PlayData.KoukandoData;
					int num1 = i;
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].SeatNo, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].PtsLove, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].PtsLike, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].PtsDislike, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].PtsHate, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].ActionCnt, true);
					dWord = checked(dWord + (long)4);
					if (koukandoData[num1].ActionCnt > 0)
					{
						koukandoData[num1].ActionData = (int[])Utils.CopyArray((Array)koukandoData[num1].ActionData, new int[checked(checked(koukandoData[num1].ActionCnt - 1) + 1)]);
						int actionCnt = checked(koukandoData[num1].ActionCnt - 1);
						for (j = 0; j <= actionCnt; j = checked(j + 1))
						{
							JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].ActionData[j], true);
							dWord = checked(dWord + (long)4);
						}
					}
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].CntLove, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].CntLike, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].CntDislike, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].CntHate, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].DirLove, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].DirLike, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].DirDislike, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].DirHate, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].MaxActionCnt, true);
					dWord = checked(dWord + (long)4);
					if (koukandoData[num1].MaxActionCnt > 0)
					{
						koukandoData[num1].MaxActionData = (int[])Utils.CopyArray((Array)koukandoData[num1].MaxActionData, new int[checked(checked(koukandoData[num1].MaxActionCnt - 1) + 1)]);
						int maxActionCnt = checked(koukandoData[num1].MaxActionCnt - 1);
						for (j = 0; j <= maxActionCnt; j = checked(j + 1))
						{
							JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].MaxActionData[j], true);
							dWord = checked(dWord + (long)4);
						}
					}
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref koukandoData[num1].MaxAction, true);
					dWord = checked(dWord + (long)4);
				}
			}
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Date1Cnt, true);
			dWord = checked(dWord + (long)4);
			if (this.PlayData.Date1Cnt > 0)
			{
				this.PlayData.Date1Data = (int[])Utils.CopyArray((Array)this.PlayData.Date1Data, new int[checked(checked(this.PlayData.Date1Cnt - 1) + 1)]);
				int date1Cnt = checked(this.PlayData.Date1Cnt - 1);
				for (i = 0; i <= date1Cnt; i = checked(i + 1))
				{
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Date1Data[i], true);
					dWord = checked(dWord + (long)4);
				}
			}
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Date2Cnt, true);
			dWord = checked(dWord + (long)4);
			if (this.PlayData.Date2Cnt > 0)
			{
				this.PlayData.Date2Data = (int[])Utils.CopyArray((Array)this.PlayData.Date2Data, new int[checked(checked(this.PlayData.Date2Cnt - 1) + 1)]);
				int date2Cnt = checked(this.PlayData.Date2Cnt - 1);
				for (i = 0; i <= date2Cnt; i = checked(i + 1))
				{
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Date2Data[i], true);
					dWord = checked(dWord + (long)4);
				}
			}
			long num2 = dWord;
			int dWord1 = JG2Importer.JG2.Common.ToDWord(ref argData, dWord, true);
			dWord = checked(dWord + (long)4);
			int num3 = dWord1;
			for (i = 1; i <= num3; i = checked(i + 1))
			{
				dWord = checked(dWord + (long)4);
				dWord = checked(checked(dWord + (long)4) + (long)(checked(checked(JG2Importer.JG2.Common.ToDWord(ref argData, dWord, true) * 4) * 5)));
			}
			dWord1 = JG2Importer.JG2.Common.ToDWord(ref argData, dWord, true);
			dWord = checked(dWord + (long)4);
			int num4 = dWord1;
			for (i = 1; i <= num4; i = checked(i + 1))
			{
				dWord = checked(dWord + (long)4);
				dWord = checked(checked(dWord + (long)4) + (long)(checked(checked(JG2Importer.JG2.Common.ToDWord(ref argData, dWord, true) * 4) * 5)));
			}
			JG2Importer.JG2.Common.FromRawData(ref argData, num2, ref this.PlayData.Filler97, checked(dWord - num2));
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Gohoubi1Cnt, true);
			dWord = checked(dWord + (long)4);
			if (this.PlayData.Gohoubi1Cnt > 0)
			{
				this.PlayData.Gohoubi1Data = (Seat.DefStruct11[])Utils.CopyArray((Array)this.PlayData.Gohoubi1Data, new Seat.DefStruct11[checked(checked(this.PlayData.Gohoubi1Cnt - 1) + 1)]);
				int gohoubi1Cnt = checked(this.PlayData.Gohoubi1Cnt - 1);
				for (i = 0; i <= gohoubi1Cnt; i = checked(i + 1))
				{
					Seat.DefStruct11[] gohoubi1Data = this.PlayData.Gohoubi1Data;
					int num5 = i;
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref gohoubi1Data[num5].SeatNo, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref gohoubi1Data[num5].Syubetu, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref gohoubi1Data[num5].Status);
					dWord = checked(dWord + (long)1);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref gohoubi1Data[num5].ID);
					dWord = checked(dWord + (long)1);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref gohoubi1Data[num5].Filler, (long)2);
					dWord = checked(dWord + (long)2);
				}
			}
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Gohoubi2Cnt, true);
			dWord = checked(dWord + (long)4);
			if (this.PlayData.Gohoubi2Cnt > 0)
			{
				this.PlayData.Gohoubi2Data = (Seat.DefStruct11[])Utils.CopyArray((Array)this.PlayData.Gohoubi2Data, new Seat.DefStruct11[checked(checked(this.PlayData.Gohoubi2Cnt - 1) + 1)]);
				int gohoubi2Cnt = checked(this.PlayData.Gohoubi2Cnt - 1);
				for (i = 0; i <= gohoubi2Cnt; i = checked(i + 1))
				{
					Seat.DefStruct11[] gohoubi2Data = this.PlayData.Gohoubi2Data;
					int num6 = i;
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref gohoubi2Data[num6].SeatNo, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref gohoubi2Data[num6].Syubetu, true);
					dWord = checked(dWord + (long)4);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref gohoubi2Data[num6].Status);
					dWord = checked(dWord + (long)1);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref gohoubi2Data[num6].ID);
					dWord = checked(dWord + (long)1);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref gohoubi2Data[num6].Filler, (long)2);
					dWord = checked(dWord + (long)2);
				}
			}
			long num7 = dWord;
			dWord1 = JG2Importer.JG2.Common.ToDWord(ref argData, dWord, true);
			dWord = checked(dWord + (long)4);
			int num8 = dWord1;
			for (i = 1; i <= num8; i = checked(i + 1))
			{
				dWord = checked(dWord + (long)21);
			}
			dWord1 = JG2Importer.JG2.Common.ToDWord(ref argData, dWord, true);
			dWord = checked(dWord + (long)4);
			int num9 = dWord1;
			for (i = 1; i <= num9; i = checked(i + 1))
			{
				dWord = checked(dWord + (long)21);
			}
			dWord1 = JG2Importer.JG2.Common.ToDWord(ref argData, dWord, true);
			dWord = checked(dWord + (long)4);
			int num10 = dWord1;
			for (i = 1; i <= num10; i = checked(i + 1))
			{
				dWord = checked(dWord + (long)21);
			}
			dWord1 = JG2Importer.JG2.Common.ToDWord(ref argData, dWord, true);
			dWord = checked(dWord + (long)4);
			int num11 = dWord1;
			for (i = 1; i <= num11; i = checked(i + 1))
			{
				dWord = checked(dWord + (long)21);
			}
			JG2Importer.JG2.Common.FromRawData(ref argData, num7, ref this.PlayData.Filler99, checked(dWord - num7));
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntKenka, true);
			dWord = checked(dWord + (long)4);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntSabori, true);
			dWord = checked(dWord + (long)4);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntSoudatu, true);
			dWord = checked(dWord + (long)4);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntKousai, true);
			dWord = checked(dWord + (long)4);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntFurareta, true);
			dWord = checked(dWord + (long)4);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Test1, true);
			dWord = checked(dWord + (long)4);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Test2, true);
			dWord = checked(dWord + (long)4);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Test3, true);
			dWord = checked(dWord + (long)4);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Rank1, true);
			dWord = checked(dWord + (long)4);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Rank2, true);
			dWord = checked(dWord + (long)4);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntTotalSeikou, true);
			dWord = checked(dWord + (long)4);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntSeikouAite, true);
			dWord = checked(dWord + (long)4);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Aite1, (long)32, false);
			dWord = checked(dWord + (long)32);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Aite2, (long)32, false);
			dWord = checked(dWord + (long)32);
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Aite3, (long)32, false);
			dWord = checked(dWord + (long)32);
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Renzoku[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntTodaySeikou[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntSeikou1[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntSeikou2[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntSeikou3[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntCondom[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntOrgasm[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntSynchronize[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Syasei1[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Syasei2[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Syasei3[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.Syasei4[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			long num12 = dWord;
			dWord = checked(dWord + (long)100);
			JG2Importer.JG2.Common.FromRawData(ref argData, num12, ref this.PlayData.Filler96, checked(dWord - num12));
			i = 0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.CntNakaKiken[i], true);
				dWord = checked(dWord + (long)4);
				i = checked(i + 1);
			}
			while (i <= 24);
			long num13 = dWord;
			dWord = checked(dWord + (long)100);
			dWord = checked(dWord + (long)100);
			dWord = checked(dWord + (long)100);
			JG2Importer.JG2.Common.FromRawData(ref argData, num13, ref this.PlayData.Filler98, checked(dWord - num13));
			if (argVersion != 100)
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.DataSet16Cnt, true);
				dWord = checked(dWord + (long)4);
			}
			else
			{
				this.PlayData.DataSet16Cnt = 75;
			}
			if (this.PlayData.DataSet16Cnt > 0)
			{
				this.PlayData.DataSet16Data = (Seat.DefStruct8[])Utils.CopyArray((Array)this.PlayData.DataSet16Data, new Seat.DefStruct8[checked(checked(this.PlayData.DataSet16Cnt - 1) + 1)]);
				int dataSet16Cnt = checked(this.PlayData.DataSet16Cnt - 1);
				for (i = 0; i <= dataSet16Cnt; i = checked(i + 1))
				{
					Seat.DefStruct8[] dataSet16Data = this.PlayData.DataSet16Data;
					int num14 = i;
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref dataSet16Data[num14].DataSet1Cnt, true);
					dWord = checked(dWord + (long)4);
					if (dataSet16Data[num14].DataSet1Cnt > 0)
					{
						dataSet16Data[num14].DataSet1Data = (Seat.DefStruct7[])Utils.CopyArray((Array)dataSet16Data[num14].DataSet1Data, new Seat.DefStruct7[checked(checked(dataSet16Data[num14].DataSet1Cnt - 1) + 1)]);
						int dataSet1Cnt = checked(dataSet16Data[num14].DataSet1Cnt - 1);
						for (j = 0; j <= dataSet1Cnt; j = checked(j + 1))
						{
							Seat.DefStruct7[] dataSet1Data = dataSet16Data[num14].DataSet1Data;
							int num15 = j;
							JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref dataSet1Data[num15].Kouban);
							dWord = checked(dWord + (long)1);
							JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref dataSet1Data[num15].Data2, true);
							dWord = checked(dWord + (long)4);
							JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref dataSet1Data[num15].Data3);
							dWord = checked(dWord + (long)1);
							JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref dataSet1Data[num15].SeatNoCnt, true);
							dWord = checked(dWord + (long)2);
							if (dataSet1Data[num15].SeatNoCnt > 0)
							{
								dataSet1Data[num15].SeatNoData = (byte[])Utils.CopyArray((Array)dataSet1Data[num15].SeatNoData, new byte[checked(checked(dataSet1Data[num15].SeatNoCnt - 1) + 1)]);
								int seatNoCnt = checked(dataSet1Data[num15].SeatNoCnt - 1);
								for (int k = 0; k <= seatNoCnt; k = checked(k + 1))
								{
									JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref dataSet1Data[num15].SeatNoData[k]);
									dWord = checked(dWord + (long)1);
								}
							}
						}
					}
				}
			}
			long num16 = dWord;
			dWord = checked(checked(dWord + (long)4) + (long)(checked(JG2Importer.JG2.Common.ToDWord(ref argData, dWord, true) * 89)));
			JG2Importer.JG2.Common.FromRawData(ref argData, num16, ref this.PlayData.Filler95, checked(dWord - num16));
			JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.KousaiCnt, true);
			dWord = checked(dWord + (long)4);
			if (this.PlayData.KousaiCnt > 0)
			{
				this.PlayData.KousaiData = (Seat.DefStruct10[])Utils.CopyArray((Array)this.PlayData.KousaiData, new Seat.DefStruct10[checked(checked(this.PlayData.KousaiCnt - 1) + 1)]);
				int kousaiCnt = checked(this.PlayData.KousaiCnt - 1);
				for (i = 0; i <= kousaiCnt; i = checked(i + 1))
				{
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.KousaiData[i].Status);
					dWord = checked(dWord + (long)1);
					JG2Importer.JG2.Common.FromRawData(ref argData, dWord, ref this.PlayData.KousaiData[i].Nissu, true);
					dWord = checked(dWord + (long)4);
				}
			}
			return dWord;
		}

		public struct DefPlayData
		{
			public byte[] Filler01;

			public int KoukandoCnt;

			public Seat.DefStruct2[] KoukandoData;

			public int Date1Cnt;

			public int[] Date1Data;

			public int Date2Cnt;

			public int[] Date2Data;

			public byte[] Filler97;

			public int Gohoubi1Cnt;

			public Seat.DefStruct11[] Gohoubi1Data;

			public int Gohoubi2Cnt;

			public Seat.DefStruct11[] Gohoubi2Data;

			public byte[] Filler99;

			public int CntKenka;

			public int CntSabori;

			public int CntSoudatu;

			public int CntKousai;

			public int CntFurareta;

			public int Test1;

			public int Test2;

			public int Test3;

			public int Rank1;

			public int Rank2;

			public int CntTotalSeikou;

			public int CntSeikouAite;

			public string Aite1;

			public string Aite2;

			public string Aite3;

			public int[] Renzoku;

			public int[] CntTodaySeikou;

			public int[] CntSeikou1;

			public int[] CntSeikou2;

			public int[] CntSeikou3;

			public int[] CntCondom;

			public int[] CntOrgasm;

			public int[] CntSynchronize;

			public int[] Syasei1;

			public int[] Syasei2;

			public int[] Syasei3;

			public int[] Syasei4;

			public byte[] Filler96;

			public int[] CntNakaKiken;

			public byte[] Filler98;

			public int DataSet16Cnt;

			public Seat.DefStruct8[] DataSet16Data;

			public byte[] Filler95;

			public int KousaiCnt;

			public Seat.DefStruct10[] KousaiData;
		}

		public struct DefStruct10
		{
			public byte Status;

			public int Nissu;
		}

		public struct DefStruct11
		{
			public int SeatNo;

			public int Syubetu;

			public byte Status;

			public byte ID;

			public byte[] Filler;
		}

		public struct DefStruct2
		{
			public int SeatNo;

			public int PtsLove;

			public int PtsLike;

			public int PtsDislike;

			public int PtsHate;

			public int ActionCnt;

			public int[] ActionData;

			public int CntLove;

			public int CntLike;

			public int CntDislike;

			public int CntHate;

			public int DirLove;

			public int DirLike;

			public int DirDislike;

			public int DirHate;

			public int MaxActionCnt;

			public int[] MaxActionData;

			public int MaxAction;
		}

		public struct DefStruct7
		{
			public byte Kouban;

			public int Data2;

			public byte Data3;

			public short SeatNoCnt;

			public byte[] SeatNoData;
		}

		public struct DefStruct8
		{
			public int DataSet1Cnt;

			public Seat.DefStruct7[] DataSet1Data;
		}
	}
}