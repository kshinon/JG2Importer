using System;

namespace JG2Importer.JG2
{
	public enum Mune : byte
	{
		Slender,
		Normal,
		Grammar
	}
}