using Microsoft.VisualBasic;
using System;
using System.IO;

namespace JG2Importer.JG2
{
	public class Cloth
	{
		public FileInfo DataFile;

		public Cloth.DefAttribute Attr;

		public Cloth()
		{
			this.Clear();
		}

		public Cloth(string argFileName)
		{
			this.Clear();
			this.DataFile = new FileInfo(argFileName);
			FileStream fileStream = new FileStream(argFileName, FileMode.Open, FileAccess.Read, FileShare.Read);
			if (!fileStream.CanRead)
			{
				return;
			}
			byte[] numArray = null;
			Array.Resize<byte>(ref numArray, checked((int)fileStream.Length));
			long num = (long)0;
			long length = checked(fileStream.Length - (long)1);
			for (long i = num; i <= length; i = checked(i + (long)1))
			{
				numArray[checked((int)i)] = checked((byte)fileStream.ReadByte());
			}
			fileStream.Close();
			fileStream.Dispose();
			fileStream = null;
			this.Set(ref numArray);
			numArray = null;
		}

		public void Clear()
		{
			this.DataFile = null;
			this.Attr = new Cloth.DefAttribute();
		}

		public byte[] Get()
		{
			byte[] numArray = null;
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Sex, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Type, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Skirt, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Socks, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Utibaki, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Sotobaki, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.FlagJyoge, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.FlagSitagi, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.FlagSkirt, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorTop1, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorTop2, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorTop3, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorTop4, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorBottom1, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorBottom2, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorSitagi, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorSocks, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorUtibaki, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorSotobaki, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Bottom1, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Sitagi, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Bottom1Sitaji1, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Bottom1Sitaji2, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.SitagiSitaji1, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.SitagiSitaji2, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Bottom1Kage1, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Bottom1Kage2, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.SitagiKage1, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.SitagiKage2, ref numArray, true);
			return numArray;
		}

		public void Set(ref byte[] argData)
		{
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)0, ref this.Attr.Sex);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)1, ref this.Attr.Type, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)5, ref this.Attr.Skirt);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)6, ref this.Attr.Socks);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)7, ref this.Attr.Utibaki);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)8, ref this.Attr.Sotobaki);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)9, ref this.Attr.FlagJyoge);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)10, ref this.Attr.FlagSitagi);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)11, ref this.Attr.FlagSkirt);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)12, ref this.Attr.ColorTop1, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)16, ref this.Attr.ColorTop2, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)20, ref this.Attr.ColorTop3, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)24, ref this.Attr.ColorTop4, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)28, ref this.Attr.ColorBottom1, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)32, ref this.Attr.ColorBottom2, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)36, ref this.Attr.ColorSitagi, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)40, ref this.Attr.ColorSocks, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)44, ref this.Attr.ColorUtibaki, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)48, ref this.Attr.ColorSotobaki, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)52, ref this.Attr.Bottom1, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)56, ref this.Attr.Sitagi, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)60, ref this.Attr.Bottom1Sitaji1, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)64, ref this.Attr.Bottom1Sitaji2, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)68, ref this.Attr.SitagiSitaji1, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)72, ref this.Attr.SitagiSitaji2, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)76, ref this.Attr.Bottom1Kage1, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)80, ref this.Attr.Bottom1Kage2, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)84, ref this.Attr.SitagiKage1, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)88, ref this.Attr.SitagiKage2, true);
		}

		public bool Update()
		{
			if (Information.IsNothing(this.DataFile))
			{
				return false;
			}
			return this.Update(this.DataFile.FullName);
		}

		public bool Update(string argNewPathName)
		{
			byte[] numArray = this.Get();
			FileStream fileStream = new FileStream(argNewPathName, FileMode.Create, FileAccess.Write, FileShare.Write);
			long num = (long)0;
			long length = (long)(checked(checked((int)numArray.Length) - 1));
			for (long i = num; i <= length; i = checked(i + (long)1))
			{
				fileStream.WriteByte(numArray[checked((int)i)]);
			}
			fileStream.Close();
			fileStream.Dispose();
			fileStream = null;
			numArray = null;
			return true;
		}

		public struct DefAttribute
		{
			public byte Sex;

			public int Type;

			public byte Skirt;

			public byte Socks;

			public byte Utibaki;

			public byte Sotobaki;

			public byte FlagJyoge;

			public byte FlagSitagi;

			public byte FlagSkirt;

			public int ColorTop1;

			public int ColorTop2;

			public int ColorTop3;

			public int ColorTop4;

			public int ColorBottom1;

			public int ColorBottom2;

			public int ColorSitagi;

			public int ColorSocks;

			public int ColorUtibaki;

			public int ColorSotobaki;

			public int Bottom1;

			public int Sitagi;

			public int Bottom1Sitaji1;

			public int Bottom1Sitaji2;

			public int SitagiSitaji1;

			public int SitagiSitaji2;

			public int Bottom1Kage1;

			public int Bottom1Kage2;

			public int SitagiKage1;

			public int SitagiKage2;
		}
	}
}