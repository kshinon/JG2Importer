using System;

namespace JG2Importer.JG2
{
	public enum CodeAction : byte
	{
		None,
		Love,
		Like,
		Dislike,
		Hate
	}
}