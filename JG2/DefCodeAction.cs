using System;

namespace JG2Importer.JG2
{
	public struct DefCodeAction
	{
		public JG2Importer.JG2.CodeAction CodeAction;

		public short CntLove;

		public short CntLike;

		public short CntDislike;

		public short CntHate;

		public short MaxAction;
	}
}