using System;

namespace JG2Importer.JG2
{
	public enum CodeCntNakaKiken : byte
	{
		None,
		Safe,
		Danger
	}
}