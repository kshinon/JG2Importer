using System;

namespace JG2Importer.JG2
{
	public enum CodeKousai : byte
	{
		ClassMates,
		Lover,
		Former
	}
}