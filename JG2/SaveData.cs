using Microsoft.VisualBasic;
using System;
using System.IO;

namespace JG2Importer.JG2
{
	public class SaveData
	{
		public FileInfo DataFile;

		public SaveData.DefHeader Header;

		public JG2Importer.JG2.Seat[] Seat;

		public SaveData.DefFooter Footer;

		public string SchoolName
		{
			get
			{
				string[] text = new string[] { JG2Importer.JG2.Common.ToText(ref this.Header.ClassName, false), "学園", Strings.Space(1), JG2Importer.JG2.Common.ToText(ref this.Header.Nen, false), "年", Strings.Space(1), JG2Importer.JG2.Common.ToText(ref this.Header.Kumi, false), "組" };
				return string.Concat(text);
			}
		}

		public SaveData()
		{
			this.Clear();
		}

		public SaveData(string argFileName)
		{
			this.Clear();
			this.DataFile = new FileInfo(argFileName);
			FileStream fileStream = new FileStream(argFileName, FileMode.Open, FileAccess.Read, FileShare.Read);
			if (!fileStream.CanRead)
			{
				return;
			}
			byte[] numArray = null;
			Array.Resize<byte>(ref numArray, checked((int)fileStream.Length));
			long num = (long)0;
			long length = checked(fileStream.Length - (long)1);
			for (long i = num; i <= length; i = checked(i + (long)1))
			{
				numArray[checked((int)i)] = checked((byte)fileStream.ReadByte());
			}
			fileStream.Close();
			fileStream.Dispose();
			fileStream = null;
			this.Set(ref numArray);
			numArray = null;
		}

		public void Clear()
		{
			this.DataFile = null;
			this.Header = new SaveData.DefHeader();
			Array.Resize<SaveData.DefBukatu>(ref this.Header.Bukatu, 8);
			this.Seat = null;
			this.Footer = new SaveData.DefFooter();
		}

		public byte[] Get()
		{
			byte[] numArray = null;
			JG2Importer.JG2.Common.ToRawData(ref this.Header.Version, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Header.ClassName, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Header.Nen, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Header.Kumi, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Header.Ninsu1, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Header.Ninsu2, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Header.Hour, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Header.Minute, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Header.Second, ref numArray, true);
			int i = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.Header.Bukatu[i].Name, ref numArray, (long)24, false);
				JG2Importer.JG2.Common.ToRawData(ref this.Header.Bukatu[i].Syubetu, ref numArray);
				i = checked(i + 1);
			}
			while (i <= 7);
			if (this.Header.Version != 100)
			{
				JG2Importer.JG2.Common.ToRawData(ref this.Header.Filler99, ref numArray);
			}
			JG2Importer.JG2.Common.ToRawData(ref this.Header.Days, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Header.Week, ref numArray);
			int length = checked(checked((int)this.Seat.Length) - 1);
			for (i = 0; i <= length; i = checked(i + 1))
			{
				byte[] numArray1 = this.Seat[i].Get(this.Header.Version);
				JG2Importer.JG2.Common.ToRawData(ref numArray1, ref numArray);
			}
			JG2Importer.JG2.Common.ToRawData(ref this.Footer.Filler, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Footer.SeatNoPlayer, ref numArray, true);
			return numArray;
		}

		public DefCodeAction GetCodeAction(int argFromIndex, int argToIndex)
		{
			DefCodeAction cntLove = new DefCodeAction()
			{
				CodeAction = CodeAction.None
			};
			int koukandoCnt = checked(this.Seat[argFromIndex].PlayData.KoukandoCnt - 1);
			int num = 0;
			while (num <= koukandoCnt)
			{
				JG2Importer.JG2.Seat.DefStruct2[] koukandoData = this.Seat[argFromIndex].PlayData.KoukandoData;
				int num1 = num;
				if (koukandoData[num1].SeatNo != this.Seat[argToIndex].SeatNo)
				{
					num = checked(num + 1);
				}
				else
				{
					if (koukandoData[num1].ActionCnt == 0)
					{
						break;
					}
					cntLove.CntLove = checked((short)koukandoData[num1].CntLove);
					cntLove.CntLike = checked((short)koukandoData[num1].CntLike);
					cntLove.CntDislike = checked((short)koukandoData[num1].CntDislike);
					cntLove.CntHate = checked((short)koukandoData[num1].CntHate);
					cntLove.CodeAction = CodeAction.Love;
					cntLove.MaxAction = checked((short)koukandoData[num1].CntLove);
					if (cntLove.MaxAction <= koukandoData[num1].CntLike)
					{
						cntLove.CodeAction = CodeAction.Like;
						cntLove.MaxAction = checked((short)koukandoData[num1].CntLike);
					}
					if (cntLove.MaxAction <= koukandoData[num1].CntDislike)
					{
						cntLove.CodeAction = CodeAction.Dislike;
						cntLove.MaxAction = checked((short)koukandoData[num1].CntDislike);
					}
					if (cntLove.MaxAction <= koukandoData[num1].CntHate)
					{
						cntLove.CodeAction = CodeAction.Hate;
						cntLove.MaxAction = checked((short)koukandoData[num1].CntHate);
					}
					break;
				}
			}
			return cntLove;
		}

		public CodeCntNakaKiken GetCodeCntNakaKiken(int argFromIndex, int argToIndex)
		{
			CodeCntNakaKiken codeCntNakaKiken = CodeCntNakaKiken.None;
			if (this.Seat[argFromIndex].Character.Attr.Sex != 1)
			{
				codeCntNakaKiken = CodeCntNakaKiken.None;
			}
			else
			{
				codeCntNakaKiken = (this.Seat[argFromIndex].PlayData.CntNakaKiken[this.Seat[argToIndex].SeatNo] != 0 ? CodeCntNakaKiken.Danger : CodeCntNakaKiken.Safe);
			}
			return codeCntNakaKiken;
		}

		public CodeCntTodaySeikou GetCodeCntTodaySeikou(int argFromIndex, int argToIndex)
		{
			CodeCntTodaySeikou codeCntTodaySeikou = CodeCntTodaySeikou.None;
			if (this.Seat[argFromIndex].PlayData.CntTodaySeikou[this.Seat[argToIndex].SeatNo] > 0)
			{
				codeCntTodaySeikou = CodeCntTodaySeikou.Play;
			}
			return codeCntTodaySeikou;
		}

		public CodeDate GetCodeDate(int argFromIndex, int argToIndex)
		{
			int num;
			CodeDate codeDate = CodeDate.None;
			if (this.Seat[argFromIndex].PlayData.Date1Cnt > 0)
			{
				int date1Cnt = checked(this.Seat[argFromIndex].PlayData.Date1Cnt - 1);
				num = 0;
				while (num <= date1Cnt)
				{
					if (this.Seat[argFromIndex].PlayData.Date1Data[num] != this.Seat[argToIndex].SeatNo)
					{
						num = checked(num + 1);
					}
					else
					{
						codeDate = CodeDate.Date;
						break;
					}
				}
			}
			if (this.Seat[argFromIndex].PlayData.Date2Cnt > 0)
			{
				int date2Cnt = checked(this.Seat[argFromIndex].PlayData.Date2Cnt - 1);
				num = 0;
				while (num <= date2Cnt)
				{
					if (this.Seat[argFromIndex].PlayData.Date2Data[num] != this.Seat[argToIndex].SeatNo)
					{
						num = checked(num + 1);
					}
					else
					{
						codeDate = CodeDate.Date;
						break;
					}
				}
			}
			return codeDate;
		}

		public Flags GetCodeFormer(int argFromIndex, int argToIndex)
		{
			Flags flag = Flags.OFF;
			JG2Importer.JG2.Seat.DefStruct8[] dataSet16Data = this.Seat[argFromIndex].PlayData.DataSet16Data;
			int num = 6;
			int dataSet1Cnt = checked(dataSet16Data[num].DataSet1Cnt - 1);
			for (int i = 0; i <= dataSet1Cnt; i = checked(i + 1))
			{
				JG2Importer.JG2.Seat.DefStruct7[] dataSet1Data = dataSet16Data[num].DataSet1Data;
				int num1 = i;
				if (dataSet1Data[num1].Kouban == 6)
				{
					int seatNoCnt = checked(dataSet1Data[num1].SeatNoCnt - 1);
					int num2 = 0;
					while (num2 <= seatNoCnt)
					{
						if (dataSet1Data[num1].SeatNoData[num2] != this.Seat[argToIndex].SeatNo)
						{
							num2 = checked(num2 + 1);
						}
						else
						{
							flag = Flags.ON;
							break;
						}
					}
					if (flag == Flags.ON)
					{
						break;
					}
				}
			}
			return flag;
		}

		public CodeGohoubi GetCodeGohoubi(int argFromIndex, int argToIndex)
		{
			CodeGohoubi codeGohoubi = CodeGohoubi.None;
			if (this.Seat[argFromIndex].PlayData.Gohoubi2Cnt > 0)
			{
				int gohoubi2Cnt = checked(this.Seat[argFromIndex].PlayData.Gohoubi2Cnt - 1);
				int num = 0;
				while (num <= gohoubi2Cnt)
				{
					if (this.Seat[argFromIndex].PlayData.Gohoubi2Data[num].SeatNo != this.Seat[argToIndex].SeatNo)
					{
						num = checked(num + 1);
					}
					else
					{
						switch (this.Seat[argFromIndex].PlayData.Gohoubi2Data[num].Status)
						{
							case 0:
							{
								codeGohoubi = CodeGohoubi.Yakusoku;
								break;
							}
							case 1:
							{
								codeGohoubi = CodeGohoubi.Gohoubi;
								break;
							}
						}
					}
				}
			}
			return codeGohoubi;
		}

		public CodeHAisyou GetCodeHAisyou(int argFromIndex, int argToIndex)
		{
			CodeHAisyou codeHAisyou = CodeHAisyou.None;
			int hAisyou = this.Seat[argFromIndex].Character.Attr.HAisyou[this.Seat[argToIndex].SeatNo];
			if (hAisyou == 0)
			{
				codeHAisyou = CodeHAisyou.None;
			}
			else if (hAisyou >= 1 && hAisyou <= 99)
			{
				codeHAisyou = CodeHAisyou.Subtle;
			}
			else if (hAisyou >= 100 && hAisyou <= 199)
			{
				codeHAisyou = CodeHAisyou.Normal;
			}
			else if (hAisyou < 200 || hAisyou > 399)
			{
				codeHAisyou = (hAisyou < 400 || hAisyou > 699 ? CodeHAisyou.Perfect : CodeHAisyou.Maximum);
			}
			else
			{
				codeHAisyou = CodeHAisyou.Good;
			}
			return codeHAisyou;
		}

		public CodeKousai GetCodeKousai(int argFromIndex, int argToIndex)
		{
			CodeKousai codeKousai = CodeKousai.ClassMates;
			if (this.Seat[argFromIndex].PlayData.KousaiData[this.Seat[argToIndex].SeatNo].Status != 1)
			{
				codeKousai = (this.GetCodeFormer(argFromIndex, argToIndex) != Flags.ON ? CodeKousai.ClassMates : CodeKousai.Former);
			}
			else
			{
				codeKousai = CodeKousai.Lover;
			}
			return codeKousai;
		}

		public CodeNinshin GetCodeNinshin(int argIndex)
		{
			CodeNinshin codeNinshin = CodeNinshin.None;
			if (this.Seat[argIndex].Character.Attr.Sex != 1)
			{
				codeNinshin = CodeNinshin.None;
			}
			else
			{
				switch (this.Seat[argIndex].Character.Attr.Ninshin[(checked((checked(this.Header.Days - 1)) % 14 + 1)) % 14])
				{
					case 0:
					{
						codeNinshin = CodeNinshin.Normal;
						break;
					}
					case 1:
					{
						codeNinshin = CodeNinshin.Safe;
						break;
					}
					case 2:
					{
						codeNinshin = CodeNinshin.Danger;
						break;
					}
				}
			}
			return codeNinshin;
		}

		public CodeRenzoku GetCodeRenzoku(int argFromIndex, int argToIndex)
		{
			CodeRenzoku codeRenzoku = CodeRenzoku.None;
			if (this.Seat[argFromIndex].PlayData.Renzoku[this.Seat[argToIndex].SeatNo] > 0)
			{
				codeRenzoku = (this.Seat[argFromIndex].PlayData.CntTodaySeikou[this.Seat[argToIndex].SeatNo] != 0 ? CodeRenzoku.Going : CodeRenzoku.End);
			}
			return codeRenzoku;
		}

		public int GetIndex(int argSeatNo)
		{
			int num = -1;
			int length = checked(checked((int)this.Seat.Length) - 1);
			int num1 = 0;
			while (num1 <= length)
			{
				if (this.Seat[num1].SeatNo != argSeatNo)
				{
					num1 = checked(num1 + 1);
				}
				else
				{
					num = num1;
					break;
				}
			}
			return num;
		}

		public string GetText(int argIndex)
		{
			string str;
			string chr = JG2Importer.JG2.Common.ToChar(new decimal(this.Seat[argIndex].SeatNo), "\t");
			Character character = this.Seat[argIndex].Character;
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Sex), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsSex[character.Attr.Sex], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(character.Attr.Name1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(character.Attr.Name2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Height), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsHeight[character.Attr.Height], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Style), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsStyle[character.Attr.Style], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.StyleM), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Atama1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Atama2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Waist), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Mune1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsMune[character.Attr.Mune], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Mune2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Mune3), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Mune4), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Mune5), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Mune6), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Mune7), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Mune8), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsMune8[character.Attr.Mune8], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Nyurin), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToHex(character.Attr.ColorSkin2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Tikubi1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Tikubi2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Tikubi3), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Hiyake1), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsHiyake1[character.Attr.Hiyake1], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Hiyake2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Mosaic), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Under1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Under2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToHex(character.Attr.ColorUnder, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Kao), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Eye1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Eye2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Eye3), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Eye4), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Eye5), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Pupil1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Pupil2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Pupil3), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Pupil4), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Pupil5), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Highlight1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToHex(character.Attr.ColorEye1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToHex(character.Attr.ColorEye2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Mayu1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Mayu2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToHex(character.Attr.ColorMayu, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Mabuta), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Matuge1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Matuge2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Glasses2), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsGlasses2[character.Attr.Glasses2], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToHex(character.Attr.ColorGlasses, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Hokuro1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Hokuro2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Hokuro3), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Hokuro4), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Lip1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Lip2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Hair1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.HairAdj1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.HairRev1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Hair2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.HairAdj2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.HairRev2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Hair3), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.HairAdj3), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.HairRev3), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Hair4), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.HairAdj4), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.HairRev4), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToHex(character.Attr.ColorHair, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Tiryoku1), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsTiryoku1[character.Attr.Tiryoku1], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Tiryoku2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal(character.Attr.Tiryoku3), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Tairyoku1), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsTairyoku1[character.Attr.Tairyoku1], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Tairyoku2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal(character.Attr.Tairyoku3), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Bukatu1), "\t"), JG2Importer.JG2.Common.ToChar(this.Header.Bukatu[character.Attr.Bukatu1].Name, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Bukatu2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal(character.Attr.Bukatu3), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Syakousei), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsSyakousei[character.Attr.Syakousei], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Kenka), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsKenka[character.Attr.Kenka], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Teisou), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsTeisou[character.Attr.Teisou], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Seiai), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsSeiai[character.Attr.Seiai], "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Seikou1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Seikou2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Voice), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsVoice[character.Attr.Voice], "\t"));
			string str1 = chr;
			string chr1 = JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Seikaku), "\t");
			if (character.Attr.Seikaku >= 0 & character.Attr.Seikaku <= checked(checked((int)JG2Importer.JG2.Common.cnsSeikaku.Length) - 1))
			{
				str = JG2Importer.JG2.Common.cnsSeikaku[character.Attr.Seikaku];
			}
			else
			{
				str = null;
			}
			chr = string.Concat(str1, chr1, JG2Importer.JG2.Common.ToChar(str, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Cyoroi), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Nekketu), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Nigate1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Nigate2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Charm), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Tundere), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Kyouki), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Miha), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Sunao), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Maemuki), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Tereya), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Yakimochi), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Toufu), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Sukebe), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Majime), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Cool), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Tyokujyo), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Poyayan), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Bouryoku), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Sousyoku), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Sewayaki), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Iintyou), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Osyaberi), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Harapeko), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Renai), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Itizu), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Yuujyufudan), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Makenki), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Haraguro), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Kinben), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Honpou), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.M), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Asekaki), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Yami), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Nantyo), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Yowami), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Miseityo), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Luck), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Rare), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Kiss), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Bust), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Seiki1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Seiki2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Cunnilingus), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Fellatio), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Back), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Onna), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Anal), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Nama), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Seiin), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Naka), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Kake), "\t"));
			int num = 0;
			do
			{
				chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(new decimal((int)character.Attr.Ninshin[num]), "\t"), JG2Importer.JG2.Common.ToChar(JG2Importer.JG2.Common.cnsNinshin[character.Attr.Ninshin[num]], "\t"));
				num = checked(num + 1);
			}
			while (num <= 13);
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(character.Attr.Goods1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(character.Attr.Goods2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(character.Attr.Goods3, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(character.Attr.Profile, "\r\n"));
			return chr;
		}

		public void Set(ref byte[] argData)
		{
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)0, ref this.Header.Version, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)4, ref this.Header.ClassName, (long)64);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)68, ref this.Header.Nen, (long)16);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)84, ref this.Header.Kumi, (long)16);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)100, ref this.Header.Ninsu1, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)104, ref this.Header.Ninsu2, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)108, ref this.Header.Hour, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)110, ref this.Header.Minute, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)112, ref this.Header.Second, true);
			long num = (long)0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, checked((long)114 + checked((long)25 * num)), ref this.Header.Bukatu[checked((int)num)].Name, (long)24, false);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked((long)138 + checked((long)25 * num)), ref this.Header.Bukatu[checked((int)num)].Syubetu);
				num = checked(num + (long)1);
			}
			while (num <= (long)7);
			if (this.Header.Version != 100)
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, (long)314, ref this.Header.Filler99);
				JG2Importer.JG2.Common.FromRawData(ref argData, (long)315, ref this.Header.Days);
				JG2Importer.JG2.Common.FromRawData(ref argData, (long)316, ref this.Header.Week);
			}
			else
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, (long)314, ref this.Header.Days);
				JG2Importer.JG2.Common.FromRawData(ref argData, (long)315, ref this.Header.Week);
			}
			long num1 = (long)316;
			num = (long)0;
			while (true)
			{
				JG2Importer.JG2.Seat seat = new JG2Importer.JG2.Seat();
				long num2 = seat.Set(ref argData, num1, this.Header.Version);
				if (num2 == (long)-1)
				{
					break;
				}
				num1 = num2;
				num = checked(num + (long)1);
				Array.Resize<JG2Importer.JG2.Seat>(ref this.Seat, checked((int)num));
				this.Seat[checked((int)(checked(num - (long)1)))] = seat;
				seat = null;
			}
			JG2Importer.JG2.Common.FromRawData(ref argData, num1, ref this.Footer.Filler, checked(checked((long)(checked((int)argData.Length)) - num1) - (long)4));
			JG2Importer.JG2.Common.FromRawData(ref argData, (long)(checked(checked((int)argData.Length) - 4)), ref this.Footer.SeatNoPlayer, true);
		}

		public bool Update()
		{
			if (Information.IsNothing(this.DataFile))
			{
				return false;
			}
			byte[] numArray = this.Get();
			FileStream fileStream = new FileStream(this.DataFile.FullName, FileMode.Create, FileAccess.Write, FileShare.Write);
			long num = (long)0;
			long length = (long)(checked(checked((int)numArray.Length) - 1));
			for (long i = num; i <= length; i = checked(i + (long)1))
			{
				fileStream.WriteByte(numArray[checked((int)i)]);
			}
			fileStream.Close();
			fileStream.Dispose();
			fileStream = null;
			numArray = null;
			this.DataFile.Refresh();
			return true;
		}

		public struct DefBukatu
		{
			public string Name;

			public byte Syubetu;
		}

		public struct DefFooter
		{
			public byte[] Filler;

			public int SeatNoPlayer;
		}

		public struct DefHeader
		{
			public int Version;

			public byte[] ClassName;

			public byte[] Nen;

			public byte[] Kumi;

			public int Ninsu1;

			public int Ninsu2;

			public short Hour;

			public short Minute;

			public short Second;

			public SaveData.DefBukatu[] Bukatu;

			public byte Filler99;

			public byte Days;

			public byte Week;
		}
	}
}