using System;

namespace JG2Importer.JG2
{
	public enum Ninshin : byte
	{
		Normal,
		Safe,
		Danger
	}
}