using System;

namespace JG2Importer.JG2
{
	public enum CodeHAisyou : byte
	{
		None,
		Subtle,
		Normal,
		Good,
		Maximum,
		Perfect
	}
}