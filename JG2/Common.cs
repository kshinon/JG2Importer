using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Text;

namespace JG2Importer.JG2
{
	internal static class Common
	{
		public static Color ColorLove;

		public static Color ColorLike;

		public static Color ColorDislike;

		public static Color ColorHate;

		public static Color ColorSeatDefault;

		public static Color ColorSeatCurrent;

		public static Color ColorSeatSelect;

		public static Color ColorSeatMale;

		public static Color ColorSeatFemale;

		public static Color ColorSeatTeacher;

		public static Color ColorNormal;

		public static Color ColorRare;

		public static Color ColorKiken;

		public static Color ColorAnzen;

		public static string[] cnsFlags;

		public static string[] cnsSex;

		public static string[] cnsMune;

		public static string[] cnsHeight;

		public static string[] cnsStyle;

		public static string[] cnsMune8;

		public static string[] cnsHiyake1;

		public static string[] cnsGlasses2;

		public static string[] cnsTiryoku1;

		public static string[] cnsTairyoku1;

		public static string[] cnsSyakousei;

		public static string[] cnsKenka;

		public static string[] cnsTeisou;

		public static string[] cnsSeiai;

		public static string[] cnsVoice;

		public static string[] cnsSeikaku;

		public static string[] cnsNinshin;

		public static string[] cnsYouto;

		public static string[] cnsType;

		public static string[] cnsBottom1;

		public static string[] cnsSitagi;

		public static string[] cnsSocks;

		public static string[] cnsUtibaki;

		public static string[] cnsSotobaki;

		public static string[] cnsSkirt;

		public static string[] cnsTest1;

		public static string[] cnsTest2;

		public static string[] cnsTest3;

		public static string[] cnsSeatKousai;

		public static string[] cnsSeatGohoubi;

		public static string[] cnsSeatRenzoku;

		public static string[] cnsCodeNinshin;

		public static string[] cnsCodeKousai;

		public static string[] cnsCodeAction;

		public static string[] cnsCodeDate;

		public static string[] cnsCodeGohoubi;

		public static string[] cnsCodeHAisyou;

		public static string[] cnsCodeCntNakaKiken;

		public static string[] cnsCodeRenzoku;

		public static string[] cnsCodeCntTodaySeikou;

		static Common()
		{
			JG2Importer.JG2.Common.ColorLove = Color.FromArgb(237, 15, 237);
			JG2Importer.JG2.Common.ColorLike = Color.FromArgb(0, 177, 88);
			JG2Importer.JG2.Common.ColorDislike = Color.FromArgb(0, 0, 0);
			JG2Importer.JG2.Common.ColorHate = Color.FromArgb(51, 51, 255);
			JG2Importer.JG2.Common.ColorSeatDefault = Color.White;
			JG2Importer.JG2.Common.ColorSeatCurrent = Color.Blue;
			JG2Importer.JG2.Common.ColorSeatSelect = Color.FromArgb(208, 255, 208);
			JG2Importer.JG2.Common.ColorSeatMale = Color.AliceBlue;
			JG2Importer.JG2.Common.ColorSeatFemale = Color.LavenderBlush;
			JG2Importer.JG2.Common.ColorSeatTeacher = Color.FloralWhite;
			JG2Importer.JG2.Common.ColorNormal = Color.White;
			JG2Importer.JG2.Common.ColorRare = Color.Gold;
			JG2Importer.JG2.Common.ColorKiken = Color.Red;
			JG2Importer.JG2.Common.ColorAnzen = Color.Blue;
			JG2Importer.JG2.Common.cnsFlags = new string[] { Resources.FilterTxtNo, Resources.FilterTxtYes };
			JG2Importer.JG2.Common.cnsSex = new string[] { Resources.FilterTxtMale, Resources.FilterTxtFemale };
			JG2Importer.JG2.Common.cnsMune = new string[] { Resources.FilterTxtBustS, Resources.FilterTxtBustM, Resources.FilterTxtBustL };
			JG2Importer.JG2.Common.cnsHeight = new string[] { Resources.FilterTxtHeightL, Resources.FilterTxtHeightM, Resources.FilterTxtHeightH };
			JG2Importer.JG2.Common.cnsStyle = new string[] { Resources.FilterTxtBodyTypeT, Resources.FilterTxtBodyTypeS, Resources.FilterTxtBodyTypeC }; 
			JG2Importer.JG2.Common.cnsMune8 = new string[] { Resources.FilterTxtBustSoftS, Resources.FilterTxtBustSoftM, Resources.FilterTxtBustSoftH };
			JG2Importer.JG2.Common.cnsHiyake1 = new string[] { Resources.FilterTxtNone, Resources.FilterTxtTan1, Resources.FilterTxtTan2, Resources.FilterTxtTan3, Resources.FilterTxtTan4, Resources.FilterTxtTan5 };
			JG2Importer.JG2.Common.cnsGlasses2 = new string[] { Resources.FilterTxtNone, Resources.FilterTxtGlasses1, Resources.FilterTxtGlasses2, Resources.FilterTxtGlasses3, Resources.FilterTxtGlasses4};
			JG2Importer.JG2.Common.cnsTiryoku1 = new string[] { Resources.FilterTxtScale1, Resources.FilterTxtScale2, Resources.FilterTxtScale3, Resources.FilterTxtScale4, Resources.FilterTxtScale5, Resources.FilterTxtScale6};
            JG2Importer.JG2.Common.cnsTairyoku1 = new string[] { Resources.FilterTxtScale1, Resources.FilterTxtScale2, Resources.FilterTxtScale3, Resources.FilterTxtScale4, Resources.FilterTxtScale5, Resources.FilterTxtScale6 };
            JG2Importer.JG2.Common.cnsSyakousei = new string[] { Resources.FilterTxtScale1, Resources.FilterTxtScale2, Resources.FilterTxtScale3, Resources.FilterTxtScale4, Resources.FilterTxtScale5, Resources.FilterTxtScale6};
			JG2Importer.JG2.Common.cnsKenka = new string[] { Resources.FilterTxtFightStyle1, Resources.FilterTxtFightStyle2, Resources.FilterTxtFightStyle3 };
			JG2Importer.JG2.Common.cnsTeisou = new string[] { Resources.FilterTxtScale1, Resources.FilterTxtScale2, Resources.FilterTxtScale3, Resources.FilterTxtScale4, Resources.FilterTxtScale5 };
            JG2Importer.JG2.Common.cnsSeiai = new string[] { Resources.FilterTxtOrientation1, Resources.FilterTxtOrientation2, Resources.FilterTxtOrientation3, Resources.FilterTxtOrientation4, Resources.FilterTxtOrientation5};
			JG2Importer.JG2.Common.cnsVoice = new string[] { Resources.FilterTxtScale1, Resources.FilterTxtScale2, Resources.FilterTxtScale3, Resources.FilterTxtScale4, Resources.FilterTxtScale5 };
			JG2Importer.JG2.Common.cnsSeikaku = new string[] { Resources.FilterTxtPersonality1, Resources.FilterTxtPersonality2, Resources.FilterTxtPersonality3, Resources.FilterTxtPersonality4, Resources.FilterTxtPersonality5,
                Resources.FilterTxtPersonality6, Resources.FilterTxtPersonality7, Resources.FilterTxtPersonality8, Resources.FilterTxtPersonality9, Resources.FilterTxtPersonality10,
                Resources.FilterTxtPersonality11, Resources.FilterTxtPersonality12, Resources.FilterTxtPersonality13, Resources.FilterTxtPersonality14, Resources.FilterTxtPersonality15,
                Resources.FilterTxtPersonality16, Resources.FilterTxtPersonality17, Resources.FilterTxtPersonality18, Resources.FilterTxtPersonality19, Resources.FilterTxtPersonality20,
                Resources.FilterTxtPersonality21, Resources.FilterTxtPersonality22, Resources.FilterTxtPersonality23, Resources.FilterTxtPersonality24, Resources.FilterTxtPersonality25,
                Resources.FilterTxtPersonality26, Resources.FilterTxtPersonality27, Resources.FilterTxtPersonality28, Resources.FilterTxtPersonality29, Resources.FilterTxtPersonality30};
			JG2Importer.JG2.Common.cnsNinshin = new string[] { Resources.FilterTxtPregRisk1, Resources.FilterTxtPregRisk2, Resources.FilterTxtPregRisk3 };
			JG2Importer.JG2.Common.cnsYouto = new string[] { Resources.FilterTxtCurrentCloth1, Resources.FilterTxtCurrentCloth2, Resources.FilterTxtCurrentCloth3, Resources.FilterTxtCurrentCloth4 };
			JG2Importer.JG2.Common.cnsType = new string[] { Resources.FilterTxtCurrentCloth1, Resources.FilterTxtCurrentCloth2, Resources.FilterTxtCurrentCloth3,
                Resources.FilterTxtHCloth1, Resources.FilterTxtHCloth2, Resources.FilterTxtHCloth3, Resources.FilterTxtHCloth4, Resources.FilterTxtHCloth5};
			JG2Importer.JG2.Common.cnsBottom1 = new string[] { Resources.FilterTxtClothSolidColour, "001", "002", "003", "004", "005", "006", "007" };
			JG2Importer.JG2.Common.cnsSitagi = new string[] { Resources.FilterTxtClothSolidColour, "001", "002", "003", "004", "005", "006", "007", "008" };
			JG2Importer.JG2.Common.cnsSocks = new string[] { Resources.FilterTxtSockType1, Resources.FilterTxtSockType2, Resources.FilterTxtSockType3, Resources.FilterTxtSockType4, Resources.FilterTxtSockType5, Resources.FilterTxtSockType6 };
			JG2Importer.JG2.Common.cnsUtibaki = new string[] { Resources.FilterTxtShoes1, Resources.FilterTxtShoes2, Resources.FilterTxtShoes3, Resources.FilterTxtShoes4, Resources.FilterTxtShoes5};
            JG2Importer.JG2.Common.cnsSotobaki = new string[] { Resources.FilterTxtShoes1, Resources.FilterTxtShoes2, Resources.FilterTxtShoes3, Resources.FilterTxtShoes4, Resources.FilterTxtShoes5 };
            JG2Importer.JG2.Common.cnsSkirt = new string[] { Resources.FilterTxtSkirtLength1, Resources.FilterTxtSkirtLength2 };
			JG2Importer.JG2.Common.cnsTest1 = new string[] { Resources.FilterTxtJudgementF, Resources.FilterTxtJudgementE, Resources.FilterTxtJudgementD,
                Resources.FilterTxtJudgementC, Resources.FilterTxtJudgementB, Resources.FilterTxtJudgementA };
            JG2Importer.JG2.Common.cnsTest2 = new string[] { Resources.FilterTxtJudgementF, Resources.FilterTxtJudgementE, Resources.FilterTxtJudgementD,
                Resources.FilterTxtJudgementC, Resources.FilterTxtJudgementB, Resources.FilterTxtJudgementA };
			JG2Importer.JG2.Common.cnsTest3 = new string[] { Resources.FilterTxtExam1, Resources.FilterTxtExam2, Resources.FilterTxtExam3, Resources.FilterTxtExam4, Resources.FilterTxtExam5, Resources.FilterTxtExam6};
			JG2Importer.JG2.Common.cnsSeatKousai = new string[] { null, "♥", "♡" };
			JG2Importer.JG2.Common.cnsSeatGohoubi = new string[] { null, Resources.FilterTxtPromise1, Resources.FilterTxtPromise2 };
			JG2Importer.JG2.Common.cnsSeatRenzoku = new string[] { null, Resources.FilterTxtOngoing1, Resources.FilterTxtOngoing2 };
			JG2Importer.JG2.Common.cnsCodeNinshin = new string[] { "(" + Resources.FilterTxtMale + ")", Resources.FilterTxtPregRisk1, Resources.FilterTxtPregRisk2, Resources.FilterTxtPregRisk3 };
			JG2Importer.JG2.Common.cnsCodeKousai = new string[] { Resources.FilterTxtRelation1, Resources.FilterTxtRelation2, Resources.FilterTxtRelation3 };
			JG2Importer.JG2.Common.cnsCodeAction = new string[] { Resources.FilterTxtDontKnow, Resources.FilterTxtAttitude1, Resources.FilterTxtAttitude2, Resources.FilterTxtAttitude3, Resources.FilterTxtAttitude4};
			JG2Importer.JG2.Common.cnsCodeDate = new string[] { Resources.FilterTxtNo, Resources.FilterTxtYes };
			JG2Importer.JG2.Common.cnsCodeGohoubi = new string[] { Resources.FilterTxtNone, Resources.FilterTxtPromise1, Resources.FilterTxtPromise2 };
			JG2Importer.JG2.Common.cnsCodeHAisyou = new string[] { Resources.FilterTxtNone, Resources.FilterTxtHCompat1, Resources.FilterTxtHCompat2, Resources.FilterTxtHCompat3, Resources.FilterTxtHCompat4, Resources.FilterTxtHCompat5};
			JG2Importer.JG2.Common.cnsCodeCntNakaKiken = new string[] { "("+Resources.FilterTxtMale+")", Resources.FilterTxtNo, Resources.FilterTxtYes };
			JG2Importer.JG2.Common.cnsCodeRenzoku = new string[] { Resources.FilterTxtNone, Resources.FilterTxtOngoing1, Resources.FilterTxtOngoing2 };
			JG2Importer.JG2.Common.cnsCodeCntTodaySeikou = new string[] { Resources.FilterTxtNo, Resources.FilterTxtYes };
        }

		public static void CopyRawData(ref byte[] argSource, ref byte[] argDestination)
		{
			Array.Resize<byte>(ref argDestination, checked((int)argSource.Length));
			Array.Copy(argSource, (long)0, argDestination, (long)0, (long)(checked((int)argSource.Length)));
		}

		public static void FromRawData(ref byte[] argSource, long argStartIndex, ref byte[] argDestination, long argLength)
		{
			Array.Resize<byte>(ref argDestination, checked((int)argLength));
			Array.Copy(argSource, argStartIndex, argDestination, (long)0, argLength);
		}

		public static void FromRawData(ref byte[] argSource, long argStartIndex, ref byte argDestination)
		{
			argDestination = argSource[checked((int)argStartIndex)];
		}

		public static void FromRawData(ref byte[] argSource, long argStartIndex, ref int argDestination, bool argLittle)
		{
			argDestination = JG2Importer.JG2.Common.ToDWord(ref argSource, argStartIndex, argLittle);
		}

		public static void FromRawData(ref byte[] argSource, long argStartIndex, ref short argDestination, bool argLittle)
		{
			argDestination = JG2Importer.JG2.Common.ToWord(ref argSource, argStartIndex, argLittle);
		}

		public static void FromRawData(ref byte[] argSource, long argStartIndex, ref string argDestination, long argLength, bool argEncryption)
		{
			argDestination = JG2Importer.JG2.Common.ToText(ref argSource, argStartIndex, checked((int)argLength), argEncryption);
		}

		public static Icon GetIcon(Bitmap argBitmap)
		{
			Bitmap bitmap = new Bitmap(argBitmap.Height, argBitmap.Height);
			Graphics graphic = Graphics.FromImage(bitmap);
			graphic.DrawImageUnscaled(argBitmap, checked((int)Math.Round((double)(checked(argBitmap.Height - argBitmap.Width)) / 2)), 0, bitmap.Width, bitmap.Height);
			Icon icon = Icon.FromHandle(JG2Importer.JG2.Common.ResizeBitmap(bitmap, 48, 48).GetHicon());
			graphic.Dispose();
			bitmap.Dispose();
			return icon;
		}

		public static Mune GetMune(byte argSex, byte argMune1)
		{
			Mune mune = Mune.Slender;
			if (argSex != 1)
			{
				mune = Mune.Normal;
			}
			else
			{
				byte num = argMune1;
				if (num < 0 || num > 33)
				{
					mune = (num < 34 || num > 66 ? Mune.Grammar : Mune.Normal);
				}
				else
				{
					mune = Mune.Slender;
				}
			}
			return mune;
		}

		public static byte GetNouryoku1(int argNouryoku3)
		{
			byte num = 0;
			int num1 = argNouryoku3;
			if (num1 == 0)
			{
				num = 0;
			}
			else if (num1 < 901 || num1 > 2147483647)
			{
				num = checked((byte)Math.Round(Math.Floor((double)(checked(argNouryoku3 - 1)) / 200)));
			}
			else
			{
				num = 5;
			}
			return num;
		}

		public static byte GetNouryoku2(int argNouryoku3)
		{
			byte num = 0;
			int num1 = argNouryoku3;
			if (num1 < 0 || num1 > 199)
			{
				num = checked((byte)Math.Round(Math.Floor((double)argNouryoku3 / 100) - 1));
			}
			else
			{
				num = 0;
			}
			return num;
		}

		public static Bitmap ResizeBitmap(Bitmap argBitmap, int argWidth, int argHeight)
		{
			Bitmap bitmap = new Bitmap(argWidth, argHeight);
			Graphics graphic = Graphics.FromImage(bitmap);
			graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
			graphic.DrawImage(argBitmap, 0, 0, argWidth, argHeight);
			graphic.Dispose();
			return bitmap;
		}

		public static long SearchData(byte[] argData, byte[] argKeyWord, long argStartIndex = 0L)
		{
			long num = (long)-1;
			long length = (long)(checked(checked((int)argData.Length) - 1));
			long num1 = argStartIndex;
			while (num1 <= length)
			{
				bool flag = true;
				long num2 = (long)0;
				long length1 = (long)(checked(checked((int)argKeyWord.Length) - 1));
				long num3 = num2;
				while (num3 <= length1)
				{
					if (argData[checked((int)(checked(num1 + num3)))] == argKeyWord[checked((int)num3)])
					{
						num3 = checked(num3 + (long)1);
					}
					else
					{
						flag = false;
						break;
					}
				}
				if (!flag)
				{
					num1 = checked(num1 + (long)1);
				}
				else
				{
					num = num1;
					break;
				}
			}
			return num;
		}

		public static string ToChar(decimal argValue, string argDelChar = "\t")
		{
			return string.Concat(Conversions.ToString(argValue), argDelChar);
		}

		public static string ToChar(string argValue, string argDelChar = "\t")
		{
			return string.Concat("\"", Strings.Trim(argValue), "\"", argDelChar);
		}

		public static int ToDWord(ref byte[] argData, long argStartIndex, bool argLittle)
		{
			byte[] numArray = new byte[4];
			Array.Copy(argData, argStartIndex, numArray, (long)0, (long)4);
			if (!argLittle)
			{
				Array.Reverse(numArray);
			}
			return BitConverter.ToInt32(numArray, 0);
		}

		public static int ToDWord(ref byte[] argData, bool argLittle)
		{
			return JG2Importer.JG2.Common.ToDWord(ref argData, (long)0, argLittle);
		}

		public static string ToHex(int argValue, string argDelChar = "\t")
		{
			return string.Concat("\"", Conversion.Hex(argValue).PadLeft(8, '0'), "\"", argDelChar);
		}

		public static Image ToImage(ref byte[] argData, long argStartIndex, long argLength)
		{
			MemoryStream memoryStream = new MemoryStream();
			long num = (long)0;
			long num1 = checked(argLength - (long)1);
			for (long i = num; i <= num1; i = checked(i + (long)1))
			{
				memoryStream.WriteByte(argData[checked((int)(checked(argStartIndex + i)))]);
			}
			memoryStream.Flush();
			Image image = Image.FromStream(memoryStream);
			memoryStream.Close();
			memoryStream.Dispose();
			memoryStream = null;
			return image;
		}

		public static Image ToImage(ref byte[] argData)
		{
			return JG2Importer.JG2.Common.ToImage(ref argData, (long)0, (long)(checked((int)argData.Length)));
		}

        /**Append an arbitrary number of bytes to a buffer.
         * 
         * \param argSource The byte array to append.
         * \param argDestination The byte array to append to.
         */
        public static void ToRawData(ref byte[] argSource, ref byte[] argDestination) {
            int length = (argDestination == null) ? 0 : argDestination.Length;
            Array.Resize<byte>(ref argDestination, length + argSource.Length);
            Array.Copy(argSource, 0, argDestination, length, argSource.Length);
        }

        /**Append a byte to a buffer.
         * 
         * \param argSource The byte to append.
         * \param argDestination The byte array to append to.
         */
		public static void ToRawData(ref byte argSource, ref byte[] argDestination)
		{
            int length = (argDestination == null) ? 0 : argDestination.Length;
            Array.Resize<byte>(ref argDestination, length + 1);
            argDestination[length] = argSource;
        }

        /**Append a 4 byte int to a buffer.
         * 
         * \param argSource The int to append.
         * \param argDestination The byte array to append to.
         * \param argLittle Is argSource little endian?
         */
        public static void ToRawData(ref int argSource, ref byte[] argDestination, bool argLittle)
        {
            byte[] bytes = BitConverter.GetBytes(argSource);
            if (!argLittle) Array.Reverse(bytes);
            ToRawData(ref bytes, ref argDestination);
        }

        /**Append a 2 byte short to a buffer.
         * 
         * \param argSource The short to append.
         * \param argDestination The byte array to append to.
         * \param argLittle Is argSource little endian?
         */
        public static void ToRawData(ref short argSource, ref byte[] argDestination, bool argLittle)
		{
            byte[] bytes = BitConverter.GetBytes(argSource);
            if (!argLittle) Array.Reverse(bytes);
            ToRawData(ref bytes, ref argDestination);
        }

		public static void ToRawData(ref string argSource, ref byte[] argDestination, long argLength, bool argEncryption)
		{
			long length;
			byte[] bytes;
			byte obj;
			long length1;
			if (!Information.IsNothing(argSource))
			{
				bytes = Encoding.GetEncoding("Shift_JIS").GetBytes(argSource);
				length = (long)(checked((int)bytes.Length));
			}
			else
			{
				bytes = null;
				length = (long)0;
			}
			Array.Resize<byte>(ref bytes, checked((int)argLength));
			int num = checked((int)(checked(argLength - (long)1)));
			for (int i = 0; i <= num; i = checked(i + 1))
			{
				if ((long)i > checked(length - (long)1))
				{
					byte[] numArray = bytes;
					int num1 = i;
					if (argEncryption)
					{
						obj = 255;
					}
					else
					{
						obj = 0;
					}
					numArray[num1] = obj;
				}
				else if (argEncryption)
				{
					bytes[i] = (byte)(~bytes[i]);
				}
			}
			if (Information.IsNothing(argDestination))
			{
				length1 = 0;
			}
			else
			{
				length1 = checked((int)argDestination.Length);
			}
			length = length1;
			Array.Resize<byte>(ref argDestination, checked((int)(checked(length + argLength))));
			Array.Copy(bytes, (long)0, argDestination, length, argLength);
		}

		public static string ToText(ref byte[] argData, long argStartIndex, int argLength, bool argEncryption)
		{
			byte[] numArray = null;
			int num = checked(argLength - 1);
			for (int i = 0; i <= num; i = checked(i + 1))
			{
				byte num1 = argData[checked((int)(checked(argStartIndex + (long)i)))];
				if (argEncryption)
				{
					num1 = (byte)(~num1);
				}
				if (num1 == 0)
				{
					break;
				}
				Array.Resize<byte>(ref numArray, checked(i + 1));
				numArray[i] = num1;
			}
			if (Information.IsNothing(numArray))
			{
				return null;
			}
			return Encoding.GetEncoding("Shift_JIS").GetString(numArray);
		}

		public static string ToText(ref byte[] argData, bool argEncryption)
		{
			return JG2Importer.JG2.Common.ToText(ref argData, (long)0, checked((int)argData.Length), argEncryption);
		}

		public static short ToWord(ref byte[] argData, long argStartIndex, bool argLittle)
		{
			byte[] numArray = new byte[2];
			Array.Copy(argData, argStartIndex, numArray, (long)0, (long)2);
			if (!argLittle)
			{
				Array.Reverse(numArray);
			}
			return BitConverter.ToInt16(numArray, 0);
		}

		public static short ToWord(ref byte[] argData, bool argLittle)
		{
			return JG2Importer.JG2.Common.ToWord(ref argData, (long)0, argLittle);
		}
	}
}