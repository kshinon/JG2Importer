using System;

namespace JG2Importer.JG2
{
	public enum Height : byte
	{
		Short,
		Normal,
		Tall
	}
}