using System;

namespace JG2Importer.JG2
{
	public enum CodeNinshin : byte
	{
		None,
		Normal,
		Safe,
		Danger
	}
}