using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Drawing;
using System.IO;

namespace JG2Importer.JG2
{
	public class Character
	{
		public FileInfo DataFile;

		public byte[] PNG;

		public Character.DefAttribute Attr;

		public int ThumbnailLength;

		public byte[] Thumbnail;

		public int DataOffset;

		public Image PNGImage;

		public Image ThumbnailImage;

		public long IxStart;

		public long IxChar;

		public long IxThumbnailLength;

		public long IxThumbnail;

		public long IxPlayData;

		public string FullName
		{
			get
			{
				return string.Concat(this.Attr.Name1, Strings.Space(1), this.Attr.Name2);
			}
		}

		public Character()
		{
			this.Clear();
		}

		public Character(string argFileName)
		{
			this.Clear();
			this.DataFile = new FileInfo(argFileName);
			FileStream fileStream = new FileStream(argFileName, FileMode.Open, FileAccess.Read, FileShare.Read);
			if (!fileStream.CanRead)
			{
				return;
			}
			byte[] numArray = null;
			Array.Resize<byte>(ref numArray, checked((int)fileStream.Length));
			long num = (long)0;
			long length = checked(fileStream.Length - (long)1);
			for (long i = num; i <= length; i = checked(i + (long)1))
			{
				numArray[checked((int)i)] = checked((byte)fileStream.ReadByte());
			}
			fileStream.Close();
			fileStream.Dispose();
			fileStream = null;
			this.Set(ref numArray, (long)0);
			numArray = null;
		}

		public void Clear()
		{
			this.DataFile = null;
			this.PNG = null;
			this.Attr = new Character.DefAttribute();
			Array.Resize<byte>(ref this.Attr.Ninshin, 14);
			Array.Resize<int>(ref this.Attr.HAisyou, 25);
			Array.Resize<Character.DefCostume>(ref this.Attr.Costume, 4);
			this.ThumbnailLength = 0;
			this.Thumbnail = null;
			this.DataOffset = 0;
			this.PNGImage = null;
			this.ThumbnailImage = null;
			this.IxStart = (long)0;
			this.IxChar = (long)0;
			this.IxThumbnailLength = (long)0;
			this.IxThumbnail = (long)0;
			this.IxPlayData = (long)0;
		}

		public byte[] Get()
		{
			byte[] numArray = null;
			JG2Importer.JG2.Common.ToRawData(ref this.PNG, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.HeaderID, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Version, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Sex, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Name1, ref numArray, (long)260, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Name2, ref numArray, (long)260, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Profile, ref numArray, (long)512, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Seikaku, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Sintyo, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Taikei, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mune, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorSkin1, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Glasses1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Seikou, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Hair, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Bukatu1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.StyleM, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Height, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Style, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Atama1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Atama2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Waist, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mune1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Nyurin, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mune2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mune3, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mune4, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mune5, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mune6, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mune7, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mune8, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorSkin2, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Under1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Under2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorUnder, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Tikubi1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Tikubi2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Tikubi3, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Hiyake1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Hiyake2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mosaic, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Kao, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Eye1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Eye2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Eye3, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Eye4, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Eye5, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Pupil1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Pupil2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Pupil3, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Pupil4, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Pupil5, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Highlight1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Highlight2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Highlight3, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Highlight4, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Highlight5, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mayu1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mayu2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorMayu, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorEye1, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorEye2, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Mabuta, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Matuge1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Matuge2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Glasses2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Hokuro1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Hokuro2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Hokuro3, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Hokuro4, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Lip1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Lip2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorGlasses, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Hair1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Hair2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Hair3, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Hair4, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.HairAdj1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.HairAdj2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.HairAdj3, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.HairAdj4, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.HairRev1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.HairRev2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.HairRev3, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.HairRev4, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.ColorHair, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Tiryoku1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Tiryoku2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Tiryoku3, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Tairyoku1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Tairyoku2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Tairyoku3, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Bukatu2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Bukatu3, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Syakousei, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Kenka, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Teisou, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Seiai, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Seikou1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Seikou2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Voice, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Cyoroi, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Nekketu, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Nigate1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Nigate2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Charm, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Tundere, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Kyouki, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Miha, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Sunao, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Maemuki, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Tereya, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Yakimochi, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Toufu, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Sukebe, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Majime, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Cool, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Tyokujyo, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Poyayan, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Bouryoku, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Sousyoku, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Sewayaki, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Iintyou, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Osyaberi, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Harapeko, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Renai, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Itizu, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Yuujyufudan, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Makenki, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Haraguro, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Kinben, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Honpou, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.M, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Asekaki, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Yami, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Nantyo, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Yowami, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Miseityo, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Luck, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Rare, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Kiss, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Bust, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Seiki1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Seiki2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Cunnilingus, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Fellatio, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Back, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Onna, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Anal, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Nama, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Seiin, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Naka, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Kake, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Sikou1, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Sikou2, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Sikou3, ref numArray);
			int num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Ninshin[num], ref numArray);
				num = checked(num + 1);
			}
			while (num <= 13);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.HAisyou[num], ref numArray, true);
				num = checked(num + 1);
			}
			while (num <= 24);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Goods1, ref numArray, (long)260, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Goods2, ref numArray, (long)260, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Attr.Goods3, ref numArray, (long)260, true);
			num = 0;
			do
			{
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].Type, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].Skirt, ref numArray);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].Socks, ref numArray);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].Utibaki, ref numArray);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].Sotobaki, ref numArray);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].ColorTop1, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].ColorTop2, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].ColorTop3, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].ColorTop4, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].ColorBottom1, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].ColorBottom2, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].ColorSitagi, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].ColorSocks, ref numArray);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].ColorUtibaki, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].ColorSotobaki, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].Bottom1, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].Sitagi, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].Bottom1Sitaji1, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].Bottom1Sitaji2, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].SitagiSitaji1, ref numArray, true);
				JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].SitagiSitaji2, ref numArray, true);
				if (this.Attr.Version >= 102)
				{
					JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].FlagJyoge, ref numArray);
					JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].FlagSitagi, ref numArray);
					JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].FlagSkirt, ref numArray);
				}
				if (this.Attr.Version >= 103)
				{
					JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].Bottom1Kage1, ref numArray, true);
					JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].Bottom1Kage2, ref numArray, true);
					JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].SitagiKage1, ref numArray, true);
					JG2Importer.JG2.Common.ToRawData(ref this.Attr.Costume[num].SitagiKage2, ref numArray, true);
				}
				num = checked(num + 1);
			}
			while (num <= 3);
			JG2Importer.JG2.Common.ToRawData(ref this.ThumbnailLength, ref numArray, true);
			JG2Importer.JG2.Common.ToRawData(ref this.Thumbnail, ref numArray);
			JG2Importer.JG2.Common.ToRawData(ref this.DataOffset, ref numArray, true);
			return numArray;
		}

		public void ReCalc()
		{
			if (this.Attr.Sex == 1) this.Attr.StyleM = 1;
			this.Attr.Sintyo = this.Attr.Height;
			this.Attr.Taikei = this.Attr.Style;
			this.Attr.Mune = (byte)JG2Importer.JG2.Common.GetMune(this.Attr.Sex, this.Attr.Mune1);
			this.Attr.ColorSkin1 = this.Attr.ColorSkin2;
			this.Attr.Glasses1 = (byte)((this.Attr.Glasses2 == 0) ? 0 : 1);
            this.Attr.Seikou = this.Attr.Seikou1;
			this.Attr.Hair = this.Attr.Hair3;
			this.Attr.Tiryoku1 = JG2Importer.JG2.Common.GetNouryoku1(this.Attr.Tiryoku3);
			this.Attr.Tiryoku2 = JG2Importer.JG2.Common.GetNouryoku2(this.Attr.Tiryoku3);
			this.Attr.Tairyoku1 = JG2Importer.JG2.Common.GetNouryoku1(this.Attr.Tairyoku3);
			this.Attr.Tairyoku2 = JG2Importer.JG2.Common.GetNouryoku2(this.Attr.Tairyoku3);
			this.Attr.Bukatu2 = JG2Importer.JG2.Common.GetNouryoku2(this.Attr.Bukatu3);
		}

		public void Replace(ref Character argReplaceCharacter)
		{
			JG2Importer.JG2.Common.CopyRawData(ref argReplaceCharacter.PNG, ref this.PNG);
			JG2Importer.JG2.Common.CopyRawData(ref argReplaceCharacter.Attr.HeaderID, ref this.Attr.HeaderID);
			this.Attr.Version = argReplaceCharacter.Attr.Version;
			this.Attr.Sex = argReplaceCharacter.Attr.Sex;
			this.Attr.Name1 = argReplaceCharacter.Attr.Name1;
			this.Attr.Name2 = argReplaceCharacter.Attr.Name2;
			this.Attr.Profile = argReplaceCharacter.Attr.Profile;
			this.Attr.Seikaku = argReplaceCharacter.Attr.Seikaku;
			this.Attr.Sintyo = argReplaceCharacter.Attr.Sintyo;
			this.Attr.Taikei = argReplaceCharacter.Attr.Taikei;
			this.Attr.Mune = argReplaceCharacter.Attr.Mune;
			this.Attr.ColorSkin1 = argReplaceCharacter.Attr.ColorSkin1;
			this.Attr.Glasses1 = argReplaceCharacter.Attr.Glasses1;
			this.Attr.Hair = argReplaceCharacter.Attr.Hair;
			this.Attr.StyleM = argReplaceCharacter.Attr.StyleM;
			this.Attr.Height = argReplaceCharacter.Attr.Height;
			this.Attr.Style = argReplaceCharacter.Attr.Style;
			this.Attr.Atama1 = argReplaceCharacter.Attr.Atama1;
			this.Attr.Atama2 = argReplaceCharacter.Attr.Atama2;
			this.Attr.Waist = argReplaceCharacter.Attr.Waist;
			this.Attr.Mune1 = argReplaceCharacter.Attr.Mune1;
			this.Attr.Nyurin = argReplaceCharacter.Attr.Nyurin;
			this.Attr.Mune2 = argReplaceCharacter.Attr.Mune2;
			this.Attr.Mune3 = argReplaceCharacter.Attr.Mune3;
			this.Attr.Mune4 = argReplaceCharacter.Attr.Mune4;
			this.Attr.Mune5 = argReplaceCharacter.Attr.Mune5;
			this.Attr.Mune6 = argReplaceCharacter.Attr.Mune6;
			this.Attr.Mune7 = argReplaceCharacter.Attr.Mune7;
			this.Attr.Mune8 = argReplaceCharacter.Attr.Mune8;
			this.Attr.ColorSkin2 = argReplaceCharacter.Attr.ColorSkin2;
			this.Attr.Under1 = argReplaceCharacter.Attr.Under1;
			this.Attr.Under2 = argReplaceCharacter.Attr.Under2;
			this.Attr.ColorUnder = argReplaceCharacter.Attr.ColorUnder;
			this.Attr.Tikubi1 = argReplaceCharacter.Attr.Tikubi1;
			this.Attr.Tikubi2 = argReplaceCharacter.Attr.Tikubi2;
			this.Attr.Tikubi3 = argReplaceCharacter.Attr.Tikubi3;
			this.Attr.Hiyake1 = argReplaceCharacter.Attr.Hiyake1;
			this.Attr.Hiyake2 = argReplaceCharacter.Attr.Hiyake2;
			this.Attr.Mosaic = argReplaceCharacter.Attr.Mosaic;
			this.Attr.Kao = argReplaceCharacter.Attr.Kao;
			this.Attr.Eye1 = argReplaceCharacter.Attr.Eye1;
			this.Attr.Eye2 = argReplaceCharacter.Attr.Eye2;
			this.Attr.Eye3 = argReplaceCharacter.Attr.Eye3;
			this.Attr.Eye4 = argReplaceCharacter.Attr.Eye4;
			this.Attr.Eye5 = argReplaceCharacter.Attr.Eye5;
			this.Attr.Pupil1 = argReplaceCharacter.Attr.Pupil1;
			this.Attr.Pupil2 = argReplaceCharacter.Attr.Pupil2;
			this.Attr.Pupil3 = argReplaceCharacter.Attr.Pupil3;
			this.Attr.Pupil4 = argReplaceCharacter.Attr.Pupil4;
			this.Attr.Pupil5 = argReplaceCharacter.Attr.Pupil5;
			this.Attr.Highlight1 = argReplaceCharacter.Attr.Highlight1;
			this.Attr.Highlight2 = argReplaceCharacter.Attr.Highlight2;
			this.Attr.Highlight3 = argReplaceCharacter.Attr.Highlight3;
			JG2Importer.JG2.Common.CopyRawData(ref argReplaceCharacter.Attr.Highlight4, ref this.Attr.Highlight4);
			JG2Importer.JG2.Common.CopyRawData(ref argReplaceCharacter.Attr.Highlight5, ref this.Attr.Highlight5);
			this.Attr.Mayu1 = argReplaceCharacter.Attr.Mayu1;
			this.Attr.Mayu2 = argReplaceCharacter.Attr.Mayu2;
			this.Attr.ColorMayu = argReplaceCharacter.Attr.ColorMayu;
			this.Attr.ColorEye1 = argReplaceCharacter.Attr.ColorEye1;
			this.Attr.ColorEye2 = argReplaceCharacter.Attr.ColorEye2;
			this.Attr.Mabuta = argReplaceCharacter.Attr.Mabuta;
			this.Attr.Matuge1 = argReplaceCharacter.Attr.Matuge1;
			this.Attr.Matuge2 = argReplaceCharacter.Attr.Matuge2;
			this.Attr.Glasses2 = argReplaceCharacter.Attr.Glasses2;
			this.Attr.Hokuro1 = argReplaceCharacter.Attr.Hokuro1;
			this.Attr.Hokuro2 = argReplaceCharacter.Attr.Hokuro2;
			this.Attr.Hokuro3 = argReplaceCharacter.Attr.Hokuro3;
			this.Attr.Hokuro4 = argReplaceCharacter.Attr.Hokuro4;
			this.Attr.Lip1 = argReplaceCharacter.Attr.Lip1;
			this.Attr.Lip2 = argReplaceCharacter.Attr.Lip2;
			this.Attr.ColorGlasses = argReplaceCharacter.Attr.ColorGlasses;
			this.Attr.Hair1 = argReplaceCharacter.Attr.Hair1;
			this.Attr.Hair2 = argReplaceCharacter.Attr.Hair2;
			this.Attr.Hair3 = argReplaceCharacter.Attr.Hair3;
			this.Attr.Hair4 = argReplaceCharacter.Attr.Hair4;
			this.Attr.HairAdj1 = argReplaceCharacter.Attr.HairAdj1;
			this.Attr.HairAdj2 = argReplaceCharacter.Attr.HairAdj2;
			this.Attr.HairAdj3 = argReplaceCharacter.Attr.HairAdj3;
			this.Attr.HairAdj4 = argReplaceCharacter.Attr.HairAdj4;
			this.Attr.HairRev1 = argReplaceCharacter.Attr.HairRev1;
			this.Attr.HairRev2 = argReplaceCharacter.Attr.HairRev2;
			this.Attr.HairRev3 = argReplaceCharacter.Attr.HairRev3;
			this.Attr.HairRev4 = argReplaceCharacter.Attr.HairRev4;
			this.Attr.ColorHair = argReplaceCharacter.Attr.ColorHair;
			this.Attr.Syakousei = argReplaceCharacter.Attr.Syakousei;
			this.Attr.Kenka = argReplaceCharacter.Attr.Kenka;
			this.Attr.Teisou = argReplaceCharacter.Attr.Teisou;
			this.Attr.Seiai = argReplaceCharacter.Attr.Seiai;
			this.Attr.Voice = argReplaceCharacter.Attr.Voice;
			this.Attr.Cyoroi = argReplaceCharacter.Attr.Cyoroi;
			this.Attr.Nekketu = argReplaceCharacter.Attr.Nekketu;
			this.Attr.Nigate1 = argReplaceCharacter.Attr.Nigate1;
			this.Attr.Nigate2 = argReplaceCharacter.Attr.Nigate2;
			this.Attr.Charm = argReplaceCharacter.Attr.Charm;
			this.Attr.Tundere = argReplaceCharacter.Attr.Tundere;
			this.Attr.Kyouki = argReplaceCharacter.Attr.Kyouki;
			this.Attr.Miha = argReplaceCharacter.Attr.Miha;
			this.Attr.Sunao = argReplaceCharacter.Attr.Sunao;
			this.Attr.Maemuki = argReplaceCharacter.Attr.Maemuki;
			this.Attr.Tereya = argReplaceCharacter.Attr.Tereya;
			this.Attr.Yakimochi = argReplaceCharacter.Attr.Yakimochi;
			this.Attr.Toufu = argReplaceCharacter.Attr.Toufu;
			this.Attr.Sukebe = argReplaceCharacter.Attr.Sukebe;
			this.Attr.Majime = argReplaceCharacter.Attr.Majime;
			this.Attr.Cool = argReplaceCharacter.Attr.Cool;
			this.Attr.Tyokujyo = argReplaceCharacter.Attr.Tyokujyo;
			this.Attr.Poyayan = argReplaceCharacter.Attr.Poyayan;
			this.Attr.Bouryoku = argReplaceCharacter.Attr.Bouryoku;
			this.Attr.Sousyoku = argReplaceCharacter.Attr.Sousyoku;
			this.Attr.Sewayaki = argReplaceCharacter.Attr.Sewayaki;
			this.Attr.Iintyou = argReplaceCharacter.Attr.Iintyou;
			this.Attr.Osyaberi = argReplaceCharacter.Attr.Osyaberi;
			this.Attr.Harapeko = argReplaceCharacter.Attr.Harapeko;
			this.Attr.Renai = argReplaceCharacter.Attr.Renai;
			this.Attr.Itizu = argReplaceCharacter.Attr.Itizu;
			this.Attr.Yuujyufudan = argReplaceCharacter.Attr.Yuujyufudan;
			this.Attr.Makenki = argReplaceCharacter.Attr.Makenki;
			this.Attr.Haraguro = argReplaceCharacter.Attr.Haraguro;
			this.Attr.Kinben = argReplaceCharacter.Attr.Kinben;
			this.Attr.Honpou = argReplaceCharacter.Attr.Honpou;
			this.Attr.M = argReplaceCharacter.Attr.M;
			this.Attr.Asekaki = argReplaceCharacter.Attr.Asekaki;
			this.Attr.Yami = argReplaceCharacter.Attr.Yami;
			this.Attr.Nantyo = argReplaceCharacter.Attr.Nantyo;
			this.Attr.Yowami = argReplaceCharacter.Attr.Yowami;
			this.Attr.Miseityo = argReplaceCharacter.Attr.Miseityo;
			this.Attr.Luck = argReplaceCharacter.Attr.Luck;
			this.Attr.Rare = argReplaceCharacter.Attr.Rare;
			this.Attr.Kiss = argReplaceCharacter.Attr.Kiss;
			this.Attr.Bust = argReplaceCharacter.Attr.Bust;
			this.Attr.Seiki1 = argReplaceCharacter.Attr.Seiki1;
			this.Attr.Seiki2 = argReplaceCharacter.Attr.Seiki2;
			this.Attr.Cunnilingus = argReplaceCharacter.Attr.Cunnilingus;
			this.Attr.Fellatio = argReplaceCharacter.Attr.Fellatio;
			this.Attr.Back = argReplaceCharacter.Attr.Back;
			this.Attr.Onna = argReplaceCharacter.Attr.Onna;
			this.Attr.Anal = argReplaceCharacter.Attr.Anal;
			this.Attr.Nama = argReplaceCharacter.Attr.Nama;
			this.Attr.Seiin = argReplaceCharacter.Attr.Seiin;
			this.Attr.Naka = argReplaceCharacter.Attr.Naka;
			this.Attr.Kake = argReplaceCharacter.Attr.Kake;
			this.Attr.Sikou1 = argReplaceCharacter.Attr.Sikou1;
			this.Attr.Sikou2 = argReplaceCharacter.Attr.Sikou2;
			this.Attr.Sikou3 = argReplaceCharacter.Attr.Sikou3;
			int num = 0;
			do
			{
				this.Attr.Ninshin[num] = argReplaceCharacter.Attr.Ninshin[num];
				num = checked(num + 1);
			}
			while (num <= 13);
			this.ThumbnailLength = argReplaceCharacter.ThumbnailLength;
			JG2Importer.JG2.Common.CopyRawData(ref argReplaceCharacter.Thumbnail, ref this.Thumbnail);
			this.DataOffset = argReplaceCharacter.DataOffset;
			this.PNGImage = argReplaceCharacter.PNGImage;
			this.ThumbnailImage = argReplaceCharacter.ThumbnailImage;
		}

		public long Set(ref byte[] argData, long argStartIndex)
		{
			long num = 0L;
			byte[] numArray = new byte[] { 137, 80, 78, 71, 13, 10, 26, 10 };
			this.IxStart = JG2Importer.JG2.Common.SearchData(argData, numArray, argStartIndex);
			if (this.IxStart == (long)-1)
			{
				return (long)-1;
			}
			numArray = new byte[] { 0, 0, 0, 13, 73, 72, 68, 82 };
			long num1 = JG2Importer.JG2.Common.SearchData(argData, numArray, checked(this.IxStart + (long)8));
			if (num1 != checked(this.IxStart + (long)8))
			{
				return (long)-1;
			}
			numArray = new byte[] { 73, 68, 65, 84 };
			num1 = JG2Importer.JG2.Common.SearchData(argData, numArray, num1);
			if (num1 == (long)-1)
			{
				return (long)-1;
			}
			numArray = new byte[] { 0, 0, 0, 0, 73, 69, 78, 68 };
			num1 = JG2Importer.JG2.Common.SearchData(argData, numArray, checked(checked(checked(num1 + (long)4) + (long)JG2Importer.JG2.Common.ToDWord(ref argData, checked(num1 - (long)4), false)) + (long)4));
			if (num1 == (long)-1)
			{
				return (long)-1;
			}
			this.IxChar = checked(num1 + (long)12);
			if (this.IxChar > (long)(checked(checked((int)argData.Length) - 1)))
			{
				return (long)-1;
			}
			this.IxThumbnailLength = checked(this.IxChar + (long)3011);
			if (this.IxThumbnailLength > (long)(checked(checked((int)argData.Length) - 1)))
			{
				return (long)-1;
			}
			this.IxThumbnail = checked(this.IxThumbnailLength + (long)4);
			if (this.IxThumbnail > (long)(checked(checked((int)argData.Length) - 1)))
			{
				return (long)-1;
			}
			this.IxPlayData = checked(checked(this.IxThumbnail + (long)JG2Importer.JG2.Common.ToDWord(ref argData, this.IxThumbnailLength, true)) + (long)4);
			if (Operators.CompareString(JG2Importer.JG2.Common.ToText(ref argData, checked(this.IxChar + (long)0), 16, false), "【エディット】", false) != 0)
			{
				return (long)-1;
			}
			JG2Importer.JG2.Common.FromRawData(ref argData, this.IxStart, ref this.PNG, checked(this.IxChar - this.IxStart));
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)0), ref this.Attr.HeaderID, (long)16);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)16), ref this.Attr.Version, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)20), ref this.Attr.Sex);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)21), ref this.Attr.Name1, (long)260, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)281), ref this.Attr.Name2, (long)260, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)541), ref this.Attr.Profile, (long)512, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1053), ref this.Attr.Seikaku);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1054), ref this.Attr.Sintyo);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1055), ref this.Attr.Taikei);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1056), ref this.Attr.Mune);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1057), ref this.Attr.ColorSkin1, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1061), ref this.Attr.Glasses1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1062), ref this.Attr.Seikou);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1063), ref this.Attr.Hair);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1064), ref this.Attr.Bukatu1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1065), ref this.Attr.StyleM);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1066), ref this.Attr.Height);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1067), ref this.Attr.Style);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1068), ref this.Attr.Atama1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1069), ref this.Attr.Atama2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1070), ref this.Attr.Waist);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1071), ref this.Attr.Mune1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1072), ref this.Attr.Nyurin);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1073), ref this.Attr.Mune2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1074), ref this.Attr.Mune3);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1075), ref this.Attr.Mune4);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1076), ref this.Attr.Mune5);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1077), ref this.Attr.Mune6);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1078), ref this.Attr.Mune7);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1079), ref this.Attr.Mune8);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1080), ref this.Attr.ColorSkin2, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1084), ref this.Attr.Under1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1085), ref this.Attr.Under2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1086), ref this.Attr.ColorUnder, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1090), ref this.Attr.Tikubi1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1091), ref this.Attr.Tikubi2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1092), ref this.Attr.Tikubi3);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1093), ref this.Attr.Hiyake1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1094), ref this.Attr.Hiyake2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1095), ref this.Attr.Mosaic);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1096), ref this.Attr.Kao);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1097), ref this.Attr.Eye1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1098), ref this.Attr.Eye2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1099), ref this.Attr.Eye3);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1100), ref this.Attr.Eye4);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1101), ref this.Attr.Eye5);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1102), ref this.Attr.Pupil1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1103), ref this.Attr.Pupil2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1104), ref this.Attr.Pupil3);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1105), ref this.Attr.Pupil4);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1106), ref this.Attr.Pupil5);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1107), ref this.Attr.Highlight1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1108), ref this.Attr.Highlight2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1109), ref this.Attr.Highlight3);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1110), ref this.Attr.Highlight4, (long)260);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1370), ref this.Attr.Highlight5, (long)260);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1630), ref this.Attr.Mayu1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1631), ref this.Attr.Mayu2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1632), ref this.Attr.ColorMayu, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1636), ref this.Attr.ColorEye1, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1640), ref this.Attr.ColorEye2, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1644), ref this.Attr.Mabuta);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1645), ref this.Attr.Matuge1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1646), ref this.Attr.Matuge2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1647), ref this.Attr.Glasses2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1648), ref this.Attr.Hokuro1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1649), ref this.Attr.Hokuro2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1650), ref this.Attr.Hokuro3);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1651), ref this.Attr.Hokuro4);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1652), ref this.Attr.Lip1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1653), ref this.Attr.Lip2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1654), ref this.Attr.ColorGlasses, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1658), ref this.Attr.Hair1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1659), ref this.Attr.Hair2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1660), ref this.Attr.Hair3);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1661), ref this.Attr.Hair4);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1662), ref this.Attr.HairAdj1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1663), ref this.Attr.HairAdj2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1664), ref this.Attr.HairAdj3);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1665), ref this.Attr.HairAdj4);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1666), ref this.Attr.HairRev1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1667), ref this.Attr.HairRev2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1668), ref this.Attr.HairRev3);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1669), ref this.Attr.HairRev4);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1670), ref this.Attr.ColorHair, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1674), ref this.Attr.Tiryoku1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1675), ref this.Attr.Tiryoku2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1676), ref this.Attr.Tiryoku3, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1680), ref this.Attr.Tairyoku1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1681), ref this.Attr.Tairyoku2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1682), ref this.Attr.Tairyoku3, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1686), ref this.Attr.Bukatu2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1687), ref this.Attr.Bukatu3, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1691), ref this.Attr.Syakousei);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1692), ref this.Attr.Kenka);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1693), ref this.Attr.Teisou);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1694), ref this.Attr.Seiai);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1695), ref this.Attr.Seikou1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1696), ref this.Attr.Seikou2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1697), ref this.Attr.Voice);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1698), ref this.Attr.Cyoroi);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1699), ref this.Attr.Nekketu);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1700), ref this.Attr.Nigate1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1701), ref this.Attr.Nigate2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1702), ref this.Attr.Charm);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1703), ref this.Attr.Tundere);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1704), ref this.Attr.Kyouki);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1705), ref this.Attr.Miha);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1706), ref this.Attr.Sunao);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1707), ref this.Attr.Maemuki);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1708), ref this.Attr.Tereya);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1709), ref this.Attr.Yakimochi);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1710), ref this.Attr.Toufu);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1711), ref this.Attr.Sukebe);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1712), ref this.Attr.Majime);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1713), ref this.Attr.Cool);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1714), ref this.Attr.Tyokujyo);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1715), ref this.Attr.Poyayan);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1716), ref this.Attr.Bouryoku);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1717), ref this.Attr.Sousyoku);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1718), ref this.Attr.Sewayaki);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1719), ref this.Attr.Iintyou);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1720), ref this.Attr.Osyaberi);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1721), ref this.Attr.Harapeko);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1722), ref this.Attr.Renai);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1723), ref this.Attr.Itizu);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1724), ref this.Attr.Yuujyufudan);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1725), ref this.Attr.Makenki);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1726), ref this.Attr.Haraguro);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1727), ref this.Attr.Kinben);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1728), ref this.Attr.Honpou);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1729), ref this.Attr.M);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1730), ref this.Attr.Asekaki);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1731), ref this.Attr.Yami);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1732), ref this.Attr.Nantyo);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1733), ref this.Attr.Yowami);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1734), ref this.Attr.Miseityo);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1735), ref this.Attr.Luck);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1736), ref this.Attr.Rare);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1737), ref this.Attr.Kiss);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1738), ref this.Attr.Bust);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1739), ref this.Attr.Seiki1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1740), ref this.Attr.Seiki2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1741), ref this.Attr.Cunnilingus);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1742), ref this.Attr.Fellatio);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1743), ref this.Attr.Back);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1744), ref this.Attr.Onna);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1745), ref this.Attr.Anal);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1746), ref this.Attr.Nama);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1747), ref this.Attr.Seiin);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1748), ref this.Attr.Naka);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1749), ref this.Attr.Kake);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1750), ref this.Attr.Sikou1);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1751), ref this.Attr.Sikou2);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1752), ref this.Attr.Sikou3);
			num1 = (long)0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)1753) + num1), ref this.Attr.Ninshin[checked((int)num1)]);
				num1 = checked(num1 + (long)1);
			}
			while (num1 <= (long)13);
			num1 = (long)0;
			do
			{
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)1767) + checked((long)4 * num1)), ref this.Attr.HAisyou[checked((int)num1)], true);
				num1 = checked(num1 + (long)1);
			}
			while (num1 <= (long)24);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)1867), ref this.Attr.Goods1, (long)260, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)2127), ref this.Attr.Goods2, (long)260, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxChar + (long)2387), ref this.Attr.Goods3, (long)260, true);
			num1 = (long)0;
			do
			{
				bool flag = true;
				if (flag == this.Attr.Version < 102)
				{
					num = checked((long)72 * num1);
				}
				else if (flag == (this.Attr.Version == 102))
				{
					num = checked((long)75 * num1);
				}
				else if (flag == this.Attr.Version >= 103)
				{
					num = checked((long)91 * num1);
				}
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2647) + num), ref this.Attr.Costume[checked((int)num1)].Type, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2651) + num), ref this.Attr.Costume[checked((int)num1)].Skirt);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2652) + num), ref this.Attr.Costume[checked((int)num1)].Socks);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2653) + num), ref this.Attr.Costume[checked((int)num1)].Utibaki);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2654) + num), ref this.Attr.Costume[checked((int)num1)].Sotobaki);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2655) + num), ref this.Attr.Costume[checked((int)num1)].ColorTop1, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2659) + num), ref this.Attr.Costume[checked((int)num1)].ColorTop2, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2663) + num), ref this.Attr.Costume[checked((int)num1)].ColorTop3, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2667) + num), ref this.Attr.Costume[checked((int)num1)].ColorTop4, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2671) + num), ref this.Attr.Costume[checked((int)num1)].ColorBottom1, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2675) + num), ref this.Attr.Costume[checked((int)num1)].ColorBottom2, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2679) + num), ref this.Attr.Costume[checked((int)num1)].ColorSitagi, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2683) + num), ref this.Attr.Costume[checked((int)num1)].ColorSocks, (long)4);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2687) + num), ref this.Attr.Costume[checked((int)num1)].ColorUtibaki, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2691) + num), ref this.Attr.Costume[checked((int)num1)].ColorSotobaki, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2695) + num), ref this.Attr.Costume[checked((int)num1)].Bottom1, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2699) + num), ref this.Attr.Costume[checked((int)num1)].Sitagi, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2703) + num), ref this.Attr.Costume[checked((int)num1)].Bottom1Sitaji1, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2707) + num), ref this.Attr.Costume[checked((int)num1)].Bottom1Sitaji2, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2711) + num), ref this.Attr.Costume[checked((int)num1)].SitagiSitaji1, true);
				JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2715) + num), ref this.Attr.Costume[checked((int)num1)].SitagiSitaji2, true);
				if (this.Attr.Version >= 102)
				{
					JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2719) + num), ref this.Attr.Costume[checked((int)num1)].FlagJyoge);
					JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2720) + num), ref this.Attr.Costume[checked((int)num1)].FlagSitagi);
					JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2721) + num), ref this.Attr.Costume[checked((int)num1)].FlagSkirt);
				}
				if (this.Attr.Version >= 103)
				{
					JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2722) + num), ref this.Attr.Costume[checked((int)num1)].Bottom1Kage1, true);
					JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2726) + num), ref this.Attr.Costume[checked((int)num1)].Bottom1Kage2, true);
					JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2730) + num), ref this.Attr.Costume[checked((int)num1)].SitagiKage1, true);
					JG2Importer.JG2.Common.FromRawData(ref argData, checked(checked(this.IxChar + (long)2734) + num), ref this.Attr.Costume[checked((int)num1)].SitagiKage2, true);
				}
				num1 = checked(num1 + (long)1);
			}
			while (num1 <= (long)3);
			JG2Importer.JG2.Common.FromRawData(ref argData, this.IxThumbnailLength, ref this.ThumbnailLength, true);
			JG2Importer.JG2.Common.FromRawData(ref argData, this.IxThumbnail, ref this.Thumbnail, (long)this.ThumbnailLength);
			JG2Importer.JG2.Common.FromRawData(ref argData, checked(this.IxPlayData - (long)4), ref this.DataOffset, true);
			this.PNGImage = JG2Importer.JG2.Common.ToImage(ref this.PNG);
			this.ThumbnailImage = JG2Importer.JG2.Common.ToImage(ref this.Thumbnail);
			return this.IxPlayData;
		}

		public bool Update()
		{
			if (Information.IsNothing(this.DataFile))
			{
				return false;
			}
			return this.Update(this.DataFile.FullName);
		}

		public bool Update(string argNewPathName)
		{
			byte[] numArray = this.Get();
			FileStream fileStream = new FileStream(argNewPathName, FileMode.Create, FileAccess.Write, FileShare.Write);
			long num = (long)0;
			long length = (long)(checked(checked((int)numArray.Length) - 1));
			for (long i = num; i <= length; i = checked(i + (long)1))
			{
				fileStream.WriteByte(numArray[checked((int)i)]);
			}
			fileStream.Close();
			fileStream.Dispose();
			fileStream = null;
			numArray = null;
			return true;
		}

		public struct DefAttribute
		{
			public byte[] HeaderID;

			public int Version;

			public byte Sex;

			public string Name1;

			public string Name2;

			public string Profile;

			public byte Seikaku;

			public byte Sintyo;

			public byte Taikei;

			public byte Mune;

			public int ColorSkin1;

			public byte Glasses1;

			public byte Seikou;

			public byte Hair;

			public byte Bukatu1;

			public byte StyleM;

			public byte Height;

			public byte Style;

			public byte Atama1;

			public byte Atama2;

			public byte Waist;

			public byte Mune1;

			public byte Nyurin;

			public byte Mune2;

			public byte Mune3;

			public byte Mune4;

			public byte Mune5;

			public byte Mune6;

			public byte Mune7;

			public byte Mune8;

			public int ColorSkin2;

			public byte Under1;

			public byte Under2;

			public int ColorUnder;

			public byte Tikubi1;

			public byte Tikubi2;

			public byte Tikubi3;

			public byte Hiyake1;

			public byte Hiyake2;

			public byte Mosaic;

			public byte Kao;

			public byte Eye1;

			public byte Eye2;

			public byte Eye3;

			public byte Eye4;

			public byte Eye5;

			public byte Pupil1;

			public byte Pupil2;

			public byte Pupil3;

			public byte Pupil4;

			public byte Pupil5;

			public byte Highlight1;

			public byte Highlight2;

			public byte Highlight3;

			public byte[] Highlight4;

			public byte[] Highlight5;

			public byte Mayu1;

			public byte Mayu2;

			public int ColorMayu;

			public int ColorEye1;

			public int ColorEye2;

			public byte Mabuta;

			public byte Matuge1;

			public byte Matuge2;

			public byte Glasses2;

			public byte Hokuro1;

			public byte Hokuro2;

			public byte Hokuro3;

			public byte Hokuro4;

			public byte Lip1;

			public byte Lip2;

			public int ColorGlasses;

			public byte Hair1;

			public byte Hair2;

			public byte Hair3;

			public byte Hair4;

			public byte HairAdj1;

			public byte HairAdj2;

			public byte HairAdj3;

			public byte HairAdj4;

			public byte HairRev1;

			public byte HairRev2;

			public byte HairRev3;

			public byte HairRev4;

			public int ColorHair;

			public byte Tiryoku1;

			public byte Tiryoku2;

			public int Tiryoku3;

			public byte Tairyoku1;

			public byte Tairyoku2;

			public int Tairyoku3;

			public byte Bukatu2;

			public int Bukatu3;

			public byte Syakousei;

			public byte Kenka;

			public byte Teisou;

			public byte Seiai;

			public byte Seikou1;

			public byte Seikou2;

			public byte Voice;

			public byte Cyoroi;

			public byte Nekketu;

			public byte Nigate1;

			public byte Nigate2;

			public byte Charm;

			public byte Tundere;

			public byte Kyouki;

			public byte Miha;

			public byte Sunao;

			public byte Maemuki;

			public byte Tereya;

			public byte Yakimochi;

			public byte Toufu;

			public byte Sukebe;

			public byte Majime;

			public byte Cool;

			public byte Tyokujyo;

			public byte Poyayan;

			public byte Bouryoku;

			public byte Sousyoku;

			public byte Sewayaki;

			public byte Iintyou;

			public byte Osyaberi;

			public byte Harapeko;

			public byte Renai;

			public byte Itizu;

			public byte Yuujyufudan;

			public byte Makenki;

			public byte Haraguro;

			public byte Kinben;

			public byte Honpou;

			public byte M;

			public byte Asekaki;

			public byte Yami;

			public byte Nantyo;

			public byte Yowami;

			public byte Miseityo;

			public byte Luck;

			public byte Rare;

			public byte Kiss;

			public byte Bust;

			public byte Seiki1;

			public byte Seiki2;

			public byte Cunnilingus;

			public byte Fellatio;

			public byte Back;

			public byte Onna;

			public byte Anal;

			public byte Nama;

			public byte Seiin;

			public byte Naka;

			public byte Kake;

			public byte Sikou1;

			public byte Sikou2;

			public byte Sikou3;

			public byte[] Ninshin;

			public int[] HAisyou;

			public string Goods1;

			public string Goods2;

			public string Goods3;

			public Character.DefCostume[] Costume;
		}

		public struct DefCostume
		{
			public int Type;

			public byte Skirt;

			public byte Socks;

			public byte Utibaki;

			public byte Sotobaki;

			public int ColorTop1;

			public int ColorTop2;

			public int ColorTop3;

			public int ColorTop4;

			public int ColorBottom1;

			public int ColorBottom2;

			public int ColorSitagi;

			public byte[] ColorSocks;

			public int ColorUtibaki;

			public int ColorSotobaki;

			public int Bottom1;

			public int Sitagi;

			public int Bottom1Sitaji1;

			public int Bottom1Sitaji2;

			public int SitagiSitaji1;

			public int SitagiSitaji2;

			public byte FlagJyoge;

			public byte FlagSitagi;

			public byte FlagSkirt;

			public int Bottom1Kage1;

			public int Bottom1Kage2;

			public int SitagiKage1;

			public int SitagiKage2;
		}
	}
}