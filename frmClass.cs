using JG2Importer.JG2;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

namespace JG2Importer
{
	[DesignerGenerated]
	public class frmClass : Form
	{
		private IContainer components;

		[AccessedThroughProperty("pnlHeader")]
		private Panel _pnlHeader;

		[AccessedThroughProperty("Label11")]
		private Label _Label11;

		[AccessedThroughProperty("lblSchool")]
		private Label _lblSchool;

		[AccessedThroughProperty("pnlClass")]
		private Panel _pnlClass;

		[AccessedThroughProperty("usrSeat20")]
		private usrSeat _usrSeat20;

		[AccessedThroughProperty("usrSeat21")]
		private usrSeat _usrSeat21;

		[AccessedThroughProperty("usrSeat22")]
		private usrSeat _usrSeat22;

		[AccessedThroughProperty("usrSeat23")]
		private usrSeat _usrSeat23;

		[AccessedThroughProperty("usrSeat24")]
		private usrSeat _usrSeat24;

		[AccessedThroughProperty("usrSeat10")]
		private usrSeat _usrSeat10;

		[AccessedThroughProperty("usrSeat11")]
		private usrSeat _usrSeat11;

		[AccessedThroughProperty("usrSeat12")]
		private usrSeat _usrSeat12;

		[AccessedThroughProperty("usrSeat13")]
		private usrSeat _usrSeat13;

		[AccessedThroughProperty("usrSeat14")]
		private usrSeat _usrSeat14;

		[AccessedThroughProperty("usrSeat15")]
		private usrSeat _usrSeat15;

		[AccessedThroughProperty("usrSeat16")]
		private usrSeat _usrSeat16;

		[AccessedThroughProperty("usrSeat17")]
		private usrSeat _usrSeat17;

		[AccessedThroughProperty("usrSeat18")]
		private usrSeat _usrSeat18;

		[AccessedThroughProperty("usrSeat19")]
		private usrSeat _usrSeat19;

		[AccessedThroughProperty("usrSeat09")]
		private usrSeat _usrSeat09;

		[AccessedThroughProperty("usrSeat07")]
		private usrSeat _usrSeat07;

		[AccessedThroughProperty("usrSeat08")]
		private usrSeat _usrSeat08;

		[AccessedThroughProperty("usrSeat06")]
		private usrSeat _usrSeat06;

		[AccessedThroughProperty("usrSeat04")]
		private usrSeat _usrSeat04;

		[AccessedThroughProperty("usrSeat05")]
		private usrSeat _usrSeat05;

		[AccessedThroughProperty("usrSeat02")]
		private usrSeat _usrSeat02;

		[AccessedThroughProperty("usrSeat03")]
		private usrSeat _usrSeat03;

		[AccessedThroughProperty("usrSeat01")]
		private usrSeat _usrSeat01;

		[AccessedThroughProperty("usrSeat00")]
		private usrSeat _usrSeat00;

		[AccessedThroughProperty("fswDataFile")]
		private FileSystemWatcher _fswDataFile;

		[AccessedThroughProperty("MenuStrip")]
		private System.Windows.Forms.MenuStrip _MenuStrip;

		[AccessedThroughProperty("mnuFile")]
		private ToolStripMenuItem _mnuFile;

		[AccessedThroughProperty("mnuOpen")]
		private ToolStripMenuItem _mnuOpen;

		[AccessedThroughProperty("toolStripSeparator")]
		private ToolStripSeparator _toolStripSeparator;

		[AccessedThroughProperty("mnuUpdate")]
		private ToolStripMenuItem _mnuUpdate;

		[AccessedThroughProperty("toolStripSeparator1")]
		private ToolStripSeparator _toolStripSeparator1;

		[AccessedThroughProperty("toolStripSeparator2")]
		private ToolStripSeparator _toolStripSeparator2;

		[AccessedThroughProperty("mnuClose")]
		private ToolStripMenuItem _mnuClose;

		[AccessedThroughProperty("mnuSelectPlayer")]
		private ToolStripMenuItem _mnuSelectPlayer;

		[AccessedThroughProperty("mnuSelectTeacher")]
		private ToolStripMenuItem _mnuSelectTeacher;

		[AccessedThroughProperty("mnuView")]
		private JG2ToolStripMenuItem _mnuView;

		[AccessedThroughProperty("mnuReload")]
		private ToolStripMenuItem _mnuReload;

		[AccessedThroughProperty("mnuAutoReload")]
		private ToolStripMenuItem _mnuAutoReload;

		[AccessedThroughProperty("ToolStripSeparator3")]
		private ToolStripSeparator _ToolStripSeparator3;

		[AccessedThroughProperty("ToolStripSeparator4")]
		private ToolStripSeparator _ToolStripSeparator4;

		[AccessedThroughProperty("mnuSelectAll")]
		private ToolStripMenuItem _mnuSelectAll;

		[AccessedThroughProperty("mnuClearAll")]
		private ToolStripMenuItem _mnuClearAll;

		[AccessedThroughProperty("mnuShuffleBukatuSeparator")]
		private ToolStripSeparator _mnuShuffleBukatuSeparator;

		[AccessedThroughProperty("mnuShuffleBukatu")]
		private ToolStripMenuItem _mnuShuffleBukatu;

		[AccessedThroughProperty("ToolStripSeparator15")]
		private ToolStripSeparator _ToolStripSeparator15;

		[AccessedThroughProperty("mnuCopy")]
		private ToolStripMenuItem _mnuCopy;

		[AccessedThroughProperty("ctmSeat")]
		private System.Windows.Forms.ContextMenuStrip _ctmSeat;

		[AccessedThroughProperty("mnuEdit")]
		private JG2ToolStripMenuItem _mnuEdit;

		[AccessedThroughProperty("mnuSelectCondition")]
		private ToolStripMenuItem _mnuSelectCondition;

		[AccessedThroughProperty("mnuShuffleSitagi")]
		private ToolStripMenuItem _mnuShuffleSitagi;

		[AccessedThroughProperty("ToolStripSeparator5")]
		private ToolStripSeparator _ToolStripSeparator5;

		[AccessedThroughProperty("mnuShuffleKenka")]
		private ToolStripMenuItem _mnuShuffleKenka;

		[AccessedThroughProperty("mnuDisplayItem01")]
		private JG2ToolStripMenuItem _mnuDisplayItem01;

		[AccessedThroughProperty("mnuDisplayItem02")]
		private JG2ToolStripMenuItem _mnuDisplayItem02;

		[AccessedThroughProperty("ToolStripSeparator6")]
		private ToolStripSeparator _ToolStripSeparator6;

		[AccessedThroughProperty("mnuDisplayItem1")]
		private ToolStripMenuItem _mnuDisplayItem1;

		[AccessedThroughProperty("mnuDisplayItem11")]
		private ToolStripMenuItem _mnuDisplayItem11;

		[AccessedThroughProperty("mnuDisplayItem12")]
		private ToolStripMenuItem _mnuDisplayItem12;

		[AccessedThroughProperty("mnuDisplayItem4")]
		private ToolStripMenuItem _mnuDisplayItem4;

		[AccessedThroughProperty("mnuDisplayItem7")]
		private ToolStripMenuItem _mnuDisplayItem7;

		[AccessedThroughProperty("mnuDisplayItem6")]
		private ToolStripMenuItem _mnuDisplayItem6;

		[AccessedThroughProperty("mnuDisplayItem2")]
		private ToolStripMenuItem _mnuDisplayItem2;

		[AccessedThroughProperty("mnuDisplayItem3")]
		private ToolStripMenuItem _mnuDisplayItem3;

		[AccessedThroughProperty("mnuDisplayItem0")]
		private JG2ToolStripMenuItem _mnuDisplayItem0;

		[AccessedThroughProperty("mnuDisplayItem")]
		private ToolStripMenuItem _mnuDisplayItem;

		[AccessedThroughProperty("mnuDisplayItem5")]
		private ToolStripMenuItem _mnuDisplayItem5;

		[AccessedThroughProperty("ToolStripSeparator7")]
		private ToolStripSeparator _ToolStripSeparator7;

		[AccessedThroughProperty("mnuDisplayItem00")]
		private JG2ToolStripMenuItem _mnuDisplayItem00;

		[AccessedThroughProperty("ToolStripSeparator8")]
		private ToolStripSeparator _ToolStripSeparator8;

		[AccessedThroughProperty("mnuDisplayItem8")]
		private ToolStripMenuItem _mnuDisplayItem8;

		[AccessedThroughProperty("mnuDisplayItem9")]
		private ToolStripMenuItem _mnuDisplayItem9;

		[AccessedThroughProperty("mnuDisplayItem10")]
		private ToolStripMenuItem _mnuDisplayItem10;

		[AccessedThroughProperty("ToolStripSeparator9")]
		private ToolStripSeparator _ToolStripSeparator9;

		[AccessedThroughProperty("mnuDisplayItem0Option1")]
		private ToolStripMenuItem _mnuDisplayItem0Option1;

		[AccessedThroughProperty("cmbEvent")]
		private JG2ComboBox _cmbEvent;

		[AccessedThroughProperty("cmbWeek")]
		private JG2ComboBox _cmbWeek;

		[AccessedThroughProperty("numDays")]
		private JG2NumericUpDown _numDays;

		private frmDetail MyDetail;

		private Color ColorClassDefault;

		public virtual JG2ComboBox cmbEvent
		{
			get
			{
				return this._cmbEvent;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.numDays_ValueChanged);
				if (this._cmbEvent != null)
				{
					this._cmbEvent.SelectedIndexChanged -= eventHandler;
				}
				this._cmbEvent = value;
				if (this._cmbEvent != null)
				{
					this._cmbEvent.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual JG2ComboBox cmbWeek
		{
			get
			{
				return this._cmbWeek;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.numDays_ValueChanged);
				if (this._cmbWeek != null)
				{
					this._cmbWeek.SelectedIndexChanged -= eventHandler;
				}
				this._cmbWeek = value;
				if (this._cmbWeek != null)
				{
					this._cmbWeek.SelectedIndexChanged += eventHandler;
				}
			}
		}

		public virtual System.Windows.Forms.ContextMenuStrip ctmSeat
		{
			get
			{
				return this._ctmSeat;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				CancelEventHandler cancelEventHandler = new CancelEventHandler(_frmClass.ctmSeat_Opening);
				if (this._ctmSeat != null)
				{
					this._ctmSeat.Opening -= cancelEventHandler;
				}
				this._ctmSeat = value;
				if (this._ctmSeat != null)
				{
					this._ctmSeat.Opening += cancelEventHandler;
				}
			}
		}

		public virtual FileSystemWatcher fswDataFile
		{
			get
			{
				return this._fswDataFile;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				FileSystemEventHandler fileSystemEventHandler = new FileSystemEventHandler(_frmClass.fswDataFile_Changed);
				if (this._fswDataFile != null)
				{
					this._fswDataFile.Changed -= fileSystemEventHandler;
				}
				this._fswDataFile = value;
				if (this._fswDataFile != null)
				{
					this._fswDataFile.Changed += fileSystemEventHandler;
				}
			}
		}

		public virtual Label Label11
		{
			get
			{
				return this._Label11;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label11 = value;
			}
		}

		public virtual Label lblSchool
		{
			get
			{
				return this._lblSchool;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblSchool = value;
			}
		}

		public virtual System.Windows.Forms.MenuStrip MenuStrip
		{
			get
			{
				return this._MenuStrip;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._MenuStrip = value;
			}
		}

		public virtual ToolStripMenuItem mnuAutoReload
		{
			get
			{
				return this._mnuAutoReload;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuAutoReload_CheckedChanged);
				if (this._mnuAutoReload != null)
				{
					this._mnuAutoReload.CheckedChanged -= eventHandler;
				}
				this._mnuAutoReload = value;
				if (this._mnuAutoReload != null)
				{
					this._mnuAutoReload.CheckedChanged += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuClearAll
		{
			get
			{
				return this._mnuClearAll;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuClearAll_Click);
				if (this._mnuClearAll != null)
				{
					this._mnuClearAll.Click -= eventHandler;
				}
				this._mnuClearAll = value;
				if (this._mnuClearAll != null)
				{
					this._mnuClearAll.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuClose
		{
			get
			{
				return this._mnuClose;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuClose_Click);
				if (this._mnuClose != null)
				{
					this._mnuClose.Click -= eventHandler;
				}
				this._mnuClose = value;
				if (this._mnuClose != null)
				{
					this._mnuClose.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuCopy
		{
			get
			{
				return this._mnuCopy;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuCopy_Click);
				if (this._mnuCopy != null)
				{
					this._mnuCopy.Click -= eventHandler;
				}
				this._mnuCopy = value;
				if (this._mnuCopy != null)
				{
					this._mnuCopy.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem
		{
			get
			{
				return this._mnuDisplayItem;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuDisplayItem = value;
			}
		}

		public virtual JG2ToolStripMenuItem mnuDisplayItem0
		{
			get
			{
				return this._mnuDisplayItem0;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuDisplayItem0 = value;
			}
		}

		public virtual JG2ToolStripMenuItem mnuDisplayItem00
		{
			get
			{
				return this._mnuDisplayItem00;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem00 != null)
				{
					this._mnuDisplayItem00.Click -= eventHandler;
				}
				this._mnuDisplayItem00 = value;
				if (this._mnuDisplayItem00 != null)
				{
					this._mnuDisplayItem00.Click += eventHandler;
				}
			}
		}

		public virtual JG2ToolStripMenuItem mnuDisplayItem01
		{
			get
			{
				return this._mnuDisplayItem01;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem01 != null)
				{
					this._mnuDisplayItem01.Click -= eventHandler;
				}
				this._mnuDisplayItem01 = value;
				if (this._mnuDisplayItem01 != null)
				{
					this._mnuDisplayItem01.Click += eventHandler;
				}
			}
		}

		public virtual JG2ToolStripMenuItem mnuDisplayItem02
		{
			get
			{
				return this._mnuDisplayItem02;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem02 != null)
				{
					this._mnuDisplayItem02.Click -= eventHandler;
				}
				this._mnuDisplayItem02 = value;
				if (this._mnuDisplayItem02 != null)
				{
					this._mnuDisplayItem02.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem0Option1
		{
			get
			{
				return this._mnuDisplayItem0Option1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem0Option1 != null)
				{
					this._mnuDisplayItem0Option1.Click -= eventHandler;
				}
				this._mnuDisplayItem0Option1 = value;
				if (this._mnuDisplayItem0Option1 != null)
				{
					this._mnuDisplayItem0Option1.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem1
		{
			get
			{
				return this._mnuDisplayItem1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem1 != null)
				{
					this._mnuDisplayItem1.Click -= eventHandler;
				}
				this._mnuDisplayItem1 = value;
				if (this._mnuDisplayItem1 != null)
				{
					this._mnuDisplayItem1.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem10
		{
			get
			{
				return this._mnuDisplayItem10;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem10 != null)
				{
					this._mnuDisplayItem10.Click -= eventHandler;
				}
				this._mnuDisplayItem10 = value;
				if (this._mnuDisplayItem10 != null)
				{
					this._mnuDisplayItem10.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem11
		{
			get
			{
				return this._mnuDisplayItem11;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem11 != null)
				{
					this._mnuDisplayItem11.Click -= eventHandler;
				}
				this._mnuDisplayItem11 = value;
				if (this._mnuDisplayItem11 != null)
				{
					this._mnuDisplayItem11.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem12
		{
			get
			{
				return this._mnuDisplayItem12;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem12 != null)
				{
					this._mnuDisplayItem12.Click -= eventHandler;
				}
				this._mnuDisplayItem12 = value;
				if (this._mnuDisplayItem12 != null)
				{
					this._mnuDisplayItem12.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem2
		{
			get
			{
				return this._mnuDisplayItem2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem2 != null)
				{
					this._mnuDisplayItem2.Click -= eventHandler;
				}
				this._mnuDisplayItem2 = value;
				if (this._mnuDisplayItem2 != null)
				{
					this._mnuDisplayItem2.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem3
		{
			get
			{
				return this._mnuDisplayItem3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem3 != null)
				{
					this._mnuDisplayItem3.Click -= eventHandler;
				}
				this._mnuDisplayItem3 = value;
				if (this._mnuDisplayItem3 != null)
				{
					this._mnuDisplayItem3.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem4
		{
			get
			{
				return this._mnuDisplayItem4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem4 != null)
				{
					this._mnuDisplayItem4.Click -= eventHandler;
				}
				this._mnuDisplayItem4 = value;
				if (this._mnuDisplayItem4 != null)
				{
					this._mnuDisplayItem4.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem5
		{
			get
			{
				return this._mnuDisplayItem5;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem5 != null)
				{
					this._mnuDisplayItem5.Click -= eventHandler;
				}
				this._mnuDisplayItem5 = value;
				if (this._mnuDisplayItem5 != null)
				{
					this._mnuDisplayItem5.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem6
		{
			get
			{
				return this._mnuDisplayItem6;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem6 != null)
				{
					this._mnuDisplayItem6.Click -= eventHandler;
				}
				this._mnuDisplayItem6 = value;
				if (this._mnuDisplayItem6 != null)
				{
					this._mnuDisplayItem6.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem7
		{
			get
			{
				return this._mnuDisplayItem7;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem7 != null)
				{
					this._mnuDisplayItem7.Click -= eventHandler;
				}
				this._mnuDisplayItem7 = value;
				if (this._mnuDisplayItem7 != null)
				{
					this._mnuDisplayItem7.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem8
		{
			get
			{
				return this._mnuDisplayItem8;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem8 != null)
				{
					this._mnuDisplayItem8.Click -= eventHandler;
				}
				this._mnuDisplayItem8 = value;
				if (this._mnuDisplayItem8 != null)
				{
					this._mnuDisplayItem8.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuDisplayItem9
		{
			get
			{
				return this._mnuDisplayItem9;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuDisplayItem_Click);
				if (this._mnuDisplayItem9 != null)
				{
					this._mnuDisplayItem9.Click -= eventHandler;
				}
				this._mnuDisplayItem9 = value;
				if (this._mnuDisplayItem9 != null)
				{
					this._mnuDisplayItem9.Click += eventHandler;
				}
			}
		}

		public virtual JG2ToolStripMenuItem mnuEdit
		{
			get
			{
				return this._mnuEdit;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuEdit = value;
			}
		}

		public virtual ToolStripMenuItem mnuFile
		{
			get
			{
				return this._mnuFile;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuFile = value;
			}
		}

		public virtual ToolStripMenuItem mnuOpen
		{
			get
			{
				return this._mnuOpen;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuOpen_Click);
				if (this._mnuOpen != null)
				{
					this._mnuOpen.Click -= eventHandler;
				}
				this._mnuOpen = value;
				if (this._mnuOpen != null)
				{
					this._mnuOpen.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuReload
		{
			get
			{
				return this._mnuReload;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuReload_Click);
				if (this._mnuReload != null)
				{
					this._mnuReload.Click -= eventHandler;
				}
				this._mnuReload = value;
				if (this._mnuReload != null)
				{
					this._mnuReload.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuSelectAll
		{
			get
			{
				return this._mnuSelectAll;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuSelectAll_Click);
				if (this._mnuSelectAll != null)
				{
					this._mnuSelectAll.Click -= eventHandler;
				}
				this._mnuSelectAll = value;
				if (this._mnuSelectAll != null)
				{
					this._mnuSelectAll.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuSelectCondition
		{
			get
			{
				return this._mnuSelectCondition;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuSelectCondition = value;
			}
		}

		public virtual ToolStripMenuItem mnuSelectPlayer
		{
			get
			{
				return this._mnuSelectPlayer;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuSelect_Click);
				if (this._mnuSelectPlayer != null)
				{
					this._mnuSelectPlayer.Click -= eventHandler;
				}
				this._mnuSelectPlayer = value;
				if (this._mnuSelectPlayer != null)
				{
					this._mnuSelectPlayer.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuSelectTeacher
		{
			get
			{
				return this._mnuSelectTeacher;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuSelect_Click);
				if (this._mnuSelectTeacher != null)
				{
					this._mnuSelectTeacher.Click -= eventHandler;
				}
				this._mnuSelectTeacher = value;
				if (this._mnuSelectTeacher != null)
				{
					this._mnuSelectTeacher.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuShuffleBukatu
		{
			get
			{
				return this._mnuShuffleBukatu;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuShuffle_Click);
				if (this._mnuShuffleBukatu != null)
				{
					this._mnuShuffleBukatu.Click -= eventHandler;
				}
				this._mnuShuffleBukatu = value;
				if (this._mnuShuffleBukatu != null)
				{
					this._mnuShuffleBukatu.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripSeparator mnuShuffleBukatuSeparator
		{
			get
			{
				return this._mnuShuffleBukatuSeparator;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuShuffleBukatuSeparator = value;
			}
		}

		public virtual ToolStripMenuItem mnuShuffleKenka
		{
			get
			{
				return this._mnuShuffleKenka;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuShuffle_Click);
				if (this._mnuShuffleKenka != null)
				{
					this._mnuShuffleKenka.Click -= eventHandler;
				}
				this._mnuShuffleKenka = value;
				if (this._mnuShuffleKenka != null)
				{
					this._mnuShuffleKenka.Click += eventHandler;
				}
			}
		}

		public virtual ToolStripMenuItem mnuShuffleSitagi
		{
			get
			{
				return this._mnuShuffleSitagi;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuShuffleSitagi = value;
			}
		}

		public virtual ToolStripMenuItem mnuUpdate
		{
			get
			{
				return this._mnuUpdate;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				EventHandler eventHandler = new EventHandler(_frmClass.mnuUpdate_Click);
				if (this._mnuUpdate != null)
				{
					this._mnuUpdate.Click -= eventHandler;
				}
				this._mnuUpdate = value;
				if (this._mnuUpdate != null)
				{
					this._mnuUpdate.Click += eventHandler;
				}
			}
		}

		public virtual JG2ToolStripMenuItem mnuView
		{
			get
			{
				return this._mnuView;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._mnuView = value;
			}
		}

		public virtual JG2NumericUpDown numDays
		{
			get
			{
				return this._numDays;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._numDays = value;
			}
		}

		internal virtual Panel pnlClass
		{
			get
			{
				return this._pnlClass;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlClass = value;
			}
		}

		public virtual Panel pnlHeader
		{
			get
			{
				return this._pnlHeader;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlHeader = value;
			}
		}

		public virtual ToolStripSeparator toolStripSeparator
		{
			get
			{
				return this._toolStripSeparator;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._toolStripSeparator = value;
			}
		}

		public virtual ToolStripSeparator toolStripSeparator1
		{
			get
			{
				return this._toolStripSeparator1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._toolStripSeparator1 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator15
		{
			get
			{
				return this._ToolStripSeparator15;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator15 = value;
			}
		}

		public virtual ToolStripSeparator toolStripSeparator2
		{
			get
			{
				return this._toolStripSeparator2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._toolStripSeparator2 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator3
		{
			get
			{
				return this._ToolStripSeparator3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator3 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator4
		{
			get
			{
				return this._ToolStripSeparator4;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator4 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator5
		{
			get
			{
				return this._ToolStripSeparator5;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator5 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator6
		{
			get
			{
				return this._ToolStripSeparator6;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator6 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator7
		{
			get
			{
				return this._ToolStripSeparator7;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator7 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator8
		{
			get
			{
				return this._ToolStripSeparator8;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator8 = value;
			}
		}

		public virtual ToolStripSeparator ToolStripSeparator9
		{
			get
			{
				return this._ToolStripSeparator9;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripSeparator9 = value;
			}
		}

		public virtual usrSeat usrSeat00
		{
			get
			{
				return this._usrSeat00;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat00 != null)
				{
					this._usrSeat00.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat00 = value;
				if (this._usrSeat00 != null)
				{
					this._usrSeat00.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat01
		{
			get
			{
				return this._usrSeat01;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat01 != null)
				{
					this._usrSeat01.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat01 = value;
				if (this._usrSeat01 != null)
				{
					this._usrSeat01.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat02
		{
			get
			{
				return this._usrSeat02;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat02 != null)
				{
					this._usrSeat02.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat02 = value;
				if (this._usrSeat02 != null)
				{
					this._usrSeat02.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat03
		{
			get
			{
				return this._usrSeat03;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat03 != null)
				{
					this._usrSeat03.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat03 = value;
				if (this._usrSeat03 != null)
				{
					this._usrSeat03.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat04
		{
			get
			{
				return this._usrSeat04;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat04 != null)
				{
					this._usrSeat04.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat04 = value;
				if (this._usrSeat04 != null)
				{
					this._usrSeat04.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat05
		{
			get
			{
				return this._usrSeat05;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat05 != null)
				{
					this._usrSeat05.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat05 = value;
				if (this._usrSeat05 != null)
				{
					this._usrSeat05.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat06
		{
			get
			{
				return this._usrSeat06;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat06 != null)
				{
					this._usrSeat06.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat06 = value;
				if (this._usrSeat06 != null)
				{
					this._usrSeat06.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat07
		{
			get
			{
				return this._usrSeat07;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat07 != null)
				{
					this._usrSeat07.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat07 = value;
				if (this._usrSeat07 != null)
				{
					this._usrSeat07.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat08
		{
			get
			{
				return this._usrSeat08;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat08 != null)
				{
					this._usrSeat08.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat08 = value;
				if (this._usrSeat08 != null)
				{
					this._usrSeat08.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat09
		{
			get
			{
				return this._usrSeat09;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat09 != null)
				{
					this._usrSeat09.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat09 = value;
				if (this._usrSeat09 != null)
				{
					this._usrSeat09.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat10
		{
			get
			{
				return this._usrSeat10;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat10 != null)
				{
					this._usrSeat10.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat10 = value;
				if (this._usrSeat10 != null)
				{
					this._usrSeat10.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat11
		{
			get
			{
				return this._usrSeat11;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat11 != null)
				{
					this._usrSeat11.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat11 = value;
				if (this._usrSeat11 != null)
				{
					this._usrSeat11.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat12
		{
			get
			{
				return this._usrSeat12;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat12 != null)
				{
					this._usrSeat12.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat12 = value;
				if (this._usrSeat12 != null)
				{
					this._usrSeat12.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat13
		{
			get
			{
				return this._usrSeat13;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat13 != null)
				{
					this._usrSeat13.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat13 = value;
				if (this._usrSeat13 != null)
				{
					this._usrSeat13.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat14
		{
			get
			{
				return this._usrSeat14;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat14 != null)
				{
					this._usrSeat14.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat14 = value;
				if (this._usrSeat14 != null)
				{
					this._usrSeat14.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat15
		{
			get
			{
				return this._usrSeat15;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat15 != null)
				{
					this._usrSeat15.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat15 = value;
				if (this._usrSeat15 != null)
				{
					this._usrSeat15.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat16
		{
			get
			{
				return this._usrSeat16;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat16 != null)
				{
					this._usrSeat16.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat16 = value;
				if (this._usrSeat16 != null)
				{
					this._usrSeat16.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat17
		{
			get
			{
				return this._usrSeat17;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat17 != null)
				{
					this._usrSeat17.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat17 = value;
				if (this._usrSeat17 != null)
				{
					this._usrSeat17.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat18
		{
			get
			{
				return this._usrSeat18;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat18 != null)
				{
					this._usrSeat18.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat18 = value;
				if (this._usrSeat18 != null)
				{
					this._usrSeat18.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat19
		{
			get
			{
				return this._usrSeat19;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat19 != null)
				{
					this._usrSeat19.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat19 = value;
				if (this._usrSeat19 != null)
				{
					this._usrSeat19.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat20
		{
			get
			{
				return this._usrSeat20;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat20 != null)
				{
					this._usrSeat20.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat20 = value;
				if (this._usrSeat20 != null)
				{
					this._usrSeat20.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat21
		{
			get
			{
				return this._usrSeat21;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat21 != null)
				{
					this._usrSeat21.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat21 = value;
				if (this._usrSeat21 != null)
				{
					this._usrSeat21.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat22
		{
			get
			{
				return this._usrSeat22;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat22 != null)
				{
					this._usrSeat22.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat22 = value;
				if (this._usrSeat22 != null)
				{
					this._usrSeat22.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat23
		{
			get
			{
				return this._usrSeat23;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat23 != null)
				{
					this._usrSeat23.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat23 = value;
				if (this._usrSeat23 != null)
				{
					this._usrSeat23.Click_Image += clickImageEventHandler;
				}
			}
		}

		public virtual usrSeat usrSeat24
		{
			get
			{
				return this._usrSeat24;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				frmClass _frmClass = this;
				usrSeat.Click_ImageEventHandler clickImageEventHandler = new usrSeat.Click_ImageEventHandler(_frmClass.usrSeat_Click_Image);
				if (this._usrSeat24 != null)
				{
					this._usrSeat24.Click_Image -= clickImageEventHandler;
				}
				this._usrSeat24 = value;
				if (this._usrSeat24 != null)
				{
					this._usrSeat24.Click_Image += clickImageEventHandler;
				}
			}
		}

		public frmClass()
		{
			frmClass _frmClass = this;
			base.Load += new EventHandler(_frmClass.frmSchool_Load);
			this.mnuFile = new ToolStripMenuItem();
			this.mnuView = new JG2ToolStripMenuItem();
			this.ColorClassDefault = Color.FromArgb(210, 183, 167);
			this.InitializeComponent();
			this.Icon = Resources.JG2Importer;
			this.Text = Application.ProductName;
			ToolStripMenuItem toolStripMenuItem = this.mnuSelectCondition;
			this.CreateSelectCondition(ref toolStripMenuItem);
			this.mnuSelectCondition = toolStripMenuItem;
			this.mnuDisplayItem8.Text = Resources.CaptionHeight;
			this.mnuDisplayItem9.Text = Resources.CaptionTaikei;
			this.mnuDisplayItem10.Text = Resources.CaptionMune;
			this.mnuDisplayItem11.Text = Resources.CaptionSeikou1;
			this.mnuDisplayItem12.Text = Resources.CaptionSeikou2;
			this.mnuDisplayItem0.Text = Resources.CaptionCodeAction;
			this.mnuDisplayItem01.Text = Resources.CaptionCodeAction1;
			this.mnuDisplayItem02.Text = Resources.CaptionCodeAction2;
			this.mnuDisplayItem1.Text = Resources.CaptionKousai;
			this.mnuDisplayItem2.Text = Resources.CaptionCodeDate;
			this.mnuDisplayItem3.Text = Resources.CaptionCodeGohoubi;
			this.mnuDisplayItem4.Text = Resources.CaptionCodeHAisyou;
			this.mnuDisplayItem5.Text = Resources.CaptionCodeCntNakaKiken;
			this.mnuDisplayItem6.Text = Resources.CaptionCodeRenzoku;
			this.mnuDisplayItem7.Text = Resources.CaptionCodeCntTodaySeikou;
			this.mnuShuffleSitagi.Visible = false;
			frmClass _frmClass1 = this;
			this.numDays.ValueChanged += new EventHandler(_frmClass1.numDays_ValueChanged);
		}

		public void ClearSeatCheck()
		{
			int num = 0;
			do
			{
				usrSeat item = (usrSeat)this.pnlClass.Controls[string.Concat("usrSeat", Strings.Format(num, "00"))];
				item.Checked = false;
				num = checked(num + 1);
			}
			while (num <= 24);
		}

		public void CreateSelectCondition(ref ToolStripMenuItem argSelectCondition)
		{
			int i;
			argSelectCondition.DropDownItems.Clear();
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionSex, "mnuSelectConditionSex"));
			ToolStripMenuItem item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionSex"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int length = checked(checked((int)JG2Importer.JG2.Common.cnsSex.Length) - 1);
			for (i = 0; i <= length; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsSex[i]));
			}
			argSelectCondition.DropDownItems.Add(new ToolStripSeparator());
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionBukatu1, "mnuSelectConditionBukatu1"));
			argSelectCondition.DropDownItems.Add(new ToolStripSeparator());
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionSyakousei, "mnuSelectConditionSyakousei"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionSyakousei"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int num = checked(checked((int)JG2Importer.JG2.Common.cnsSyakousei.Length) - 1);
			for (i = 0; i <= num; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsSyakousei[i]));
			}
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionTeisou, "mnuSelectConditionTeisou"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionTeisou"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int length1 = checked(checked((int)JG2Importer.JG2.Common.cnsTeisou.Length) - 1);
			for (i = 0; i <= length1; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsTeisou[i]));
			}
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionSeikou1, "mnuSelectConditionSeikou1"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionSeikou1"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int num1 = checked(checked((int)JG2Importer.JG2.Common.cnsFlags.Length) - 1);
			for (i = 0; i <= num1; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsFlags[i]));
			}
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionSeikou2, "mnuSelectConditionSeikou2"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionSeikou2"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int length2 = checked(checked((int)JG2Importer.JG2.Common.cnsFlags.Length) - 1);
			for (i = 0; i <= length2; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsFlags[i]));
			}
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionCodeNinshin, "mnuSelectConditionNinshin"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionNinshin"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int num2 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeNinshin.Length) - 1);
			for (i = 0; i <= num2; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsCodeNinshin[i]));
			}
			argSelectCondition.DropDownItems.Add(new ToolStripSeparator());
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(string.Concat(Resources.CaptionCodeAction, "/", Resources.CaptionCodeAction1), "mnuSelectConditionAction"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionAction"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int length3 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeAction.Length) - 1);
			for (i = 0; i <= length3; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsCodeAction[i]));
			}
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(string.Concat(Resources.CaptionCodeAction, "/", Resources.CaptionCodeAction2), "mnuSelectConditionActionRev"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionActionRev"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int num3 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeAction.Length) - 1);
			for (i = 0; i <= num3; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsCodeAction[i]));
			}
			argSelectCondition.DropDownItems.Add(new ToolStripSeparator());
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionKousai, "mnuSelectConditionKousai"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionKousai"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int length4 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeKousai.Length) - 1);
			for (i = 0; i <= length4; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsCodeKousai[i]));
			}
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionCodeDate, "mnuSelectConditionDate"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionDate"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int num4 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeDate.Length) - 1);
			for (i = 0; i <= num4; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsCodeDate[i]));
			}
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionCodeGohoubi, "mnuSelectConditionGohoubi"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionGohoubi"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int length5 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeGohoubi.Length) - 1);
			for (i = 0; i <= length5; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsCodeGohoubi[i]));
			}
			argSelectCondition.DropDownItems.Add(new ToolStripSeparator());
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionCodeHAisyou, "mnuSelectConditionHAisyou"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionHAisyou"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int num5 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeHAisyou.Length) - 1);
			for (i = 0; i <= num5; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsCodeHAisyou[i]));
			}
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionCodeCntNakaKiken, "mnuSelectConditionCntNakaKiken"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionCntNakaKiken"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int length6 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeCntNakaKiken.Length) - 1);
			for (i = 0; i <= length6; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsCodeCntNakaKiken[i]));
			}
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionCodeRenzoku, "mnuSelectConditionRenzoku"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionRenzoku"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int num6 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeRenzoku.Length) - 1);
			for (i = 0; i <= num6; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsCodeRenzoku[i]));
			}
			argSelectCondition.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.CaptionCodeCntTodaySeikou, "mnuSelectConditionCntTodaySeikou"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionCntTodaySeikou"];
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int length7 = checked(checked((int)JG2Importer.JG2.Common.cnsCodeCntTodaySeikou.Length) - 1);
			for (i = 0; i <= length7; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.JG2.Common.cnsCodeCntTodaySeikou[i]));
			}
			argSelectCondition.DropDownItems.Add(new ToolStripSeparator());
			frmClass _frmClass = this;
			argSelectCondition.DropDownItems.Add(new ToolStripMenuItem(Resources.CaptionSelectOption1, null, new EventHandler(_frmClass.mnuSelectAll_Click), "mnuSelectOption1"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectOption1"];
			item.CheckOnClick = true;
			item.Checked = true;
			frmClass _frmClass1 = this;
			argSelectCondition.DropDownItems.Add(new ToolStripMenuItem(Resources.CaptionSelectOption2, null, new EventHandler(_frmClass1.mnuSelectAll_Click), "mnuSelectOption2"));
			item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectOption2"];
			item.CheckOnClick = true;
			item.Checked = true;
			int count = checked(argSelectCondition.DropDownItems.Count - 1);
			for (i = 0; i <= count; i = checked(i + 1))
			{
				if (argSelectCondition.DropDownItems[i] is JG2ToolStripMenuItem)
				{
					item = (ToolStripMenuItem)argSelectCondition.DropDownItems[i];
					if (item.DropDownItems.Count > 0)
					{
						((JG2ToolStripMenuItem)item.DropDownItems[0]).Checked = true;
					}
				}
			}
		}

		public void CreateSelectConditionBukatu1(ref ToolStripMenuItem argSelectCondition)
		{
			ToolStripMenuItem item = (ToolStripMenuItem)argSelectCondition.DropDownItems["mnuSelectConditionBukatu1"];
			item.DropDownItems.Clear();
			item.DropDownItems.Add(new JG2ToolStripMenuItem(Resources.DescriptionNone));
			int length = checked(checked((int)JG2Importer.Common.MySaveData.Header.Bukatu.Length) - 1);
			for (int i = 0; i <= length; i = checked(i + 1))
			{
				item.DropDownItems.Add(new JG2ToolStripMenuItem(JG2Importer.Common.MySaveData.Header.Bukatu[i].Name));
			}
			if (item.DropDownItems.Count > 0)
			{
				((JG2ToolStripMenuItem)item.DropDownItems[0]).Checked = true;
			}
		}

		private void CreateSelectConditionEvent(ref ToolStripMenuItem argSelectCondition)
		{
			int count = checked(argSelectCondition.DropDownItems.Count - 1);
			for (int i = 0; i <= count; i = checked(i + 1))
			{
				bool flag = true;
				if (flag == argSelectCondition.DropDownItems[i] is JG2ToolStripMenuItem)
				{
					ToolStripMenuItem item = (ToolStripMenuItem)argSelectCondition.DropDownItems[i];
					if (item.DropDownItems.Count > 0)
					{
						int num = checked(item.DropDownItems.Count - 1);
						for (int j = 0; j <= num; j = checked(j + 1))
						{
							frmClass _frmClass = this;
							item.DropDownItems[j].Click += new EventHandler(_frmClass.mnuSelectAll_Click);
						}
					}
				}
				else if (flag == argSelectCondition.DropDownItems[i] is ToolStripMenuItem)
				{
					frmClass _frmClass1 = this;
					argSelectCondition.DropDownItems[i].Click += new EventHandler(_frmClass1.mnuSelectAll_Click);
				}
			}
		}

		private void ctmSeat_Opening(object sender, CancelEventArgs e)
		{
			int index = JG2Importer.Common.MySaveData.GetIndex(Conversions.ToInteger(Strings.Mid(Conversions.ToString(NewLateBinding.LateGet(NewLateBinding.LateGet(sender, null, "SourceControl", new object[0], null, null, null), null, "Name", new object[0], null, null, null)), 8)));
			int num = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			if (!(index == -1 | num == -1))
			{
				ToolStripMenuItem toolStripMenuItem = this.mnuSelectCondition;
				this.ctmSeat.Items.Clear();
				this.ctmSeat.Items.Add(new ToolStripLabel(string.Concat(toolStripMenuItem.Text, Resources.MenuTxtApplyTo), Resources.IconFilter));
				this.ctmSeat.Items[0].Font = new System.Drawing.Font(this.Font, FontStyle.Bold);
				this.ctmSeat.Items.Add(new ToolStripSeparator());
				frmClass _frmClass = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionSex.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsSex[JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sex], "'"), null, new EventHandler(_frmClass.ctmSeatItem_Click), "Sex"));
				this.ctmSeat.Items.Add(new ToolStripSeparator());
				frmClass _frmClass1 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionBukatu1.PadRight(8, ' '), "'", JG2Importer.Common.MySaveData.Header.Bukatu[JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bukatu1].Name, "'"), null, new EventHandler(_frmClass1.ctmSeatItem_Click), "Bukatu1"));
				this.ctmSeat.Items.Add(new ToolStripSeparator());
				frmClass _frmClass2 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionSyakousei.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsSyakousei[JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Syakousei], "'"), null, new EventHandler(_frmClass2.ctmSeatItem_Click), "Syakousei"));
				frmClass _frmClass3 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionTeisou.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsTeisou[JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Teisou], "'"), null, new EventHandler(_frmClass3.ctmSeatItem_Click), "Teisou"));
				frmClass _frmClass4 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionSeikou1.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsFlags[JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikou1], "'"), null, new EventHandler(_frmClass4.ctmSeatItem_Click), "Seikou1"));
				frmClass _frmClass5 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionSeikou2.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsFlags[JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikou2], "'"), null, new EventHandler(_frmClass5.ctmSeatItem_Click), "Seikou2"));
				frmClass _frmClass6 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionCodeNinshin.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsCodeNinshin[(int)JG2Importer.Common.MySaveData.GetCodeNinshin(index)], "'"), null, new EventHandler(_frmClass6.ctmSeatItem_Click), "Ninshin"));
				this.ctmSeat.Items.Add(new ToolStripSeparator());
				ToolStripItemCollection items = this.ctmSeat.Items;
				string str = Resources.CaptionCodeAction1.PadRight(8, ' ');
				string[] strArrays = JG2Importer.JG2.Common.cnsCodeAction;
				DefCodeAction codeAction = JG2Importer.Common.MySaveData.GetCodeAction(index, num);
				frmClass _frmClass7 = this;
				items.Add(new ToolStripMenuItem(string.Concat(str, "'", strArrays[(int)codeAction.CodeAction], "'"), null, new EventHandler(_frmClass7.ctmSeatItem_Click), "Action"));
				ToolStripItemCollection toolStripItemCollections = this.ctmSeat.Items;
				string str1 = Resources.CaptionCodeAction2.PadRight(8, ' ');
				string[] strArrays1 = JG2Importer.JG2.Common.cnsCodeAction;
				codeAction = JG2Importer.Common.MySaveData.GetCodeAction(num, index);
				frmClass _frmClass8 = this;
				toolStripItemCollections.Add(new ToolStripMenuItem(string.Concat(str1, "'", strArrays1[(int)codeAction.CodeAction], "'"), null, new EventHandler(_frmClass8.ctmSeatItem_Click), "ActionRev"));
				this.ctmSeat.Items.Add(new ToolStripSeparator());
				frmClass _frmClass9 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionKousai.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsCodeKousai[(int)JG2Importer.Common.MySaveData.GetCodeKousai(index, num)], "'"), null, new EventHandler(_frmClass9.ctmSeatItem_Click), "Kousai"));
				frmClass _frmClass10 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionCodeDate.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsFlags[(int)JG2Importer.Common.MySaveData.GetCodeDate(index, num)], "'"), null, new EventHandler(_frmClass10.ctmSeatItem_Click), "Date"));
				frmClass _frmClass11 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionCodeGohoubi.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsCodeGohoubi[(int)JG2Importer.Common.MySaveData.GetCodeGohoubi(index, num)], "'"), null, new EventHandler(_frmClass11.ctmSeatItem_Click), "Gohoubi"));
				this.ctmSeat.Items.Add(new ToolStripSeparator());
				frmClass _frmClass12 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionCodeHAisyou.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsCodeHAisyou[(int)JG2Importer.Common.MySaveData.GetCodeHAisyou(index, num)], "'"), null, new EventHandler(_frmClass12.ctmSeatItem_Click), "HAisyou"));
				frmClass _frmClass13 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionCodeCntNakaKiken.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsCodeCntNakaKiken[(int)JG2Importer.Common.MySaveData.GetCodeCntNakaKiken(index, num)], "'"), null, new EventHandler(_frmClass13.ctmSeatItem_Click), "CntNakaKiken"));
				frmClass _frmClass14 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionCodeRenzoku.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsCodeRenzoku[(int)JG2Importer.Common.MySaveData.GetCodeRenzoku(index, num)], "'"), null, new EventHandler(_frmClass14.ctmSeatItem_Click), "Renzoku"));
				frmClass _frmClass15 = this;
				this.ctmSeat.Items.Add(new ToolStripMenuItem(string.Concat(Resources.CaptionCodeCntTodaySeikou.PadRight(8, ' '), "'", JG2Importer.JG2.Common.cnsCodeCntTodaySeikou[(int)JG2Importer.Common.MySaveData.GetCodeCntTodaySeikou(index, num)], "'"), null, new EventHandler(_frmClass15.ctmSeatItem_Click), "CntTodaySeikou"));
				e.Cancel = false;
			}
			else
			{
				e.Cancel = true;
			}
		}

		private void ctmSeatItem_Click(object sender, EventArgs e)
		{
			DefCodeAction codeAction;
			int index = JG2Importer.Common.MySaveData.GetIndex(Conversions.ToInteger(Strings.Mid(Conversions.ToString(NewLateBinding.LateGet(NewLateBinding.LateGet(NewLateBinding.LateGet(sender, null, "Owner", new object[0], null, null, null), null, "SourceControl", new object[0], null, null, null), null, "Name", new object[0], null, null, null)), 8)));
			int num = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			if (!(index == -1 | num == -1))
			{
				int count = checked(this.mnuSelectCondition.DropDownItems.Count - 1);
				for (int i = 0; i <= count; i = checked(i + 1))
				{
					if (this.mnuSelectCondition.DropDownItems[i] is JG2ToolStripMenuItem)
					{
						((JG2ToolStripMenuItem)((JG2ToolStripMenuItem)this.mnuSelectCondition.DropDownItems[i]).DropDownItems[0]).Checked = true;
					}
				}
				ToolStripItemCollection dropDownItems = this.mnuSelectCondition.DropDownItems;
				object[] objArray = new object[] { Operators.ConcatenateObject("mnuSelectCondition", NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null)) };
				JG2ToolStripMenuItem jG2ToolStripMenuItem = (JG2ToolStripMenuItem)NewLateBinding.LateGet(dropDownItems, null, "Item", objArray, null, null, null);
				object obj = NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null);
				if (Operators.ConditionalCompareObjectEqual(obj, "Sex", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sex + 1)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "Bukatu1", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bukatu1 + 1)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "Syakousei", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Syakousei + 1)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "Teisou", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Teisou + 1)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "Seikou1", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikou1 + 1)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "Seikou2", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked(JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikou2 + 1)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "Ninshin", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked((byte)JG2Importer.Common.MySaveData.GetCodeNinshin(index) + (byte)CodeNinshin.Normal)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "Action", false))
				{
					ToolStripItemCollection toolStripItemCollections = jG2ToolStripMenuItem.DropDownItems;
					codeAction = JG2Importer.Common.MySaveData.GetCodeAction(index, num);
					((JG2ToolStripMenuItem)toolStripItemCollections[checked((byte)codeAction.CodeAction + (byte)CodeAction.Love)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "ActionRev", false))
				{
					ToolStripItemCollection dropDownItems1 = jG2ToolStripMenuItem.DropDownItems;
					codeAction = JG2Importer.Common.MySaveData.GetCodeAction(num, index);
					((JG2ToolStripMenuItem)dropDownItems1[checked((byte)codeAction.CodeAction + (byte)CodeAction.Love)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "Kousai", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked((byte)JG2Importer.Common.MySaveData.GetCodeKousai(index, num) + (byte)CodeKousai.Lover)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "Date", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked((byte)JG2Importer.Common.MySaveData.GetCodeDate(index, num) + (byte)CodeDate.Date)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "Gohoubi", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked((byte)JG2Importer.Common.MySaveData.GetCodeGohoubi(index, num) + (byte)CodeGohoubi.Yakusoku)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "HAisyou", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked((byte)JG2Importer.Common.MySaveData.GetCodeHAisyou(index, num) + (byte)CodeHAisyou.Subtle)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "CntNakaKiken", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked((byte)JG2Importer.Common.MySaveData.GetCodeCntNakaKiken(index, num) + (byte)CodeCntNakaKiken.Safe)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "Renzoku", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked((byte)JG2Importer.Common.MySaveData.GetCodeRenzoku(index, num) + (byte)CodeRenzoku.End)]).Checked = true;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "CntTodaySeikou", false))
				{
					((JG2ToolStripMenuItem)jG2ToolStripMenuItem.DropDownItems[checked((byte)JG2Importer.Common.MySaveData.GetCodeCntTodaySeikou(index, num) + (byte)CodeCntTodaySeikou.Play)]).Checked = true;
				}
				this.SelectSeatCheck(this.mnuSelectCondition);
			}
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public void DrawClass()
		{
			Image bitmap;
			Image image;
			ToolStripMenuItem toolStripMenuItem = this.mnuSelectPlayer;
			if (JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer) == -1)
			{
				bitmap = null;
			}
			else
			{
				bitmap = JG2Importer.JG2.Common.GetIcon((Bitmap)JG2Importer.Common.MySaveData.Seat[JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer)].Character.ThumbnailImage).ToBitmap();
			}
			toolStripMenuItem.Image = bitmap;
			ToolStripMenuItem toolStripMenuItem1 = this.mnuSelectTeacher;
			if (JG2Importer.Common.MySaveData.GetIndex(24) == -1)
			{
				image = null;
			}
			else
			{
				image = JG2Importer.JG2.Common.GetIcon((Bitmap)JG2Importer.Common.MySaveData.Seat[JG2Importer.Common.MySaveData.GetIndex(24)].Character.ThumbnailImage).ToBitmap();
			}
			toolStripMenuItem1.Image = image;
			int num = 0;
			do
			{
				this.DrawSeat(num);
				num = checked(num + 1);
			}
			while (num <= 24);
		}

		private void DrawHeader()
		{
			this.numDays.SetValue(new decimal((int)JG2Importer.Common.MySaveData.Header.Days));
			this.cmbWeek.SetValue((int)JG2Importer.Common.MySaveData.Header.Week);
			switch ((checked(JG2Importer.Common.MySaveData.Header.Days - 1)) % 42)
			{
				case 11:
				{
					this.cmbEvent.SetValue(0);
					break;
				}
				case 12:
				{
					this.cmbEvent.SetValue(1);
					break;
				}
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				case 20:
				case 21:
				case 22:
				case 23:
				case 24:
				case 27:
				case 28:
				case 29:
				case 30:
				case 31:
				case 32:
				case 33:
				case 34:
				case 35:
				case 36:
				case 37:
				case 38:
				{
					this.cmbEvent.SetValue(-1);
					break;
				}
				case 25:
				{
					this.cmbEvent.SetValue(2);
					break;
				}
				case 26:
				{
					this.cmbEvent.SetValue(3);
					break;
				}
				case 39:
				{
					this.cmbEvent.SetValue(4);
					break;
				}
				case 40:
				{
					this.cmbEvent.SetValue(5);
					break;
				}
				default:
				{
					goto case 38;
				}
			}
		}

		private void DrawSchool()
		{
			this.Icon = Resources.JG2Importer;
			string[] name = new string[] { JG2Importer.Common.MySaveData.DataFile.Name, " (", Strings.Format(JG2Importer.Common.MySaveData.DataFile.LastWriteTime, "yyyy/MM/dd HH:mm"), ") - ", Application.ProductName };
			this.Text = string.Concat(name);
			ToolStripMenuItem toolStripMenuItem = this.mnuSelectCondition;
			this.CreateSelectConditionBukatu1(ref toolStripMenuItem);
			this.mnuSelectCondition = toolStripMenuItem;
			toolStripMenuItem = this.mnuSelectCondition;
			this.CreateSelectConditionEvent(ref toolStripMenuItem);
			this.mnuSelectCondition = toolStripMenuItem;
			this.lblSchool.Text = JG2Importer.Common.MySaveData.SchoolName;
			this.DrawHeader();
			this.DrawClass();
			this.mnuUpdate.Enabled = true;
			this.mnuEdit.Enabled = true;
			this.mnuView.Enabled = true;
			this.pnlHeader.Visible = true;
			this.pnlClass.BackColor = this.ColorClassDefault;
			this.mnuSelectTeacher.Enabled = JG2Importer.Common.MySaveData.GetIndex(24) != -1;
			if ((Information.IsNothing(this.MyDetail) ? false : !this.MyDetail.IsDisposed))
			{
				this.MyDetail.Close(false);
			}
			if (!Information.IsNothing(RuntimeHelpers.GetObjectValue(this.pnlClass.Tag)))
			{
				object tag = this.pnlClass.Tag;
				object[] objArray = new object[] { false };
				NewLateBinding.LateSetComplex(tag, null, "CurrentSeat", objArray, null, null, false, true);
				this.pnlClass.Tag = null;
			}
			this.ClearSeatCheck();
		}

		public void DrawSeat(int argSeatNo)
		{
			usrSeat item = (usrSeat)this.pnlClass.Controls[string.Concat("usrSeat", Strings.Format(argSeatNo, "00"))];
			int index = JG2Importer.Common.MySaveData.GetIndex(argSeatNo);
			int num = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			if (!(index == -1 | num == -1))
			{
				usrSeat.DefAttr defAttr = new usrSeat.DefAttr()
				{
					Sex = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sex,
					Seikaku = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikaku,
					Rare = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Rare,
					ThumbnailImage = JG2Importer.Common.MySaveData.Seat[index].Character.ThumbnailImage,
					FullName = JG2Importer.Common.MySaveData.Seat[index].Character.FullName,
					Player = argSeatNo == JG2Importer.Common.MySaveData.Footer.SeatNoPlayer,
					Teacher = argSeatNo == 24,
					CodeNinshin = JG2Importer.Common.MySaveData.GetCodeNinshin(index),
					CodeAction = JG2Importer.Common.MySaveData.GetCodeAction(index, num),
					CodeActionRev = JG2Importer.Common.MySaveData.GetCodeAction(num, index),
					BukatuName = JG2Importer.Common.MySaveData.Header.Bukatu[JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bukatu1].Name,
					CodeKousai = JG2Importer.Common.MySaveData.GetCodeKousai(index, num),
					CodeDate = JG2Importer.Common.MySaveData.GetCodeDate(index, num),
					CodeGohoubi = JG2Importer.Common.MySaveData.GetCodeGohoubi(index, num),
					CodeHAisyou = JG2Importer.Common.MySaveData.GetCodeHAisyou(index, num),
					CodeCntNakaKiken = JG2Importer.Common.MySaveData.GetCodeCntNakaKiken(index, num),
					CodeRenzoku = JG2Importer.Common.MySaveData.GetCodeRenzoku(index, num),
					CodeCntTodaySeikou = JG2Importer.Common.MySaveData.GetCodeCntTodaySeikou(index, num),
					Height = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Height,
					Style = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Style,
					Mune = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Mune,
					Seikou1 = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikou1,
					Seikou2 = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seikou2,
					Syakousei = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Syakousei,
					Teisou = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Teisou
				};
				item.Attr = defAttr;
				item.Visible = true;
			}
			else
			{
				item.Visible = false;
			}
		}

		private void frmSchool_Load(object sender, EventArgs e)
		{
			JG2Importer.Common.INSTALLDIR_PLAY = Conversions.ToString(JG2Importer.Common.GetRegistry("AA2Play", "INSTALLDIR"));
			if (Information.IsNothing(JG2Importer.Common.INSTALLDIR_PLAY))
			{
				JG2Importer.Common.INSTALLDIR_PLAY = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			}
			JG2Importer.Common.INSTALLDIR_EDIT = Conversions.ToString(JG2Importer.Common.GetRegistry("AA2Edit", "INSTALLDIR"));
			if (Information.IsNothing(JG2Importer.Common.INSTALLDIR_EDIT))
			{
				JG2Importer.Common.INSTALLDIR_EDIT = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			}
			JG2Importer.Common.MyDetailLocation = new Point();
			this.pnlClass.Tag = null;
			this.mnuUpdate.Enabled = false;
			this.mnuEdit.Enabled = false;
			this.mnuView.Enabled = false;
			JG2Importer.Common.MyColorDialog = new ColorDialog()
			{
				FullOpen = true
			};
		}

		private void fswDataFile_Changed(object sender, FileSystemEventArgs e)
		{
			FileStream fileStream;
			try
			{
				fileStream = new FileStream(JG2Importer.Common.MySaveData.DataFile.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);
			}
			catch (Exception exception)
			{
				ProjectData.SetProjectError(exception);
				ProjectData.ClearProjectError();
				return;
			}
			fileStream.Close();
			fileStream.Dispose();
			fileStream = null;
			this.mnuReload_Click(RuntimeHelpers.GetObjectValue(sender), e);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.pnlHeader = new Panel();
			this.cmbEvent = new JG2ComboBox();
			this.cmbWeek = new JG2ComboBox();
			this.Label11 = new Label();
			this.lblSchool = new Label();
			this.numDays = new JG2NumericUpDown();
			this.pnlClass = new Panel();
			this.usrSeat20 = new usrSeat();
			this.ctmSeat = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.usrSeat21 = new usrSeat();
			this.usrSeat22 = new usrSeat();
			this.usrSeat23 = new usrSeat();
			this.usrSeat24 = new usrSeat();
			this.usrSeat10 = new usrSeat();
			this.usrSeat11 = new usrSeat();
			this.usrSeat12 = new usrSeat();
			this.usrSeat13 = new usrSeat();
			this.usrSeat14 = new usrSeat();
			this.usrSeat15 = new usrSeat();
			this.usrSeat16 = new usrSeat();
			this.usrSeat17 = new usrSeat();
			this.usrSeat18 = new usrSeat();
			this.usrSeat19 = new usrSeat();
			this.usrSeat09 = new usrSeat();
			this.usrSeat07 = new usrSeat();
			this.usrSeat08 = new usrSeat();
			this.usrSeat06 = new usrSeat();
			this.usrSeat04 = new usrSeat();
			this.usrSeat05 = new usrSeat();
			this.usrSeat02 = new usrSeat();
			this.usrSeat03 = new usrSeat();
			this.usrSeat01 = new usrSeat();
			this.usrSeat00 = new usrSeat();
			this.fswDataFile = new FileSystemWatcher();
			this.MenuStrip = new System.Windows.Forms.MenuStrip();
			this.mnuFile = new ToolStripMenuItem();
			this.mnuOpen = new ToolStripMenuItem();
			this.toolStripSeparator = new ToolStripSeparator();
			this.mnuUpdate = new ToolStripMenuItem();
			this.toolStripSeparator1 = new ToolStripSeparator();
			this.mnuClose = new ToolStripMenuItem();
			this.mnuEdit = new JG2ToolStripMenuItem();
			this.mnuCopy = new ToolStripMenuItem();
			this.ToolStripSeparator15 = new ToolStripSeparator();
			this.mnuSelectPlayer = new ToolStripMenuItem();
			this.mnuSelectTeacher = new ToolStripMenuItem();
			this.mnuShuffleBukatuSeparator = new ToolStripSeparator();
			this.mnuShuffleBukatu = new ToolStripMenuItem();
			this.mnuShuffleKenka = new ToolStripMenuItem();
			this.mnuShuffleSitagi = new ToolStripMenuItem();
			this.ToolStripSeparator3 = new ToolStripSeparator();
			this.mnuSelectCondition = new ToolStripMenuItem();
			this.ToolStripSeparator4 = new ToolStripSeparator();
			this.mnuSelectAll = new ToolStripMenuItem();
			this.mnuClearAll = new ToolStripMenuItem();
			this.mnuView = new JG2ToolStripMenuItem();
			this.mnuReload = new ToolStripMenuItem();
			this.mnuAutoReload = new ToolStripMenuItem();
			this.ToolStripSeparator5 = new ToolStripSeparator();
			this.mnuDisplayItem = new ToolStripMenuItem();
			this.mnuDisplayItem8 = new ToolStripMenuItem();
			this.mnuDisplayItem9 = new ToolStripMenuItem();
			this.mnuDisplayItem10 = new ToolStripMenuItem();
			this.mnuDisplayItem11 = new ToolStripMenuItem();
			this.mnuDisplayItem12 = new ToolStripMenuItem();
			this.ToolStripSeparator8 = new ToolStripSeparator();
			this.mnuDisplayItem0 = new JG2ToolStripMenuItem();
			this.mnuDisplayItem00 = new JG2ToolStripMenuItem();
			this.mnuDisplayItem01 = new JG2ToolStripMenuItem();
			this.mnuDisplayItem02 = new JG2ToolStripMenuItem();
			this.ToolStripSeparator9 = new ToolStripSeparator();
			this.mnuDisplayItem0Option1 = new ToolStripMenuItem();
			this.ToolStripSeparator6 = new ToolStripSeparator();
			this.mnuDisplayItem1 = new ToolStripMenuItem();
			this.mnuDisplayItem2 = new ToolStripMenuItem();
			this.mnuDisplayItem3 = new ToolStripMenuItem();
			this.ToolStripSeparator7 = new ToolStripSeparator();
			this.mnuDisplayItem4 = new ToolStripMenuItem();
			this.mnuDisplayItem5 = new ToolStripMenuItem();
			this.mnuDisplayItem6 = new ToolStripMenuItem();
			this.mnuDisplayItem7 = new ToolStripMenuItem();
			this.toolStripSeparator2 = new ToolStripSeparator();
			this.pnlHeader.SuspendLayout();
			((ISupportInitialize)this.numDays).BeginInit();
			this.pnlClass.SuspendLayout();
			((ISupportInitialize)this.fswDataFile).BeginInit();
			this.MenuStrip.SuspendLayout();
			this.SuspendLayout();
			this.pnlHeader.Controls.Add(this.cmbEvent);
			this.pnlHeader.Controls.Add(this.cmbWeek);
			this.pnlHeader.Controls.Add(this.Label11);
			this.pnlHeader.Controls.Add(this.lblSchool);
			this.pnlHeader.Controls.Add(this.numDays);
			Panel panel = this.pnlHeader;
			Point point = new Point(6, 34);
			panel.Location = point;
			this.pnlHeader.Name = "pnlHeader";
			Panel panel1 = this.pnlHeader;
			System.Drawing.Size size = new System.Drawing.Size(922, 36);
			panel1.Size = size;
			this.pnlHeader.TabIndex = 1;
			this.pnlHeader.Visible = false;
			this.cmbEvent.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cmbEvent.ForeColor = SystemColors.WindowText;
			ComboBox.ObjectCollection items = this.cmbEvent.Items;
            object[] objArray = new object[] { Resources.ComboTxt2DExam, Resources.ComboTxt1DExam, Resources.ComboTxt2DFitness, Resources.ComboTxt1DFitness, Resources.ComboTxt2DClub, Resources.ComboTxt1DClub };
            items.AddRange(objArray);
			JG2ComboBox jG2ComboBox = this.cmbEvent;
			point = new Point(802, 5);
			jG2ComboBox.Location = point;
			this.cmbEvent.Name = "cmbEvent";
			JG2ComboBox jG2ComboBox1 = this.cmbEvent;
			size = new System.Drawing.Size(120, 26);
			jG2ComboBox1.Size = size;
			this.cmbEvent.TabIndex = 4;
			this.cmbWeek.DropDownStyle = ComboBoxStyle.DropDownList;
			ComboBox.ObjectCollection objectCollections = this.cmbWeek.Items;
			objArray = new object[] { Resources.ComboTxtSunday, Resources.ComboTxtMonday, Resources.ComboTxtTuesday, Resources.ComboTxtWednsday, Resources.ComboTxtThursday, Resources.ComboTxtFriday, Resources.ComboTxtSaturday };
			objectCollections.AddRange(objArray);
			JG2ComboBox jG2ComboBox2 = this.cmbWeek;
			point = new Point(748, 5);
			jG2ComboBox2.Location = point;
			this.cmbWeek.Name = "cmbWeek";
			JG2ComboBox jG2ComboBox3 = this.cmbWeek;
			size = new System.Drawing.Size(50, 26);
			jG2ComboBox3.Size = size;
			this.cmbWeek.TabIndex = 3;
			Label label11 = this.Label11;
			point = new Point(712, 9);
			label11.Location = point;
			this.Label11.Name = "Label11";
			Label label = this.Label11;
			size = new System.Drawing.Size(32, 18);
			label.Size = size;
			this.Label11.TabIndex = 2;
            this.Label11.Text = Resources.CaptionSunday;
			this.lblSchool.Font = new System.Drawing.Font(Resources.TextMerioFont, 14.25f, FontStyle.Regular, GraphicsUnit.Point, 128);
			Label label1 = this.lblSchool;
			point = new Point(0, 4);
			label1.Location = point;
			this.lblSchool.Name = "lblSchool";
			Label label2 = this.lblSchool;
			size = new System.Drawing.Size(637, 27);
			label2.Size = size;
			this.lblSchool.TabIndex = 0;
			JG2NumericUpDown jG2NumericUpDown = this.numDays;
			point = new Point(658, 6);
			jG2NumericUpDown.Location = point;
			JG2NumericUpDown jG2NumericUpDown1 = this.numDays;
			int[] numArray = new int[] { 42, 0, 0, 0 };
			decimal num = new decimal(numArray);
			jG2NumericUpDown1.Maximum = num;
			JG2NumericUpDown jG2NumericUpDown2 = this.numDays;
			numArray = new int[] { 1, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown2.Minimum = num;
			this.numDays.Name = "numDays";
			JG2NumericUpDown jG2NumericUpDown3 = this.numDays;
			size = new System.Drawing.Size(50, 25);
			jG2NumericUpDown3.Size = size;
			this.numDays.TabIndex = 1;
			this.numDays.TextAlign = HorizontalAlignment.Right;
			JG2NumericUpDown jG2NumericUpDown4 = this.numDays;
			numArray = new int[] { 1, 0, 0, 0 };
			num = new decimal(numArray);
			jG2NumericUpDown4.Value = num;
			this.pnlClass.Controls.Add(this.usrSeat20);
			this.pnlClass.Controls.Add(this.usrSeat21);
			this.pnlClass.Controls.Add(this.usrSeat22);
			this.pnlClass.Controls.Add(this.usrSeat23);
			this.pnlClass.Controls.Add(this.usrSeat24);
			this.pnlClass.Controls.Add(this.usrSeat10);
			this.pnlClass.Controls.Add(this.usrSeat11);
			this.pnlClass.Controls.Add(this.usrSeat12);
			this.pnlClass.Controls.Add(this.usrSeat13);
			this.pnlClass.Controls.Add(this.usrSeat14);
			this.pnlClass.Controls.Add(this.usrSeat15);
			this.pnlClass.Controls.Add(this.usrSeat16);
			this.pnlClass.Controls.Add(this.usrSeat17);
			this.pnlClass.Controls.Add(this.usrSeat18);
			this.pnlClass.Controls.Add(this.usrSeat19);
			this.pnlClass.Controls.Add(this.usrSeat09);
			this.pnlClass.Controls.Add(this.usrSeat07);
			this.pnlClass.Controls.Add(this.usrSeat08);
			this.pnlClass.Controls.Add(this.usrSeat06);
			this.pnlClass.Controls.Add(this.usrSeat04);
			this.pnlClass.Controls.Add(this.usrSeat05);
			this.pnlClass.Controls.Add(this.usrSeat02);
			this.pnlClass.Controls.Add(this.usrSeat03);
			this.pnlClass.Controls.Add(this.usrSeat01);
			this.pnlClass.Controls.Add(this.usrSeat00);
			Panel panel2 = this.pnlClass;
			point = new Point(6, 78);
			panel2.Location = point;
			this.pnlClass.Name = "pnlClass";
			Panel panel3 = this.pnlClass;
			size = new System.Drawing.Size(922, 542);
			panel3.Size = size;
			this.pnlClass.TabIndex = 2;
			this.usrSeat20.BackColor = Color.AliceBlue;
			this.usrSeat20.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat20.Checked = false;
			this.usrSeat20.ContextMenuStrip = this.ctmSeat;
			this.usrSeat20.CurrentSeat = false;
			this.usrSeat20.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat = this.usrSeat20;
			point = new Point(2, 110);
			_usrSeat.Location = point;
			usrSeat _usrSeat1 = this.usrSeat20;
			System.Windows.Forms.Padding padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat1.Margin = padding;
			this.usrSeat20.Name = "usrSeat20";
			usrSeat _usrSeat2 = this.usrSeat20;
			size = new System.Drawing.Size(182, 106);
			_usrSeat2.Size = size;
			this.usrSeat20.TabIndex = 21;
			this.usrSeat20.Visible = false;
			this.ctmSeat.Name = "ContextMenuStrip";
			this.ctmSeat.ShowImageMargin = false;
			System.Windows.Forms.ContextMenuStrip contextMenuStrip = this.ctmSeat;
			size = new System.Drawing.Size(36, 4);
			contextMenuStrip.Size = size;
			this.usrSeat21.BackColor = Color.AliceBlue;
			this.usrSeat21.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat21.Checked = false;
			this.usrSeat21.ContextMenuStrip = this.ctmSeat;
			this.usrSeat21.CurrentSeat = false;
			this.usrSeat21.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat3 = this.usrSeat21;
			point = new Point(2, 218);
			_usrSeat3.Location = point;
			usrSeat _usrSeat4 = this.usrSeat21;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat4.Margin = padding;
			this.usrSeat21.Name = "usrSeat21";
			usrSeat _usrSeat5 = this.usrSeat21;
			size = new System.Drawing.Size(182, 106);
			_usrSeat5.Size = size;
			this.usrSeat21.TabIndex = 22;
			this.usrSeat21.Visible = false;
			this.usrSeat22.BackColor = Color.AliceBlue;
			this.usrSeat22.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat22.Checked = false;
			this.usrSeat22.ContextMenuStrip = this.ctmSeat;
			this.usrSeat22.CurrentSeat = false;
			this.usrSeat22.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat6 = this.usrSeat22;
			point = new Point(2, 326);
			_usrSeat6.Location = point;
			usrSeat _usrSeat7 = this.usrSeat22;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat7.Margin = padding;
			this.usrSeat22.Name = "usrSeat22";
			usrSeat _usrSeat8 = this.usrSeat22;
			size = new System.Drawing.Size(182, 106);
			_usrSeat8.Size = size;
			this.usrSeat22.TabIndex = 23;
			this.usrSeat22.Visible = false;
			this.usrSeat23.BackColor = Color.AliceBlue;
			this.usrSeat23.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat23.Checked = false;
			this.usrSeat23.ContextMenuStrip = this.ctmSeat;
			this.usrSeat23.CurrentSeat = false;
			this.usrSeat23.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat9 = this.usrSeat23;
			point = new Point(2, 434);
			_usrSeat9.Location = point;
			usrSeat _usrSeat10 = this.usrSeat23;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat10.Margin = padding;
			this.usrSeat23.Name = "usrSeat23";
			usrSeat _usrSeat11 = this.usrSeat23;
			size = new System.Drawing.Size(182, 106);
			_usrSeat11.Size = size;
			this.usrSeat23.TabIndex = 24;
			this.usrSeat23.Visible = false;
			this.usrSeat24.BackColor = Color.AliceBlue;
			this.usrSeat24.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat24.Checked = false;
			this.usrSeat24.ContextMenuStrip = this.ctmSeat;
			this.usrSeat24.CurrentSeat = false;
			this.usrSeat24.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat12 = this.usrSeat24;
			point = new Point(370, 2);
			_usrSeat12.Location = point;
			usrSeat _usrSeat13 = this.usrSeat24;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat13.Margin = padding;
			this.usrSeat24.Name = "usrSeat24";
			usrSeat _usrSeat14 = this.usrSeat24;
			size = new System.Drawing.Size(182, 106);
			_usrSeat14.Size = size;
			this.usrSeat24.TabIndex = 0;
			this.usrSeat24.Visible = false;
			this.usrSeat10.BackColor = Color.AliceBlue;
			this.usrSeat10.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat10.Checked = false;
			this.usrSeat10.ContextMenuStrip = this.ctmSeat;
			this.usrSeat10.CurrentSeat = false;
			this.usrSeat10.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat15 = this.usrSeat10;
			point = new Point(370, 110);
			_usrSeat15.Location = point;
			usrSeat _usrSeat16 = this.usrSeat10;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat16.Margin = padding;
			this.usrSeat10.Name = "usrSeat10";
			usrSeat _usrSeat17 = this.usrSeat10;
			size = new System.Drawing.Size(182, 106);
			_usrSeat17.Size = size;
			this.usrSeat10.TabIndex = 11;
			this.usrSeat10.Visible = false;
			this.usrSeat11.BackColor = Color.AliceBlue;
			this.usrSeat11.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat11.Checked = false;
			this.usrSeat11.ContextMenuStrip = this.ctmSeat;
			this.usrSeat11.CurrentSeat = false;
			this.usrSeat11.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat18 = this.usrSeat11;
			point = new Point(370, 218);
			_usrSeat18.Location = point;
			usrSeat _usrSeat19 = this.usrSeat11;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat19.Margin = padding;
			this.usrSeat11.Name = "usrSeat11";
			usrSeat _usrSeat20 = this.usrSeat11;
			size = new System.Drawing.Size(182, 106);
			_usrSeat20.Size = size;
			this.usrSeat11.TabIndex = 12;
			this.usrSeat11.Visible = false;
			this.usrSeat12.BackColor = Color.AliceBlue;
			this.usrSeat12.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat12.Checked = false;
			this.usrSeat12.ContextMenuStrip = this.ctmSeat;
			this.usrSeat12.CurrentSeat = false;
			this.usrSeat12.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat21 = this.usrSeat12;
			point = new Point(370, 326);
			_usrSeat21.Location = point;
			usrSeat _usrSeat22 = this.usrSeat12;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat22.Margin = padding;
			this.usrSeat12.Name = "usrSeat12";
			usrSeat _usrSeat23 = this.usrSeat12;
			size = new System.Drawing.Size(182, 106);
			_usrSeat23.Size = size;
			this.usrSeat12.TabIndex = 13;
			this.usrSeat12.Visible = false;
			this.usrSeat13.BackColor = Color.AliceBlue;
			this.usrSeat13.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat13.Checked = false;
			this.usrSeat13.ContextMenuStrip = this.ctmSeat;
			this.usrSeat13.CurrentSeat = false;
			this.usrSeat13.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat24 = this.usrSeat13;
			point = new Point(370, 434);
			_usrSeat24.Location = point;
			usrSeat _usrSeat25 = this.usrSeat13;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat25.Margin = padding;
			this.usrSeat13.Name = "usrSeat13";
			usrSeat _usrSeat26 = this.usrSeat13;
			size = new System.Drawing.Size(182, 106);
			_usrSeat26.Size = size;
			this.usrSeat13.TabIndex = 14;
			this.usrSeat13.Visible = false;
			this.usrSeat14.BackColor = Color.AliceBlue;
			this.usrSeat14.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat14.Checked = false;
			this.usrSeat14.ContextMenuStrip = this.ctmSeat;
			this.usrSeat14.CurrentSeat = false;
			this.usrSeat14.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat27 = this.usrSeat14;
			point = new Point(186, 2);
			_usrSeat27.Location = point;
			usrSeat _usrSeat28 = this.usrSeat14;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat28.Margin = padding;
			this.usrSeat14.Name = "usrSeat14";
			usrSeat _usrSeat29 = this.usrSeat14;
			size = new System.Drawing.Size(182, 106);
			_usrSeat29.Size = size;
			this.usrSeat14.TabIndex = 15;
			this.usrSeat14.Visible = false;
			this.usrSeat15.BackColor = Color.AliceBlue;
			this.usrSeat15.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat15.Checked = false;
			this.usrSeat15.ContextMenuStrip = this.ctmSeat;
			this.usrSeat15.CurrentSeat = false;
			this.usrSeat15.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat30 = this.usrSeat15;
			point = new Point(186, 110);
			_usrSeat30.Location = point;
			usrSeat _usrSeat31 = this.usrSeat15;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat31.Margin = padding;
			this.usrSeat15.Name = "usrSeat15";
			usrSeat _usrSeat32 = this.usrSeat15;
			size = new System.Drawing.Size(182, 106);
			_usrSeat32.Size = size;
			this.usrSeat15.TabIndex = 16;
			this.usrSeat15.Visible = false;
			this.usrSeat16.BackColor = Color.AliceBlue;
			this.usrSeat16.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat16.Checked = false;
			this.usrSeat16.ContextMenuStrip = this.ctmSeat;
			this.usrSeat16.CurrentSeat = false;
			this.usrSeat16.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat33 = this.usrSeat16;
			point = new Point(186, 218);
			_usrSeat33.Location = point;
			usrSeat _usrSeat34 = this.usrSeat16;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat34.Margin = padding;
			this.usrSeat16.Name = "usrSeat16";
			usrSeat _usrSeat35 = this.usrSeat16;
			size = new System.Drawing.Size(182, 106);
			_usrSeat35.Size = size;
			this.usrSeat16.TabIndex = 17;
			this.usrSeat16.Visible = false;
			this.usrSeat17.BackColor = Color.AliceBlue;
			this.usrSeat17.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat17.Checked = false;
			this.usrSeat17.ContextMenuStrip = this.ctmSeat;
			this.usrSeat17.CurrentSeat = false;
			this.usrSeat17.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat36 = this.usrSeat17;
			point = new Point(186, 326);
			_usrSeat36.Location = point;
			usrSeat _usrSeat37 = this.usrSeat17;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat37.Margin = padding;
			this.usrSeat17.Name = "usrSeat17";
			usrSeat _usrSeat38 = this.usrSeat17;
			size = new System.Drawing.Size(182, 106);
			_usrSeat38.Size = size;
			this.usrSeat17.TabIndex = 18;
			this.usrSeat17.Visible = false;
			this.usrSeat18.BackColor = Color.AliceBlue;
			this.usrSeat18.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat18.Checked = false;
			this.usrSeat18.ContextMenuStrip = this.ctmSeat;
			this.usrSeat18.CurrentSeat = false;
			this.usrSeat18.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat39 = this.usrSeat18;
			point = new Point(186, 434);
			_usrSeat39.Location = point;
			usrSeat _usrSeat40 = this.usrSeat18;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat40.Margin = padding;
			this.usrSeat18.Name = "usrSeat18";
			usrSeat _usrSeat41 = this.usrSeat18;
			size = new System.Drawing.Size(182, 106);
			_usrSeat41.Size = size;
			this.usrSeat18.TabIndex = 19;
			this.usrSeat18.Visible = false;
			this.usrSeat19.BackColor = Color.AliceBlue;
			this.usrSeat19.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat19.Checked = false;
			this.usrSeat19.ContextMenuStrip = this.ctmSeat;
			this.usrSeat19.CurrentSeat = false;
			this.usrSeat19.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat42 = this.usrSeat19;
			point = new Point(2, 2);
			_usrSeat42.Location = point;
			usrSeat _usrSeat43 = this.usrSeat19;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat43.Margin = padding;
			this.usrSeat19.Name = "usrSeat19";
			usrSeat _usrSeat44 = this.usrSeat19;
			size = new System.Drawing.Size(182, 106);
			_usrSeat44.Size = size;
			this.usrSeat19.TabIndex = 20;
			this.usrSeat19.Visible = false;
			this.usrSeat09.BackColor = Color.AliceBlue;
			this.usrSeat09.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat09.Checked = false;
			this.usrSeat09.ContextMenuStrip = this.ctmSeat;
			this.usrSeat09.CurrentSeat = false;
			this.usrSeat09.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat45 = this.usrSeat09;
			point = new Point(554, 434);
			_usrSeat45.Location = point;
			usrSeat _usrSeat46 = this.usrSeat09;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat46.Margin = padding;
			this.usrSeat09.Name = "usrSeat09";
			usrSeat _usrSeat47 = this.usrSeat09;
			size = new System.Drawing.Size(182, 106);
			_usrSeat47.Size = size;
			this.usrSeat09.TabIndex = 10;
			this.usrSeat09.Visible = false;
			this.usrSeat07.BackColor = Color.AliceBlue;
			this.usrSeat07.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat07.Checked = false;
			this.usrSeat07.ContextMenuStrip = this.ctmSeat;
			this.usrSeat07.CurrentSeat = false;
			this.usrSeat07.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat48 = this.usrSeat07;
			point = new Point(554, 218);
			_usrSeat48.Location = point;
			usrSeat _usrSeat49 = this.usrSeat07;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat49.Margin = padding;
			this.usrSeat07.Name = "usrSeat07";
			usrSeat _usrSeat50 = this.usrSeat07;
			size = new System.Drawing.Size(182, 106);
			_usrSeat50.Size = size;
			this.usrSeat07.TabIndex = 8;
			this.usrSeat07.Visible = false;
			this.usrSeat08.BackColor = Color.AliceBlue;
			this.usrSeat08.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat08.Checked = false;
			this.usrSeat08.ContextMenuStrip = this.ctmSeat;
			this.usrSeat08.CurrentSeat = false;
			this.usrSeat08.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat51 = this.usrSeat08;
			point = new Point(554, 326);
			_usrSeat51.Location = point;
			usrSeat _usrSeat52 = this.usrSeat08;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat52.Margin = padding;
			this.usrSeat08.Name = "usrSeat08";
			usrSeat _usrSeat53 = this.usrSeat08;
			size = new System.Drawing.Size(182, 106);
			_usrSeat53.Size = size;
			this.usrSeat08.TabIndex = 9;
			this.usrSeat08.Visible = false;
			this.usrSeat06.BackColor = Color.AliceBlue;
			this.usrSeat06.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat06.Checked = false;
			this.usrSeat06.ContextMenuStrip = this.ctmSeat;
			this.usrSeat06.CurrentSeat = false;
			this.usrSeat06.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat54 = this.usrSeat06;
			point = new Point(554, 110);
			_usrSeat54.Location = point;
			usrSeat _usrSeat55 = this.usrSeat06;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat55.Margin = padding;
			this.usrSeat06.Name = "usrSeat06";
			usrSeat _usrSeat56 = this.usrSeat06;
			size = new System.Drawing.Size(182, 106);
			_usrSeat56.Size = size;
			this.usrSeat06.TabIndex = 7;
			this.usrSeat06.Visible = false;
			this.usrSeat04.BackColor = Color.AliceBlue;
			this.usrSeat04.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat04.Checked = false;
			this.usrSeat04.ContextMenuStrip = this.ctmSeat;
			this.usrSeat04.CurrentSeat = false;
			this.usrSeat04.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat57 = this.usrSeat04;
			point = new Point(738, 434);
			_usrSeat57.Location = point;
			usrSeat _usrSeat58 = this.usrSeat04;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat58.Margin = padding;
			this.usrSeat04.Name = "usrSeat04";
			usrSeat _usrSeat59 = this.usrSeat04;
			size = new System.Drawing.Size(182, 106);
			_usrSeat59.Size = size;
			this.usrSeat04.TabIndex = 5;
			this.usrSeat04.Visible = false;
			this.usrSeat05.BackColor = Color.AliceBlue;
			this.usrSeat05.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat05.Checked = false;
			this.usrSeat05.ContextMenuStrip = this.ctmSeat;
			this.usrSeat05.CurrentSeat = false;
			this.usrSeat05.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat60 = this.usrSeat05;
			point = new Point(554, 2);
			_usrSeat60.Location = point;
			usrSeat _usrSeat61 = this.usrSeat05;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat61.Margin = padding;
			this.usrSeat05.Name = "usrSeat05";
			usrSeat _usrSeat62 = this.usrSeat05;
			size = new System.Drawing.Size(182, 106);
			_usrSeat62.Size = size;
			this.usrSeat05.TabIndex = 6;
			this.usrSeat05.Visible = false;
			this.usrSeat02.BackColor = Color.AliceBlue;
			this.usrSeat02.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat02.Checked = false;
			this.usrSeat02.ContextMenuStrip = this.ctmSeat;
			this.usrSeat02.CurrentSeat = false;
			this.usrSeat02.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat63 = this.usrSeat02;
			point = new Point(738, 218);
			_usrSeat63.Location = point;
			usrSeat _usrSeat64 = this.usrSeat02;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat64.Margin = padding;
			this.usrSeat02.Name = "usrSeat02";
			usrSeat _usrSeat65 = this.usrSeat02;
			size = new System.Drawing.Size(182, 106);
			_usrSeat65.Size = size;
			this.usrSeat02.TabIndex = 3;
			this.usrSeat02.Visible = false;
			this.usrSeat03.BackColor = Color.AliceBlue;
			this.usrSeat03.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat03.Checked = false;
			this.usrSeat03.ContextMenuStrip = this.ctmSeat;
			this.usrSeat03.CurrentSeat = false;
			this.usrSeat03.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat66 = this.usrSeat03;
			point = new Point(738, 326);
			_usrSeat66.Location = point;
			usrSeat _usrSeat67 = this.usrSeat03;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat67.Margin = padding;
			this.usrSeat03.Name = "usrSeat03";
			usrSeat _usrSeat68 = this.usrSeat03;
			size = new System.Drawing.Size(182, 106);
			_usrSeat68.Size = size;
			this.usrSeat03.TabIndex = 4;
			this.usrSeat03.Visible = false;
			this.usrSeat01.BackColor = Color.AliceBlue;
			this.usrSeat01.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat01.Checked = false;
			this.usrSeat01.ContextMenuStrip = this.ctmSeat;
			this.usrSeat01.CurrentSeat = false;
			this.usrSeat01.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat69 = this.usrSeat01;
			point = new Point(738, 110);
			_usrSeat69.Location = point;
			usrSeat _usrSeat70 = this.usrSeat01;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat70.Margin = padding;
			this.usrSeat01.Name = "usrSeat01";
			usrSeat _usrSeat71 = this.usrSeat01;
			size = new System.Drawing.Size(182, 106);
			_usrSeat71.Size = size;
			this.usrSeat01.TabIndex = 2;
			this.usrSeat01.Visible = false;
			this.usrSeat00.BackColor = Color.AliceBlue;
			this.usrSeat00.BorderStyle = BorderStyle.FixedSingle;
			this.usrSeat00.Checked = false;
			this.usrSeat00.ContextMenuStrip = this.ctmSeat;
			this.usrSeat00.CurrentSeat = false;
			this.usrSeat00.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			usrSeat _usrSeat72 = this.usrSeat00;
			point = new Point(738, 2);
			_usrSeat72.Location = point;
			usrSeat _usrSeat73 = this.usrSeat00;
			padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
			_usrSeat73.Margin = padding;
			this.usrSeat00.Name = "usrSeat00";
			usrSeat _usrSeat74 = this.usrSeat00;
			size = new System.Drawing.Size(182, 106);
			_usrSeat74.Size = size;
			this.usrSeat00.TabIndex = 1;
			this.usrSeat00.Visible = false;
			this.fswDataFile.EnableRaisingEvents = true;
			this.fswDataFile.NotifyFilter = NotifyFilters.LastWrite;
			this.fswDataFile.SynchronizingObject = this;
			this.MenuStrip.BackColor = SystemColors.Window;
			ToolStripItemCollection toolStripItemCollections = this.MenuStrip.Items;
			ToolStripItem[] toolStripSeparator15 = new ToolStripItem[] { this.mnuFile, this.mnuEdit, this.mnuView };
			toolStripItemCollections.AddRange(toolStripSeparator15);
			System.Windows.Forms.MenuStrip menuStrip = this.MenuStrip;
			point = new Point(0, 0);
			menuStrip.Location = point;
			this.MenuStrip.Name = "MenuStrip";
			System.Windows.Forms.MenuStrip menuStrip1 = this.MenuStrip;
			size = new System.Drawing.Size(934, 26);
			menuStrip1.Size = size;
			this.MenuStrip.TabIndex = 0;
			ToolStripItemCollection dropDownItems = this.mnuFile.DropDownItems;
			toolStripSeparator15 = new ToolStripItem[] { this.mnuOpen, this.toolStripSeparator, this.mnuUpdate, this.toolStripSeparator1, this.mnuClose };
			dropDownItems.AddRange(toolStripSeparator15);
			this.mnuFile.Name = "mnuFile";
			ToolStripMenuItem toolStripMenuItem = this.mnuFile;
			size = new System.Drawing.Size(85, 22);
			toolStripMenuItem.Size = size;
            this.mnuFile.Text = Resources.MenuTxtFile;
            this.mnuOpen.Image = Resources.IconOpen;
			this.mnuOpen.ImageTransparentColor = Color.Magenta;
			this.mnuOpen.Name = "mnuOpen";
			this.mnuOpen.ShortcutKeys = Keys.LButton | Keys.RButton | Keys.Cancel | Keys.MButton | Keys.XButton1 | Keys.XButton2 | Keys.Back | Keys.Tab | Keys.LineFeed | Keys.Clear | Keys.Return | Keys.Enter | Keys.A | Keys.B | Keys.C | Keys.D | Keys.E | Keys.F | Keys.G | Keys.H | Keys.I | Keys.J | Keys.K | Keys.L | Keys.M | Keys.N | Keys.O | Keys.Control;
			ToolStripMenuItem toolStripMenuItem1 = this.mnuOpen;
			size = new System.Drawing.Size(201, 22);
			toolStripMenuItem1.Size = size;
            this.mnuOpen.Text = Resources.MenuTxtClassOpen;
			this.toolStripSeparator.Name = "toolStripSeparator";
			ToolStripSeparator toolStripSeparator = this.toolStripSeparator;
			size = new System.Drawing.Size(198, 6);
			toolStripSeparator.Size = size;
			this.mnuUpdate.Enabled = false;
			this.mnuUpdate.Image = Resources.IconUpdate;
			this.mnuUpdate.ImageTransparentColor = Color.Magenta;
			this.mnuUpdate.Name = "mnuUpdate";
			this.mnuUpdate.ShortcutKeys = Keys.LButton | Keys.RButton | Keys.Cancel | Keys.ShiftKey | Keys.ControlKey | Keys.Menu | Keys.Pause | Keys.A | Keys.B | Keys.C | Keys.P | Keys.Q | Keys.R | Keys.S | Keys.Control;
			ToolStripMenuItem toolStripMenuItem2 = this.mnuUpdate;
			size = new System.Drawing.Size(201, 22);
			toolStripMenuItem2.Size = size;
            this.mnuUpdate.Text = Resources.MenuTxtClassSave;
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			ToolStripSeparator toolStripSeparator1 = this.toolStripSeparator1;
			size = new System.Drawing.Size(198, 6);
			toolStripSeparator1.Size = size;
			this.mnuClose.Name = "mnuClose";
			ToolStripMenuItem toolStripMenuItem3 = this.mnuClose;
			size = new System.Drawing.Size(201, 22);
			toolStripMenuItem3.Size = size;
            this.mnuClose.Text = Resources.MenuTxtClose;
			ToolStripItemCollection dropDownItems1 = this.mnuEdit.DropDownItems;
			toolStripSeparator15 = new ToolStripItem[] { this.mnuCopy, this.ToolStripSeparator15, this.mnuSelectPlayer, this.mnuSelectTeacher, this.mnuShuffleBukatuSeparator, this.mnuShuffleBukatu, this.mnuShuffleKenka, this.mnuShuffleSitagi, this.ToolStripSeparator3, this.mnuSelectCondition, this.ToolStripSeparator4, this.mnuSelectAll, this.mnuClearAll };
			dropDownItems1.AddRange(toolStripSeparator15);
			this.mnuEdit.Name = "mnuEdit";
			JG2ToolStripMenuItem jG2ToolStripMenuItem = this.mnuEdit;
			size = new System.Drawing.Size(61, 22);
			jG2ToolStripMenuItem.Size = size;
            this.mnuEdit.Text = Resources.MenuTxtEdit;
			this.mnuCopy.Image = Resources.IconCopy;
			this.mnuCopy.ImageTransparentColor = Color.Magenta;
			this.mnuCopy.Name = "mnuCopy";
			this.mnuCopy.ShortcutKeys = Keys.LButton | Keys.RButton | Keys.Cancel | Keys.A | Keys.B | Keys.C | Keys.Control;
			ToolStripMenuItem toolStripMenuItem4 = this.mnuCopy;
			size = new System.Drawing.Size(244, 22);
			toolStripMenuItem4.Size = size;
            this.mnuCopy.Text = Resources.MenuTxtClassCopy;
			this.ToolStripSeparator15.Name = "ToolStripSeparator15";
			ToolStripSeparator toolStripSeparator151 = this.ToolStripSeparator15;
			size = new System.Drawing.Size(241, 6);
			toolStripSeparator151.Size = size;
			this.mnuSelectPlayer.Name = "mnuSelectPlayer";
			this.mnuSelectPlayer.ShortcutKeys = Keys.ShiftKey | Keys.P | Keys.Control;
			ToolStripMenuItem toolStripMenuItem5 = this.mnuSelectPlayer;
			size = new System.Drawing.Size(244, 22);
			toolStripMenuItem5.Size = size;
            this.mnuSelectPlayer.Text = Resources.MenuTxtSelectPC;
			this.mnuSelectTeacher.Name = "mnuSelectTeacher";
			this.mnuSelectTeacher.ShortcutKeys = Keys.MButton | Keys.ShiftKey | Keys.Capital | Keys.CapsLock | Keys.D | Keys.P | Keys.T | Keys.Control;
			ToolStripMenuItem toolStripMenuItem6 = this.mnuSelectTeacher;
			size = new System.Drawing.Size(244, 22);
			toolStripMenuItem6.Size = size;
            this.mnuSelectTeacher.Text = Resources.MenuTxtSelectTeacher;
			this.mnuShuffleBukatuSeparator.Name = "mnuShuffleBukatuSeparator";
			ToolStripSeparator toolStripSeparator2 = this.mnuShuffleBukatuSeparator;
			size = new System.Drawing.Size(241, 6);
			toolStripSeparator2.Size = size;
			this.mnuShuffleBukatu.Name = "mnuShuffleBukatu";
			ToolStripMenuItem toolStripMenuItem7 = this.mnuShuffleBukatu;
			size = new System.Drawing.Size(244, 22);
			toolStripMenuItem7.Size = size;
            this.mnuShuffleBukatu.Text = Resources.MenuTxtShuffleClubs;
			this.mnuShuffleKenka.Name = "mnuShuffleKenka";
			ToolStripMenuItem toolStripMenuItem8 = this.mnuShuffleKenka;
			size = new System.Drawing.Size(244, 22);
			toolStripMenuItem8.Size = size;
            this.mnuShuffleKenka.Text = Resources.MenuTxtShuffleFightingStyle;
			this.mnuShuffleSitagi.Name = "mnuShuffleSitagi";
			ToolStripMenuItem toolStripMenuItem9 = this.mnuShuffleSitagi;
			size = new System.Drawing.Size(244, 22);
			toolStripMenuItem9.Size = size;
            this.mnuShuffleSitagi.Text = Resources.MenuTxtShuffleUnderwear;
			this.ToolStripSeparator3.Name = "ToolStripSeparator3";
			ToolStripSeparator toolStripSeparator3 = this.ToolStripSeparator3;
			size = new System.Drawing.Size(241, 6);
			toolStripSeparator3.Size = size;
			this.mnuSelectCondition.Image = Resources.IconFilter;
			this.mnuSelectCondition.Name = "mnuSelectCondition";
			ToolStripMenuItem toolStripMenuItem10 = this.mnuSelectCondition;
			size = new System.Drawing.Size(244, 22);
			toolStripMenuItem10.Size = size;
            this.mnuSelectCondition.Text = Resources.MenuTxtFilter;
			this.ToolStripSeparator4.Name = "ToolStripSeparator4";
			ToolStripSeparator toolStripSeparator4 = this.ToolStripSeparator4;
			size = new System.Drawing.Size(241, 6);
			toolStripSeparator4.Size = size;
			this.mnuSelectAll.Name = "mnuSelectAll";
			this.mnuSelectAll.ShortcutKeys = Keys.LButton | Keys.A | Keys.Control;
			ToolStripMenuItem toolStripMenuItem11 = this.mnuSelectAll;
			size = new System.Drawing.Size(244, 22);
			toolStripMenuItem11.Size = size;
            this.mnuSelectAll.Text = Resources.MenuTxtSelectTarget;
			this.mnuClearAll.Name = "mnuClearAll";
			this.mnuClearAll.ShortcutKeys = Keys.RButton | Keys.ShiftKey | Keys.Menu | Keys.B | Keys.P | Keys.R | Keys.Control;
			ToolStripMenuItem toolStripMenuItem12 = this.mnuClearAll;
			size = new System.Drawing.Size(244, 22);
			toolStripMenuItem12.Size = size;
            this.mnuClearAll.Text = Resources.MenuTxtUnselect;
			ToolStripItemCollection toolStripItemCollections1 = this.mnuView.DropDownItems;
			toolStripSeparator15 = new ToolStripItem[] { this.mnuReload, this.mnuAutoReload, this.ToolStripSeparator5, this.mnuDisplayItem };
			toolStripItemCollections1.AddRange(toolStripSeparator15);
			this.mnuView.Name = "mnuView";
			this.mnuView.ShortcutKeys = Keys.F5;
			JG2ToolStripMenuItem jG2ToolStripMenuItem1 = this.mnuView;
			size = new System.Drawing.Size(62, 22);
			jG2ToolStripMenuItem1.Size = size;
            this.mnuView.Text = Resources.MenuTxtView;
			this.mnuReload.Image = Resources.IconRefresh;
			this.mnuReload.Name = "mnuReload";
			this.mnuReload.ShortcutKeys = Keys.F5;
			ToolStripMenuItem toolStripMenuItem13 = this.mnuReload;
			size = new System.Drawing.Size(220, 22);
			toolStripMenuItem13.Size = size;
            this.mnuReload.Text = Resources.MenuTxtUpdateClass;
			this.mnuAutoReload.CheckOnClick = true;
			this.mnuAutoReload.Name = "mnuAutoReload";
			ToolStripMenuItem toolStripMenuItem14 = this.mnuAutoReload;
			size = new System.Drawing.Size(220, 22);
			toolStripMenuItem14.Size = size;
            this.mnuAutoReload.Text = Resources.MenuTxtSaveMonitor;
			this.ToolStripSeparator5.Name = "ToolStripSeparator5";
			ToolStripSeparator toolStripSeparator5 = this.ToolStripSeparator5;
			size = new System.Drawing.Size(217, 6);
			toolStripSeparator5.Size = size;
			ToolStripItemCollection dropDownItems2 = this.mnuDisplayItem.DropDownItems;
			toolStripSeparator15 = new ToolStripItem[] { this.mnuDisplayItem8, this.mnuDisplayItem9, this.mnuDisplayItem10, this.mnuDisplayItem11, this.mnuDisplayItem12, this.ToolStripSeparator8, this.mnuDisplayItem0, this.ToolStripSeparator6, this.mnuDisplayItem1, this.mnuDisplayItem2, this.mnuDisplayItem3, this.ToolStripSeparator7, this.mnuDisplayItem4, this.mnuDisplayItem5, this.mnuDisplayItem6, this.mnuDisplayItem7 };
			dropDownItems2.AddRange(toolStripSeparator15);
			this.mnuDisplayItem.Name = "mnuDisplayItem";
			ToolStripMenuItem toolStripMenuItem15 = this.mnuDisplayItem;
			size = new System.Drawing.Size(220, 22);
			toolStripMenuItem15.Size = size;
            this.mnuDisplayItem.Text = Resources.MenuTxtSelectDisplayItems;
			this.mnuDisplayItem8.Checked = true;
			this.mnuDisplayItem8.CheckOnClick = true;
			this.mnuDisplayItem8.CheckState = CheckState.Checked;
			this.mnuDisplayItem8.Name = "mnuDisplayItem8";
			ToolStripMenuItem toolStripMenuItem16 = this.mnuDisplayItem8;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem16.Size = size;
			this.mnuDisplayItem9.Checked = true;
			this.mnuDisplayItem9.CheckOnClick = true;
			this.mnuDisplayItem9.CheckState = CheckState.Checked;
			this.mnuDisplayItem9.Name = "mnuDisplayItem9";
			ToolStripMenuItem toolStripMenuItem17 = this.mnuDisplayItem9;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem17.Size = size;
			this.mnuDisplayItem10.Checked = true;
			this.mnuDisplayItem10.CheckOnClick = true;
			this.mnuDisplayItem10.CheckState = CheckState.Checked;
			this.mnuDisplayItem10.Name = "mnuDisplayItem10";
			ToolStripMenuItem toolStripMenuItem18 = this.mnuDisplayItem10;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem18.Size = size;
			this.mnuDisplayItem11.Checked = true;
			this.mnuDisplayItem11.CheckOnClick = true;
			this.mnuDisplayItem11.CheckState = CheckState.Checked;
			this.mnuDisplayItem11.Name = "mnuDisplayItem11";
			ToolStripMenuItem toolStripMenuItem19 = this.mnuDisplayItem11;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem19.Size = size;
			this.mnuDisplayItem12.Checked = true;
			this.mnuDisplayItem12.CheckOnClick = true;
			this.mnuDisplayItem12.CheckState = CheckState.Checked;
			this.mnuDisplayItem12.Name = "mnuDisplayItem12";
			ToolStripMenuItem toolStripMenuItem20 = this.mnuDisplayItem12;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem20.Size = size;
			this.ToolStripSeparator8.Name = "ToolStripSeparator8";
			ToolStripSeparator toolStripSeparator8 = this.ToolStripSeparator8;
			size = new System.Drawing.Size(65, 6);
			toolStripSeparator8.Size = size;
			ToolStripItemCollection toolStripItemCollections2 = this.mnuDisplayItem0.DropDownItems;
			toolStripSeparator15 = new ToolStripItem[] { this.mnuDisplayItem00, this.mnuDisplayItem01, this.mnuDisplayItem02, this.ToolStripSeparator9, this.mnuDisplayItem0Option1 };
			toolStripItemCollections2.AddRange(toolStripSeparator15);
			this.mnuDisplayItem0.Name = "mnuDisplayItem0";
			JG2ToolStripMenuItem jG2ToolStripMenuItem2 = this.mnuDisplayItem0;
			size = new System.Drawing.Size(68, 22);
			jG2ToolStripMenuItem2.Size = size;
			this.mnuDisplayItem00.CheckOnClick = true;
			this.mnuDisplayItem00.Name = "mnuDisplayItem00";
			JG2ToolStripMenuItem jG2ToolStripMenuItem3 = this.mnuDisplayItem00;
			size = new System.Drawing.Size(160, 22);
			jG2ToolStripMenuItem3.Size = size;
            this.mnuDisplayItem00.Text = Resources.MenuTxtDontShow;
			this.mnuDisplayItem01.Checked = true;
			this.mnuDisplayItem01.CheckOnClick = true;
			this.mnuDisplayItem01.CheckState = CheckState.Indeterminate;
			this.mnuDisplayItem01.Name = "mnuDisplayItem01";
			JG2ToolStripMenuItem jG2ToolStripMenuItem4 = this.mnuDisplayItem01;
			size = new System.Drawing.Size(160, 22);
			jG2ToolStripMenuItem4.Size = size;
			this.mnuDisplayItem02.CheckOnClick = true;
			this.mnuDisplayItem02.Name = "mnuDisplayItem02";
			JG2ToolStripMenuItem jG2ToolStripMenuItem5 = this.mnuDisplayItem02;
			size = new System.Drawing.Size(160, 22);
			jG2ToolStripMenuItem5.Size = size;
			this.ToolStripSeparator9.Name = "ToolStripSeparator9";
			ToolStripSeparator toolStripSeparator9 = this.ToolStripSeparator9;
			size = new System.Drawing.Size(157, 6);
			toolStripSeparator9.Size = size;
			this.mnuDisplayItem0Option1.Checked = true;
			this.mnuDisplayItem0Option1.CheckOnClick = true;
			this.mnuDisplayItem0Option1.CheckState = CheckState.Checked;
			this.mnuDisplayItem0Option1.Name = "mnuDisplayItem0Option1";
			ToolStripMenuItem toolStripMenuItem21 = this.mnuDisplayItem0Option1;
			size = new System.Drawing.Size(160, 22);
			toolStripMenuItem21.Size = size;
            this.mnuDisplayItem0Option1.Text = Resources.MenuTxtDisplayRatio;
			this.ToolStripSeparator6.Name = "ToolStripSeparator6";
			ToolStripSeparator toolStripSeparator6 = this.ToolStripSeparator6;
			size = new System.Drawing.Size(65, 6);
			toolStripSeparator6.Size = size;
			this.mnuDisplayItem1.Checked = true;
			this.mnuDisplayItem1.CheckOnClick = true;
			this.mnuDisplayItem1.CheckState = CheckState.Checked;
			this.mnuDisplayItem1.Name = "mnuDisplayItem1";
			ToolStripMenuItem toolStripMenuItem22 = this.mnuDisplayItem1;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem22.Size = size;
			this.mnuDisplayItem2.Checked = true;
			this.mnuDisplayItem2.CheckOnClick = true;
			this.mnuDisplayItem2.CheckState = CheckState.Checked;
			this.mnuDisplayItem2.Name = "mnuDisplayItem2";
			ToolStripMenuItem toolStripMenuItem23 = this.mnuDisplayItem2;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem23.Size = size;
			this.mnuDisplayItem3.Checked = true;
			this.mnuDisplayItem3.CheckOnClick = true;
			this.mnuDisplayItem3.CheckState = CheckState.Checked;
			this.mnuDisplayItem3.Name = "mnuDisplayItem3";
			ToolStripMenuItem toolStripMenuItem24 = this.mnuDisplayItem3;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem24.Size = size;
			this.ToolStripSeparator7.Name = "ToolStripSeparator7";
			ToolStripSeparator toolStripSeparator7 = this.ToolStripSeparator7;
			size = new System.Drawing.Size(65, 6);
			toolStripSeparator7.Size = size;
			this.mnuDisplayItem4.Checked = true;
			this.mnuDisplayItem4.CheckOnClick = true;
			this.mnuDisplayItem4.CheckState = CheckState.Checked;
			this.mnuDisplayItem4.Name = "mnuDisplayItem4";
			ToolStripMenuItem toolStripMenuItem25 = this.mnuDisplayItem4;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem25.Size = size;
			this.mnuDisplayItem5.Checked = true;
			this.mnuDisplayItem5.CheckOnClick = true;
			this.mnuDisplayItem5.CheckState = CheckState.Checked;
			this.mnuDisplayItem5.Name = "mnuDisplayItem5";
			ToolStripMenuItem toolStripMenuItem26 = this.mnuDisplayItem5;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem26.Size = size;
			this.mnuDisplayItem6.Checked = true;
			this.mnuDisplayItem6.CheckOnClick = true;
			this.mnuDisplayItem6.CheckState = CheckState.Checked;
			this.mnuDisplayItem6.Name = "mnuDisplayItem6";
			ToolStripMenuItem toolStripMenuItem27 = this.mnuDisplayItem6;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem27.Size = size;
			this.mnuDisplayItem7.Checked = true;
			this.mnuDisplayItem7.CheckOnClick = true;
			this.mnuDisplayItem7.CheckState = CheckState.Checked;
			this.mnuDisplayItem7.Name = "mnuDisplayItem7";
			ToolStripMenuItem toolStripMenuItem28 = this.mnuDisplayItem7;
			size = new System.Drawing.Size(68, 22);
			toolStripMenuItem28.Size = size;
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			ToolStripSeparator toolStripSeparator10 = this.toolStripSeparator2;
			size = new System.Drawing.Size(198, 6);
			toolStripSeparator10.Size = size;
			this.AutoScaleDimensions = new SizeF(7f, 18f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = SystemColors.Control;
			size = new System.Drawing.Size(934, 626);
			this.ClientSize = size;
			this.Controls.Add(this.MenuStrip);
			this.Controls.Add(this.pnlClass);
			this.Controls.Add(this.pnlHeader);
			this.Font = new System.Drawing.Font(Resources.TextMerioFont, 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MainMenuStrip = this.MenuStrip;
			padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Margin = padding;
			this.MaximizeBox = false;
			size = new System.Drawing.Size(940, 654);
			this.MaximumSize = size;
			size = new System.Drawing.Size(940, 654);
			this.MinimumSize = size;
			this.Name = "frmSchool";
			this.pnlHeader.ResumeLayout(false);
			((ISupportInitialize)this.numDays).EndInit();
			this.pnlClass.ResumeLayout(false);
			((ISupportInitialize)this.fswDataFile).EndInit();
			this.MenuStrip.ResumeLayout(false);
			this.MenuStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void mnuAutoReload_CheckedChanged(object sender, EventArgs e)
		{
			if (!Conversions.ToBoolean(NewLateBinding.LateGet(sender, null, "Checked", new object[0], null, null, null)))
			{
				this.fswDataFile.EnableRaisingEvents = false;
			}
			else
			{
				this.fswDataFile.EnableRaisingEvents = false;
				this.fswDataFile.Path = JG2Importer.Common.MySaveData.DataFile.DirectoryName;
				this.fswDataFile.Filter = JG2Importer.Common.MySaveData.DataFile.Name;
				this.fswDataFile.EnableRaisingEvents = true;
			}
		}

		private void mnuClearAll_Click(object sender, EventArgs e)
		{
			this.ClearSeatCheck();
		}

		[MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
		private void mnuClose_Click(object sender, EventArgs e)
		{
			JG2Importer.Common.MyColorDialog.Dispose();
			JG2Importer.Common.MyColorDialog = null;
			if ((Information.IsNothing(this.MyDetail) ? false : !this.MyDetail.IsDisposed))
			{
				this.MyDetail.Close(false);
				this.MyDetail.Dispose();
				this.MyDetail = null;
			}
			this.Close();
			ProjectData.EndApp();
		}

		private void mnuCopy_Click(object sender, EventArgs e)
		{
			string chr = JG2Importer.JG2.Common.ToChar(Resources.CaptionSeatNo, "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSex, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionName1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionName2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHeight, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTaikei, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionStyleM, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionAtama1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionAtama2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionWaist, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMune1, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMune2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMune3, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMune4, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMune5, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMune6, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMune7, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMune8, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionNyurin, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionColorSkin, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTikubi1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTikubi2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTikubi3, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHiyake1, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHiyake2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMosaic, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionUnder1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionUnder2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionColorUnder, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionKao, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionEye1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionEye2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionEye3, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionEye4, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionEye5, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionPupil1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionPupil2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionPupil3, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionPupil4, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionPupil5, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHighlight1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionColorEye1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionColorEye2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMayu1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMayu2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionColorMayu, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMabuta, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMatuge1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMatuge2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionGlasses, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionColorGlasses, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(string.Concat(Resources.CaptionHokuro, "/", Resources.CaptionHokuro1), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(string.Concat(Resources.CaptionHokuro, "/", Resources.CaptionHokuro2), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(string.Concat(Resources.CaptionHokuro, "/", Resources.CaptionHokuro3), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(string.Concat(Resources.CaptionHokuro, "/", Resources.CaptionHokuro4), "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionLip1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionLip2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHair1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHairAdj1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHairRev1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHair2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHairAdj2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHairRev2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHair3, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHairAdj3, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHairRev3, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHair4, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHairAdj4, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHairRev4, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionColorHair, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTiryoku1, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTiryoku2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTiryoku3, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTairyoku1, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTairyoku2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTairyoku3, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionBukatu1, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionBukatu2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionBukatu3, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSyakousei, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionKenka, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTeisou, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSeiai, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSeikou1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSeikou2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionVoice, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSeikaku, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionCyoroi, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionNekketu, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionNigate1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionNigate2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionCharm, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTundere, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionKyouki, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMiha, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSunao, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMaemuki, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTereya, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionYakimochi, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionToufu, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSukebe, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMajime, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionCool, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTyokujyo, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionPoyayan, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionBouryoku, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSousyoku, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSewayaki, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionIintyou, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionOsyaberi, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHarapeko, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionRenai, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionItizu, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionYuujyufudan, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMakenki, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHaraguro, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionKinben, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionHonpou, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionM, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionAsekaki, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionYami, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionNantyo, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionYowami, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMiseityo, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionLuck, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionRare, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionKiss, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionBust, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSeiki1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSeiki2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionCunnilingus, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionFellatio, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionBack, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionOnna, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionAnal, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionNama, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSeiin, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionNaka, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionKake, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSunday1, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMonday1, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTuesday1, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionWednesday1, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionThursday1, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionFriday1, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSaturday1, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSunday2, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionMonday2, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionTuesday2, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionWednesday2, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionThursday2, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionFriday2, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionSaturday2, "\t"), "\t");
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionGoods1, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionGoods2, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionGoods3, "\t"));
			chr = string.Concat(chr, JG2Importer.JG2.Common.ToChar(Resources.CaptionProfile, "\r\n"));
			int num = 0;
			do
			{
				if (((usrSeat)this.pnlClass.Controls[string.Concat("usrSeat", Strings.Format(num, "00"))]).Checked)
				{
					int index = JG2Importer.Common.MySaveData.GetIndex(num);
					if (index != -1)
					{
						chr = string.Concat(chr, JG2Importer.Common.MySaveData.GetText(index));
					}
				}
				num = checked(num + 1);
			}
			while (num <= 24);
			Clipboard.SetDataObject(chr, true);
		}

		private void mnuDisplayItem_Click(object sender, EventArgs e)
		{
			this.DrawClass();
		}

		private void mnuOpen_Click(object sender, EventArgs e)
		{
			string str = JG2Importer.Common.BrowseOpenFile(this, "SAV", Resources.DialogTxtSaveFile + " (*.sav)|*.sav", Resources.DialogTxtSpecifySaveFile, Path.Combine(JG2Importer.Common.INSTALLDIR_PLAY, "data\\save\\class\\"));
			if (Operators.CompareString(str, null, false) == 0)
			{
				return;
			}
			JG2Importer.Common.MySaveData = new SaveData(str);
			this.mnuAutoReload_CheckedChanged(this.mnuAutoReload, e);
			this.DrawSchool();
			this.numDays.Select();
		}

		private void mnuReload_Click(object sender, EventArgs e)
		{
			string fullName = JG2Importer.Common.MySaveData.DataFile.FullName;
			JG2Importer.Common.MySaveData = null;
			JG2Importer.Common.MySaveData = new SaveData(fullName);
			this.DrawSchool();
			this.numDays.Select();
		}

		private void mnuSelect_Click(object sender, EventArgs e)
		{
			int seatNoPlayer;
			object obj = NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null);
			if (!Operators.ConditionalCompareObjectEqual(obj, "mnuSelectPlayer", false))
			{
				if (!Operators.ConditionalCompareObjectEqual(obj, "mnuSelectTeacher", false))
				{
					return;
				}
				seatNoPlayer = 24;
			}
			else
			{
				seatNoPlayer = JG2Importer.Common.MySaveData.Footer.SeatNoPlayer;
			}
			if (JG2Importer.Common.MySaveData.GetIndex(seatNoPlayer) != -1)
			{
				usrSeat item = (usrSeat)this.pnlClass.Controls[string.Concat("usrSeat", Strings.Format(seatNoPlayer, "00"))];
				this.usrSeat_Click_Image(item, e);
			}
		}

		private void mnuSelectAll_Click(object sender, EventArgs e)
		{
			this.SelectSeatCheck(this.mnuSelectCondition);
		}

		private void mnuShuffle_Click(object sender, EventArgs e)
		{
			int num = 0;
			int num1 = 0;
			byte num2 = 0;
			int index = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			if (index != -1)
			{
				Random random = new Random(DateAndTime.Now.Millisecond);
				object obj = NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null);
				if (Operators.ConditionalCompareObjectEqual(obj, "mnuShuffleBukatu", false))
				{
					num1 = 8;
					num = 3;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj, "mnuShuffleKenka", false))
				{
					num1 = 3;
					num = 8;
				}
				int[] numArray = null;
				Array.Resize<int>(ref numArray, num1);
				int num3 = num;
				for (int i = 0; i <= num3; i = checked(i + 1))
				{
					int num4 = checked(num1 - 1);
					for (int j = 0; j <= num4; j = checked(j + 1))
					{
						int num5 = checked(checked(i * num1) + j);
						if (num5 > checked(checked((int)JG2Importer.Common.MySaveData.Seat.Length) - 1))
						{
							break;
						}
						while (true)
						{
							num2 = checked((byte)Math.Round(Math.Floor(random.NextDouble() * (double)num1)));
							if (numArray[num2] <= i)
							{
								break;
							}
						}
						numArray[num2] = checked(numArray[num2] + 1);
						object obj1 = NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null);
						if (Operators.ConditionalCompareObjectEqual(obj1, "mnuShuffleBukatu", false))
						{
							JG2Importer.Common.MySaveData.Seat[num5].Character.Attr.Bukatu1 = num2;
						}
						else if (Operators.ConditionalCompareObjectEqual(obj1, "mnuShuffleKenka", false))
						{
							JG2Importer.Common.MySaveData.Seat[num5].Character.Attr.Kenka = num2;
						}
					}
				}
				object obj2 = NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null);
				if (Operators.ConditionalCompareObjectEqual(obj2, "mnuShuffleBukatu", false))
				{
					JG2Importer.Common.MySaveData.Seat[checked(checked((int)JG2Importer.Common.MySaveData.Seat.Length) - 1)].Character.Attr.Bukatu1 = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bukatu1;
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Bukatu1 = num2;
				}
				else if (Operators.ConditionalCompareObjectEqual(obj2, "mnuShuffleKenka", false))
				{
					JG2Importer.Common.MySaveData.Seat[checked(checked((int)JG2Importer.Common.MySaveData.Seat.Length) - 1)].Character.Attr.Kenka = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kenka;
					JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Kenka = num2;
				}
				random = null;
				this.DrawSchool();
				this.numDays.Select();
			}
		}

		private void mnuUpdate_Click(object sender, EventArgs e)
		{
			if (Interaction.MsgBox(Resources.DialogTxtOverwriteSaveDataClass, MsgBoxStyle.OkCancel | MsgBoxStyle.Critical | MsgBoxStyle.Question | MsgBoxStyle.Exclamation | MsgBoxStyle.DefaultButton2, null) == MsgBoxResult.Ok)
			{
				this.Cursor = Cursors.WaitCursor;
				if (this.mnuAutoReload.Checked)
				{
					this.fswDataFile.EnableRaisingEvents = false;
				}
				JG2Importer.Common.MySaveData.Update();
				string[] name = new string[] { JG2Importer.Common.MySaveData.DataFile.Name, " (", Strings.Format(JG2Importer.Common.MySaveData.DataFile.LastWriteTime, "yyyy/MM/dd HH:mm"), ") - ", Application.ProductName };
				this.Text = string.Concat(name);
				this.numDays.Select();
				if (this.mnuAutoReload.Checked)
				{
					this.fswDataFile.EnableRaisingEvents = true;
				}
				this.Cursor = Cursors.Default;
			}
		}

		private void numDays_ValueChanged(object sender, EventArgs e)
		{
			object obj = NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null);
			if (Operators.ConditionalCompareObjectEqual(obj, "numDays", false))
			{
				JG2Importer.Common.MySaveData.Header.Days = Convert.ToByte(this.numDays.Value);
			}
			else if (Operators.ConditionalCompareObjectEqual(obj, "cmbWeek", false))
			{
				JG2Importer.Common.MySaveData.Header.Days = Convert.ToByte(decimal.Add(this.numDays.Value, new decimal(checked(this.cmbWeek.SelectedIndex - JG2Importer.Common.MySaveData.Header.Week))));
			}
			else if (Operators.ConditionalCompareObjectEqual(obj, "cmbEvent", false))
			{
				switch (this.cmbEvent.SelectedIndex)
				{
					case 0:
					{
						JG2Importer.Common.MySaveData.Header.Days = 12;
						break;
					}
					case 1:
					{
						JG2Importer.Common.MySaveData.Header.Days = 13;
						break;
					}
					case 2:
					{
						JG2Importer.Common.MySaveData.Header.Days = 26;
						break;
					}
					case 3:
					{
						JG2Importer.Common.MySaveData.Header.Days = 27;
						break;
					}
					case 4:
					{
						JG2Importer.Common.MySaveData.Header.Days = 40;
						break;
					}
					case 5:
					{
						JG2Importer.Common.MySaveData.Header.Days = 41;
						break;
					}
				}
			}
			JG2Importer.Common.MySaveData.Header.Week = checked((byte)((checked(JG2Importer.Common.MySaveData.Header.Days - 1)) % 7));
			this.DrawHeader();
			this.DrawClass();
		}

		public void SelectSeatCheck(ToolStripMenuItem argMenuItem)
		{
			int index = JG2Importer.Common.MySaveData.GetIndex(JG2Importer.Common.MySaveData.Footer.SeatNoPlayer);
			if (index != -1)
			{
				byte sex = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Sex;
				byte seiai = JG2Importer.Common.MySaveData.Seat[index].Character.Attr.Seiai;
				int num = 0;
				do
				{
					usrSeat item = (usrSeat)this.pnlClass.Controls[string.Concat("usrSeat", Strings.Format(num, "00"))];
					int index1 = JG2Importer.Common.MySaveData.GetIndex(num);
					if (index1 == -1)
					{
						item.Checked = false;
					}
					else if (!((ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectOption2"]).Checked || JG2Importer.Common.MySaveData.Seat[index1].SeatNo != JG2Importer.Common.MySaveData.Footer.SeatNoPlayer)
					{
						JG2ToolStripMenuItem jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionSex"];
						if (jG2ToolStripMenuItem.SelectedIndex <= 0 || JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Sex == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
						{
							jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionBukatu1"];
							if (jG2ToolStripMenuItem.SelectedIndex <= 0 || JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Bukatu1 == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
							{
								jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionSyakousei"];
								if (jG2ToolStripMenuItem.SelectedIndex <= 0 || JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Syakousei == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
								{
									jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionTeisou"];
									if (jG2ToolStripMenuItem.SelectedIndex <= 0 || JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Teisou == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
									{
										jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionSeikou1"];
										if (jG2ToolStripMenuItem.SelectedIndex <= 0 || JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Seikou1 == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
										{
											jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionSeikou2"];
											if (jG2ToolStripMenuItem.SelectedIndex <= 0 || JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Seikou2 == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
											{
												jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionNinshin"];
												if (jG2ToolStripMenuItem.SelectedIndex <= 0 || (int)JG2Importer.Common.MySaveData.GetCodeNinshin(index1) == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
												{
													jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionAction"];
													if (jG2ToolStripMenuItem.SelectedIndex <= 0 || (int)JG2Importer.Common.MySaveData.GetCodeAction(index1, index).CodeAction == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
													{
														jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionActionRev"];
														if (jG2ToolStripMenuItem.SelectedIndex <= 0 || (int)JG2Importer.Common.MySaveData.GetCodeAction(index, index1).CodeAction == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
														{
															jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionKousai"];
															if (jG2ToolStripMenuItem.SelectedIndex <= 0 || (int)JG2Importer.Common.MySaveData.GetCodeKousai(index1, index) == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
															{
																jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionDate"];
																if (jG2ToolStripMenuItem.SelectedIndex <= 0 || (int)JG2Importer.Common.MySaveData.GetCodeDate(index1, index) == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
																{
																	jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionGohoubi"];
																	if (jG2ToolStripMenuItem.SelectedIndex <= 0 || (int)JG2Importer.Common.MySaveData.GetCodeGohoubi(index1, index) == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
																	{
																		jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionHAisyou"];
																		if (jG2ToolStripMenuItem.SelectedIndex <= 0 || (int)JG2Importer.Common.MySaveData.GetCodeHAisyou(index1, index) == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
																		{
																			jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionCntNakaKiken"];
																			if (jG2ToolStripMenuItem.SelectedIndex <= 0 || (int)JG2Importer.Common.MySaveData.GetCodeCntNakaKiken(index1, index) == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
																			{
																				jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionRenzoku"];
																				if (jG2ToolStripMenuItem.SelectedIndex <= 0 || (int)JG2Importer.Common.MySaveData.GetCodeRenzoku(index1, index) == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
																				{
																					jG2ToolStripMenuItem = (JG2ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectConditionCntTodaySeikou"];
																					if (jG2ToolStripMenuItem.SelectedIndex <= 0 || (int)JG2Importer.Common.MySaveData.GetCodeCntTodaySeikou(index1, index) == checked(jG2ToolStripMenuItem.SelectedIndex - 1))
																					{
																						if (((ToolStripMenuItem)argMenuItem.DropDownItems["mnuSelectOption1"]).Checked)
																						{
																							switch (sex)
																							{
																								case 0:
																								{
																									switch (seiai)
																									{
																										case 0:
																										{
																											if (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Sex == 1)
																											{
																												if (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Seiai != 4)
																												{
																													goto Label1;
																												}
																												item.Checked = false;
																												goto Label0;
																											}
																											else
																											{
																												item.Checked = false;
																												goto Label0;
																											}
																										}
																										case 1:
																										case 2:
																										case 3:
																										{
																											switch (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Sex)
																											{
																												case 0:
																												{
																													if (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Seiai != 0)
																													{
																														break;
																													}
																													item.Checked = false;
																													goto Label0;
																												}
																												case 1:
																												{
																													if (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Seiai != 4)
																													{
																														break;
																													}
																													item.Checked = false;
																													goto Label0;
																												}
																												default:
																												{
																													item.Checked = false;
																													goto Label0;
																												}
																											}
																											break;
																										}
																										case 4:
																										{
																											if (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Sex == 0)
																											{
																												if (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Seiai != 0)
																												{
																													goto Label1;
																												}
																												item.Checked = false;
																												goto Label0;
																											}
																											else
																											{
																												item.Checked = false;
																												goto Label0;
																											}
																										}
																										default:
																										{
																											goto case 3;
																										}
																									}
																									break;
																								}
																								case 1:
																								{
																									switch (seiai)
																									{
																										case 0:
																										{
																											if (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Sex == 0)
																											{
																												if (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Seiai != 4)
																												{
																													goto Label1;
																												}
																												item.Checked = false;
																												goto Label0;
																											}
																											else
																											{
																												item.Checked = false;
																												goto Label0;
																											}
																										}
																										case 1:
																										case 2:
																										case 3:
																										{
																											switch (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Sex)
																											{
																												case 0:
																												{
																													if (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Seiai != 4)
																													{
																														break;
																													}
																													item.Checked = false;
																													goto Label0;
																												}
																												case 1:
																												{
																													if (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Seiai != 0)
																													{
																														break;
																													}
																													item.Checked = false;
																													goto Label0;
																												}
																												default:
																												{
																													item.Checked = false;
																													goto Label0;
																												}
																											}
																											break;
																										}
																										case 4:
																										{
																											if (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Sex == 1)
																											{
																												if (JG2Importer.Common.MySaveData.Seat[index1].Character.Attr.Seiai != 0)
																												{
																													goto Label1;
																												}
																												item.Checked = false;
																												goto Label0;
																											}
																											else
																											{
																												item.Checked = false;
																												goto Label0;
																											}
																										}
																										default:
																										{
																											goto case 3;
																										}
																									}
																									break;
																								}
																								default:
																								{
																									item.Checked = false;
																									goto Label0;
																								}
																							}
																						}
																					Label1:
																						item.Checked = true;
																					}
																					else
																					{
																						item.Checked = false;
																					}
																				}
																				else
																				{
																					item.Checked = false;
																				}
																			}
																			else
																			{
																				item.Checked = false;
																			}
																		}
																		else
																		{
																			item.Checked = false;
																		}
																	}
																	else
																	{
																		item.Checked = false;
																	}
																}
																else
																{
																	item.Checked = false;
																}
															}
															else
															{
																item.Checked = false;
															}
														}
														else
														{
															item.Checked = false;
														}
													}
													else
													{
														item.Checked = false;
													}
												}
												else
												{
													item.Checked = false;
												}
											}
											else
											{
												item.Checked = false;
											}
										}
										else
										{
											item.Checked = false;
										}
									}
									else
									{
										item.Checked = false;
									}
								}
								else
								{
									item.Checked = false;
								}
							}
							else
							{
								item.Checked = false;
							}
						}
						else
						{
							item.Checked = false;
						}
					}
					else
					{
						item.Checked = false;
					}
				Label0:
					num = checked(num + 1);
				}
				while (num <= 24);
			}
		}

		private void usrSeat_Click_Image(object sender, EventArgs e)
		{
			object[] objArray;
			if (Information.IsNothing(RuntimeHelpers.GetObjectValue(this.pnlClass.Tag)))
			{
				objArray = new object[] { true };
				NewLateBinding.LateSet(sender, null, "CurrentSeat", objArray, null, null);
			}
			else if (Operators.ConditionalCompareObjectNotEqual(NewLateBinding.LateGet(this.pnlClass.Tag, null, "Name", new object[0], null, null, null), NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null), false))
			{
				object tag = this.pnlClass.Tag;
				objArray = new object[] { false };
				NewLateBinding.LateSetComplex(tag, null, "CurrentSeat", objArray, null, null, false, true);
				objArray = new object[] { true };
				NewLateBinding.LateSet(sender, null, "CurrentSeat", objArray, null, null);
			}
			this.pnlClass.Tag = RuntimeHelpers.GetObjectValue(sender);
			Application.DoEvents();
			if ((Information.IsNothing(this.MyDetail) ? true : this.MyDetail.IsDisposed))
			{
				this.MyDetail = new frmDetail();
				if (!this.MyDetail.DrawDetail(Conversions.ToInteger(Strings.Mid(Conversions.ToString(NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null)), 8))))
				{
					this.MyDetail.Close(false);
				}
				else
				{
					this.MyDetail.Show(this);
				}
			}
			else if (!this.MyDetail.DrawDetail(Conversions.ToInteger(Strings.Mid(Conversions.ToString(NewLateBinding.LateGet(sender, null, "Name", new object[0], null, null, null)), 8))))
			{
				this.MyDetail.Close(false);
			}
		}
	}
}