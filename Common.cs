using JG2Importer.JG2;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.Win32;
using System;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace JG2Importer
{
	internal static class Common
	{
		public static string INSTALLDIR_PLAY;

		public static string INSTALLDIR_EDIT;

		public static Point MyDetailLocation;

		public static ColorDialog MyColorDialog;

		public static SaveData MySaveData;

		public static string BrowseOpenFile(Form argForm, string argDefaultExt, string argFilter, string argTitle, string argInitPath = null)
		{
			string directoryName;
			string fileName = null;
			try
			{
				directoryName = Path.GetDirectoryName(argInitPath);
			}
			catch (Exception exception)
			{
				ProjectData.SetProjectError(exception);
				directoryName = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
				ProjectData.ClearProjectError();
			}
			OpenFileDialog openFileDialog = new OpenFileDialog()
			{
				AddExtension = true,
				AutoUpgradeEnabled = true,
				CheckFileExists = true,
				CheckPathExists = true,
				DefaultExt = argDefaultExt,
				DereferenceLinks = true,
				Filter = string.Concat(argFilter, "|" + Resources.MenuTxtAllFileTypes + " (*.*)|*.*"),
				FilterIndex = 1,
				InitialDirectory = directoryName,
				Multiselect = false,
				RestoreDirectory = true,
				Title = argTitle
			};
			if (openFileDialog.ShowDialog(argForm) == DialogResult.OK)
			{
				fileName = openFileDialog.FileName;
			}
			openFileDialog = null;
			return fileName;
		}

		public static string BrowseSaveFile(Form argForm, string argDefaultExt, string argFilter, string argTitle, string argFileName, string argInitPath = null)
		{
			string directoryName;
			string fileName = null;
			try
			{
				directoryName = Path.GetDirectoryName(argInitPath);
			}
			catch (Exception exception)
			{
				ProjectData.SetProjectError(exception);
				directoryName = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
				ProjectData.ClearProjectError();
			}
			SaveFileDialog saveFileDialog = new SaveFileDialog()
			{
				AddExtension = true,
				AutoUpgradeEnabled = true,
				CheckFileExists = false,
				CheckPathExists = true,
				DefaultExt = argDefaultExt,
				DereferenceLinks = true,
				Filter = string.Concat(argFilter, "|" + Resources.MenuTxtAllFileTypes + " (*.*)|*.*"),
				FilterIndex = 1,
				InitialDirectory = directoryName,
				CreatePrompt = false,
				OverwritePrompt = true,
				RestoreDirectory = true,
				Title = argTitle,
				FileName = argFileName
			};
			if (saveFileDialog.ShowDialog(argForm) == DialogResult.OK)
			{
				fileName = saveFileDialog.FileName;
			}
			saveFileDialog = null;
			return fileName;
		}

		public static object GetRegistry(object argKey = null, object argSubKey = null)
		{
			object obj;
			object obj1;
			object objectValue = null;
			if (!Operators.ConditionalCompareObjectEqual(argKey, null, false))
			{
				obj1 = Operators.ConcatenateObject("illusion\\", argKey);
			}
			else
			{
				obj1 = "illusion";
			}
			try
			{
				RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(Conversions.ToString(Operators.ConcatenateObject("Software\\", obj1)));
				if (!Operators.ConditionalCompareObjectEqual(argSubKey, null, false))
				{
					objectValue = RuntimeHelpers.GetObjectValue(registryKey.GetValue(Conversions.ToString(argSubKey)));
				}
				registryKey.Close();
				registryKey = null;
				return objectValue;
			}
			catch (Exception exception)
			{
				ProjectData.SetProjectError(exception);
				obj = null;
				ProjectData.ClearProjectError();
			}
			return obj;
		}

		public struct HSVColor
		{
			public float Hue;

			public float Saturation;

			public float Brightness;
		}
	}
}