using JG2Importer.JG2;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace JG2Importer
{
	[DesignerGenerated]
	public class usrSeat : UserControl
	{
		private IContainer components;

		[AccessedThroughProperty("pnlBase")]
		private Panel _pnlBase;

		[AccessedThroughProperty("chkFullName")]
		private CheckBox _chkFullName;

		[AccessedThroughProperty("btnThumbnail")]
		private Button _btnThumbnail;

		[AccessedThroughProperty("lblAction")]
		private Label _lblAction;

		[AccessedThroughProperty("lblBukatu")]
		private Label _lblBukatu;

		[AccessedThroughProperty("lblStatus1")]
		private Label _lblStatus1;

		[AccessedThroughProperty("lblStatus2")]
		private Label _lblStatus2;

		[AccessedThroughProperty("lblProfile1")]
		private Label _lblProfile1;

		[AccessedThroughProperty("lblProfile2")]
		private Label _lblProfile2;

		[AccessedThroughProperty("lblProfile3")]
		private Label _lblProfile3;

		private bool m_CurrentSeat;

		private usrSeat.DefAttr m_Attr;

		private Image MonochromeThumbnail;

		public usrSeat.DefAttr Attr
		{
			get
			{
				return this.m_Attr;
			}
			set
			{
				string str;
				string str1;
				string str2;
				string str3;
				string str4;
				string str5;
				string str6;
				string str7;
				string str8;
				string str9;
				string str10;
				string str11;
				string str12;
				string str13;
				string str14;
				string str15;
				string str16;
				string str17;
				this.m_Attr = value;
				ToolStripMenuItem item = (ToolStripMenuItem)((ToolStripMenuItem)this.ParentForm.MainMenuStrip.Items["mnuView"]).DropDownItems["mnuDisplayItem"];
				Color foreColor = this.ParentForm.ForeColor;
				switch (((JG2ToolStripMenuItem)item.DropDownItems["mnuDisplayItem0"]).SelectedIndex)
				{
					case 1:
					{
						switch (this.m_Attr.CodeAction.CodeAction)
						{
							case CodeAction.Love:
							{
								foreColor = JG2Importer.JG2.Common.ColorLove;
								break;
							}
							case CodeAction.Like:
							{
								foreColor = JG2Importer.JG2.Common.ColorLike;
								break;
							}
							case CodeAction.Dislike:
							{
								foreColor = JG2Importer.JG2.Common.ColorDislike;
								break;
							}
							case CodeAction.Hate:
							{
								foreColor = JG2Importer.JG2.Common.ColorHate;
								break;
							}
						}
						break;
					}
					case 2:
					{
						switch (this.m_Attr.CodeActionRev.CodeAction)
						{
							case CodeAction.Love:
							{
								foreColor = JG2Importer.JG2.Common.ColorLove;
								break;
							}
							case CodeAction.Like:
							{
								foreColor = JG2Importer.JG2.Common.ColorLike;
								break;
							}
							case CodeAction.Dislike:
							{
								foreColor = JG2Importer.JG2.Common.ColorDislike;
								break;
							}
							case CodeAction.Hate:
							{
								foreColor = JG2Importer.JG2.Common.ColorHate;
								break;
							}
						}
						break;
					}
				}
				this.btnThumbnail.ForeColor = (this.m_Attr.Rare == 1 ? JG2Importer.JG2.Common.ColorRare : JG2Importer.JG2.Common.ColorNormal);
				Button button = this.btnThumbnail;
				if (this.m_Attr.Seikaku >= 0 & this.m_Attr.Seikaku <= checked(checked((int)JG2Importer.JG2.Common.cnsSeikaku.Length) - 1))
				{
					str = JG2Importer.JG2.Common.cnsSeikaku[this.m_Attr.Seikaku];
				}
				else
				{
					str = null;
				}
				if (this.m_Attr.Rare == 1)
				{
					str1 = Resources.CaptionRare;
				}
				else
				{
					str1 = null;
				}
				button.Text = string.Concat(str, str1);
				this.btnThumbnail.Image = this.m_Attr.ThumbnailImage;
				this.chkFullName.Font = new System.Drawing.Font(this.ParentForm.Font, (this.m_Attr.Player ? FontStyle.Bold : FontStyle.Regular));
				switch (this.m_Attr.CodeNinshin)
				{
					case CodeNinshin.Safe:
					{
						this.chkFullName.ForeColor = JG2Importer.JG2.Common.ColorAnzen;
						break;
					}
					case CodeNinshin.Danger:
					{
						this.chkFullName.ForeColor = JG2Importer.JG2.Common.ColorKiken;
						break;
					}
					default:
					{
						this.chkFullName.ForeColor = this.ParentForm.ForeColor;
						break;
					}
				}
				this.chkFullName.Text = this.m_Attr.FullName;
				this.lblAction.ForeColor = foreColor;
				if (!((ToolStripMenuItem)((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem0"]).DropDownItems["mnuDisplayItem0Option1"]).Checked)
				{
					switch (((JG2ToolStripMenuItem)item.DropDownItems["mnuDisplayItem0"]).SelectedIndex)
					{
						case 1:
						{
							Label label = this.lblAction;
							if (this.m_Attr.CodeAction.CodeAction == CodeAction.None)
							{
								str2 = null;
							}
							else
							{
								str2 = string.Concat("→", Strings.Format(this.m_Attr.CodeAction.MaxAction, "00"));
							}
							label.Text = str2;
							break;
						}
						case 2:
						{
							Label label1 = this.lblAction;
							if (this.m_Attr.CodeActionRev.CodeAction == CodeAction.None)
							{
								str15 = null;
							}
							else
							{
								str15 = string.Concat("←", Strings.Format(this.m_Attr.CodeActionRev.MaxAction, "00"));
							}
							label1.Text = str15;
							break;
						}
						default:
						{
							this.lblAction.Text = null;
							break;
						}
					}
				}
				else
				{
					switch (((JG2ToolStripMenuItem)item.DropDownItems["mnuDisplayItem0"]).SelectedIndex)
					{
						case 1:
						{
							Label label2 = this.lblAction;
							if (this.m_Attr.CodeAction.CodeAction == CodeAction.None)
							{
								str16 = null;
							}
							else
							{
								str16 = string.Concat("→", Strings.Format(Math.Floor((double)(checked(this.m_Attr.CodeAction.MaxAction * 100)) / 30), "0"));
							}
							label2.Text = str16;
							break;
						}
						case 2:
						{
							Label label3 = this.lblAction;
							if (this.m_Attr.CodeActionRev.CodeAction == CodeAction.None)
							{
								str17 = null;
							}
							else
							{
								str17 = string.Concat("←", Strings.Format(Math.Floor((double)(checked(this.m_Attr.CodeActionRev.MaxAction * 100)) / 30), "0"));
							}
							label3.Text = str17;
							break;
						}
						default:
						{
							this.lblAction.Text = null;
							break;
						}
					}
				}
				this.lblBukatu.Text = this.m_Attr.BukatuName;
				this.lblStatus1.ForeColor = foreColor;
				Label label4 = this.lblStatus1;
				if (!((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem1"]).Checked)
				{
					str3 = null;
				}
				else if (this.m_Attr.CodeKousai == CodeKousai.ClassMates)
				{
					str3 = null;
				}
				else
				{
					str3 = string.Concat(Strings.Space(1), JG2Importer.JG2.Common.cnsSeatKousai[(int)this.m_Attr.CodeKousai]);
				}
				if (!((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem2"]).Checked)
				{
					str4 = null;
				}
				else if (this.m_Attr.CodeDate == CodeDate.None)
				{
					str4 = null;
				}
				else
				{
					str4 = string.Concat(Strings.Space(1), Resources.FilterTxtDate);
				}
				if (!((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem3"]).Checked)
				{
					str5 = null;
				}
				else if (this.m_Attr.CodeGohoubi == CodeGohoubi.None)
				{
					str5 = null;
				}
				else
				{
					str5 = string.Concat(Strings.Space(1), JG2Importer.JG2.Common.cnsSeatGohoubi[(int)this.m_Attr.CodeGohoubi]);
				}
				label4.Text = Strings.Trim(string.Concat(str3, str4, str5));
				this.lblStatus2.ForeColor = foreColor;
				Label label5 = this.lblStatus2;
				if (!((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem4"]).Checked)
				{
					str6 = null;
				}
				else if (this.m_Attr.CodeHAisyou == CodeHAisyou.None)
				{
					str6 = null;
				}
				else
				{
					str6 = string.Concat(Strings.Space(1), JG2Importer.JG2.Common.cnsCodeHAisyou[(int)this.m_Attr.CodeHAisyou]);
				}
				if (!((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem5"]).Checked)
				{
					str7 = null;
				}
				else if (this.m_Attr.CodeCntNakaKiken == CodeCntNakaKiken.Danger)
				{
					str7 = string.Concat(Strings.Space(1), Resources.FilterTxtCreampie);
				}
				else
				{
					str7 = null;
				}
				if (!((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem6"]).Checked)
				{
					str8 = null;
				}
				else if (this.m_Attr.CodeRenzoku == CodeRenzoku.None)
				{
					str8 = null;
				}
				else
				{
					str8 = string.Concat(Strings.Space(1), JG2Importer.JG2.Common.cnsSeatRenzoku[(int)this.m_Attr.CodeRenzoku]);
				}
				if (!((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem7"]).Checked)
				{
					str9 = null;
				}
				else if (this.m_Attr.CodeCntTodaySeikou == CodeCntTodaySeikou.None)
				{
					str9 = null;
				}
				else
				{
					str9 = string.Concat(Strings.Space(1), Resources.CaptionCodeCntTodaySeikou);
				}
				label5.Text = Strings.Trim(string.Concat(str6, str7, str8, str9));
				Label label6 = this.lblProfile1;
				string[] strArrays = new string[5];
				string[] strArrays1 = strArrays;
				if (!((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem8"]).Checked)
				{
					str10 = null;
				}
				else if (this.m_Attr.Height == 0 | this.m_Attr.Height == 2)
				{
					str10 = string.Concat(Strings.Space(1), JG2Importer.JG2.Common.cnsHeight[this.m_Attr.Height]);
				}
				else
				{
					str10 = null;
				}
				strArrays1[0] = str10;
				string[] strArrays2 = strArrays;
				if (!((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem9"]).Checked)
				{
					str11 = null;
				}
				else if (this.m_Attr.Style == 0 | this.m_Attr.Style == 2)
				{
					str11 = string.Concat(Strings.Space(1), JG2Importer.JG2.Common.cnsStyle[this.m_Attr.Style]);
				}
				else
				{
					str11 = null;
				}
				strArrays2[1] = str11;
				string[] strArrays3 = strArrays;
				if (!((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem10"]).Checked)
				{
					str12 = null;
				}
				else if (this.m_Attr.Mune == 0 | this.m_Attr.Mune == 2)
				{
					str12 = string.Concat(Strings.Space(1), JG2Importer.JG2.Common.cnsMune[this.m_Attr.Mune]);
				}
				else
				{
					str12 = null;
				}
				strArrays3[2] = str12;
				string[] strArrays4 = strArrays;
				if (!((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem11"]).Checked)
				{
					str13 = null;
				}
				else if (this.m_Attr.Seikou1 == 1)
				{
					str13 = string.Concat(Strings.Space(1), Resources.FilterTxtSex);
				}
				else
				{
					str13 = null;
				}
				strArrays4[3] = str13;
				string[] strArrays5 = strArrays;
				if (!((ToolStripMenuItem)item.DropDownItems["mnuDisplayItem12"]).Checked)
				{
					str14 = null;
				}
				else if (this.m_Attr.Seikou2 == 1)
				{
					str14 = string.Concat(Strings.Space(1), Resources.FilterTxtAnal);
				}
				else
				{
					str14 = null;
				}
				strArrays5[4] = str14;
				label6.Text = Strings.Trim(string.Concat(strArrays));
				this.lblProfile2.Text = string.Concat("社:", JG2Importer.JG2.Common.cnsSyakousei[this.m_Attr.Syakousei]);
				this.lblProfile3.Text = string.Concat("貞:", JG2Importer.JG2.Common.cnsTeisou[this.m_Attr.Teisou]);
				this.chkFullName_CheckedChanged(this.chkFullName, null);
			}
		}

		public virtual Button btnThumbnail
		{
			get
			{
				return this._btnThumbnail;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				usrSeat _usrSeat = this;
				EventHandler eventHandler = new EventHandler(_usrSeat.btnThumbnail_Click);
				if (this._btnThumbnail != null)
				{
					this._btnThumbnail.Click -= eventHandler;
				}
				this._btnThumbnail = value;
				if (this._btnThumbnail != null)
				{
					this._btnThumbnail.Click += eventHandler;
				}
			}
		}

		public bool Checked
		{
			get
			{
				return this.chkFullName.Checked;
			}
			set
			{
				this.chkFullName.Checked = value;
			}
		}

		public virtual CheckBox chkFullName
		{
			get
			{
				return this._chkFullName;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				usrSeat _usrSeat = this;
				EventHandler eventHandler = new EventHandler(_usrSeat.chkFullName_CheckedChanged);
				if (this._chkFullName != null)
				{
					this._chkFullName.CheckedChanged -= eventHandler;
				}
				this._chkFullName = value;
				if (this._chkFullName != null)
				{
					this._chkFullName.CheckedChanged += eventHandler;
				}
			}
		}

		public bool CurrentSeat
		{
			get
			{
				return this.m_CurrentSeat;
			}
			set
			{
				this.m_CurrentSeat = value;
				if (this.m_CurrentSeat)
				{
					this.BackColor = JG2Importer.JG2.Common.ColorSeatCurrent;
				}
				else if (!this.chkFullName.Checked)
				{
					bool flag = true;
					if (flag == this.m_Attr.Teacher)
					{
						this.BackColor = JG2Importer.JG2.Common.ColorSeatTeacher;
					}
					else if (flag == (this.m_Attr.Sex == 0))
					{
						this.BackColor = JG2Importer.JG2.Common.ColorSeatMale;
					}
					else if (flag != (this.m_Attr.Sex == 1))
					{
						this.BackColor = JG2Importer.JG2.Common.ColorSeatDefault;
					}
					else
					{
						this.BackColor = JG2Importer.JG2.Common.ColorSeatFemale;
					}
				}
				else
				{
					this.BackColor = JG2Importer.JG2.Common.ColorSeatSelect;
				}
			}
		}

		public virtual Label lblAction
		{
			get
			{
				return this._lblAction;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblAction = value;
			}
		}

		public virtual Label lblBukatu
		{
			get
			{
				return this._lblBukatu;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblBukatu = value;
			}
		}

		public virtual Label lblProfile1
		{
			get
			{
				return this._lblProfile1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblProfile1 = value;
			}
		}

		public virtual Label lblProfile2
		{
			get
			{
				return this._lblProfile2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblProfile2 = value;
			}
		}

		public virtual Label lblProfile3
		{
			get
			{
				return this._lblProfile3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblProfile3 = value;
			}
		}

		public virtual Label lblStatus1
		{
			get
			{
				return this._lblStatus1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblStatus1 = value;
			}
		}

		public virtual Label lblStatus2
		{
			get
			{
				return this._lblStatus2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._lblStatus2 = value;
			}
		}

		public virtual Panel pnlBase
		{
			get
			{
				return this._pnlBase;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._pnlBase = value;
			}
		}

		public usrSeat()
		{
			this.InitializeComponent();
		}

		private void btnThumbnail_Click(object sender, EventArgs e)
		{
			usrSeat.Click_ImageEventHandler clickImageEventHandler = this.Click_Image;
			if (clickImageEventHandler != null)
			{
				clickImageEventHandler(this, e);
			}
		}

		private void chkFullName_CheckedChanged(object sender, EventArgs e)
		{
			this.Invalidate();
			if (!this.m_CurrentSeat)
			{
				if (!Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "Checked", new object[0], null, null, null), true, false))
				{
					bool flag = true;
					if (flag == this.m_Attr.Teacher)
					{
						this.BackColor = JG2Importer.JG2.Common.ColorSeatTeacher;
					}
					else if (flag == (this.m_Attr.Sex == 0))
					{
						this.BackColor = JG2Importer.JG2.Common.ColorSeatMale;
					}
					else if (flag != (this.m_Attr.Sex == 1))
					{
						this.BackColor = JG2Importer.JG2.Common.ColorSeatDefault;
					}
					else
					{
						this.BackColor = JG2Importer.JG2.Common.ColorSeatFemale;
					}
				}
				else
				{
					this.BackColor = JG2Importer.JG2.Common.ColorSeatSelect;
				}
			}
			if (!Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "Checked", new object[0], null, null, null), true, false))
			{
				bool flag1 = true;
				if (flag1 == this.m_Attr.Teacher)
				{
					this.pnlBase.BackColor = JG2Importer.JG2.Common.ColorSeatTeacher;
				}
				else if (flag1 == (this.m_Attr.Sex == 0))
				{
					this.pnlBase.BackColor = JG2Importer.JG2.Common.ColorSeatMale;
				}
				else if (flag1 != (this.m_Attr.Sex == 1))
				{
					this.pnlBase.BackColor = JG2Importer.JG2.Common.ColorSeatDefault;
				}
				else
				{
					this.pnlBase.BackColor = JG2Importer.JG2.Common.ColorSeatFemale;
				}
			}
			else
			{
				this.pnlBase.BackColor = JG2Importer.JG2.Common.ColorSeatSelect;
			}
			this.Update();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.chkFullName = new CheckBox();
			this.btnThumbnail = new Button();
			this.lblAction = new Label();
			this.lblBukatu = new Label();
			this.lblStatus1 = new Label();
			this.lblStatus2 = new Label();
			this.lblProfile1 = new Label();
			this.lblProfile2 = new Label();
			this.lblProfile3 = new Label();
			this.pnlBase = new Panel();
			this.pnlBase.SuspendLayout();
			this.SuspendLayout();
			this.chkFullName.AutoEllipsis = true;
			CheckBox checkBox = this.chkFullName;
			Point point = new Point(79, 3);
			checkBox.Location = point;
			this.chkFullName.Name = "chkFullName";
			CheckBox checkBox1 = this.chkFullName;
			System.Drawing.Size size = new System.Drawing.Size(97, 20);
			checkBox1.Size = size;
			this.chkFullName.TabIndex = 1;
			this.chkFullName.UseVisualStyleBackColor = true;
			this.btnThumbnail.Font = new System.Drawing.Font("メイリオ", 9f, FontStyle.Bold, GraphicsUnit.Point, 128);
			Button button = this.btnThumbnail;
			point = new Point(2, 5);
			button.Location = point;
			this.btnThumbnail.Name = "btnThumbnail";
			Button button1 = this.btnThumbnail;
			size = new System.Drawing.Size(74, 90);
			button1.Size = size;
			this.btnThumbnail.TabIndex = 0;
			this.btnThumbnail.TabStop = false;
			this.btnThumbnail.TextAlign = ContentAlignment.TopLeft;
			this.btnThumbnail.UseVisualStyleBackColor = true;
			this.lblAction.AutoEllipsis = true;
			this.lblAction.Font = new System.Drawing.Font("メイリオ", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.lblAction.ForeColor = SystemColors.ControlText;
			Label label = this.lblAction;
			point = new Point(76, 21);
			label.Location = point;
			this.lblAction.Name = "lblAction";
			Label label1 = this.lblAction;
			size = new System.Drawing.Size(40, 17);
			label1.Size = size;
			this.lblAction.TabIndex = 2;
			this.lblBukatu.AutoEllipsis = true;
			this.lblBukatu.Font = new System.Drawing.Font("メイリオ", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 128);
			Label label2 = this.lblBukatu;
			point = new Point(112, 21);
			label2.Location = point;
			this.lblBukatu.Name = "lblBukatu";
			Label label3 = this.lblBukatu;
			size = new System.Drawing.Size(64, 17);
			label3.Size = size;
			this.lblBukatu.TabIndex = 3;
			this.lblBukatu.TextAlign = ContentAlignment.TopRight;
			this.lblStatus1.AutoEllipsis = true;
			this.lblStatus1.Font = new System.Drawing.Font("メイリオ", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 128);
			Label label4 = this.lblStatus1;
			point = new Point(76, 36);
			label4.Location = point;
			this.lblStatus1.Name = "lblStatus1";
			Label label5 = this.lblStatus1;
			size = new System.Drawing.Size(100, 17);
			label5.Size = size;
			this.lblStatus1.TabIndex = 4;
			this.lblStatus2.AutoEllipsis = true;
			this.lblStatus2.Font = new System.Drawing.Font("メイリオ", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 128);
			Label label6 = this.lblStatus2;
			point = new Point(76, 51);
			label6.Location = point;
			this.lblStatus2.Name = "lblStatus2";
			Label label7 = this.lblStatus2;
			size = new System.Drawing.Size(100, 17);
			label7.Size = size;
			this.lblStatus2.TabIndex = 5;
			this.lblProfile1.AutoEllipsis = true;
			this.lblProfile1.Font = new System.Drawing.Font("メイリオ", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 128);
			Label label8 = this.lblProfile1;
			point = new Point(76, 66);
			label8.Location = point;
			this.lblProfile1.Name = "lblProfile1";
			Label label9 = this.lblProfile1;
			size = new System.Drawing.Size(100, 17);
			label9.Size = size;
			this.lblProfile1.TabIndex = 6;
			this.lblProfile2.AutoEllipsis = true;
			this.lblProfile2.Font = new System.Drawing.Font("メイリオ", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 128);
			Label label10 = this.lblProfile2;
			point = new Point(76, 81);
			label10.Location = point;
			this.lblProfile2.Name = "lblProfile2";
			Label label11 = this.lblProfile2;
			size = new System.Drawing.Size(52, 17);
			label11.Size = size;
			this.lblProfile2.TabIndex = 7;
			this.lblProfile3.AutoEllipsis = true;
			this.lblProfile3.Font = new System.Drawing.Font("メイリオ", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 128);
			Label label12 = this.lblProfile3;
			point = new Point(124, 81);
			label12.Location = point;
			this.lblProfile3.Name = "lblProfile3";
			Label label13 = this.lblProfile3;
			size = new System.Drawing.Size(52, 17);
			label13.Size = size;
			this.lblProfile3.TabIndex = 8;
			this.lblProfile3.TextAlign = ContentAlignment.TopRight;
			this.pnlBase.Controls.Add(this.lblAction);
			this.pnlBase.Controls.Add(this.lblProfile3);
			this.pnlBase.Controls.Add(this.lblProfile2);
			this.pnlBase.Controls.Add(this.chkFullName);
			this.pnlBase.Controls.Add(this.btnThumbnail);
			this.pnlBase.Controls.Add(this.lblBukatu);
			this.pnlBase.Controls.Add(this.lblStatus1);
			this.pnlBase.Controls.Add(this.lblStatus2);
			this.pnlBase.Controls.Add(this.lblProfile1);
			Panel panel = this.pnlBase;
			point = new Point(2, 2);
			panel.Location = point;
			this.pnlBase.Name = "pnlBase";
			Panel panel1 = this.pnlBase;
			size = new System.Drawing.Size(176, 100);
			panel1.Size = size;
			this.pnlBase.TabIndex = 0;
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this.pnlBase);
			this.Font = new System.Drawing.Font("メイリオ", 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "usrJG2";
			size = new System.Drawing.Size(180, 104);
			this.Size = size;
			this.pnlBase.ResumeLayout(false);
			this.ResumeLayout(false);
		}

		public event usrSeat.Click_ImageEventHandler Click_Image;

		public delegate void Click_ImageEventHandler(object sender, EventArgs e);

		public struct DefAttr
		{
			public byte Sex;

			public byte Seikaku;

			public byte Rare;

			public Image ThumbnailImage;

			public string FullName;

			public bool Player;

			public bool Teacher;

			public CodeNinshin CodeNinshin;

			public DefCodeAction CodeAction;

			public DefCodeAction CodeActionRev;

			public string BukatuName;

			public CodeKousai CodeKousai;

			public CodeDate CodeDate;

			public CodeGohoubi CodeGohoubi;

			public CodeHAisyou CodeHAisyou;

			public CodeCntNakaKiken CodeCntNakaKiken;

			public CodeRenzoku CodeRenzoku;

			public CodeCntTodaySeikou CodeCntTodaySeikou;

			public byte Height;

			public byte Style;

			public byte Mune;

			public byte Seikou1;

			public byte Seikou2;

			public byte Syakousei;

			public byte Teisou;
		}
	}
}